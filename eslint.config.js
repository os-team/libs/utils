import nodeConfig from 'eslint-config-os-team-node';

export default [
  ...nodeConfig.configs.recommended,
  {
    ignores: ['**/dist/'],
  },
  {
    files: ['**/*.{js,mjs,cjs,ts,jsx,tsx}'],
    rules: {},
  },
];
