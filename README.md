# utils

Shared utils.

1. [@os-team/app-store-server-notifications](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/app-store-server-notifications) – Types for the App Store Server Notification. Contains the function to validate receipts.
1. [@os-team/app-ua-parser](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/app-ua-parser) – Detects the mobile app, OS, and device information from the user agent created by the @os-team/relay-network-mw-app-user-agent library.
1. [@os-team/aws-ses](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/aws-ses) – The wrapper of the @aws-sdk/client-ses which allows to set the shared mail data.
1. [@os-team/domain-utils](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/domain-utils) – Utils for extracting parts of a domain name.
1. [@os-team/downloadfile](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/downloadfile) – Downloads a file using JS. Supports: tracking the download progress, aborting the download, changing the name of the downloaded file.
1. [@os-team/draft-to-html](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/draft-to-html) – Converts the Draft.js state (RawDraftContentState) to HTML.
1. [@os-team/draft-to-react-nodes](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/draft-to-react-nodes) – Converts the Draft.js state (RawDraftContentState) to ReactNode.
1. [@os-team/escape-string-regexp](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/escape-string-regexp) – Escapes special characters in a string for regular expressions.
1. [@os-team/graphql-test-client](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/graphql-test-client) – The wrapper of the superagent to make GraphQL queries in tests.
1. [@os-team/graphql-to-rest](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/graphql-to-rest) – Creates a REST API using the GraphQL schema.
1. [@os-team/graphql-transformers](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/graphql-transformers) – The type-graphql decorator to transform arguments. Contains the most used transformers.
1. [@os-team/graphql-utils](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/graphql-utils) – Shared utils for GraphQL.
1. [@os-team/graphql-validators](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/graphql-validators) – The type-graphql decorator to validate arguments using i18next. Contains the most used validators.
1. [@os-team/gtm](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/gtm) – The Google Tag Manager initializer.
1. [@os-team/i18next-react-native-language-detector](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/i18next-react-native-language-detector) – The i18next language detector, which is used to detect the user's language in React Native.
1. [@os-team/i18next-tld-language-detector](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/i18next-tld-language-detector) – The i18next language detector, which is used to detect the user's language by a top-level domain (TLD).
1. [@os-team/image-storage](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/image-storage) – Library for uploading images to a storage.
1. [@os-team/google-image-storage](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/image-storage-google) – Library for uploading images to the Google Cloud Storage.
1. [@os-team/yandex-image-storage](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/image-storage-yandex) – Library for uploading images to the Yandex Cloud Storage.
1. [@os-team/lexical-rules](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/lexical-rules) – Utils that helps to create a lexical analyzer.
1. [@os-team/measurement-protocol](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/measurement-protocol) – The Measurement Protocol allows sending events to Google Analytics by making HTTP requests.
1. [@os-team/plural-forms](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/plural-forms) – Declines nouns in the plural form.
1. [@os-team/pretty-size](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/pretty-size) – A tiny converter of number of bytes into a human-readable string. Support for all measures up to petabyte.
1. [@os-team/relay-network-creator](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/relay-network-creator) – Super tiny network layer for Relay.
1. [@os-team/relay-network-mw-app-user-agent](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/relay-network-mw-app-user-agent) – The middleware for @os-team/relay-network-creator to include more detailed app's user agent in each request made from React Native.
1. [@os-team/relay-network-mw-upload](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/relay-network-mw-upload) – The middleware for @os-team/relay-network-creator to transform each request by GraphQL multipart request specification for file uploads.
1. [@os-team/session](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/session) – Reliable, feature-rich, easy-to-use session middleware for Express, developed based on the OWASP recommendations. Stores sessions in Redis. 100% test coverage.
1. [@os-team/sitemap](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/sitemap) – Creates the express middleware to generate the sitemap.xml file.
1. [@os-team/slonik-seeder](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/slonik-seeder) – A simple helper that seeds data to the database using slonik.
1. [@os-team/stream-length](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/stream-length) – Gets and validates the length of a stream.
1. [@os-team/tinkoff-oplata](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/tinkoff-oplata) – Tinkoff Oplata API. All methods described in the official documentation are implemented.
1. [@os-team/xml-parser](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/xml-parser) – Converts XML to an object and back. Fully complies with the W3C XML recommendations (Fifth Edition).
1. [@os-team/yc-storage](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/yc-storage) – Yandex Cloud Object Storage API. All methods described in the official documentation are implemented (buckets, objects, multipart upload, static website hosting, CORS, lifecycles, ACL, bucket policy).
1. [@os-team/yc-translate](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/yc-translate) – Translates the text to the specific language by Yandex.Cloud.
1. [@os-team/yoo-money](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/yoo-money) – YooMoney API v3. All methods described in the official documentation are implemented (payments, refunds and receipts).
1. [@os-team/youtube-captions](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/youtube-captions) – The library for extracting subtitles of videos from YouTube without using the API.
