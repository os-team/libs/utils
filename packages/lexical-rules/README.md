# @os-team/lexical-rules [![NPM version](https://img.shields.io/npm/v/@os-team/lexical-rules)](https://yarnpkg.com/package/@os-team/lexical-rules) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/lexical-rules)](https://bundlephobia.com/result?p=@os-team/lexical-rules)

Utils that helps to create a lexical analyzer.

## Usage

Install the package using the following command:

```
yarn add @os-team/lexical-rules
```

See the `@os-team/xml-parser` package as an example of using this library.
