import { describe, it, before } from 'node:test';
import assert from 'node:assert';
import CharRule from './CharRule.js';

let charRule: CharRule;

describe('Allowed chars', () => {
  before(() => {
    charRule = new CharRule({ allowed: [['0', '9']] });
  });

  it('Should not match because the char is incorrect', () => {
    const data = '_a';
    const [isValid, nextPos] = charRule.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should match', () => {
    const data = '_5';
    const [isValid, nextPos, res] = charRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, '5');
  });
});

describe('Disallowed chars', () => {
  before(() => {
    charRule = new CharRule({ disallowed: ['&'] });
  });

  it('Should not match because the char is incorrect', () => {
    const data = '_&';
    const [isValid, nextPos] = charRule.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should match', () => {
    const data = '_5';
    const [isValid, nextPos, res] = charRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, '5');
  });
});
