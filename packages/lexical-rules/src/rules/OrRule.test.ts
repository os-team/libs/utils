import { test } from 'node:test';
import assert from 'node:assert';
import LiteralRule from './LiteralRule.js';
import OrRule from './OrRule.js';

const firstLiteralRule = new LiteralRule('First');
const secondLiteralRule = new LiteralRule('Second');
const orRule = new OrRule([firstLiteralRule, secondLiteralRule]);

test('Should not match because none of the rule is correct', () => {
  const data = '_Secone';
  const [isValid, nextPos] = orRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

test('Should match the second rule', () => {
  const data = '_Second';
  const [isValid, nextPos, res] = orRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'Second');
});
