import type { Rule, DataRef, RuleTestResponse } from './Rule.js';

class LiteralRule implements Rule<string> {
  private readonly literal: string;

  private readonly caseInsensitive: boolean;

  public constructor(literal: string, caseInsensitive = false) {
    this.literal = literal;
    this.caseInsensitive = caseInsensitive;
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    let buffer = '';

    for (let i = 0; i < this.literal.length; i += 1) {
      const dataPos = pos + i;
      if (dataPos >= ref.data.length) return [false, dataPos];

      const isEquals = this.caseInsensitive
        ? ref.data[dataPos].toLowerCase() === this.literal[i].toLowerCase()
        : ref.data[dataPos] === this.literal[i];
      if (!isEquals) return [false, dataPos];

      buffer = `${buffer}${ref.data[dataPos]}`;
    }

    return [true, pos + this.literal.length, buffer];
  }
}

export default LiteralRule;
