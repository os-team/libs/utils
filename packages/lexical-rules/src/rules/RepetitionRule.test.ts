import { describe, test, before } from 'node:test';
import assert from 'node:assert';
import LiteralRule from './LiteralRule.js';
import RepetitionRule from './RepetitionRule.js';

const literalRule = new LiteralRule('Token');
let repetitionRule: RepetitionRule<string>;

describe('*', () => {
  before(() => {
    repetitionRule = new RepetitionRule(literalRule, 0);
  });

  test('No matches', () => {
    const data = '_Tokee';
    const [isValid, nextPos, res] = repetitionRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, 1);
    assert.deepStrictEqual(res, []);
  });

  test('One match', () => {
    const data = '_TokenTokee';
    const [isValid, nextPos, res] = repetitionRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, 6);
    assert.deepStrictEqual(res, ['Token']);
  });

  test('Two matches', () => {
    const data = '_TokenTokenTokee';
    const [isValid, nextPos, res] = repetitionRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, 11);
    assert.deepStrictEqual(res, ['Token', 'Token']);
  });
});

describe('+', () => {
  before(() => {
    repetitionRule = new RepetitionRule(literalRule, 1);
  });

  test('No matches', () => {
    const data = '_Tokee';
    const [isValid, nextPos] = repetitionRule.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 5);
  });

  test('One match', () => {
    const data = '_TokenTokee';
    const [isValid, nextPos, res] = repetitionRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, 6);
    assert.deepStrictEqual(res, ['Token']);
  });

  test('Two matches', () => {
    const data = '_TokenTokenTokee';
    const [isValid, nextPos, res] = repetitionRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, 11);
    assert.deepStrictEqual(res, ['Token', 'Token']);
  });
});

describe('?', () => {
  before(() => {
    repetitionRule = new RepetitionRule(literalRule, 0, 1);
  });

  test('No matches', () => {
    const data = '_Tokee';
    const [isValid, nextPos, res] = repetitionRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, 1);
    assert.deepStrictEqual(res, []);
  });

  test('One match', () => {
    const data = '_TokenToken';
    const [isValid, nextPos, res] = repetitionRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, 6);
    assert.deepStrictEqual(res, ['Token']);
  });
});

describe('{1}', () => {
  before(() => {
    repetitionRule = new RepetitionRule(literalRule, 1, 1);
  });

  test('No matches', () => {
    const data = '_Tokee';
    const [isValid, nextPos] = repetitionRule.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 5);
  });

  test('One match', () => {
    const data = '_TokenToken';
    const [isValid, nextPos, res] = repetitionRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, 6);
    assert.deepStrictEqual(res, ['Token']);
  });
});

describe('{1,}', () => {
  before(() => {
    repetitionRule = new RepetitionRule(literalRule, 1);
  });

  test('No matches', () => {
    const data = '_Tokee';
    const [isValid, nextPos] = repetitionRule.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 5);
  });

  test('One match', () => {
    const data = '_TokenTokee';
    const [isValid, nextPos, res] = repetitionRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, 6);
    assert.deepStrictEqual(res, ['Token']);
  });

  test('Two matches', () => {
    const data = '_TokenTokenTokee';
    const [isValid, nextPos, res] = repetitionRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, 11);
    assert.deepStrictEqual(res, ['Token', 'Token']);
  });

  test('Three matches', () => {
    const data = '_TokenTokenToken';
    const [isValid, nextPos, res] = repetitionRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, ['Token', 'Token', 'Token']);
  });
});

describe('{1,2}', () => {
  before(() => {
    repetitionRule = new RepetitionRule(literalRule, 1, 2);
  });

  test('No matches', () => {
    const data = '_Tokee';
    const [isValid, nextPos] = repetitionRule.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 5);
  });

  test('One match', () => {
    const data = '_TokenTokee';
    const [isValid, nextPos, res] = repetitionRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, 6);
    assert.deepStrictEqual(res, ['Token']);
  });

  test('Two matches', () => {
    const data = '_TokenTokenToken';
    const [isValid, nextPos, res] = repetitionRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, 11);
    assert.deepStrictEqual(res, ['Token', 'Token']);
  });
});
