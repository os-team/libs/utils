import type { Rule, DataRef, RuleTestResponse } from './Rule.js';

export type Char = string;
export type CharRange = [string, string]; // [from, to]
export type CharListItem = Char | CharRange;
export type CharList = CharListItem[];

export interface CharRuleConfig {
  allowed?: CharList;
  disallowed?: CharList;
}

class CharRule implements Rule<string> {
  protected readonly allowed: CharList;

  protected readonly disallowed: CharList;

  public constructor(config: CharRuleConfig) {
    this.allowed = config.allowed || [];
    this.disallowed = config.disallowed || [];
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    if (pos >= ref.data.length) return [false, pos];

    const char = ref.data[pos];

    const charIs = (item: CharListItem) =>
      typeof item === 'string'
        ? char === item
        : char >= item[0] && char <= item[1];

    if (this.allowed.length > 0) {
      const isAllowed = this.allowed.some(charIs);
      if (!isAllowed) return [false, pos];
    }

    const isDisallowed = this.disallowed.some(charIs);
    if (isDisallowed) return [false, pos];

    return [true, pos + 1, char];
  }
}

export default CharRule;
