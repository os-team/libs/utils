import type { Rule, DataRef, RuleTestResponse } from './Rule.js';

/* eslint-disable @typescript-eslint/no-explicit-any */

class AndRule<T extends any[]> implements Rule<T> {
  private readonly rules: Rule<any>[];

  public constructor(rules: Rule<any>[]) {
    this.rules = rules;
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<T> {
    let position = pos;
    const results: T = [] as any;

    for (const rule of this.rules) {
      const [isValid, nextPos, res] = rule.test(ref, position);
      if (!isValid) return [false, nextPos];
      position = nextPos;
      results.push(res);
    }

    return [true, position, results];
  }
}

export default AndRule;
