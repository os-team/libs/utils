import type { Rule, DataRef, RuleTestResponse } from './Rule.js';

class RepetitionRule<T> implements Rule<T[]> {
  private readonly rule: Rule<T>;

  private readonly min: number;

  private readonly max?: number;

  public constructor(rule: Rule<T>, min: number, max?: number) {
    this.rule = rule;
    this.min = min;
    this.max = max;
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<T[]> {
    let position = pos;
    const results: T[] = [];

    for (let i = 0; i < this.min; i += 1) {
      if (position >= ref.data.length) return [false, position];
      const [isValid, nextPos, res] = this.rule.test(ref, position);
      if (!isValid) return [false, nextPos];
      position = nextPos;
      results.push(res as T);
    }

    let i = this.min;
    while (this.max === undefined || i < this.max) {
      if (position >= ref.data.length) return [true, position, results];
      const [isValid, nextPos, res] = this.rule.test(ref, position);
      if (!isValid) return [true, position, results];
      position = nextPos;
      results.push(res as T);
      i += 1;
    }

    return [true, position, results];
  }
}

export default RepetitionRule;
