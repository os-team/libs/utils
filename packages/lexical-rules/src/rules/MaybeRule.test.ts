import { it, test } from 'node:test';
import assert from 'node:assert';
import LiteralRule from './LiteralRule.js';
import MaybeRule from './MaybeRule.js';

const literalRule = new LiteralRule('Token');
const maybeRule = new MaybeRule(literalRule);

it('Should not match because the literal is not found', () => {
  const data = '_Tokee';
  const [isValid, nextPos, res] = maybeRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, 1);
  assert.equal(res, undefined);
});

test('Should match the literal', () => {
  const data = '_TokenToken';
  const [isValid, nextPos, res] = maybeRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, 6);
  assert.equal(res, 'Token');
});
