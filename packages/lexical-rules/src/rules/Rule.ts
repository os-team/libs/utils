export type RuleTestResponse<T> = [false, number] | [true, number, T]; // [isValid, nextPos, res]

export interface DataRef {
  data: string;
}

export interface Rule<T> {
  test: (ref: DataRef, pos: number) => RuleTestResponse<T>;
}
