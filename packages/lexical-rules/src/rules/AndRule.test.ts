import { test } from 'node:test';
import assert from 'node:assert';
import LiteralRule from './LiteralRule.js';
import AndRule from './AndRule.js';

const firstLiteralRule = new LiteralRule('First');
const secondLiteralRule = new LiteralRule('Second');
const andRule = new AndRule([firstLiteralRule, secondLiteralRule]);

test('Should not match because the second literal is incorrect', () => {
  const data = '_FirstSecone';
  const [isValid, nextPos] = andRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 11);
});

test('Should match', () => {
  const data = '_FirstSecond';
  const [isValid, nextPos, res] = andRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.deepStrictEqual(res, ['First', 'Second']);
});
