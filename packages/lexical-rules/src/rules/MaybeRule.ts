import type { Rule, DataRef, RuleTestResponse } from './Rule.js';
import RepetitionRule from './RepetitionRule.js';

class MaybeRule<T> implements Rule<T | undefined> {
  private readonly rule: Rule<T[]>;

  public constructor(rule: Rule<T>) {
    this.rule = new RepetitionRule(rule, 0, 1);
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<T | undefined> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res[0]];
  }
}

export default MaybeRule;
