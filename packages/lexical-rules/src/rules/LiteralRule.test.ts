import { it } from 'node:test';
import assert from 'node:assert';
import LiteralRule from './LiteralRule.js';

it('Should not match because the token is incorrect', () => {
  const literalRule = new LiteralRule('Token');
  const data = '_Tokee';
  const [isValid, nextPos] = literalRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 5);
});

it('Should not match because a string ended too early', () => {
  const literalRule = new LiteralRule('Token');
  const data = '_Toke';
  const [isValid, nextPos] = literalRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, data.length);
});

it('Should match the literal', () => {
  const literalRule = new LiteralRule('Token');
  const data = '_Token';
  const [isValid, nextPos, res] = literalRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'Token');
});

it('Should match the case-insensitive literal', () => {
  const literalRule = new LiteralRule('token', true);
  const data = '_Token';
  const [isValid, nextPos, res] = literalRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'Token');
});
