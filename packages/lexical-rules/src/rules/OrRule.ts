import type { Rule, DataRef, RuleTestResponse } from './Rule.js';

/* eslint-disable @typescript-eslint/no-explicit-any */

class OrRule<T extends any[]> implements Rule<T> {
  private readonly rules: Rule<any>[];

  public constructor(rules: Rule<any>[]) {
    this.rules = rules;
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<any> {
    for (const rule of this.rules) {
      const [isValid, nextPos, res] = rule.test(ref, pos);
      if (isValid) return [true, nextPos, res];
    }
    return [false, pos];
  }
}

export default OrRule;
