import { Field, ID, InputType } from 'type-graphql';

@InputType({
  description: JSON.stringify({
    en: 'The object that stores the ID of the object being deleted.',
    ru: 'Объект, в котором хранится ID удаляемого объекта.',
  }),
})
class DeleteInput {
  @Field(() => ID, {
    description: JSON.stringify({
      en: 'The ID of the object being deleted.',
      ru: 'ID удаляемого объекта.',
    }),
  })
  public id!: string;
}

export default DeleteInput;
