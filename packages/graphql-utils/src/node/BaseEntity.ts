import { Field, ID, ObjectType } from 'type-graphql';
import { z } from 'zod';
import GraphQLLong from '../scalars/GraphQLLong.js';
import Node from './Node.js';

@ObjectType()
abstract class BaseEntity implements Node {
  @Field(() => ID, {
    description: JSON.stringify({
      en: 'The object ID.',
      ru: 'ID объекта.',
    }),
  })
  public id!: number;

  @Field(() => GraphQLLong, {
    description: JSON.stringify({
      en: 'The date the object was created.',
      ru: 'Дата создания объекта.',
    }),
  })
  public createdAt!: number;

  @Field(() => GraphQLLong, {
    description: JSON.stringify({
      en: 'The date of the last update of the object.',
      ru: 'Дата последнего обновления объекта.',
    }),
  })
  public updatedAt!: number;
}

export const baseSchema = z.object({
  id: z.number(),
  createdAt: z.number(),
  updatedAt: z.number(),
});

export default BaseEntity;
