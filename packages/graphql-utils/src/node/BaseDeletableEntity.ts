import { Field, ObjectType } from 'type-graphql';
import { z } from 'zod';
import GraphQLLong from '../scalars/GraphQLLong.js';
import BaseEntity, { baseSchema } from './BaseEntity.js';

@ObjectType()
abstract class BaseDeletableEntity extends BaseEntity {
  @Field(() => GraphQLLong, {
    nullable: true,
    description: JSON.stringify({
      en: 'The date the object was deleted.',
      ru: 'Дата удаления объекта.',
    }),
  })
  public deletedAt!: number | null;
}

export const baseDeletableSchema = baseSchema.merge(
  z.object({ deletedAt: z.number().nullable() })
);

export default BaseDeletableEntity;
