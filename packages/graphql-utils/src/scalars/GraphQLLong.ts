import { GraphQLError, GraphQLScalarType, Kind, print } from 'graphql';

function parseValue(value: unknown): number {
  if (typeof value !== 'number' || !Number.isInteger(value)) {
    throw new GraphQLError(`Long cannot represent non-integer value: ${value}`);
  }
  if (value < Number.MIN_SAFE_INTEGER || value > Number.MAX_SAFE_INTEGER) {
    throw new GraphQLError(
      `Long cannot represent non 52-bit signed integer value: ${value}`
    );
  }
  return value;
}

const GraphQLLong = new GraphQLScalarType({
  name: 'Long',
  description:
    'The `Long` scalar type represents non-fractional signed whole numeric values. Long can represent values between -(2^53 - 1) and 2^53 - 1.',
  serialize: parseValue,
  parseValue,
  parseLiteral(valueNode) {
    if (valueNode.kind !== Kind.INT || /^-?[0-9]+$/.test(valueNode.value)) {
      throw new GraphQLError(
        `Long cannot represent non-integer value: ${print(valueNode)}`,
        { nodes: valueNode }
      );
    }
    return parseValue(Number(valueNode.value));
  },
});

export default GraphQLLong;
