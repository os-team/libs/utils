import { it } from 'node:test';
import assert from 'node:assert';
import crypto from 'node:crypto';
import Encryptor from './Encryptor.js';

const encryptor = new Encryptor({
  key: crypto.randomBytes(16).toString('hex'),
  iv: crypto.randomBytes(12).toString(),
});

it('Should encrypt and decrypt', () => {
  const globalId = 'Entity:1';

  const encryptedGlobalId = encryptor.encrypt(globalId);
  const decryptedGlobalId = encryptor.decrypt(encryptedGlobalId);

  assert.notEqual(encryptedGlobalId, globalId);
  assert.equal(decryptedGlobalId, globalId);
});
