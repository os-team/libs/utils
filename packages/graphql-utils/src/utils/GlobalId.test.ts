import { it } from 'node:test';
import assert from 'node:assert';
import crypto from 'node:crypto';
import Encryptor from './Encryptor.js';
import GlobalId from './GlobalId.js';

const encryptor = new Encryptor({
  key: crypto.randomBytes(16).toString('hex'),
  iv: crypto.randomBytes(16).toString(),
});

const globalId = new GlobalId(encryptor);

class Entity {}

it('Should throw the error because you have passed a plain object instead of a class', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  assert.throws(() => globalId.encrypt({} as any, 1));
});

it('Should throw the error because the name contains the separator', () => {
  assert.throws(() => globalId.encrypt('Ent:ity', 1));
});

it('Should throw the error because the global ID is incorrect', () => {
  const encryptedId = globalId.encrypt('Entity', 1);
  assert.throws(() => globalId.decrypt('AnotherEntity', encryptedId));
});

it('Should encrypt and decrypt a global ID using a class', () => {
  const id = 1;

  const encryptedId = globalId.encrypt(Entity, id);
  const decryptedId = globalId.decrypt(Entity, encryptedId);

  assert.notEqual(encryptedId, id);
  assert.notEqual(encryptedId, `Entity:${id}`);
  assert.equal(decryptedId, id);
});

it('Should encrypt and decrypt a global ID using a name', () => {
  const id = 1;

  const encryptedId = globalId.encrypt('Entity', id);
  const decryptedId = globalId.decrypt('Entity', encryptedId);

  assert.notEqual(encryptedId, id);
  assert.notEqual(encryptedId, `Entity:${id}`);
  assert.equal(decryptedId, id);
});
