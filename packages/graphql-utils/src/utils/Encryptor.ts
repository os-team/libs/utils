import crypto from 'node:crypto';

interface Options {
  key: string;
  iv: string;
}

const AUTH_TAG_LENGTH = 4;

class Encryptor {
  private key: Buffer;

  private iv: string;

  public constructor(options: Options) {
    const { key, iv } = options;
    if (!/^[0-9a-f]{32}$/.test(key)) {
      throw new Error('The key must be a hexadecimal 32-length string');
    }
    this.key = Buffer.from(key, 'hex');
    this.iv = iv;
  }

  public encrypt(value: string) {
    const cipher = crypto.createCipheriv('aes-128-gcm', this.key, this.iv, {
      authTagLength: AUTH_TAG_LENGTH,
    });

    return Buffer.concat([
      cipher.update(value),
      cipher.final(),
      cipher.getAuthTag(),
    ]).toString('base64url');
  }

  public decrypt(value: string) {
    const input = Buffer.from(value, 'base64url');
    const tag = input.subarray(input.length - AUTH_TAG_LENGTH);
    const encrypted = input.subarray(0, input.length - tag.length);

    const decipher = crypto.createDecipheriv('aes-128-gcm', this.key, this.iv, {
      authTagLength: AUTH_TAG_LENGTH,
    });
    decipher.setAuthTag(tag);

    return Buffer.concat([
      decipher.update(encrypted),
      decipher.final(),
    ]).toString('utf8');
  }
}

export default Encryptor;
