import { type ClassType } from 'type-graphql';
import Encryptor from './Encryptor.js';

const SEPARATOR = ':';

class GlobalId {
  private encryptor: Encryptor;

  public constructor(encryptor: Encryptor) {
    this.encryptor = encryptor;
  }

  private getClassName(classType: ClassType | string) {
    const name = typeof classType === 'string' ? classType : classType.name;
    if (name === undefined) {
      throw new Error(
        'GlobalId cannot get the class name. Seems you have passed a plain object instead of a class.'
      );
    }
    if (name.indexOf(SEPARATOR) >= 0) {
      throw new Error(
        `GlobalId cannot use the name "${name}" because it contains "${SEPARATOR}".`
      );
    }
    return name;
  }

  public encrypt(classType: ClassType, id: number): string;
  public encrypt(classType: string, id: number): string;
  public encrypt(classType: ClassType | string, id: number): string {
    const className = this.getClassName(classType);
    const decryptedGlobalId = `${className}${SEPARATOR}${id}`;
    return this.encryptor.encrypt(decryptedGlobalId);
  }

  public decrypt(classType: ClassType, globalId: string): number;
  public decrypt(classType: string, globalId: string): number;
  public decrypt(classType: ClassType | string, globalId: string): number {
    const className = this.getClassName(classType);
    const decryptedGlobalId = this.encryptor.decrypt(globalId);
    const [name, id] = decryptedGlobalId.split(SEPARATOR);
    if (name !== className || id === undefined || !/^[0-9]+$/.test(id)) {
      throw new Error(`The global id "${globalId}" is incorrect.`);
    }
    return Number(id);
  }
}

export default GlobalId;
