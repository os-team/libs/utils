import { type ClassType, Field, ObjectType } from 'type-graphql';
import {
  type Connection as ConnectionType,
  type ConnectionCursor,
  type Edge as EdgeType,
} from './connectionTypes.js';
import PageInfo from './PageInfo.js';

const createConnectionType = <T>(
  type: ClassType | object,
  name?: string
): ClassType<ConnectionType<T>> => {
  const detectedName = typeof type === 'function' ? type.name : null;
  const typeName = name || detectedName || 'Unknown';

  @ObjectType(`${typeName}Edge`, {
    description: JSON.stringify({
      en: 'A list item.',
      ru: 'Элемент списка.',
    }),
  })
  class Edge implements EdgeType<T> {
    @Field(() => type, {
      description: JSON.stringify({
        en: 'An object.',
        ru: 'Объект.',
      }),
    })
    node!: T;

    @Field({
      description: JSON.stringify({
        en: 'A cursor that is used to get the next page.',
        ru: 'Курсор, который используется для получения следующей страницы.',
      }),
    })
    cursor!: ConnectionCursor;
  }

  @ObjectType(`${typeName}Connection`, {
    description: JSON.stringify({
      en: 'A type that provides a standard mechanism for paginating the result set.',
      ru: 'Тип, обеспечивающий стандартный механизм разбиения набора результатов на страницы.',
    }),
  })
  class Connection implements ConnectionType<T> {
    @Field(() => [Edge], {
      description: JSON.stringify({
        en: 'Item list.',
        ru: 'Список элементов.',
      }),
    })
    edges!: readonly Edge[];

    @Field({
      description: JSON.stringify({
        en: 'Information about the current page.',
        ru: 'Информация о текущей странице.',
      }),
    })
    pageInfo!: PageInfo;
  }

  return Connection;
};

export default createConnectionType;
