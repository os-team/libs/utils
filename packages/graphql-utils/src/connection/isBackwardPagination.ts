import ConnectionArgs from './ConnectionArgs.js';

const isSpecified = (value: unknown) => value !== undefined && value !== null;

const isBackwardPagination = (args: ConnectionArgs): boolean =>
  isSpecified(args.last) || isSpecified(args.before);

export default isBackwardPagination;
