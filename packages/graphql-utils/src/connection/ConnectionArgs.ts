import { ArgsType, Field, Int } from 'type-graphql';
import { max, min, nullable } from '@os-team/graphql-validators';
import { type ConnectionCursor } from './connectionTypes.js';

@ArgsType()
class ConnectionArgs {
  // Forward pagination arguments
  @Field(() => Int, {
    nullable: true,
    description: JSON.stringify({
      en: 'The number of items on the page. Used for forward pagination.',
      ru: 'Количество элементов на странице. Используется для навигации вперед.',
    }),
  })
  first?: number | null;

  @Field(() => String, {
    nullable: true,
    description: JSON.stringify({
      en: 'The cursor of the item relative to which the next items should be returned. Used for forward pagination.',
      ru: 'Курсор элемента, относительно которого должны быть возвращены следующие элементы. Используется для навигации вперед.',
    }),
  })
  after?: ConnectionCursor | null;

  // Backward pagination arguments
  @Field(() => Int, {
    nullable: true,
    description: JSON.stringify({
      en: 'The number of items on the page. Used for backward pagination.',
      ru: 'Количество элементов на странице. Используется для навигации назад.',
    }),
  })
  last?: number | null;

  @Field(() => String, {
    nullable: true,
    description: JSON.stringify({
      en: 'The cursor of the item relative to which the previous items should be returned. Used for backward pagination.',
      ru: 'Курсор элемента, относительно которого должны быть возвращены предыдущие элементы. Используется для навигации назад.',
    }),
  })
  before?: ConnectionCursor | null;
}

export const MIN_LIMIT = 1;
export const MAX_LIMIT = 200;

export const connectionValidators = {
  first: [nullable, min(MIN_LIMIT), max(MAX_LIMIT)],
  last: [nullable, min(MIN_LIMIT), max(MAX_LIMIT)],
};

export default ConnectionArgs;
