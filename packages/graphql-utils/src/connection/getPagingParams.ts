import ConnectionArgs, { MAX_LIMIT } from './ConnectionArgs.js';
import { type ConnectionCursor } from './connectionTypes.js';
import isBackwardPagination from './isBackwardPagination.js';

export interface Condition<Params> {
  operator: '>' | '<';
  params: Params;
}

export interface PagingParams<Params> {
  limit: number;
  sortOrder: 'ASC' | 'DESC';
  condition?: Condition<Params>;
}

interface Options<Params> {
  reverseOrder?: boolean;
  defaultLimit?: number;
  paramsCreator: (cursor: ConnectionCursor) => Params;
}

const getPagingParams = <Params>(
  args: ConnectionArgs,
  options: Options<Params>
): PagingParams<Params> => {
  const { first, after, last, before } = args;
  const {
    reverseOrder = false,
    defaultLimit = MAX_LIMIT,
    paramsCreator,
  } = options;

  // The backward pagination
  if (isBackwardPagination(args)) {
    return {
      limit: last || defaultLimit,
      sortOrder: reverseOrder ? 'ASC' : 'DESC',
      condition: before
        ? {
            operator: reverseOrder ? '>' : '<',
            params: paramsCreator(before),
          }
        : undefined,
    };
  }

  // The forward pagination
  return {
    limit: first || defaultLimit,
    sortOrder: reverseOrder ? 'DESC' : 'ASC',
    condition: after
      ? {
          operator: reverseOrder ? '<' : '>',
          params: paramsCreator(after),
        }
      : undefined,
  };
};

export default getPagingParams;
