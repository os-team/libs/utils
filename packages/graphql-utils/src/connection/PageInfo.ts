import { Field, ObjectType } from 'type-graphql';
import type { ConnectionCursor } from './connectionTypes.js';

@ObjectType({
  description: JSON.stringify({
    en: 'Information about the current page.',
    ru: 'Информация о текущей странице.',
  }),
})
class PageInfo {
  @Field({
    description: JSON.stringify({
      en: 'Indicates whether the list items exist on the next page. Used to navigate forward.',
      ru: 'Показывает, существуют ли элементы списка на следующей странице. Используется для навигации вперед.',
    }),
  })
  hasNextPage!: boolean;

  @Field({
    description: JSON.stringify({
      en: 'Indicates whether the list items exist on the previous page. Used to navigate backwards.',
      ru: 'Существуют ли элементы списка на предыдущей странице. Используется для навигации назад.',
    }),
  })
  hasPreviousPage!: boolean;

  @Field(() => String, {
    nullable: true,
    description: JSON.stringify({
      en: 'Indicates whether the list items exist on the previous page. Used to navigate forward.',
      ru: 'Существуют ли элементы списка на предыдущей странице. Используется для навигации вперед.',
    }),
  })
  startCursor!: ConnectionCursor | null;

  @Field(() => String, {
    nullable: true,
    description: JSON.stringify({
      en: 'Indicates whether the list items exist on the previous page. Used to navigate backwards.',
      ru: 'Существуют ли элементы списка на предыдущей странице. Используется для навигации назад.',
    }),
  })
  endCursor!: ConnectionCursor | null;
}

export default PageInfo;
