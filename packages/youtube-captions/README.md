# @os-team/youtube-captions [![NPM version](https://img.shields.io/npm/v/@os-team/youtube-captions)](https://yarnpkg.com/package/@os-team/youtube-captions) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/youtube-captions)](https://bundlephobia.com/result?p=@os-team/youtube-captions)

Extracts video subtitles from YouTube using the yt-dlp.

## Installation

Install the package using the following command:

```
yarn add @os-team/youtube-captions
```

The library uses [yt-dlp](https://github.com/yt-dlp/yt-dlp), so you need to install it in [one of the following ways](https://github.com/yt-dlp/yt-dlp/wiki/Installation).

## Usage

Example for getting subtitles for the video:

```ts
import { getCaptions, getLanguages } from '@os-team/youtube-captions';

const youtubeId = 'R7p-nPg8t_g';
const languages = getLanguages(youtubeId); // ['en', 'de', 'pl', 'pt', 'es']
const captions = await getCaptions(youtubeId, 'en'); // [{ start: 0, end: 1000, text: 'subtitle' }]
```

The `start` and `end` fields are measured in milliseconds.
