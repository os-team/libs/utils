import { execSync } from 'child_process';

const getLanguages = (youtubeId: string): string[] => {
  let stdout = '';

  try {
    stdout = execSync(`yt-dlp --list-subs ${youtubeId}`).toString();
  } catch {
    return [];
  }

  const lines = stdout.split('\n');
  const languages: string[] = [];
  let readingStarted = false;

  for (let i = 0; i < lines.length; i += 1) {
    if (readingStarted) {
      const groups = lines[i].match(/^([a-z]{2,3}(?:-[a-z]{2,4})?)/i);
      if (groups) languages.push(groups[1]);
    } else if (lines[i] === `[info] Available subtitles for ${youtubeId}:`) {
      i += 1; // Skip titles
      readingStarted = true;
    }
  }

  return languages;
};

export default getLanguages;
