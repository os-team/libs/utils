import { execSync } from 'node:child_process';
import crypto from 'node:crypto';
import readline from 'node:readline';
import fs from 'node:fs';
import path from 'node:path';
import { fileURLToPath } from 'node:url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

interface Caption {
  start: number;
  end: number;
  text: string;
}

const timeToMs = (time: string) => {
  const t = time.match(/^(\d{2}):(\d{2}):(\d{2})\.(\d{3})$/);
  if (!t) return 0;
  const s = Number(t[1]) * 3600 + Number(t[2]) * 60 + Number(t[3]);
  return s * 1000 + Number(t[4]);
};

const getCaptions = async (
  youtubeId: string,
  language: string,
  directory = __dirname
): Promise<Caption[]> => {
  const hash = crypto.randomBytes(2).toString('hex');
  const name = `${youtubeId}-${hash}`;
  const filePath = path.resolve(directory, name);

  // Save the file with subtitles
  try {
    execSync(
      `yt-dlp --write-sub --sub-lang ${language} --convert-subs vtt -o ${filePath} --skip-download ${youtubeId}`
    ).toString();

    const generatedFilePath = `${filePath}.${language}.vtt`;

    const rl = readline.createInterface({
      input: fs.createReadStream(generatedFilePath),
      crlfDelay: Infinity,
    });

    const captions: Caption[] = [];
    let curCaption: Caption | null = null;

    for await (const l of rl) {
      const line = l.trim();
      const time = line.match(
        /^(\d{2}:\d{2}:\d{2}\.\d{3}) --> (\d{2}:\d{2}:\d{2}\.\d{3})$/
      );

      if (time) {
        if (curCaption && curCaption.text) {
          captions.push(curCaption);
          curCaption = null;
        }
        curCaption = {
          text: '',
          start: timeToMs(time[1]),
          end: timeToMs(time[2]),
        };
      } else if (curCaption && line) {
        curCaption.text = `${curCaption.text} ${line}`.trimStart();
      }
    }

    if (curCaption && curCaption.text) {
      captions.push(curCaption);
    }

    // Delete the file with subtitles
    fs.unlinkSync(generatedFilePath);

    return captions;
  } catch {
    return [];
  }
};

export default getCaptions;
