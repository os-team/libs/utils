import ImageStorage, {
  type ImageStorageOptions,
  type Storage,
} from '@os-team/image-storage';
import GoogleStorage, { type GoogleStorageOptions } from './GoogleStorage.js';

export type GoogleImageStorageOptions = ImageStorageOptions &
  GoogleStorageOptions;

class GoogleImageStorage extends ImageStorage {
  protected readonly storage: Storage;

  public constructor(options: GoogleImageStorageOptions) {
    const { storage, concurrentFilesToDelete, ...rest } = options;
    super(rest);
    this.storage = new GoogleStorage({ storage, concurrentFilesToDelete });
  }
}

export default GoogleImageStorage;
