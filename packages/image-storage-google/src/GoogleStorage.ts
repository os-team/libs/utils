import {
  type Storage,
  type StorageUploadOptions,
} from '@os-team/image-storage';
import { Storage as GStorage } from '@google-cloud/storage';

export interface GoogleStorageOptions {
  /**
   * The instance of the Google Cloud Storage.
   */
  storage: GStorage;
  /**
   * The number of files deleted from a storage in parallel.
   * @default 4
   */
  concurrentFilesToDelete?: number;
}

class GoogleStorage implements Storage {
  private readonly storage: GStorage;

  private readonly concurrentFilesToDelete: number;

  public constructor(options: GoogleStorageOptions) {
    const { storage, concurrentFilesToDelete = 4 } = options;
    this.storage = storage;
    this.concurrentFilesToDelete = concurrentFilesToDelete;
  }

  public async list(bucket: string, prefix: string): Promise<string[]> {
    const [files] = await this.storage.bucket(bucket).getFiles({
      prefix,
      maxResults: 100,
    });
    return files.map((file) => file.name);
  }

  public async upload(options: StorageUploadOptions): Promise<void> {
    const { bucket, key, body, contentType } = options;

    const writeStream = this.storage
      .bucket(bucket)
      .file(key)
      .createWriteStream({ contentType });

    await new Promise((resolve, reject) => {
      body.pipe(writeStream).on('error', reject).on('finish', resolve);
    });
  }

  public async delete(bucket: string, key: string): Promise<void> {
    await this.storage
      .bucket(bucket)
      .file(key)
      .delete({ ignoreNotFound: true });
  }

  public async deleteMultiple(bucket: string, keys: string[]): Promise<void> {
    const keysToDelete = [...keys];
    const promises = new Array(this.concurrentFilesToDelete).fill(
      Promise.resolve()
    );

    const chainNext = (promise: Promise<void>) => {
      const key = keysToDelete.shift();
      if (!key) return promise;
      return promise.then(() => chainNext(this.delete(bucket, key)));
    };

    await Promise.all(promises.map(chainNext));
  }
}

export default GoogleStorage;
