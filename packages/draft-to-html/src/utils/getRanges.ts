import { isRawDraftContentRange, isString } from './validators.js';

/* eslint-disable @typescript-eslint/no-explicit-any */

export enum RangeType {
  INLINE_STYLE,
  ENTITY,
}

interface BaseRange {
  offset: number;
  length: number;
}

interface InlineStyleRange extends BaseRange {
  type: RangeType.INLINE_STYLE;
  style: string;
}

interface EntityRange extends BaseRange {
  type: RangeType.ENTITY;
  key: number;
}

type Range = InlineStyleRange | EntityRange;

/**
 * Merge inline style ranges and entity ranges.
 * Sort all ranges by offset in ascending order.
 */
const getRanges = (block: any) => {
  const ranges: Range[] = [];
  const { inlineStyleRanges, entityRanges } = block;

  // Add inline style ranges
  if (Array.isArray(inlineStyleRanges)) {
    ranges.push(
      ...inlineStyleRanges
        .filter(
          (range) => isRawDraftContentRange(range) && isString(range.style)
        )
        .map<InlineStyleRange>((range) => ({
          type: RangeType.INLINE_STYLE,
          offset: range.offset,
          length: range.length,
          style: range.style,
        }))
    );
  }

  // Add entity ranges
  if (Array.isArray(entityRanges)) {
    ranges.push(
      ...entityRanges
        .filter(
          (range) =>
            isRawDraftContentRange(range) && Number.isInteger(range.key)
        )
        .map<EntityRange>((range) => ({
          type: RangeType.ENTITY,
          offset: range.offset,
          length: range.length,
          key: range.key,
        }))
    );
  }

  // Sort all ranges by offset in ascending order
  return ranges.sort((a, b) => a.offset - b.offset);
};

export default getRanges;
