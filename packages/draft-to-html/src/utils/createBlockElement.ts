import createBlockChildren, {
  type EntityRenderer,
} from './createBlockChildren.js';
import { type InlineStyleRenderer } from './createInlineStyleElement.js';
import htmlElementToString, {
  type HTMLElement,
} from './htmlElementToString.js';

export const mapBlockTypeTag = {
  unstyled: 'p',
  paragraph: 'p',
  'header-one': 'h1',
  'header-two': 'h2',
  'header-three': 'h3',
  'header-four': 'h4',
  'header-five': 'h5',
  'header-six': 'h6',
  'unordered-list-item': 'li',
  'ordered-list-item': 'li',
  blockquote: 'blockquote',
  'code-block': 'pre',
  atomic: 'figure',
};

interface Block {
  key: number;
  type: string;
  text: string;
  data: Record<string, any>; // eslint-disable-line @typescript-eslint/no-explicit-any
}

export type BlockRenderer = (block: Block, children: string) => string | null;

interface Props {
  block: any; // eslint-disable-line @typescript-eslint/no-explicit-any
  entityMap: any; // eslint-disable-line @typescript-eslint/no-explicit-any
  entityRenderer?: EntityRenderer;
  blockRenderer?: BlockRenderer;
  inlineStyleRenderer?: InlineStyleRenderer;
}

const createBlockElement = (props: Props): HTMLElement | string => {
  const {
    block,
    entityMap,
    entityRenderer,
    blockRenderer = () => null,
    inlineStyleRenderer = () => null,
  } = props;

  const children = createBlockChildren({
    block,
    entityMap,
    entityRenderer,
    inlineStyleRenderer,
  });

  // Try to render the block by `blockRenderer`
  const blockElement = blockRenderer(
    block,
    children.reduce<string>(
      (acc, el) =>
        `${acc}${typeof el === 'object' ? htmlElementToString(el) : el}`,
      ''
    )
  );
  if (blockElement) return blockElement;

  // Render the block by default
  const tag = mapBlockTypeTag[block.type];
  return { tag: tag || 'p', children };
};

export default createBlockElement;
