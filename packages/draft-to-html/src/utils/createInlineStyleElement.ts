import { type HTMLElement } from './htmlElementToString.js';

export const mapInlineStyleTag = {
  BOLD: 'b',
  CODE: 'code',
  ITALIC: 'i',
  STRIKETHROUGH: 's',
  UNDERLINE: 'u',
};

interface InlineStyle {
  text: string;
  style: string;
}

export type InlineStyleRenderer = (inlineStyle: InlineStyle) => string | null;

const createInlineStyleElement = (
  inlineStyle: InlineStyle,
  inlineStyleRenderer: InlineStyleRenderer = () => null
): HTMLElement | string => {
  // Try to render the inline style by `inlineStyleRenderer`
  const inlineStyleElement = inlineStyleRenderer(inlineStyle);
  if (inlineStyleElement) return inlineStyleElement;

  // Render the inline style by default
  const tag = mapInlineStyleTag[inlineStyle.style];
  if (!tag) return inlineStyle.text;
  return { tag, children: inlineStyle.text };
};

export default createInlineStyleElement;
