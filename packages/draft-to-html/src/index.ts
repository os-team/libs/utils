import {
  isObject,
  isRawDraftContentBlock,
  isRawDraftContentState,
} from './utils/validators.js';
import createBlockElement, {
  type BlockRenderer,
} from './utils/createBlockElement.js';
import { type EntityRenderer } from './utils/createBlockChildren.js';
import { type InlineStyleRenderer } from './utils/createInlineStyleElement.js';
import htmlElementToString, {
  type HTMLElement,
} from './utils/htmlElementToString.js';

export interface Options {
  value: any; // eslint-disable-line @typescript-eslint/no-explicit-any
  entityRenderer?: EntityRenderer;
  blockRenderer?: BlockRenderer;
  inlineStyleRenderer?: InlineStyleRenderer;
}

const draftToHtml = (options: Options): string => {
  const { value, entityRenderer, blockRenderer, inlineStyleRenderer } = options;
  if (!isRawDraftContentState(value)) return '';

  const nodes: Array<HTMLElement | string> = [];
  let ulNodes: Array<HTMLElement | string> = [];
  let olNodes: Array<HTMLElement | string> = [];

  value.blocks.forEach((block) => {
    if (!isRawDraftContentBlock(block)) return;

    // Add an unordered list
    if (block.type !== 'unordered-list-item' && ulNodes.length > 0) {
      nodes.push({ tag: 'ul', children: ulNodes });
      ulNodes = [];
    }

    // Add an ordered list
    if (block.type !== 'ordered-list-item' && olNodes.length > 0) {
      nodes.push({ tag: 'ol', children: olNodes });
      olNodes = [];
    }

    const entityMap = isObject(value.entityMap) ? value.entityMap : {};
    const element = createBlockElement({
      block,
      entityMap,
      entityRenderer,
      blockRenderer,
      inlineStyleRenderer,
    });

    if (block.type === 'unordered-list-item') ulNodes.push(element);
    else if (block.type === 'ordered-list-item') olNodes.push(element);
    else nodes.push(element);
  });

  // Add an unordered list located at the end
  if (ulNodes.length > 0) {
    nodes.push({ tag: 'ul', children: ulNodes });
  }

  // Add an ordered list  located at the end
  if (olNodes.length > 0) {
    nodes.push({ tag: 'ol', children: olNodes });
  }

  return nodes.reduce<string>(
    (acc, el) =>
      `${acc}${typeof el === 'object' ? htmlElementToString(el) : el}`,
    ''
  );
};

export * from './utils/createBlockChildren.js';
export * from './utils/createBlockElement.js';
export * from './utils/createInlineStyleElement.js';

export default draftToHtml;
