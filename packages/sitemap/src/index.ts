import type { Request, Response } from 'express';

const joinUrls = (baseUrl: string, url: string) => {
  if (url.startsWith('/')) return `${baseUrl}${url}`;
  return `${baseUrl}/${url}`;
};

const sitemap =
  (urls: string[], baseUrl: string) =>
  (_: Request, res: Response): void => {
    const urlSetContent = urls.reduce(
      (acc, url) => `${acc}<url><loc>${joinUrls(baseUrl, url)}</loc></url>`,
      ''
    );
    const urlSet = `<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">${urlSetContent}</urlset>`;
    const content = `<?xml version="1.0" encoding="UTF-8"?>\n${urlSet}`;

    res.type('text/xml');
    res.send(content);
  };

export default sitemap;
