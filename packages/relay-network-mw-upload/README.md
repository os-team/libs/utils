# @os-team/relay-network-mw-upload [![NPM version](https://img.shields.io/npm/v/@os-team/relay-network-mw-upload)](https://yarnpkg.com/package/@os-team/relay-network-mw-upload) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/relay-network-mw-upload)](https://bundlephobia.com/result?p=@os-team/relay-network-mw-upload)

The middleware for @os-team/relay-network-creator to transform each request by [GraphQL multipart request specification](https://github.com/jaydenseric/graphql-multipart-request-spec) for file uploads.

## Usage

### Step 1. Install the package

Install the package using the following command:

```
yarn add @os-team/relay-network-mw-upload
```

### Step 2. Add the middleware

```ts
import createRelayNetwork from '@os-team/relay-network-creator';
import upload from '@os-team/relay-network-mw-upload';

createRelayNetwork({
  url: 'https://api.domain.com/graphql',
  middlewares: [upload],
});
```
