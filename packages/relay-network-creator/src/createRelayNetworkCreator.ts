import {
  type CacheConfig,
  type FetchFunction,
  type GraphQLSingularResponse,
  type INetwork,
  Network,
  type RequestParameters,
  type SubscribeFunction,
  type UploadableMap,
  type Variables,
} from 'relay-runtime';

export interface Options {
  url: string;
  method?: string;
  headers?: Record<string, string>;
  timeout?: number;
  timeoutMessage?: string;
  middlewares?: RelayNetworkMiddleware[];
  subscribeFn?: SubscribeFunction;
}

export interface Abortable {
  abort: () => void;
  message: string;
}

export const createAbortable = (message = 'Request aborted'): Abortable => ({
  abort: () => {},
  message,
});

export interface UploadProgressEvent {
  readonly loaded: number;
  readonly total: number;
  readonly percent: number;
}

export interface RelayNetworkCacheConfig extends CacheConfig {
  abortable?: Abortable;
  onUploadProgress?: (e: UploadProgressEvent) => void;
}

interface FetchFunctionParameters {
  readonly request: RequestParameters;
  readonly variables: Variables;
  readonly cacheConfig: RelayNetworkCacheConfig;
  readonly uploadables?: UploadableMap | null;
}

export interface RelayNetworkRequest extends FetchFunctionParameters {
  url: string;
  method: string;
  headers: Record<string, string>;
  timeout?: number;
  timeoutMessage?: string;
  body: any; // eslint-disable-line @typescript-eslint/no-explicit-any
}

export type RelayNetworkResponse = GraphQLSingularResponse & {
  readonly headers: Record<string, string>;
  readonly statusCode?: number;
  readonly statusMessage?: string;
};

export type RelayNetworkNextFunction = (
  req: RelayNetworkRequest
) => Promise<RelayNetworkResponse>;

export type RelayNetworkMiddleware = (
  next: RelayNetworkNextFunction
) => RelayNetworkNextFunction;

export type MakeRequest = (
  req: RelayNetworkRequest
) => Promise<RelayNetworkResponse>;

export class TimeoutError extends Error {
  public constructor(message = 'Request timeout') {
    super(message);
  }
}
export class AbortError extends Error {
  public constructor(message = 'Request aborted') {
    super(message);
  }
}
export class ResponseError extends Error {
  public constructor() {
    super('The response must be an object');
  }
}

const createRelayNetworkCreator =
  (makeRequest: MakeRequest) =>
  (options: Options): INetwork => {
    const {
      url,
      method = 'POST',
      headers = {},
      timeout,
      timeoutMessage,
      middlewares = [],
      subscribeFn,
    } = options;

    const fetchFn: FetchFunction = (
      request,
      variables,
      cacheConfig,
      uploadables
    ) => {
      const next: RelayNetworkNextFunction = (req) => makeRequest(req);

      const fn = middlewares.reduceRight(
        (previousNextFunction, currentMiddleware) =>
          currentMiddleware(previousNextFunction),
        (req: RelayNetworkRequest) => next(req)
      );

      headers['Content-Type'] ||= 'application/json';
      const body = JSON.stringify({
        query: request.text,
        variables,
      });

      return fn({
        request,
        variables,
        cacheConfig,
        uploadables,
        url,
        method,
        headers,
        timeout,
        timeoutMessage,
        body,
      });
    };

    return Network.create(fetchFn, subscribeFn);
  };

export default createRelayNetworkCreator;
