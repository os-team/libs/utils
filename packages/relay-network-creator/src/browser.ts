import createRelayNetworkCreator, {
  AbortError,
  type MakeRequest,
  ResponseError,
  TimeoutError,
  type UploadProgressEvent,
} from './createRelayNetworkCreator.js';
import isObject from './isObject.js';

const makeRequest: MakeRequest = (req) =>
  new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open(req.method, req.url, true);
    xhr.responseType = 'json';
    xhr.withCredentials = true;

    Object.entries(req.headers).forEach(([name, value]) => {
      xhr.setRequestHeader(name, value);
    });

    xhr.addEventListener('error', reject);
    xhr.addEventListener('load', () => {
      const headers = xhr
        .getAllResponseHeaders()
        .split('\r\n')
        .reduce((acc, header) => {
          const [name, value] = header.split(': ');
          return { ...acc, [name]: value };
        }, {});
      if (!isObject(xhr.response)) {
        reject(new ResponseError());
      }
      resolve({
        ...xhr.response,
        headers,
        statusCode: xhr.status,
        statusMessage: xhr.statusText,
      });
    });

    if (req.timeout && !req.uploadables) {
      xhr.timeout = req.timeout;
      xhr.addEventListener('timeout', () => {
        reject(new TimeoutError(req.timeoutMessage));
      });
    }

    const { abortable } = req.cacheConfig;
    if (abortable && isObject(abortable)) {
      abortable.abort = () => xhr.abort();
      xhr.addEventListener('abort', () => {
        reject(new AbortError(abortable.message));
      });
    }

    const { onUploadProgress } = req.cacheConfig;
    if (typeof onUploadProgress === 'function') {
      xhr.upload.addEventListener('progress', (e) => {
        const event: UploadProgressEvent = {
          loaded: e.loaded,
          total: e.total,
          percent: Math.round((e.loaded / e.total) * 100),
        };
        onUploadProgress(event);
      });
    }

    xhr.send(req.body);
  });

const createRelayNetwork = createRelayNetworkCreator(makeRequest);

export * from './createRelayNetworkCreator.js';

export default createRelayNetwork;
