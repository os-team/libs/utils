import http from 'http';
import https from 'https';
import createRelayNetworkCreator, {
  type MakeRequest,
  ResponseError,
  TimeoutError,
} from './createRelayNetworkCreator.js';
import isObject from './isObject.js';

const makeRequest: MakeRequest = (req) =>
  new Promise((resolve, reject) => {
    const agent = /^https:\/\//.test(req.url) ? https : http;

    const options = {
      method: req.method,
      headers: req.headers,
      timeout: req.timeout && !req.uploadables ? req.timeout : undefined,
    };

    const request = agent.request(req.url, options, (readableStream) => {
      const chunks: Buffer[] = [];
      let totalLength = 0;

      readableStream.on('error', reject);
      readableStream.on('data', (chunk) => {
        chunks.push(chunk);
        totalLength += chunk.length;
      });

      readableStream.on('end', () => {
        const responseStr = Buffer.concat(chunks, totalLength).toString();
        try {
          const response = JSON.parse(responseStr);
          if (!isObject(response)) {
            reject(new ResponseError());
          }
          resolve({
            ...response,
            headers: readableStream.headers,
            statusCode: readableStream.statusCode,
            statusMessage: readableStream.statusMessage,
          });
        } catch {
          reject(new ResponseError());
        }
      });
    });

    request.on('error', reject);
    request.on('timeout', () => {
      request.destroy();
      reject(new TimeoutError(req.timeoutMessage));
    });

    if (req.body) {
      request.write(req.body);
    }

    request.end();
  });

const createRelayNetwork = createRelayNetworkCreator(makeRequest);

export * from './createRelayNetworkCreator.js';

export default createRelayNetwork;
