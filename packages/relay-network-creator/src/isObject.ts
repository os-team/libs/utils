// eslint-disable-next-line @typescript-eslint/no-explicit-any
const isObject = (value: any) =>
  typeof value === 'object' && !Array.isArray(value) && value !== null;

export default isObject;
