import fetch from 'node-fetch';

/* eslint-disable @typescript-eslint/no-explicit-any */

export interface InitProps {
  /**
   * The measurement ID associated with a data stream.
   * See Google Analytics Admin > Data Streams > [stream] > Measurement ID.
   */
  measurementId: string;
  /**
   * The API secret.
   * See Google Analytics Admin > Data Streams > [stream] > Measurement Protocol > Create.
   */
  apiSecret: string;
}

export interface Event {
  /**
   * The name for the event.
   */
  name: string;
  /**
   * The parameters for the event.
   */
  params?: Record<string, any>;
}

export interface Data {
  /**
   * Uniquely identifies a user instance of a web client.
   * See https://developers.google.com/gtagjs/reference/api#get_mp_example
   */
  client_id: string;
  /**
   * A unique identifier for a user.
   * See https://support.google.com/analytics/answer/9213390
   */
  user_id?: string;
  /**
   * A Unix timestamp (in microseconds) for the time to associate with the event.
   * This should only be set to record events that happened in the past.
   * This value can be overridden via user_property or event timestamps.
   * Events can be backdated up to 48 hours.
   */
  timestamp_micros?: number;
  /**
   * The user properties for the measurement.
   * See https://developers.google.com/analytics/devguides/collection/protocol/ga4/user-properties
   */
  user_properties?: Record<string, any>;
  /**
   * Set to true to indicate these events should not be used for personalized ads.
   */
  non_personalized_ads?: boolean;
  /**
   * An array of event items. Up to 25 events can be sent per request.
   * See https://developers.google.com/analytics/devguides/collection/protocol/ga4/reference/events
   */
  events: Event[];
}

// Limitations: https://developers.google.com/analytics/devguides/collection/protocol/ga4/sending-events?client_type=gtag#limitations
// Reserved event names: https://developers.google.com/analytics/devguides/collection/protocol/ga4/reference?client_type=gtag#reserved_names
export type ValidationCode =
  | 'VALUE_INVALID' // The value provided for a fieldPath was invalid.
  | 'VALUE_REQUIRED' // A required value for a fieldPath was not provided.
  | 'NAME_INVALID' // The name provided was invalid.
  | 'NAME_RESERVED' // The name provided was one of the reserved names.
  | 'VALUE_OUT_OF_BOUNDS' // The value provided was too large.
  | 'EXCEEDED_MAX_ENTITIES' // There were too many parameters in the request.
  | 'NAME_DUPLICATED'; // The same name was provided more than once in the request.

export interface ValidationMessage {
  /**
   * The path to the field that was invalid.
   */
  fieldPath: string;
  /**
   * A description of the error.
   */
  description: string;
  /**
   * A validation code that corresponds to the error.
   */
  validationCode: ValidationCode;
}

export interface ValidateResponse {
  validationMessages: ValidationMessage[];
}

const API_ENDPOINT = 'https://www.google-analytics.com/mp/collect';
const API_DEBUG_ENDPOINT = 'https://www.google-analytics.com/debug/mp/collect';

class MeasurementProtocol {
  protected measurementId: string;

  protected apiSecret: string;

  public constructor(props: InitProps) {
    this.measurementId = props.measurementId;
    this.apiSecret = props.apiSecret;
  }

  protected getQueryParams(): string {
    return `?measurement_id=${this.measurementId}&api_secret=${this.apiSecret}`;
  }

  /**
   * Sends events to the Measurement Protocol.
   */
  public async send(data: Data): Promise<void> {
    await fetch(`${API_ENDPOINT}${this.getQueryParams()}`, {
      method: 'POST',
      body: JSON.stringify(data),
    });
  }

  /**
   * Sends events to the Measurement Protocol Validation Server.
   * See https://developers.google.com/analytics/devguides/collection/protocol/ga4/validating-events
   */
  public async validate(data: Data): Promise<ValidateResponse> {
    const response = await fetch(
      `${API_DEBUG_ENDPOINT}${this.getQueryParams()}`,
      {
        method: 'POST',
        body: JSON.stringify(data),
      }
    );
    const res = await response.json();
    return res as ValidateResponse;
  }
}

export default MeasurementProtocol;
