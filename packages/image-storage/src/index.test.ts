import { describe, it, before, beforeEach, afterEach } from 'node:test';
import assert from 'node:assert';
import * as fs from 'node:fs';
import * as path from 'node:path';
import { fileURLToPath } from 'node:url';
import ImageStorage, {
  type ImageStorageOptions,
  type Storage,
  type StorageUploadOptions,
} from './index.js';

class FileStorage implements Storage {
  public list(bucket: string, prefix: string): Promise<string[]> {
    return new Promise((resolve, reject) => {
      fs.readdir(bucket, (error, files) => {
        if (error) reject(error);
        else resolve(files.filter((file) => file.startsWith(prefix)));
      });
    });
  }

  public async upload(options: StorageUploadOptions): Promise<void> {
    const { bucket, key, body } = options;
    return new Promise((resolve, reject) => {
      const filePath = path.resolve(bucket, key);
      const writeStream = fs.createWriteStream(filePath);
      body.pipe(writeStream).on('error', reject).on('close', resolve);
    });
  }

  public delete(bucket: string, key: string): Promise<void> {
    return new Promise((resolve, reject) => {
      const filePath = path.resolve(bucket, key);
      fs.unlink(filePath, (error) => {
        if (error) reject(error);
        else resolve(undefined);
      });
    });
  }

  public deleteMultiple(bucket: string, keys: string[]): Promise<void> {
    if (keys.length === 0) return Promise.resolve();
    let loadedFilesCount = 0;
    return new Promise((resolve, reject) => {
      keys.forEach((key) => {
        const filePath = path.resolve(bucket, key);
        fs.unlink(filePath, (error) => {
          if (error) reject(error);
          else if (loadedFilesCount < keys.length - 1) loadedFilesCount += 1;
          else resolve(undefined);
        });
      });
    });
  }
}

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const INPUT_DIR = 'test-files';
const OUTPUT_DIR = 'test-storage';
const INPUT_DIR_PATH = path.resolve(__dirname, INPUT_DIR);
const OUTPUT_DIR_PATH = path.resolve(__dirname, OUTPUT_DIR);

const createReadStream = (fileName: string) => {
  const filePath = path.resolve(INPUT_DIR_PATH, fileName);
  return fs.createReadStream(filePath);
};

let imageStorage: ImageStorage;
const fileStorage = new FileStorage();

class FileImageStorage extends ImageStorage {
  protected readonly storage: Storage;

  public constructor(options: ImageStorageOptions) {
    super(options);
    this.storage = fileStorage;
  }
}

before(() => {
  imageStorage = new FileImageStorage({
    bucket: OUTPUT_DIR_PATH,
    sizes: [50, 100],
  });
});

beforeEach(() => {
  fs.mkdirSync(OUTPUT_DIR_PATH);
});

afterEach(() => {
  fs.rmSync(OUTPUT_DIR_PATH, { recursive: true, force: true });
});

describe('upload', () => {
  it('Should throw the error because the name contains the ~ character', async () => {
    await assert.rejects(
      () =>
        imageStorage.upload({
          body: createReadStream('717kb.jpg'),
          name: 'na~me',
          useHash: true,
        }),
      new Error('The image name must not contain the ~ character')
    );
  });

  it('Should throw the error because the file type is not detected', async () => {
    await assert.rejects(
      () =>
        imageStorage.upload({
          body: createReadStream('text.txt'),
          name: 'name',
        }),
      new Error(
        'Only the following file types are supported: jpg, png, webp, gif, avif, tif'
      )
    );
  });

  it('Should throw the error because the file type is not allowed', async () => {
    await assert.rejects(
      () =>
        imageStorage.upload({
          body: createReadStream('video.mov'),
          name: 'name',
        }),
      new Error(
        'The mov type is not allowed. Only the following file types are supported: jpg, png, webp, gif, avif, tif.'
      )
    );
  });

  it('Should throw the error because an image in JPG format is not allowed', async () => {
    await assert.rejects(
      () =>
        imageStorage.upload({
          body: createReadStream('717kb.jpg'),
          name: 'name',
          types: ['png'],
        }),
      new Error(
        'The jpg type is not allowed. Only the following file types are supported: png.'
      )
    );
  });

  it('Should throw the error because the file size is too large', async () => {
    await assert.rejects(
      () =>
        imageStorage.upload({
          body: createReadStream('717kb.jpg'),
          name: 'name',
          maxSize: 500 * 1024,
        }),
      new Error('The image size exceeds 500 KB')
    );
  });

  it('Should upload an image', async () => {
    // Check if the image has been uploaded
    const imageName = 'name';
    const { name, contentLength } = await imageStorage.upload({
      body: createReadStream('717kb.jpg'),
      name: imageName,
    });
    assert.equal(name, imageName);
    assert.equal(contentLength[`${name}-50`] > 0, true);
    assert.equal(contentLength[`${name}-50-c`] > 0, true);
    assert.equal(contentLength[`${name}-100`] > 0, true);
    assert.equal(contentLength[`${name}-100-c`] > 0, true);

    // Check if the image in all sizes has been created on the disk
    const files = await fileStorage.list(OUTPUT_DIR_PATH, '');
    assert.equal(files.length, 4);
  });

  it('Should upload an image to a subdirectory', async () => {
    const dirName = 'dir';

    // Create a subdirectory
    fs.mkdirSync(path.resolve(OUTPUT_DIR_PATH, dirName));

    // Upload an image
    const { name } = await imageStorage.upload({
      body: createReadStream('717kb.jpg'),
      name: 'name',
      path: dirName,
    });

    // Check if the name does not contain the path
    assert.equal(name.startsWith(dirName), false);

    // Check if the image in all sizes has been created on the disk
    const files = await fileStorage.list(`${OUTPUT_DIR_PATH}/${dirName}`, name);
    assert.equal(files.length, 4);
  });

  it('Should append a hash to the image name', async () => {
    // Upload an image
    const { name } = await imageStorage.upload({
      body: createReadStream('717kb.jpg'),
      name: 'name',
      useHash: true,
    });

    assert.equal(name.startsWith('name~'), true);
  });

  it('Should delete old images', async () => {
    // Upload an image for the first time
    await imageStorage.upload({
      body: createReadStream('717kb.jpg'),
      name: 'name',
      useHash: true,
    });

    // Upload an image for the second time
    const { name } = await imageStorage.upload({
      body: createReadStream('717kb.jpg'),
      name: 'name',
      useHash: true,
      deleteOldImages: true,
    });

    // Check if only the second image is stored on the disk
    const files = await fileStorage.list(OUTPUT_DIR_PATH, '');
    const secondFiles = files.filter((file) => file.startsWith(name));
    assert.equal(files.length, 4);
    assert.equal(secondFiles.length, 4);
  });

  it('Should upload an image in JPG format', async () => {
    // Upload an image
    const { name } = await imageStorage.upload({
      body: createReadStream('717kb.jpg'),
      name: 'name',
    });

    // Check if the image in all sizes has been created on the disk
    const files = await fileStorage.list(OUTPUT_DIR_PATH, name);
    assert.equal(files.length, 4);
  });

  it('Should upload an image in JPEG format', async () => {
    // Upload an image
    const { name } = await imageStorage.upload({
      body: createReadStream('717kb.jpeg'),
      name: 'name',
    });

    // Check if the image in all sizes has been created on the disk
    const files = await fileStorage.list(OUTPUT_DIR_PATH, name);
    assert.equal(files.length, 4);
  });

  it('Should upload an image in PNG format', async () => {
    // Upload an image
    const { name } = await imageStorage.upload({
      body: createReadStream('4.3mb.png'),
      name: 'name',
    });

    // Check if the image in all sizes has been created on the disk
    const files = await fileStorage.list(OUTPUT_DIR_PATH, name);
    assert.equal(files.length, 4);
  });

  it('Should upload an image in WEBP format', async () => {
    // Upload an image
    const { name } = await imageStorage.upload({
      body: createReadStream('508kb.webp'),
      name: 'name',
    });

    // Check if the image in all sizes has been created on the disk
    const files = await fileStorage.list(OUTPUT_DIR_PATH, name);
    assert.equal(files.length, 4);
  });

  it('Should upload an image in GIF format', async () => {
    // Upload an image
    const { name } = await imageStorage.upload({
      body: createReadStream('3.2mb.gif'),
      name: 'name',
    });

    // Check if the image in all sizes has been created on the disk
    const files = await fileStorage.list(OUTPUT_DIR_PATH, name);
    assert.equal(files.length, 4);
  });

  it('Should upload an image in AVIF format', async () => {
    // Upload an image
    const { name } = await imageStorage.upload({
      body: createReadStream('356kb.avif'),
      name: 'name',
    });

    // Check if the image in all sizes has been created on the disk
    const files = await fileStorage.list(OUTPUT_DIR_PATH, name);
    assert.equal(files.length, 4);
  });

  it('Should upload an image in TIFF format', async () => {
    // Upload an image
    const { name } = await imageStorage.upload({
      body: createReadStream('8.4mb.tiff'),
      name: 'name',
    });

    // Check if the image in all sizes has been created on the disk
    const files = await fileStorage.list(OUTPUT_DIR_PATH, name);
    assert.equal(files.length, 4);
  });

  it('Should upload an image using a buffer', async () => {
    // Upload an image
    const { name } = await imageStorage.upload({
      body: fs.readFileSync(path.resolve(INPUT_DIR_PATH, '717kb.jpg')),
      name: 'name',
    });

    // Check if the image in all sizes has been created on the disk
    const files = await fileStorage.list(OUTPUT_DIR_PATH, name);
    assert.equal(files.length, 4);
  });

  it('Should upload an image in the specified sizes', async () => {
    // Upload an image
    const { name, contentLength } = await imageStorage.upload({
      body: createReadStream('717kb.jpg'),
      name: 'name',
      sizes: [50, 100, 200, 500],
    });
    assert.equal(contentLength[`${name}-50`] > 0, true);
    assert.equal(contentLength[`${name}-50-c`] > 0, true);
    assert.equal(contentLength[`${name}-100`] > 0, true);
    assert.equal(contentLength[`${name}-100-c`] > 0, true);
    assert.equal(contentLength[`${name}-200`] > 0, true);
    assert.equal(contentLength[`${name}-200-c`] > 0, true);
    assert.equal(contentLength[`${name}-500`] > 0, true);
    assert.equal(contentLength[`${name}-500-c`] > 0, true);

    // Check if the image in all sizes has been created on the disk
    const files = await fileStorage.list(OUTPUT_DIR_PATH, name);
    assert.equal(files.length, 8);
  });

  it('Should upload an image in the specified cropped sizes', async () => {
    // Upload an image
    const { name, contentLength } = await imageStorage.upload({
      body: createReadStream('717kb.jpg'),
      name: 'name',
      croppedSizes: [200],
    });
    assert.equal(contentLength[`${name}-50`] > 0, true);
    assert.equal(contentLength[`${name}-100`] > 0, true);
    assert.equal(contentLength[`${name}-200-c`] > 0, true);

    // Check if the image in all sizes has been created on the disk
    const files = await fileStorage.list(OUTPUT_DIR_PATH, name);
    assert.equal(files.length, 3);
  });

  it('Should change a suffix for cropped images', async () => {
    // Upload an image
    const { name, contentLength } = await imageStorage.upload({
      body: createReadStream('717kb.jpg'),
      name: 'name',
      sizes: [],
      croppedSizes: [50],
      cropSuffix: '-cropped',
    });
    assert.equal(contentLength[`${name}-50-cropped`] > 0, true);

    // Check if the image in all sizes has been created on the disk
    const files = await fileStorage.list(OUTPUT_DIR_PATH, name);
    assert.equal(files.length, 1);
    assert.equal(files[0].endsWith('-cropped'), true);
  });
});

describe('delete', () => {
  it('Should delete an image in all sizes', async () => {
    // Upload an image
    const { name } = await imageStorage.upload({
      body: createReadStream('717kb.jpg'),
      name: 'name',
    });

    // Create a text file with a name starting with the same prefix (must remain on the disk)
    const fileName = `${name}-text.txt`;
    const filePath = path.resolve(OUTPUT_DIR_PATH, fileName);
    fs.writeFileSync(filePath, Buffer.from('text'));

    // Check if all files was created
    const files = await fileStorage.list(OUTPUT_DIR_PATH, name);
    assert.equal(files.length, 5);

    // Delete the image
    await imageStorage.delete(name);

    // Check if the image in all sizes has been deleted, and the text file remains on the disk
    const filesAfterDeletion = await fileStorage.list(OUTPUT_DIR_PATH, name);
    assert.equal(filesAfterDeletion.length, 1);
    assert.equal(filesAfterDeletion[0], fileName);
  });

  it('Should delete an image in the specified sizes', async () => {
    // Upload an image
    const { name } = await imageStorage.upload({
      body: createReadStream('717kb.jpg'),
      name: 'name',
    });

    // Check if all files was created
    const files = await fileStorage.list(OUTPUT_DIR_PATH, name);
    assert.equal(files.length, 4);

    // Delete the image
    await imageStorage.delete(name, [50]);

    // Check if the image in the specified sizes has been deleted
    const filesAfterDeletion = await fileStorage.list(OUTPUT_DIR_PATH, name);
    assert.equal(filesAfterDeletion.length, 2);
  });
});
