import crypto from 'crypto';
import { PassThrough, Readable } from 'stream';
import { fileTypeStream } from 'file-type';
import createSharp, { type Sharp } from 'sharp';

export interface StorageUploadOptions {
  /**
   * The bucket name.
   */
  bucket: string;
  /**
   * The file name.
   */
  key: string;
  /**
   * The file read stream.
   */
  body: Readable;
  /**
   * The content type of the file.
   */
  contentType: string;
}

export interface Storage {
  /**
   * Returns the list of files in a bucket starting with the specified prefix.
   */
  list(bucket: string, prefix: string): Promise<string[]>;

  /**
   * Uploads the file to a bucket with the specified name.
   */
  upload(options: StorageUploadOptions): Promise<void>;

  /**
   * Deletes a file.
   */
  delete(bucket: string, key: string): Promise<void>;

  /**
   * Deletes multiple files at once.
   */
  deleteMultiple(bucket: string, keys: string[]): Promise<void>;
}

export type ImageType = 'jpg' | 'png' | 'webp' | 'gif' | 'avif' | 'tif';

export interface ImageOptions {
  /**
   * Each image is saved in the specified sizes.
   * @default [72, 192, 512, 1024, 2560]
   */
  sizes?: number[];
  /**
   * Each cropped image is saved in the specified sizes.
   * @default sizes
   */
  croppedSizes?: number[];
  /**
   * The max file size in bytes.
   * By default, 20 MB.
   * @default 20971520
   */
  maxSize?: number;
  /**
   * The supported image types.
   * @default ['jpg', 'png', 'webp', 'gif', 'avif', 'tif']
   */
  types?: ImageType[];
  /**
   * The suffix for cropped images.
   * @default -c
   */
  cropSuffix?: string;
  /**
   * The number of files uploaded to a storage in parallel.
   * @default 4
   */
  concurrentFiles?: number;
  /**
   * The callback using which you can change the output image type, quality, and perform any transformations
   * supported by the sharp library. Do not resize the image here to avoid the quality loss.
   * By default, the image is converted to a progressive JPEG with a quality of 90.
   * @default undefined
   */
  transformer?: (sharp: Sharp) => Sharp;
}

export interface ImageStorageOptions extends ImageOptions {
  /**
   * The bucket name.
   */
  bucket: string;
}

export interface UploadOptions extends ImageOptions {
  /**
   * The content of the image.
   * Can be a buffer or a readable stream.
   */
  body: Buffer | Readable;
  /**
   * The name of the image.
   * Note that the final image name has a suffix with a hash and size (e.g. `my-img~hash-1024` if the name was `my-img`).
   * It may contain a path (e.g. `my-dir/name`), but it is recommended to use the `path` option for it.
   */
  name: string;
  /**
   * Whether the hash is appended to the name to avoid caching.
   * @default false
   */
  useHash?: boolean;
  /**
   * Whether the old images will be deleted after the new one is uploaded.
   * @default false
   */
  deleteOldImages?: boolean;
  /**
   * The path to the directory in a bucket where the image will be uploaded.
   * @default undefined
   */
  path?: string;
}

export interface UploadResponse {
  /**
   * The image name with a hash.
   */
  name: string;
  /**
   * The content length for each image.
   */
  contentLength: Record<string, number>;
}

interface DeleteAllImagesOptions {
  includePrefix: string;
  excludePrefix: string;
}

abstract class ImageStorage {
  protected abstract readonly storage: Storage;

  private readonly bucket: string;

  private readonly options: Required<ImageOptions>;

  public constructor(options: ImageStorageOptions) {
    const { bucket, ...imageOptions } = options;
    const defaultSizes = [72, 192, 512, 1024, 2560];

    this.bucket = bucket;
    this.options = {
      sizes: defaultSizes,
      croppedSizes: imageOptions.sizes || defaultSizes,
      maxSize: 20971520, // 20 MB
      types: ['jpg', 'png', 'webp', 'gif', 'avif', 'tif'],
      cropSuffix: '-c',
      concurrentFiles: 4,
      transformer: (sharp) =>
        sharp.flatten({ background: { r: 255, g: 255, b: 255 } }).jpeg({
          quality: 90,
          progressive: true,
        }),
      ...imageOptions,
    };
  }

  /**
   * Uploads an image.
   */
  public async upload(options: UploadOptions): Promise<UploadResponse> {
    const {
      body,
      name,
      useHash = false,
      deleteOldImages = false,
      path,
      sizes,
      maxSize,
      types,
      cropSuffix,
      concurrentFiles,
      transformer,
    } = { ...this.options, ...options };

    // Check if the name of the image does not contain the ~ character.
    // Otherwise, there is a possibility that the image may be deleted when another image is uploaded again.
    // For example, if we have 2 images «xyz» (e.g. the full name is «xyz~hash-1024») and «xyz~» (e.g. the full name is
    // «xyz~~hash-1024»), and you re-upload the «xyz» image, the «xyz~» image will also be deleted in the deleteOldImages
    // function by the «xyz~» prefix.
    if (useHash && name.includes('~')) {
      throw new Error('The image name must not contain the ~ character');
    }

    const nameWithPath = path ? `${path}/${name}` : name;

    const croppedSizes =
      options.sizes && !options.croppedSizes
        ? options.sizes
        : options.croppedSizes || this.options.croppedSizes;

    const bodyStream = Buffer.isBuffer(body)
      ? ImageStorage.convertBufferToStream(body)
      : body;

    const readStream = await fileTypeStream(bodyStream);
    const { fileType } = readStream;

    // Check if the file type is detected
    if (!fileType) {
      throw new Error(
        `Only the following file types are supported: ${types.join(', ')}`
      );
    }

    // Check if the file type is allowed
    const { ext, mime } = fileType;
    if (!types.includes(ext as ImageType)) {
      throw new Error(
        `The ${
          fileType.ext
        } type is not allowed. Only the following file types are supported: ${types.join(
          ', '
        )}.`
      );
    }

    // Generate a hash that is appended to the file name to avoid caching
    const hash = crypto.randomBytes(2).toString('hex');
    const finalName = useHash
      ? ImageStorage.concatNameAndHash(nameWithPath, hash)
      : nameWithPath;

    // Check if the file size does not exceed maxSize
    let fileSize = 0;
    readStream.on('data', (chunk) => {
      fileSize += chunk.length;
      if (fileSize > maxSize) {
        readStream.destroy(
          new Error(
            `The image size exceeds ${ImageStorage.prettyFileSize(maxSize)}`
          )
        );
      }
    });

    const maxWidth = Math.max(...sizes, ...croppedSizes);
    const sharpStream = createSharp()
      .rotate() // Rotate the image based on the EXIF
      .resize({ width: maxWidth }); // Resize the base image to speed up subsequent transformations
    const writeStream = transformer(sharpStream); // Perform base transformations (e.g. change the image format)
    writeStream.setMaxListeners(0); // Allow unlimited number of listeners
    const uploadOptionsList: Array<StorageUploadOptions> = [];
    const contentLength: Record<string, number> = {};

    // Add a new output stream to upload an image
    const addImage = (size: number, cropped = false) => {
      const sizeOptions = cropped
        ? { width: size, height: size }
        : { width: size };
      const nameWithSize = ImageStorage.concatNameAndSize(finalName, size);
      const outputName = cropped
        ? `${nameWithSize}${cropSuffix}`
        : nameWithSize;

      const stream = writeStream.clone().resize(sizeOptions);
      stream.on('data', (chunk) => {
        if (!contentLength[outputName]) contentLength[outputName] = 0;
        contentLength[outputName] += chunk.length;
      });

      const passThroughStream = new PassThrough();
      stream.pipe(passThroughStream);

      uploadOptionsList.push({
        bucket: this.bucket,
        key: outputName,
        body: passThroughStream,
        contentType: mime,
      });
    };

    // Add an output stream for each image size
    sizes.forEach((size) => addImage(size));
    croppedSizes.forEach((size) => addImage(size, true));

    // Resize all the images and save them to a storage. Run it in parallel.
    const promises = new Array(concurrentFiles).fill(Promise.resolve());
    const chainNext = (promise: Promise<void>) => {
      const uploadOptions = uploadOptionsList.shift();
      if (!uploadOptions) return promise;
      return promise.then(() => chainNext(this.storage.upload(uploadOptions)));
    };

    // Attach the sharp stream (write) to the file stream (read)
    readStream.pipe(writeStream);

    // Wait until the upload is complete or an error occurs
    await new Promise((resolve, reject) => {
      readStream.on('error', reject);
      Promise.all(promises.map(chainNext)).then(resolve).catch(reject);
    });

    // Delete old images from a storage
    if (deleteOldImages) {
      await this.deleteOldImages({
        includePrefix: ImageStorage.concatNameAndHash(nameWithPath, ''),
        excludePrefix: finalName,
      });
    }

    // Return the image name with a hash without a path
    return {
      name: useHash ? ImageStorage.concatNameAndHash(name, hash) : name,
      contentLength,
    };
  }

  /**
   * Deletes an image in all sizes.
   */
  public async delete(key: string, sizes?: number[]): Promise<void> {
    const keys = (sizes || this.options.sizes).map((size) =>
      ImageStorage.concatNameAndSize(key, size)
    );
    const croppedKeys = keys.map((item) => `${item}${this.options.cropSuffix}`);
    await this.storage.deleteMultiple(this.bucket, [...keys, ...croppedKeys]);
  }

  /**
   * Deletes all images starting with the specified prefix.
   */
  private async deleteOldImages(
    options: DeleteAllImagesOptions
  ): Promise<void> {
    const { includePrefix, excludePrefix } = options;
    const names = await this.storage.list(this.bucket, includePrefix);
    const filteredNames = names.filter(
      (item) => !item.startsWith(excludePrefix)
    );
    await this.storage.deleteMultiple(this.bucket, filteredNames);
  }

  /**
   * Concatenates the image name and its hash.
   */
  private static concatNameAndHash(name: string, hash: string) {
    return `${name}~${hash}`;
  }

  /**
   * Concatenates the image name and its size.
   */
  private static concatNameAndSize(name: string, size: number) {
    return `${name}-${size}`;
  }

  /**
   * Converts a buffer to a readable stream.
   */
  private static convertBufferToStream(buffer: Buffer): Readable {
    const readable = new Readable({ read: () => {} });
    readable.push(buffer);
    readable.push(null);
    return readable;
  }

  /**
   * Converts the file size to a human-readable string.
   */
  private static prettyFileSize(size: number) {
    if (size > 1073741824) return `${this.round(size / 1073741824)} GB`;
    if (size > 1048576) return `${this.round(size / 1048576)} MB`;
    if (size > 1024) return `${this.round(size / 1024)} KB`;
    return `${this.round(size)} B`;
  }

  /**
   * Rounds the file size to 2 decimal places.
   */
  private static round(n: number) {
    return Math.round(n * 100) / 100;
  }
}

export default ImageStorage;
