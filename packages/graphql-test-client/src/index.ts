import { Blob } from 'node:buffer';
import fs from 'node:fs';
import superagent, { type Agent, type SuperAgentStatic } from 'superagent';

type Headers = Record<string, string>;
type MultipartValueSingle =
  | Blob
  | Buffer
  | fs.ReadStream
  | string
  | boolean
  | number;

interface TestClientProps {
  url: string;
  sharedHeaders?: Headers;
  saveCookies?: boolean;
}

interface TestClientQueryProps {
  query: string;
  variables?: Record<string, any>; // eslint-disable-line @typescript-eslint/no-explicit-any
  uploadables?: Record<string, MultipartValueSingle>;
  headers?: Headers;
}

class GraphQLTestClient {
  private readonly url: string;

  private readonly sharedHeaders: Record<string, string>;

  private readonly agent: SuperAgentStatic | Agent;

  public constructor(props: TestClientProps) {
    const { url, sharedHeaders = {}, saveCookies } = props;
    this.url = url;
    this.sharedHeaders = sharedHeaders;
    this.agent = saveCookies ? new superagent.agent() : superagent;
  }

  /**
   * Make a GraphQL request.
   */
  public query(props: TestClientQueryProps): superagent.SuperAgentRequest {
    const { query, variables, uploadables, headers = {} } = props;

    const request = this.agent
      .post(this.url)
      .set({ ...this.sharedHeaders, ...headers });

    // Uses the specification for GraphQL multipart form requests.
    // See https://github.com/jaydenseric/graphql-multipart-request-spec
    if (uploadables) {
      if (!variables || !variables.input) {
        throw new Error('Pass all parameters to the input variable');
      }
      const map = {};

      Object.keys(uploadables).forEach((key, index) => {
        map[index.toString()] = [`variables.input.${key}`];
      });

      request.field({
        operations: JSON.stringify({ query, variables }),
        map: JSON.stringify(map),
      });

      // Append files
      Object.values(uploadables).forEach((value, index) => {
        request.attach(index.toString(), value);
      });

      return request;
    }

    return request.send({ query, variables });
  }

  /**
   * Create a copy of TestClient that saves cookies.
   */
  public clone(): GraphQLTestClient {
    return new GraphQLTestClient({
      url: this.url,
      sharedHeaders: this.sharedHeaders,
      saveCookies: true,
    });
  }
}

export default GraphQLTestClient;
