import { test } from 'node:test';
import assert from 'node:assert';
import AppUAParser from './index.js';

test('Invalid user agent', () => {
  const userAgent = 'okhttp/3.12.12';
  const appUAParser = new AppUAParser(userAgent);
  const app = appUAParser.getApp();
  const os = appUAParser.getOS();
  const device = appUAParser.getDevice();
  assert.equal(AppUAParser.isValid(userAgent), false);
  assert.deepStrictEqual(app, {
    bundleId: '',
    version: '',
    buildNumber: 0,
  });
  assert.deepStrictEqual(os, {
    name: '',
    version: '',
    buildId: '',
  });
  assert.deepStrictEqual(device, {
    type: '',
    manufacturer: '',
    brand: '',
    model: '',
    id: '',
    name: '',
    uniqueId: '',
  });
});

test('iOS user agent', () => {
  const userAgent =
    "App com.myproject/1.0.1 (Handset; iOS/12.5.4; 16H50; Apple; Apple; iPhone 6; iPhone7,2; Becca's iPhone; 12ABCDE3-45FG-678H-I90J-K1L2MN34O567)";
  const appUAParser = new AppUAParser(userAgent);
  const app = appUAParser.getApp();
  const os = appUAParser.getOS();
  const device = appUAParser.getDevice();
  assert.equal(AppUAParser.isValid(userAgent), true);
  assert.deepStrictEqual(app, {
    bundleId: 'com.myproject',
    version: '1.0',
    buildNumber: 1,
  });
  assert.deepStrictEqual(os, {
    name: 'iOS',
    version: '12.5.4',
    buildId: '16H50',
  });
  assert.deepStrictEqual(device, {
    type: 'Handset',
    manufacturer: 'Apple',
    brand: 'Apple',
    model: 'iPhone 6',
    id: 'iPhone7,2',
    name: "Becca's iPhone",
    uniqueId: '12ABCDE3-45FG-678H-I90J-K1L2MN34O567',
  });
});

test('Android user agent', () => {
  const userAgent =
    'App com.myproject/1.0.1 (Handset; Android/10; QP1A.190711.020; Xiaomi; Redmi; M2006C3MNG; angelican; Android Bluedroid; 12a345b678c90d12)';
  const appUAParser = new AppUAParser(userAgent);
  const app = appUAParser.getApp();
  const os = appUAParser.getOS();
  const device = appUAParser.getDevice();
  assert.equal(AppUAParser.isValid(userAgent), true);
  assert.deepStrictEqual(app, {
    bundleId: 'com.myproject',
    version: '1.0',
    buildNumber: 1,
  });
  assert.deepStrictEqual(os, {
    name: 'Android',
    version: '10',
    buildId: 'QP1A.190711.020',
  });
  assert.deepStrictEqual(device, {
    type: 'Handset',
    manufacturer: 'Xiaomi',
    brand: 'Redmi',
    model: 'M2006C3MNG',
    id: 'angelican',
    name: 'Android Bluedroid',
    uniqueId: '12a345b678c90d12',
  });
});
