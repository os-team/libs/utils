export type RawErrorCode =
  | 'invalid_request'
  | 'not_supported'
  | 'invalid_credentials'
  | 'forbidden'
  | 'not_found'
  | 'too_many_requests'
  | 'internal_server_error';

export interface YCRawError {
  id: string;
  code: RawErrorCode;
  description?: string;
  parameter?: string;
}

const isObject = (value: unknown): value is object =>
  typeof value === 'object' && !Array.isArray(value) && value !== null;

export const isYCRawError = (value: unknown): value is YCRawError =>
  isObject(value) &&
  'id' in value &&
  typeof value.id === 'string' &&
  'code' in value &&
  typeof value.id === 'string' &&
  (!('description' in value) || typeof value.description === 'string') &&
  (!('parameter' in value) || typeof value.parameter === 'string');

export class YCError extends Error {
  public readonly id: string;

  public readonly code: RawErrorCode;

  public readonly description?: string;

  public readonly parameter?: string;

  public constructor(rawError: YCRawError) {
    super(rawError.description);
    this.id = rawError.id;
    this.code = rawError.code;
    this.description = rawError.description;
    this.parameter = rawError.parameter;
  }
}

export class YCInvalidRequestError extends YCError {}

export class YCNotSupportedError extends YCError {}

export class YCInvalidCredentialsError extends YCError {}

export class YCForbiddenError extends YCError {}

export class YCNotFoundError extends YCError {}

export class YCTooManyRequestsError extends YCError {}

export class YCInternalServerError extends YCError {}

export const generateError = (rawError: YCRawError): Error => {
  switch (rawError.code) {
    case 'invalid_request':
      return new YCInvalidRequestError(rawError);
    case 'not_supported':
      return new YCNotSupportedError(rawError);
    case 'invalid_credentials':
      return new YCInvalidCredentialsError(rawError);
    case 'forbidden':
      return new YCForbiddenError(rawError);
    case 'not_found':
      return new YCNotFoundError(rawError);
    case 'too_many_requests':
      return new YCTooManyRequestsError(rawError);
    case 'internal_server_error':
      return new YCInternalServerError(rawError);
    default:
      return new YCError(rawError);
  }
};
