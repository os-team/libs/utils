import type { ReceiptItem, TaxSystemCode } from './receipt.js';

export interface CustomerData {
  /**
   * Name of the organization for companies, full name for sole proprietors and individuals.
   */
  full_name?: string;
  /**
   * User's Tax Identification Number (INN).
   */
  inn?: string;
  /**
   * User's email address.
   */
  email?: string;
  /**
   * User's phone number.
   * Specified in the ITU-T E.164, format, for example, 79000000000.
   */
  phone?: string;
}

export interface ReceiptItemData extends ReceiptItem {
  /**
   * Product code is a unique number assigned to a unit of product during marking process.
   * Format: hexadecimal number with spaces.
   * Example: 00 00 00 01 00 21 FA 41 00 23 05 41 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 12 00 AB 00.
   */
  product_code?: string;
  /**
   * Country of origin code according to the Russian classifier of world countries.
   * Format: OK (MK (ISO 3166) 004-97) 025-2001.
   * Example: RU.
   */
  country_of_origin_code?: string;
  /**
   * Customs declaration number.
   */
  customs_declaration_number?: string;
  /**
   * Amount of excise tax on products including kopeks.
   * Decimal number with 2 digits after the period.
   */
  excise?: string;
}

export interface ReceiptData {
  /**
   * User details.
   */
  customer?: CustomerData;
  /**
   * List of products in an order.
   */
  items: ReceiptItemData[];
  /**
   * Store's tax system.
   */
  tax_system_code?: TaxSystemCode;
  /**
   * User's phone number.
   * Specified in the ITU-T E.164, format, for example, 79000000000.
   */
  phone?: string;
  /**
   * User's email address.
   */
  email?: string;
}
