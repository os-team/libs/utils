import type { BaseList, BaseListParams } from '../baseList.js';
import type { Receipt, ReceiptStatus } from './receipt.js';

export interface ReceiptListParams extends BaseListParams {
  /**
   * Filter by receipt status.
   */
  status?: ReceiptStatus;
  /**
   * Filter by payment ID.
   */
  payment_id?: string;
  /**
   * Filter by refund ID.
   */
  refund_id?: string;
}

export type ReceiptList = BaseList<Receipt>;
