import type { Amount } from '../amount.js';

export type ReceiptType = 'payment' | 'refund';

export type ReceiptStatus = 'pending' | 'succeeded' | 'canceled';

export type TaxSystemCode =
  | 1 // General tax system
  | 2 // Simplified (STS, income)
  | 3 // Simplified (STS, income with costs deducted)
  | 4 // Unified tax on imputed income (ENVD)
  | 5 // Unified agricultural tax (ESN)
  | 6; // Patent Based Tax System

export type VatCode =
  | 1 // VAT not included
  | 2 // 0% VAT rate
  | 3 // 10% VAT rate
  | 4 // 20% receipt's VAT rate
  | 5 // 10/110 receipt's estimate VAT rate
  | 6; // 20/120 receipt's estimate VAT rate

export type PaymentSubject =
  | 'commodity' // Product
  | 'excise' // Excisable goods
  | 'job' // Job
  | 'service' // Service
  | 'gambling_bet' // Gambling bet
  | 'gambling_prize' // Gambling winnings
  | 'lottery' // Lottery ticket
  | 'lottery_prize' // Lottery winnings
  | 'intellectual_activity' // Intellectual property
  | 'payment' // Payment
  | 'agent_commission' // Agent's commission
  | 'property_right' // Property rights
  | 'non_operating_gain' // Non-operating income
  | 'insurance_premium' // Insurance tax
  | 'sales_tax' // Sales tax
  | 'resort_fee' // Resort fee
  | 'composite' // Several subjects
  | 'another'; // Other

export type PaymentMode =
  | 'full_prepayment' // Full prepayment
  | 'partial_prepayment' // Partial prepayment
  | 'advance' // Advance payment
  | 'full_payment' // Full payment
  | 'partial_payment' // Partial payment and loan
  | 'credit' // Loan
  | 'credit_payment'; // Loan repayment

export type AgentType =
  | 'banking_payment_agent'
  | 'banking_payment_subagent'
  | 'payment_agent'
  | 'payment_subagent'
  | 'attorney'
  | 'commissioner'
  | 'agent';

export interface Supplier {
  /**
   * Supplier's name.
   */
  name?: string;
  /**
   * Supplier's phone number.
   */
  phone?: string;
  /**
   * Supplier's Tax Identification Number.
   */
  inn?: string;
}

export interface ReceiptItem {
  /**
   * Product name.
   */
  description: string;
  /**
   * Product quantity.
   */
  quantity: string;
  /**
   * Product price.
   */
  amount: Amount;
  /**
   * VAT rate.
   */
  vat_code: VatCode;
  /**
   * Payment subject attribute.
   */
  payment_subject?: PaymentSubject;
  /**
   * Payment method attribute.
   */
  payment_mode?: PaymentMode;
  /**
   * Supplier of the product or service.
   */
  supplier?: Supplier;
  /**
   * Type of agent that sells a product or service.
   */
  agent_type?: AgentType;
}

export type ReceiptSettlementType =
  | 'cashless' // Cashless payment
  | 'prepayment' // Prepayment (advance payment)
  | 'postpayment' // Postpayment (loan)
  | 'consideration'; // Consideration

export interface ReceiptSettlement {
  /**
   * Type of settlement.
   */
  type: ReceiptSettlementType;
  /**
   * Settlement amount.
   */
  amount: Amount;
}

export interface Receipt {
  /**
   * Receipt's ID in YooMoney.
   */
  id: string;
  /**
   * Type of receipt in the online sales register.
   */
  type: ReceiptType;
  /**
   * ID of the payment that the receipt was created for.
   */
  payment_id?: string;
  /**
   * ID of the refund that the receipt was created for.
   */
  refund_id?: string;
  /**
   * Delivery status of receipt data to online sales register.
   */
  status: ReceiptStatus;
  /**
   * Fiscal document number.
   */
  fiscal_document_number?: string;
  /**
   * Number of fiscal storage drive in online sales register.
   */
  fiscal_storage_number?: string;
  /**
   * Fiscal attribute of the receipt.
   */
  fiscal_attribute?: string;
  /**
   * Date and time of receipt creation in the fiscal storage drive.
   * Based on UTC and specified in the ISO 8601 format. Example: 2017-11-03T11:52:31.827Z.
   */
  registered_at?: string;
  /**
   * Receipt's ID in online sales register.
   */
  fiscal_provider_id?: string;
  /**
   * Store's taxation system.
   */
  tax_system_code?: TaxSystemCode;
  /**
   * List of products in the receipt.
   */
  items: ReceiptItem[];
  /**
   * List of completed settlements.
   */
  settlements?: ReceiptSettlement[];
  /**
   * ID of the store to send the receipt on behalf of.
   */
  on_behalf_of?: string;
}
