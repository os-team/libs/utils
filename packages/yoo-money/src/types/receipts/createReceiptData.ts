import type { CustomerData, ReceiptItemData } from './receiptData.js';
import type {
  ReceiptSettlement,
  ReceiptType,
  TaxSystemCode,
} from './receipt.js';

export interface CreateReceiptData {
  /**
   * Type of receipt in online sales register.
   */
  type: ReceiptType;
  /**
   * Payment ID in YooMoney for displaying the receipt information in Merchant Profile.
   */
  payment_id?: string;
  /**
   * Refund ID in YooMoney for displaying the receipt information in Merchant Profile.
   */
  refund_id?: string;
  /**
   * User details.
   */
  customer: CustomerData;
  /**
   * List of products in the receipt.
   */
  items: ReceiptItemData[];
  /**
   * Store's tax system.
   */
  tax_system_code?: TaxSystemCode;
  /**
   * Creation of receipt in the online sales register immediately after receipt object creation.
   */
  send: boolean;
  /**
   * List of completed settlements.
   */
  settlements: ReceiptSettlement[];
  /**
   * ID of the store to send the receipt on behalf of.
   */
  on_behalf_of?: string;
}
