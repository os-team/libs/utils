export interface Amount {
  /**
   * Amount in the selected currency, in the form of a string with a dot separator,
   * for example, 10.00.
   */
  value: string;
  /**
   * Currency code in the ISO-4217 format.
   */
  currency: string;
}
