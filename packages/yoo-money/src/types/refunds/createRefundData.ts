import type { Amount } from '../amount.js';
import type { ReceiptData } from '../receipts/receiptData.js';
import type { RefundSource } from './refund.js';

export interface CreateRefundData {
  /**
   * Payment ID in YooMoney.
   */
  payment_id: string;
  /**
   * Amount to be refunded to the user.
   */
  amount: Amount;
  /**
   * Commentary to the refund, reason behind returning the funds to the user.
   */
  description?: string;
  /**
   * Data for creating a receipt in the online sales register for compliance with 54-FZ.
   */
  receipt?: ReceiptData;
  /**
   * Information about which store and how much you need to deduct to make a refund.
   */
  sources?: RefundSource[];
}
