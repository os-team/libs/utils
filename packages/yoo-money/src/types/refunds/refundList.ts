import type { BaseList, BaseListParams } from '../baseList.js';
import type { Refund, RefundStatus } from './refund.js';

export interface RefundListParams extends BaseListParams {
  /**
   * Filter by payment ID.
   */
  payment_id?: string;
  /**
   * Filter by refund status.
   */
  status?: RefundStatus;
}

export type RefundList = BaseList<Refund>;
