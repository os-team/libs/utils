import { type Amount } from '../amount.js';

export type RefundStatus = 'pending' | 'canceled' | 'succeeded';

export interface RefundSource {
  /**
   * ID of the store you want to make a refund for.
   */
  account_id: string;
  /**
   * The amount of the refund.
   */
  amount: Amount;
  /**
   * Commission that you withheld when making the payment and want to refund.
   */
  platform_fee_amount?: Amount;
}

export interface Refund {
  /**
   * Refund's ID in YooMoney.
   */
  id: string;
  /**
   * Payment ID in YooMoney.
   */
  payment_id: string;
  /**
   * Refund status.
   */
  status: RefundStatus;
  /**
   * Time of refund creation.
   * Based on UTC and specified in the ISO 8601 format. Example: 2017-11-03T11:52:31.827Z.
   */
  created_at: string;
  /**
   * Amount refunded to the user.
   */
  amount: Amount;
  /**
   * Reason behind the refund.
   */
  description?: string;
  /**
   * Information about which store and how much you need to deduct to make a refund.
   */
  sources?: RefundSource[];
}
