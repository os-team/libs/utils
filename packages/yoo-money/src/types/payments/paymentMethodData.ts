import type { VatData } from './vatData.js';

export interface BasePaymentMethodData {
  /**
   * Payment method code.
   */
  type: string;
}

export interface AlfaClickPaymentMethodData extends BasePaymentMethodData {
  /**
   * Payment method code.
   */
  type: 'alfabank';
  /**
   * User's login in Alfa-Click.
   */
  login?: string;
}

export interface ApplePayPaymentMethodData extends BasePaymentMethodData {
  /**
   * Payment method code.
   */
  type: 'apple_pay';
  /**
   * Contents of the paymentData field in the PKPaymentToken object (Apple Pay payment token)
   * in Base64 format.
   */
  payment_data: string;
}

export interface CardDetails {
  /**
   * Bank card number.
   */
  number: string;
  /**
   * Expiration date, year, YYYY.
   */
  expiry_year: string;
  /**
   * Expiration date, month, MM.
   */
  expiry_month: string;
  /**
   * The CVC2 or CVV2 code printed on the back of the card, 3 or 4 digits.
   */
  csc?: string;
  /**
   * Cardholder's name.
   */
  cardholder?: string;
}

export interface BankCardPaymentMethodData extends BasePaymentMethodData {
  /**
   * Payment method code.
   */
  type: 'bank_card';
  /**
   * Bank card details.
   */
  card?: CardDetails;
}

export interface CashPaymentMethodData extends BasePaymentMethodData {
  /**
   * Payment method code.
   */
  type: 'cash';
  /**
   * User's phone number for sending the text message containing the payment password,
   * specified in the ITU-T E.164 format, for example, 79000000000.
   */
  phone?: string;
}

export interface DirectCarrierBillingPaymentMethodData
  extends BasePaymentMethodData {
  /**
   * Payment method code.
   */
  type: 'mobile_balance';
  /**
   * Mobile phone number used for conducting a payment,
   * specified in the ITU-T E.164 format, for example, 79000000000.
   */
  phone: string;
}

export interface GooglePayPaymentMethodData extends BasePaymentMethodData {
  /**
   * Payment method code.
   */
  type: 'google_pay';
  /**
   * Payment Token Cryptography for making payments via Google Pay.
   */
  payment_method_token: string;
}

export interface InstallmentsPaymentMethodData extends BasePaymentMethodData {
  /**
   * Payment method code.
   */
  type: 'installments';
}

export interface QiwiWalletPaymentMethodData extends BasePaymentMethodData {
  /**
   * Payment method code.
   */
  type: 'qiwi';
  /**
   * The phone number specified during the registration process of the QIWI account,
   * specified in the ITU-T E.164 format, for example, 79000000000.
   */
  phone?: string;
}

export interface SberbankBusinessOnlinePaymentMethodData
  extends BasePaymentMethodData {
  /**
   * Payment method code.
   */
  type: 'b2b_sberbank';
  /**
   * Purpose of payment.
   */
  payment_purpose: string;
  /**
   * Information about the value-added tax (VAT).
   */
  vat_data: VatData;
}

export interface SberbankOnlinePaymentMethodData extends BasePaymentMethodData {
  /**
   * Payment method code.
   */
  type: 'sberbank';
  /**
   * The phone number specified during the registration process of the Sberbank Online account,
   * specified in the ITU-T E.164 format, for example, 79000000000.
   */
  phone?: string;
}

export interface TinkoffPaymentMethodData extends BasePaymentMethodData {
  /**
   * Payment method code.
   */
  type: 'tinkoff_bank';
}

export interface WeChatPaymentMethodData extends BasePaymentMethodData {
  /**
   * Payment method code.
   */
  type: 'wechat';
}

export interface WebmoneyPaymentMethodData extends BasePaymentMethodData {
  /**
   * Payment method code.
   */
  type: 'webmoney';
}

export interface YooMoneyPaymentMethodData extends BasePaymentMethodData {
  /**
   * Payment method code.
   */
  type: 'yoo_money';
}

export interface SbpPaymentMethodData extends BasePaymentMethodData {
  /**
   * Payment method code.
   */
  type: 'sbp';
}

export type PaymentMethodData =
  | AlfaClickPaymentMethodData
  | ApplePayPaymentMethodData
  | BankCardPaymentMethodData
  | CashPaymentMethodData
  | DirectCarrierBillingPaymentMethodData
  | GooglePayPaymentMethodData
  | InstallmentsPaymentMethodData
  | QiwiWalletPaymentMethodData
  | SberbankBusinessOnlinePaymentMethodData
  | SberbankOnlinePaymentMethodData
  | TinkoffPaymentMethodData
  | WeChatPaymentMethodData
  | WebmoneyPaymentMethodData
  | YooMoneyPaymentMethodData
  | SbpPaymentMethodData;
