import type { Amount } from '../amount.js';

export interface BaseVatData {
  /**
   * VAT taxation method code.
   */
  type: string;
}

export interface MixedVatData extends BaseVatData {
  /**
   * VAT taxation method code.
   */
  type: 'mixed';
  /**
   * VAT amount.
   */
  amount: Amount;
}

export interface UntaxedVatData extends BaseVatData {
  /**
   * VAT taxation method code.
   */
  type: 'untaxed';
}

export type TaxRate = '7' | '10' | '18' | '20';

export interface CalculatedVatData extends BaseVatData {
  /**
   * VAT taxation method code.
   */
  type: 'calculated';
  /**
   * VAT amount.
   */
  amount: Amount;
  /**
   * Tax rate (in percentage).
   */
  rate: TaxRate;
}

export type VatData = MixedVatData | UntaxedVatData | CalculatedVatData;
