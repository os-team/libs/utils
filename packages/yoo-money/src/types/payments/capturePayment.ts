import type { Amount } from '../amount.js';
import type { CreatePaymentData } from './createPaymentData.js';

export interface CapturePaymentData
  extends Pick<CreatePaymentData, 'receipt' | 'airline' | 'transfers'> {
  /**
   * Total amount charged to the user.
   */
  amount?: Amount;
}
