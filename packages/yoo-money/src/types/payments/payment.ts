import type { Amount } from '../amount.js';
import type { ConfirmationScenario } from './confirmationScenario.js';
import type { PaymentMethod } from './paymentMethod.js';

export interface Recipient {
  /**
   * Store's ID in YooMoney.
   */
  account_id: string;
  /**
   * Subaccount's ID.
   */
  gateway_id: string;
}

export type CancellationDetailsParty =
  | 'merchant' // Seller of goods and services (you)
  | 'yoo_money' // YooMoney
  | 'payment_network'; // External participants of the payment process

export type CancellationDetailsReason =
  | '3d_secure_failed' // 3-D Secure authentication failed
  | 'call_issuer' // Payment made with this payment method was declined for unknown reasons
  | 'canceled_by_merchant' // The payment is cancelled via the API in case of payment in two stages
  | 'card_expired' // The bank card has expired
  | 'country_forbidden' // Payments with a bank card issued in this country are not allowed
  | 'expired_on_capture' // The two-step payment debiting period has expired
  | 'expired_on_confirmation' // Payment expired
  | 'fraud_suspected' // The payment was blocked due to suspected fraud
  | 'general_decline' // No detailed reason provided
  | 'identification_required' // Exceeded payment limit on the YooMoney wallet
  | 'insufficient_funds' // Not enough money to make the payment
  | 'internal_timeout' // Technical problems on the YooMoney side
  | 'invalid_card_number' // Invalid card number
  | 'invalid_csc' // The CVV2 code (CVC2, CID) was entered incorrectly
  | 'issuer_unavailable' // The organization that provides the payment method is not available
  | 'payment_method_limit_exceeded' // Payment limit for this payment method or your store has been reached
  | 'payment_method_restricted' // Transactions made with this payment method are forbidden
  | 'permission_revoked'; // Unable to make automatic debit: the user disabled recurring payments

export interface CancellationDetails {
  /**
   * The participant of the payment process that made the decision to cancel the payment.
   */
  party: CancellationDetailsParty;
  /**
   * Reason behind the cancelation.
   */
  reason: CancellationDetailsReason;
}

export interface AuthorizationDetails {
  /**
   * Retrieval Reference Number is a unique identifier of a transaction in the issuer's system.
   */
  rrn?: string;
  /**
   * Bank card's authorization code.
   */
  auth_code?: string;
}

export type PaymentStatus =
  | 'pending'
  | 'waiting_for_capture'
  | 'succeeded'
  | 'canceled';

export type ReceiptRegistration = 'pending' | 'succeeded' | 'canceled';

export interface Transfer {
  /**
   * ID of the store that you accept payment for.
   */
  account_id: string;
  /**
   * The amount to be transferred to the merchant.
   */
  amount: Amount;
  /**
   * Status of money distribution between stores.
   */
  status: PaymentStatus;
  /**
   * Commission for goods and services sold, which is deducted from the store in your favor.
   */
  platform_fee_amount?: Amount;
}

export interface Payment {
  /**
   * Payment ID in YooMoney.
   */
  id: string;
  /**
   * Payment status.
   */
  status: PaymentStatus;
  /**
   * Payment amount.
   */
  amount: Amount;
  /**
   * The amount of the payment that will be received by the store.
   */
  income_amount?: Amount;
  /**
   * Description of the transaction displayed in your YooMoney Merchant Profile,
   * and shown to the user during checkout.
   */
  description?: string;
  /**
   * Payment recipient.
   */
  recipient: Recipient;
  /**
   * Payment method  used for this payment.
   */
  payment_method: PaymentMethod;
  /**
   * Time of payment capture.
   * Based on UTC and specified in the ISO 8601 format. Example: 2017-11-03T11:52:31.827Z.
   */
  captured_at?: string;
  /**
   * Time of order creation.
   * Based on UTC and specified in the ISO 8601 format. Example: 2017-11-03T11:52:31.827Z.
   */
  created_at: string;
  /**
   * The period during which you can cancel or capture a payment for free.
   * Based on UTC and specified in the ISO 8601 format. Example: 2017-11-03T11:52:31.827Z.
   */
  expires_at?: string;
  /**
   * Selected payment confirmation scenario.
   */
  confirmation?: ConfirmationScenario;
  /**
   * The attribute of a test transaction.
   */
  test: boolean;
  /**
   * The amount refunded to the user.
   */
  refunded_amount?: Amount;
  /**
   * The attribute of a paid order.
   */
  paid: boolean;
  /**
   * Availability of the option to make a refund via API.
   */
  refundable: boolean;
  /**
   * Delivery status of receipt data to online sales register.
   */
  receipt_registration?: ReceiptRegistration;
  /**
   * Any additional data you might require for processing payments.
   */
  metadata?: Record<string, string | number | boolean>;
  /**
   * Commentary to the canceled status: who and why canceled the payment.
   */
  cancellation_details?: CancellationDetails;
  /**
   * Payment authorization details.
   */
  authorization_details?: AuthorizationDetails;
  /**
   * Data on the distribution of money — how much and to which store you need to transfer.
   */
  transfers?: Transfer[];
}
