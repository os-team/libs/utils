import type { BaseList, BaseListParams } from '../baseList.js';
import type { Payment, PaymentStatus } from './payment.js';
import type { PaymentMethodType } from './paymentMethod.js';

export interface PaymentListParams extends BaseListParams {
  /**
   * Filter by payment confirmation time:
   * the time must be greater than or equal to the specified value.
   */
  'captured_at.gte'?: string;
  /**
   * Filter by payment confirmation time:
   * the time must be greater than the specified value.
   */
  'captured_at.gt'?: string;
  /**
   * Filter by payment confirmation time:
   * the time must be less than or equal to the specified value.
   */
  'captured_at.lte'?: string;
  /**
   * Filter by payment confirmation time:
   * the time must be less than the specified value.
   */
  'captured_at.lt'?: string;
  /**
   * Filter by payment method code.
   */
  payment_method?: PaymentMethodType;
  /**
   * Filter by payment status.
   */
  status?: PaymentStatus;
}

export type PaymentList = BaseList<Payment>;
