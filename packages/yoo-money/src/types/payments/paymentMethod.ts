import type { VatData } from './vatData.js';

export type PaymentMethodType =
  | 'alfabank'
  | 'apple_pay'
  | 'bank_card'
  | 'cash'
  | 'mobile_balance'
  | 'google_pay'
  | 'installments'
  | 'qiwi'
  | 'b2b_sberbank'
  | 'sberbank'
  | 'tinkoff_bank'
  | 'wechat'
  | 'webmoney'
  | 'yoo_money';

export interface BasePaymentMethod {
  /**
   * Payment method code.
   */
  type: PaymentMethodType;
  /**
   * Payment method ID.
   */
  id: string;
  /**
   * Saving payment methods allows conducting automatic recurring payments.
   */
  saved: boolean;
  /**
   * Payment method name.
   */
  title?: string;
}

export interface AlfaClickPaymentMethod extends BasePaymentMethod {
  /**
   * Payment method code.
   */
  type: 'alfabank';
  /**
   * User's login in Alfa-Click.
   */
  login?: string;
}

export interface ApplePayPaymentMethod extends BasePaymentMethod {
  /**
   * Payment method code.
   */
  type: 'apple_pay';
}

export type CardType =
  | 'MasterCard'
  | 'Visa'
  | 'Mir'
  | 'UnionPay'
  | 'JCB'
  | 'AmericanExpress'
  | 'DinersClub'
  | 'Unknown';

export type CardSource = 'apple_pay' | 'google_pay';

export interface Card {
  /**
   * First 6 digits of the card's number (BIN).
   */
  first6?: string;
  /**
   * Last 4 digits of the card's number.
   */
  last4: string;
  /**
   * Expiration date, year, YYYY.
   */
  expiry_year: string;
  /**
   * Expiration date, month, MM.
   */
  expiry_month: string;
  /**
   * Type of bank card.
   */
  card_type: CardType;
  /**
   * Code of the country where the bank card was issued according to ISO-3166 alpha-2.
   */
  issuer_country?: string;
  /**
   * Name of the issuing bank.
   */
  issuer_name?: string;
  /**
   * Source of bank card details.
   */
  source?: CardSource;
}

export interface BankCardPaymentMethod extends BasePaymentMethod {
  /**
   * Payment method code.
   */
  type: 'bank_card';
  /**
   * Bank card details.
   */
  card?: Card;
}

export interface CashPaymentMethod extends BasePaymentMethod {
  /**
   * Payment method code.
   */
  type: 'cash';
}

export interface DirectCarrierBillingPaymentMethod extends BasePaymentMethod {
  /**
   * Payment method code.
   */
  type: 'mobile_balance';
}

export interface GooglePayPaymentMethod extends BasePaymentMethod {
  /**
   * Payment method code.
   */
  type: 'google_pay';
}

export interface InstallmentsPaymentMethod extends BasePaymentMethod {
  /**
   * Payment method code.
   */
  type: 'installments';
}

export interface QiwiWalletPaymentMethod extends BasePaymentMethod {
  /**
   * Payment method code.
   */
  type: 'qiwi';
}

export interface PayerBankDetails {
  /**
   * Full name of the organization.
   */
  full_name: string;
  /**
   * Abbreviated name of the organization.
   */
  short_name: string;
  /**
   * Address of the organization.
   */
  address: string;
  /**
   * Taxpayer Identification Number (INN) of the organization.
   */
  inn: string;
  /**
   * Tax Registration Reason Code (KPP) of the organization.
   */
  kpp: string;
  /**
   * Name of the organization's bank.
   */
  bank_name: string;
  /**
   * Branch of the organization's bank.
   */
  bank_branch: string;
  /**
   * Bank Identification Code (BIC) of the organization's bank.
   */
  bank_bik: string;
  /**
   * Account number of the organization.
   */
  account: string;
}

export interface SberbankBusinessOnlinePaymentMethod extends BasePaymentMethod {
  /**
   * Payment method code.
   */
  type: 'b2b_sberbank';
  /**
   * Banking details of the payer.
   */
  payer_bank_details: PayerBankDetails;
  /**
   * Purpose of payment.
   */
  payment_purpose: string;
  /**
   * Information about the value-added tax (VAT).
   */
  vat_data: VatData;
}

export interface SberbankOnlinePaymentMethod extends BasePaymentMethod {
  /**
   * Payment method code.
   */
  type: 'sberbank';
  /**
   * The phone number specified during the registration process of the Sberbank Online account,
   * specified in the ITU-T E.164 format, for example, 79000000000.
   */
  phone?: string;
}

export interface TinkoffPaymentMethod extends BasePaymentMethod {
  /**
   * Payment method code.
   */
  type: 'tinkoff_bank';
}

export interface WeChatPaymentMethod extends BasePaymentMethod {
  /**
   * Payment method code.
   */
  type: 'wechat';
}

export interface WebmoneyPaymentMethod extends BasePaymentMethod {
  /**
   * Payment method code.
   */
  type: 'webmoney';
}

export interface YooMoneyPaymentMethod extends BasePaymentMethod {
  /**
   * Payment method code.
   */
  type: 'yoo_money';
  /**
   * The number of the YooMoney wallet used for making the payment.
   */
  account_number?: string;
}

export type PaymentMethod =
  | AlfaClickPaymentMethod
  | ApplePayPaymentMethod
  | BankCardPaymentMethod
  | CashPaymentMethod
  | DirectCarrierBillingPaymentMethod
  | GooglePayPaymentMethod
  | InstallmentsPaymentMethod
  | QiwiWalletPaymentMethod
  | SberbankBusinessOnlinePaymentMethod
  | SberbankOnlinePaymentMethod
  | TinkoffPaymentMethod
  | WeChatPaymentMethod
  | WebmoneyPaymentMethod
  | YooMoneyPaymentMethod;
