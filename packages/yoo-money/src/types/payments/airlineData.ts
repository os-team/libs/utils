export interface FlightPassengerData {
  /**
   * Passenger's first name.
   */
  first_name: string;
  /**
   * Passenger's last name.
   */
  last_name: string;
}

export interface FlightLegData {
  /**
   * Code of the departure airport according to IATA, for example, LED.
   */
  departure_airport: string;
  /**
   * Code of the arrival airport according to IATA, for example, AMS.
   */
  destination_airport: string;
  /**
   * Departure date in the YYYY-MM-DD ISO 8601:2004 format.
   */
  departure_date: string;
  /**
   * Airline code according to IATA.
   */
  carrier_code?: string;
}

export interface AirlineData {
  /**
   * Unique ticket number.
   */
  ticket_number?: string;
  /**
   * Booking reference number.
   */
  booking_reference?: string;
  /**
   * List of passengers.
   */
  passengers?: FlightPassengerData[];
  /**
   * List of flight legs.
   */
  legs?: FlightLegData[];
}
