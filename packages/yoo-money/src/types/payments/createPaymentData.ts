import type { Amount } from '../amount.js';
import type { ReceiptData } from '../receipts/receiptData.js';
import type { AirlineData } from './airlineData.js';
import type { ConfirmationScenarioData } from './confirmationScenarioData.js';
import type { Transfer } from './payment.js';
import type { PaymentMethodData } from './paymentMethodData.js';
import type { RecipientData } from './recipientData.js';

export type TransferData = Pick<
  Transfer,
  'account_id' | 'amount' | 'platform_fee_amount'
>;

export interface CreatePaymentData {
  /**
   * Payment amount.
   */
  amount: Amount;
  /**
   * Description of the transaction displayed in your YooMoney Merchant Profile,
   * and shown to the user during checkout.
   */
  description?: string;
  /**
   * Data for creating a receipt in the online sales register for compliance with 54-FZ.
   */
  receipt?: ReceiptData;
  /**
   * Payment recipient.
   */
  recipient?: RecipientData;
  /**
   * One-time payment token generated with the web or mobile SDK.
   */
  payment_token?: string;
  /**
   * Saved payment method's ID.
   */
  payment_method_id?: string;
  /**
   * Data for making payments using a certain method.
   */
  payment_method_data?: PaymentMethodData;
  /**
   * Information required to initiate the selected payment confirmation scenario by the user.
   */
  confirmation?: ConfirmationScenarioData;
  /**
   * Saving payment data (can be used for direct debits).
   */
  save_payment_method?: boolean;
  /**
   * Automatic acceptance of an incoming payment.
   */
  capture?: boolean;
  /**
   * User's IPv4 or IPv6 address.
   */
  client_ip?: string;
  /**
   * Any additional data you might require for processing payments.
   */
  metadata?: Record<string, string | number | boolean>;
  /**
   * Object containing the data for selling airline tickets, used only for bank card payments.
   */
  airline?: AirlineData;
  /**
   * Data on the distribution of money — how much and to which store you need to transfer.
   */
  transfers?: TransferData[];
}
