import fetch, { type RequestInit } from 'node-fetch';
import * as types from './types/index.js';
import { generateError, isYCRawError } from './errors.js';

/* eslint-disable @typescript-eslint/no-explicit-any */

export interface InitProps {
  shopId: string;
  secretKey: string;
}

interface RequestOptions {
  path: string;
  method: string;
  params?: Record<string, string>;
  data?: Record<string, any>;
  idempotenceKey?: string;
}

const API_ENDPOINT = 'https://api.yookassa.ru/v3/';

class YooMoney {
  private shopId: string;

  private secretKey: string;

  public constructor(props: InitProps) {
    this.shopId = props.shopId;
    this.secretKey = props.secretKey;
  }

  public readonly payments = {
    create: (
      data: types.CreatePaymentData,
      idempotenceKey: string
    ): Promise<types.Payment> =>
      this.makeRequest({
        path: 'payments',
        method: 'POST',
        data,
        idempotenceKey,
      }),
    list: (params: types.PaymentListParams): Promise<types.PaymentList> =>
      this.makeRequest({
        path: 'payments',
        method: 'GET',
        params,
      }),
    get: (paymentId: string): Promise<types.Payment> =>
      this.makeRequest({
        path: `payments/${paymentId}`,
        method: 'GET',
      }),
    capture: (
      paymentId: string,
      data: types.CapturePaymentData,
      idempotenceKey: string
    ): Promise<types.Payment> =>
      this.makeRequest({
        path: `payments/${paymentId}/capture`,
        method: 'POST',
        data,
        idempotenceKey,
      }),
    cancel: (
      paymentId: string,
      idempotenceKey: string
    ): Promise<types.Payment> =>
      this.makeRequest({
        path: `payments/${paymentId}/cancel`,
        method: 'POST',
        idempotenceKey,
      }),
  };

  public readonly refunds = {
    create: (
      data: types.CreateRefundData,
      idempotenceKey: string
    ): Promise<types.Refund> =>
      this.makeRequest({
        path: 'refunds',
        method: 'POST',
        data,
        idempotenceKey,
      }),
    list: (params: types.RefundListParams): Promise<types.RefundList> =>
      this.makeRequest({
        path: 'refunds',
        method: 'GET',
        params,
      }),
    get: (refundId: string): Promise<types.Refund> =>
      this.makeRequest({
        path: `refunds/${refundId}`,
        method: 'GET',
      }),
  };

  public readonly receipts = {
    create: (
      data: types.CreateReceiptData,
      idempotenceKey: string
    ): Promise<types.Receipt> =>
      this.makeRequest({
        path: 'receipts',
        method: 'POST',
        data,
        idempotenceKey,
      }),
    list: (params: types.ReceiptListParams): Promise<types.ReceiptList> =>
      this.makeRequest({
        path: 'receipts',
        method: 'GET',
        params,
      }),
    get: (receiptId: string): Promise<types.Receipt> =>
      this.makeRequest({
        path: `receipts/${receiptId}`,
        method: 'GET',
      }),
  };

  protected async makeRequest(options: RequestOptions): Promise<any> {
    let url = `${API_ENDPOINT}${options.path}`;

    // Make search params
    if (options.method === 'GET' && options.params) {
      const urlParams = Object.entries(options.params)
        .map(
          ([key, value]) =>
            `${encodeURIComponent(key)}=${encodeURIComponent(value)}`
        )
        .join('&');
      url += `?${urlParams}`;
    }

    // Make headers
    const authKey = Buffer.from(`${this.shopId}:${this.secretKey}`).toString(
      'base64'
    );
    const headers: Record<string, string> = {
      Authorization: `Basic ${authKey}`,
    };
    if (options.method === 'POST') {
      headers['Content-Type'] = 'application/json';
      if (options.idempotenceKey)
        headers['Idempotence-Key'] = options.idempotenceKey;
    }

    // Make fetch options
    const fetchOptions: RequestInit = {
      method: options.method,
      headers,
    };
    if (options.data) fetchOptions.body = JSON.stringify(options.data);

    // Make request
    const response = await fetch(url, fetchOptions);
    const result = await response.json();
    if (isYCRawError(result)) {
      throw generateError(result);
    }
    return result;
  }
}

export * from './types/index.js';
export * from './errors.js';
export default YooMoney;
