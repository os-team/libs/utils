import {
  type DataRef,
  OrRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import EntityRefRule from './EntityRefRule.js';
import CharRefRule from './CharRefRule.js';

/**
 * The reference.
 * See https://www.w3.org/TR/xml/#NT-Reference
 */
class ReferenceRule implements Rule<string> {
  private readonly rule: Rule<string>;

  public constructor() {
    const entityRefRule = new EntityRefRule(); // EntityRef
    const charRefRule = new CharRefRule(); // CharRef
    this.rule = new OrRule([entityRefRule, charRefRule]); // EntityRef | CharRef
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res];
  }
}

export default ReferenceRule;
