import { it } from 'node:test';
import assert from 'node:assert';
import NameRule from './NameRule.js';

const nameRule = new NameRule();

it('Should not match because the first character is invalid', () => {
  const data = '_-';
  const [isValid, nextPos] = nameRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should match the single-character name', () => {
  const data = '_a';
  const [isValid, nextPos, res] = nameRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'a');
});

it('Should match the name', () => {
  const data = '_a-a';
  const [isValid, nextPos, res] = nameRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'a-a');
});
