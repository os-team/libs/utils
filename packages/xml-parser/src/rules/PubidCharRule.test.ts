import { it } from 'node:test';
import assert from 'node:assert';
import PubidCharRule from './PubidCharRule.js';

const pubidCharRule = new PubidCharRule();

it('Should not match because the char is incorrect', () => {
  const data = '_^';
  const [isValid, nextPos] = pubidCharRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should match a char', () => {
  const data = '_a';
  const [isValid, nextPos, res] = pubidCharRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, 2);
  assert.equal(res, 'a');
});
