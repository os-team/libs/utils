import { AndRule, LiteralRule } from '@os-team/lexical-rules';
import SRule from './SRule.js';
import AuxiliaryRule from './AuxiliaryRule.js';

/**
 * The equal sign surrounded by white spaces.
 * See https://www.w3.org/TR/xml/#NT-Eq
 */
class EqRule extends AuxiliaryRule {
  public constructor() {
    super();
    const anySRule = new SRule('*'); // S?
    const eqSignRule = new LiteralRule('='); // '='
    this.rule = new AndRule([anySRule, eqSignRule, anySRule]); // S? '=' S?
  }
}

export default EqRule;
