import {
  CharRule,
  type DataRef,
  RepetitionRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';

/**
 * The number.
 */
class NumberRule implements Rule<number> {
  private readonly rule: Rule<string[]>;

  public constructor() {
    const digitRule = new CharRule({ allowed: [['0', '9']] }); // [0-9]
    this.rule = new RepetitionRule(digitRule, 1); // [0-9]+
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<number> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, Number(res.join(''))];
  }
}

export default NumberRule;
