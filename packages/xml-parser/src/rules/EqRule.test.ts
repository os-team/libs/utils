import { it } from 'node:test';
import assert from 'node:assert';
import EqRule from './EqRule.js';

const eqRule = new EqRule();

it('Should not match because there is no equal sign', () => {
  const data = 'a b';
  const [isValid, nextPos] = eqRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 2);
});

it('Should match regardless of there are no white space symbols', () => {
  const data = 'a=b';
  const [isValid, nextPos, res] = eqRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, 2);
  assert.equal(res, undefined);
});

it('Should match the equal sign', () => {
  const data = 'a  = b';
  const [isValid, nextPos, res] = eqRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, 5);
  assert.equal(res, undefined);
});
