import {
  type DataRef,
  RepetitionRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import NameCharRule from './NameCharRule.js';

/**
 * Any mixture of name characters.
 * See https://www.w3.org/TR/xml/#NT-Nmtoken
 */
class NmtokenRule implements Rule<string> {
  private readonly rule: Rule<string[]>;

  public constructor() {
    const nameCharRule = new NameCharRule(); // NameChar
    this.rule = new RepetitionRule(nameCharRule, 1); // (NameChar)+
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, `${res.join('')}`];
  }
}

export default NmtokenRule;
