import { it } from 'node:test';
import assert from 'node:assert';
import PITargetRule from './PITargetRule.js';

const piTargetRule = new PITargetRule();

it('Should not match because the xml literal is not allowed', () => {
  const data = '_xml';
  const [isValid, nextPos] = piTargetRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should match the PI target', () => {
  const data = '_xml-stylesheet';
  const [isValid, nextPos, res] = piTargetRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'xml-stylesheet');
});
