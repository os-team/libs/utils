import { it } from 'node:test';
import assert from 'node:assert';
import EntityValueRule from './EntityValueRule.js';

const entityValueRule = new EntityValueRule();

it('Should not match because the first quotation mark was skipped', () => {
  const data = '_&a;"';
  const [isValid, nextPos] = entityValueRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should not match because the second quotation mark was skipped', () => {
  const data = '_"&a;';
  const [isValid, nextPos] = entityValueRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should match the reference in quotes', () => {
  const data = '_"abc&a;"';
  const [isValid, nextPos, res] = entityValueRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'abc&a;');
});

it('Should match the reference in apostrophes', () => {
  const data = "_'abc&a;'";
  const [isValid, nextPos, res] = entityValueRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'abc&a;');
});
