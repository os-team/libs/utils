import { it } from 'node:test';
import assert from 'node:assert';
import VersionNumRule from './VersionNumRule.js';

const versionNumRule = new VersionNumRule();

it('Should not match because the prefix is incorrect', () => {
  const data = '_2.0';
  const [isValid, nextPos] = versionNumRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should match the version number', () => {
  const data = '_1.1';
  const [isValid, nextPos, res] = versionNumRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, '1.1');
});
