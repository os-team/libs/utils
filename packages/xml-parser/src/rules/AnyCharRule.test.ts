import { it } from 'node:test';
import assert from 'node:assert';
import CharRule from './CharRule.js';
import AnyCharRule from './AnyCharRule.js';

const charRule = new CharRule();
const anyCharRule = new AnyCharRule(charRule, ['one', 'two']);

it('Should match a string', () => {
  const data = '_abcontw';
  const [isValid, nextPos, res] = anyCharRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'abcontw');
});

it('Should match a string up to one of the sequences', () => {
  const data = '_abctwo_';
  const [isValid, nextPos, res] = anyCharRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, 4);
  assert.equal(res, 'abc');
});

it('Should match an empty string', () => {
  const data = '_one';
  const [isValid, nextPos, res] = anyCharRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, 1);
  assert.equal(res, '');
});
