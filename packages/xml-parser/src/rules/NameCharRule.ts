import type { CharRuleConfig } from '@os-team/lexical-rules';
import NameStartCharRule from './NameStartCharRule.js';

/**
 * Any character of a name except the first one.
 * See https://www.w3.org/TR/xml/#NT-NameChar
 */
class NameCharRule extends NameStartCharRule {
  public constructor(config: CharRuleConfig = {}) {
    const { allowed = [], disallowed = [] } = config;
    super({
      allowed: [
        '-',
        '.',
        ['0', '9'],
        '\xB7',
        ['\u0300', '\u036F'],
        ['\u203F', '\u2040'],
        ...allowed,
      ],
      disallowed,
    }); // ":" | [A-Z] | "_" | [a-z] | [#xC0-#xD6] | [#xD8-#xF6] | [#xF8-#x2FF] | [#x370-#x37D] | [#x37F-#x1FFF] | [#x200C-#x200D] | [#x2070-#x218F] | [#x2C00-#x2FEF] | [#x3001-#xD7FF] | [#xF900-#xFDCF] | [#xFDF0-#xFFFD] | [#x10000-#xEFFFF]
  }
}

export default NameCharRule;
