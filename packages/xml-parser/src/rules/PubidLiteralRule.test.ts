import { it } from 'node:test';
import assert from 'node:assert';
import PubidLiteralRule from './PubidLiteralRule.js';

const pubidLiteralRule = new PubidLiteralRule();

it('Should not match because the first quotation mark was skipped', () => {
  const data = '_abc"';
  const [isValid, nextPos] = pubidLiteralRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should not match because the second quotation mark was skipped', () => {
  const data = '_"abc';
  const [isValid, nextPos] = pubidLiteralRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should match the literal in quotes', () => {
  const data = '_"abc"';
  const [isValid, nextPos, res] = pubidLiteralRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'abc');
});

it('Should match the literal in apostrophes', () => {
  const data = "_'abc'";
  const [isValid, nextPos, res] = pubidLiteralRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'abc');
});
