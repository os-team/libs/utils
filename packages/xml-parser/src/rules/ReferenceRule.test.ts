import { it } from 'node:test';
import assert from 'node:assert';
import ReferenceRule from './ReferenceRule.js';

const referenceRule = new ReferenceRule();

it('Should match the entity reference', () => {
  const data = '_&a;';
  const [isValid, nextPos, res] = referenceRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, '&a;');
});

it('Should match the char reference &#', () => {
  const data = '_&#1;';
  const [isValid, nextPos, res] = referenceRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, '&#1;');
});

it('Should match the char reference &#x', () => {
  const data = '_&#xa;';
  const [isValid, nextPos, res] = referenceRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, '&#xa;');
});
