import {
  CharRule,
  type DataRef,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';

class AnyCharRule implements Rule<string> {
  private readonly charRule: CharRule;

  private readonly upToSequences: string[];

  public constructor(charRule: CharRule, upToSequences: string[] = []) {
    this.charRule = charRule;
    this.upToSequences = upToSequences;
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    let position = pos;
    let buffer = '';

    while (position < ref.data.length) {
      const [isValid, nextPos, res] = this.charRule.test(ref, position);
      if (!isValid) return [true, nextPos, buffer];

      for (let i = 0; i < this.upToSequences.length; i += 1) {
        const seq = this.upToSequences[i];
        let j = 0;
        while (j < seq.length && position + j < ref.data.length) {
          if (ref.data[position + j] !== seq[j]) break;
          else if (j === seq.length - 1) return [true, position, buffer];
          j += 1;
        }
      }

      buffer = `${buffer}${res}`;
      position += 1;
    }

    return [true, position, buffer];
  }
}

export default AnyCharRule;
