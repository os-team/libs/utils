import {
  CharRule,
  type DataRef,
  OrRule,
  RepetitionRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import QuotedRule from './QuotedRule.js';

/**
 * See https://www.w3.org/TR/xml/#NT-SystemLiteral
 */
class SystemLiteralRule implements Rule<string> {
  private readonly rule: Rule<string[]>;

  public constructor() {
    const charExceptQuot = new CharRule({ disallowed: ['"'] }); // [^"]
    const anyCharExceptQuot = new RepetitionRule(charExceptQuot, 0); // [^"]*
    const quotedAnyCharExceptQuot = new QuotedRule(anyCharExceptQuot, '"'); // '"' [^"]* '"'
    const charExceptApos = new CharRule({ disallowed: ["'"] }); // [^']
    const anyCharExceptApos = new RepetitionRule(charExceptApos, 0); // [^']*
    const quotedAnyCharExceptApos = new QuotedRule(anyCharExceptApos, "'"); // "'" [^']* "'"
    this.rule = new OrRule([quotedAnyCharExceptQuot, quotedAnyCharExceptApos]); // ('"' [^"]* '"') | ("'" [^']* "'")
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res.join('')];
  }
}

export default SystemLiteralRule;
