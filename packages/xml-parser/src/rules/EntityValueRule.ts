import {
  type DataRef,
  OrRule,
  RepetitionRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import CharRule from './CharRule.js';
import PeReferenceRule from './PeReferenceRule.js';
import ReferenceRule from './ReferenceRule.js';
import QuotedRule from './QuotedRule.js';

/**
 * The content of internal entities.
 * See https://www.w3.org/TR/xml/#NT-EntityValue
 */
class EntityValueRule implements Rule<string> {
  private readonly rule: Rule<string[]>;

  public constructor() {
    const charWithoutQuotRule = new CharRule({ disallowed: ['%', '&', '"'] }); // [^%&"]
    const charWithoutAposRule = new CharRule({ disallowed: ['%', '&', "'"] }); // [^%&']
    const peReferenceRule = new PeReferenceRule(); // PEReference
    const referenceRule = new ReferenceRule(); // Reference
    const quotRule = new OrRule([
      charWithoutQuotRule,
      peReferenceRule,
      referenceRule,
    ]); // [^%&"] | PEReference | Reference
    const aposRule = new OrRule([
      charWithoutAposRule,
      peReferenceRule,
      referenceRule,
    ]); // [^%&'] | PEReference | Reference
    const anyQuotRule = new RepetitionRule(quotRule, 0); // ([^%&"] | PEReference | Reference)*
    const anyAposRule = new RepetitionRule(aposRule, 0); // ([^%&'] | PEReference | Reference)*
    const quotedAnyQuotRule = new QuotedRule(anyQuotRule, '"'); // '"' ([^%&"] | PEReference | Reference)* '"'
    const quotedAnyAposRule = new QuotedRule(anyAposRule, "'"); // "'" ([^%&'] | PEReference | Reference)* "'"
    this.rule = new OrRule([quotedAnyQuotRule, quotedAnyAposRule]); // '"' ([^%&"] | PEReference | Reference)* '"' |  "'" ([^%&'] | PEReference | Reference)* "'"
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res.join('')];
  }
}

export default EntityValueRule;
