import {
  type DataRef,
  LiteralRule,
  OrRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';

/**
 * The 'yes' or 'no' literal.
 * Used as the standalone value in the XML declaration.
 */
class YesOrNoRule implements Rule<boolean> {
  private readonly rule: Rule<string>;

  public constructor() {
    const yesRule = new LiteralRule('yes'); // 'yes'
    const noRule = new LiteralRule('no'); // 'no'
    this.rule = new OrRule([yesRule, noRule]); // 'yes' | 'no'
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<boolean> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res === 'yes'];
  }
}

export default YesOrNoRule;
