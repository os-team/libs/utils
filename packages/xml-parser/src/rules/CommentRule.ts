import {
  AndRule,
  LiteralRule,
  OrRule,
  RepetitionRule,
} from '@os-team/lexical-rules';
import CharRule from './CharRule.js';
import AuxiliaryRule from './AuxiliaryRule.js';

/**
 * The comment.
 * See https://www.w3.org/TR/xml/#NT-Comment
 */
class CommentRule extends AuxiliaryRule {
  public constructor() {
    super();
    const prefixRule = new LiteralRule('<!--'); // '<!--'
    const charRule = new CharRule({ disallowed: ['-'] }); // Char - '-'
    const dashRule = new LiteralRule('-'); // '-'
    const dashAndCharRule = new AndRule([dashRule, charRule]); // '-' (Char - '-')
    const commentCharRule = new OrRule([charRule, dashAndCharRule]); // (Char - '-') | ('-' (Char - '-'))
    const anyCommentCharRule = new RepetitionRule(commentCharRule, 0); // ((Char - '-') | ('-' (Char - '-')))*
    const suffixRule = new LiteralRule('-->'); // '-->'
    this.rule = new AndRule([prefixRule, anyCommentCharRule, suffixRule]); // '<!--' ((Char - '-') | ('-' (Char - '-')))* '-->'
  }
}

export default CommentRule;
