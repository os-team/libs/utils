import { it } from 'node:test';
import assert from 'node:assert';
import NameStartCharRule from './NameStartCharRule.js';

const nameStartCharRule = new NameStartCharRule();

it('Should not match because the char is incorrect', () => {
  const data = '_-';
  const [isValid, nextPos] = nameStartCharRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should match a char', () => {
  const data = '_a';
  const [isValid, nextPos, res] = nameStartCharRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, 2);
  assert.equal(res, 'a');
});
