import {
  CharRule as BaseCharRule,
  type CharRuleConfig,
} from '@os-team/lexical-rules';

/**
 * The first character of a name.
 * See https://www.w3.org/TR/xml/#NT-NameStartChar
 */
class NameStartCharRule extends BaseCharRule {
  public constructor(config: CharRuleConfig = {}) {
    const { allowed = [], disallowed = [] } = config;
    super({
      allowed: [
        ':',
        ['A', 'Z'],
        '_',
        ['a', 'z'],
        ['\xC0', '\xD6'],
        ['\xD8', '\xF6'],
        ['\u00F8', '\u02FF'],
        ['\u0370', '\u037D'],
        ['\u037F', '\u1FFF'],
        ['\u200C', '\u200D'],
        ['\u2070', '\u218F'],
        ['\u2C00', '\u2FEF'],
        ['\u3001', '\uD7FF'],
        ['\uF900', '\uFDCF'],
        ['\uFDF0', '\uFFFD'],
        ['\u{10000}', '\u{EFFFF}'],
        ...allowed,
      ],
      disallowed,
    }); // ":" | [A-Z] | "_" | [a-z] | [#xC0-#xD6] | [#xD8-#xF6] | [#xF8-#x2FF] | [#x370-#x37D] | [#x37F-#x1FFF] | [#x200C-#x200D] | [#x2070-#x218F] | [#x2C00-#x2FEF] | [#x3001-#xD7FF] | [#xF900-#xFDCF] | [#xFDF0-#xFFFD] | [#x10000-#xEFFFF]
  }
}

export default NameStartCharRule;
