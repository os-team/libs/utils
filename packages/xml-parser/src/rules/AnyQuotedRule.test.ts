import { it } from 'node:test';
import assert from 'node:assert';
import { LiteralRule } from '@os-team/lexical-rules';
import AnyQuotedRule from './AnyQuotedRule.js';

const literalRule = new LiteralRule('abc');
const anyQuotedRule = new AnyQuotedRule(literalRule);

it('Should match the literal in quotes', () => {
  const data = '_"abc"';
  const [isValid, nextPos, res] = anyQuotedRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'abc');
});

it('Should match the literal in apostrophes', () => {
  const data = "_'abc'";
  const [isValid, nextPos, res] = anyQuotedRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'abc');
});
