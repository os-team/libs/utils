import { it } from 'node:test';
import assert from 'node:assert';
import CharDataRule from './CharDataRule.js';

const charDataRule = new CharDataRule();

it('Should match the character data', () => {
  const data = '_abc';
  const [isValid, nextPos, res] = charDataRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'abc');
});

it('Should match the character data before the < character', () => {
  const data = '_ab<c';
  const [isValid, nextPos, res] = charDataRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, 3);
  assert.equal(res, 'ab');
});

it('Should match the character data before the & character', () => {
  const data = '_ab&c';
  const [isValid, nextPos, res] = charDataRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, 3);
  assert.equal(res, 'ab');
});

it('Should match the character data before the ]]> sequence', () => {
  const data = '_ab]]>c';
  const [isValid, nextPos, res] = charDataRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, 3);
  assert.equal(res, 'ab');
});
