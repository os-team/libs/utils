import {
  AndRule,
  type DataRef,
  LiteralRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';

/**
 * The string in the specified quotes.
 */
class QuotedRule<T> implements Rule<T> {
  private readonly rule: Rule<[string, T, string]>;

  public constructor(rule: Rule<T>, type: '"' | "'") {
    const quotRule = new LiteralRule(type);
    this.rule = new AndRule([quotRule, rule, quotRule]);
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<T> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res[1]];
  }
}

export default QuotedRule;
