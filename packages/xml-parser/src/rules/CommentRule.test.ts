import { it } from 'node:test';
import assert from 'node:assert';
import CommentRule from './CommentRule.js';

const commentRule = new CommentRule();

it('Should not match because the prefix is incorrect', () => {
  const data = '_<!- Comment -->';
  const [isValid, nextPos] = commentRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 4);
});

it('Should not match because the suffix is incorrect', () => {
  const data = '_<!-- Comment --';
  const [isValid, nextPos] = commentRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, data.length);
});

it('Should not match because the comment ends with a dash', () => {
  const data = '_<!-- Comment --->';
  const [isValid, nextPos] = commentRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 16);
});

it('Should not match because the comment contains a doubled dash', () => {
  const data = '_<!-- Comm--ent -->';
  const [isValid, nextPos] = commentRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 12);
});

it('Should match the comment', () => {
  const data = '_<!-- Comm-ent -->';
  const [isValid, nextPos, res] = commentRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, undefined);
});
