import {
  CharRule,
  type DataRef,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import AnyCharRule from './AnyCharRule.js';

/**
 * The character data.
 * See https://www.w3.org/TR/xml/#NT-CharData
 */
class CharDataRule implements Rule<string> {
  private readonly rule: Rule<string>;

  public constructor() {
    const charRule = new CharRule({ disallowed: ['<', '&'] }); // [^<&]
    this.rule = new AnyCharRule(charRule, [']]>']); // [^<&]* - ([^<&]* ']]>' [^<&]*)
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res];
  }
}

export default CharDataRule;
