import {
  AndRule,
  type DataRef,
  RepetitionRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import NameStartCharRule from './NameStartCharRule.js';
import NameCharRule from './NameCharRule.js';

/**
 * The name.
 * See https://www.w3.org/TR/xml/#NT-Name
 */
class NameRule implements Rule<string> {
  private readonly rule: Rule<[string, string[]]>;

  public constructor() {
    const nameStartCharRule = new NameStartCharRule(); // NameStartChar
    const nameCharRule = new NameCharRule(); // NameChar
    const anyNameCharRule = new RepetitionRule(nameCharRule, 0); // (NameChar)*
    this.rule = new AndRule([nameStartCharRule, anyNameCharRule]); // NameStartChar (NameChar)*
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, `${res[0]}${res[1].join('')}`];
  }
}

export default NameRule;
