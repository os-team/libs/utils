import { it } from 'node:test';
import assert from 'node:assert';
import NameCharRule from './NameCharRule.js';

const nameCharRule = new NameCharRule();

it('Should not match because the char is incorrect', () => {
  const data = '_^';
  const [isValid, nextPos] = nameCharRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should match a char', () => {
  const data = '_-';
  const [isValid, nextPos, res] = nameCharRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, 2);
  assert.equal(res, '-');
});
