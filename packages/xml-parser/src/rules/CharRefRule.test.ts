import { describe, it } from 'node:test';
import assert from 'node:assert';
import CharRefRule from './CharRefRule.js';

const charRefRule = new CharRefRule();

describe('&#', () => {
  it('Should not match because the prefix is incorrect', () => {
    const data = '_&1;';
    const [isValid, nextPos] = charRefRule.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should not match because the suffix was skipped', () => {
    const data = '_&#1';
    const [isValid, nextPos] = charRefRule.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should not match because the code is incorrect', () => {
    const data = '_&#a;';
    const [isValid, nextPos] = charRefRule.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should match the reference to the character', () => {
    const data = '_&#1;';
    const [isValid, nextPos, res] = charRefRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, '&#1;');
  });
});

describe('&#x', () => {
  it('Should not match because the suffix was skipped', () => {
    const data = '_&#xa';
    const [isValid, nextPos] = charRefRule.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should not match because the code is incorrect', () => {
    const data = '_&#xg;';
    const [isValid, nextPos] = charRefRule.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should match the reference to the character', () => {
    const data = '_&#xa;';
    const [isValid, nextPos, res] = charRefRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, '&#xa;');
  });
});
