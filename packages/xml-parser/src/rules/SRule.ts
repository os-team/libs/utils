import { CharRule, RepetitionRule } from '@os-team/lexical-rules';
import AuxiliaryRule from './AuxiliaryRule.js';

export const whiteSpaceChars = ['\x20', '\x09', '\x0D', '\x0A'];

/**
 * White spaces.
 * See https://www.w3.org/TR/xml/#NT-S
 */
class SRule extends AuxiliaryRule {
  public constructor(type: '*' | '+') {
    super();
    const whiteSpaceRule = new CharRule({
      allowed: whiteSpaceChars,
    });
    this.rule = new RepetitionRule(whiteSpaceRule, type === '*' ? 0 : 1);
  }
}

export default SRule;
