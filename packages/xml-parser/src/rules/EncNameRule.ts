import {
  AndRule,
  type CharList,
  CharRule,
  type DataRef,
  RepetitionRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';

const startChars: CharList = [
  ['A', 'Z'],
  ['a', 'z'],
];

/**
 * The encoding name.
 * See https://www.w3.org/TR/xml/#NT-EncName
 */
class EncNameRule implements Rule<string> {
  private readonly rule: Rule<[string, string[]]>;

  public constructor() {
    const firstCharRule = new CharRule({ allowed: startChars }); // [A-Za-z]
    const secondCharRule = new CharRule({
      allowed: [...startChars, ['0', '9'], '.', '_', '-'],
    }); // [A-Za-z0-9._] | '-'
    const anySecondCharRule = new RepetitionRule(secondCharRule, 0); // ([A-Za-z0-9._] | '-')*
    this.rule = new AndRule([firstCharRule, anySecondCharRule]); // [A-Za-z] ([A-Za-z0-9._] | '-')*
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, `${res[0]}${res[1].join('')}`];
  }
}

export default EncNameRule;
