import { it } from 'node:test';
import assert from 'node:assert';
import QuantifierRule from './QuantifierRule.js';

const quantifierRule = new QuantifierRule();

it('Should match, although the quantifier is not specified', () => {
  const data = '_a';
  const [isValid, nextPos, res] = quantifierRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, 1);
  assert.equal(res, undefined);
});

it('Should match the ? quantifier', () => {
  const data = '_?';
  const [isValid, nextPos, res] = quantifierRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, '?');
});

it('Should match the * quantifier', () => {
  const data = '_*';
  const [isValid, nextPos, res] = quantifierRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, '*');
});

it('Should match the + quantifier', () => {
  const data = '_+';
  const [isValid, nextPos, res] = quantifierRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, '+');
});
