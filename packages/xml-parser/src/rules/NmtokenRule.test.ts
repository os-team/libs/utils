import { it } from 'node:test';
import assert from 'node:assert';
import NmtokenRule from './NmtokenRule.js';

const nmtokenRule = new NmtokenRule();

it('Should not match because the first character is invalid', () => {
  const data = '_^';
  const [isValid, nextPos] = nmtokenRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should match the single-character name', () => {
  const data = '_-';
  const [isValid, nextPos, res] = nmtokenRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, '-');
});

it('Should match the name', () => {
  const data = '_-a-';
  const [isValid, nextPos, res] = nmtokenRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, '-a-');
});
