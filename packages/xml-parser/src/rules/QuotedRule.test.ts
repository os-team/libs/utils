import { it } from 'node:test';
import assert from 'node:assert';
import { LiteralRule } from '@os-team/lexical-rules';
import QuotedRule from './QuotedRule.js';

const literalRule = new LiteralRule('abc');
const quotedRule = new QuotedRule(literalRule, '"');
const aposQuotedRule = new QuotedRule(literalRule, "'");

it('Should not match because the first quotation mark was skipped', () => {
  const data = '_abc"';
  const [isValid, nextPos] = quotedRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should not match because the second quotation mark was skipped', () => {
  const data = '_"abc';
  const [isValid, nextPos] = quotedRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, data.length);
});

it('Should match the literal in quotes', () => {
  const data = '_"abc"';
  const [isValid, nextPos, res] = quotedRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'abc');
});

it('Should match the literal in apostrophes', () => {
  const data = "_'abc'";
  const [isValid, nextPos, res] = aposQuotedRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'abc');
});
