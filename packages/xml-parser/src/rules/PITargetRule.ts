import type { DataRef, Rule, RuleTestResponse } from '@os-team/lexical-rules';
import NameRule from './NameRule.js';

/**
 * The name of the processing instruction.
 * See https://www.w3.org/TR/xml/#NT-PITarget
 */
class PITargetRule implements Rule<string> {
  private readonly rule: Rule<string>;

  public constructor() {
    this.rule = new NameRule();
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    if (res.toLowerCase() === 'xml') return [false, pos];
    return [true, nextPos, res];
  }
}

export default PITargetRule;
