import { it } from 'node:test';
import assert from 'node:assert';
import DeclSepRule from './DeclSepRule.js';

const declSepRule = new DeclSepRule();

it('Should not match because the parameter-entity reference is incorrect', () => {
  const data = '_abc';
  const [isValid, nextPos] = declSepRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should match the parameter-entity reference', () => {
  const data = '_%a;';
  const [isValid, nextPos, res] = declSepRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, '%a;');
});

it('Should match white spaces', () => {
  const data = '_\t \n';
  const [isValid, nextPos, res] = declSepRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, undefined);
});
