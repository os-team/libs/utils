import { describe, test } from 'node:test';
import assert from 'node:assert';
import SRule from './SRule.js';

const anySRule = new SRule('*');
const oneOrMoreSRule = new SRule('+');

describe('*', () => {
  test('No matches', () => {
    const data = '_a';
    const [isValid, nextPos, res] = anySRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, 1);
    assert.equal(res, undefined);
  });

  test('Two matches', () => {
    const data = '_  a';
    const [isValid, nextPos, res] = anySRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, 3);
    assert.equal(res, undefined);
  });
});

describe('+', () => {
  test('No matches', () => {
    const data = '_a';
    const [isValid, nextPos] = oneOrMoreSRule.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  test('Two matches', () => {
    const data = '_  a';
    const [isValid, nextPos, res] = oneOrMoreSRule.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, 3);
    assert.equal(res, undefined);
  });
});
