import {
  type DataRef,
  OrRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import PeReferenceRule from './PeReferenceRule.js';
import SRule from './SRule.js';

/**
 * See https://www.w3.org/TR/xml/#NT-DeclSep
 */
class DeclSepRule implements Rule<string | undefined> {
  private readonly rule: Rule<string | undefined>;

  public constructor() {
    const peReferenceRule = new PeReferenceRule(); // PEReference
    const sRule = new SRule('+'); // S
    this.rule = new OrRule([peReferenceRule, sRule]); // PEReference | S
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string | undefined> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid) return [false, nextPos];
    return [true, nextPos, res];
  }
}

export default DeclSepRule;
