import {
  CharRule as BaseCharRule,
  type CharRuleConfig,
} from '@os-team/lexical-rules';

/**
 * See https://www.w3.org/TR/xml/#NT-PubidChar
 */
class PubidCharRule extends BaseCharRule {
  public constructor(config: CharRuleConfig = {}) {
    const { allowed = [], disallowed = [] } = config;
    super({
      allowed: [
        '\x20',
        '\x0D',
        '\x0A',
        ['a', 'z'],
        ['A', 'Z'],
        ['0', '9'],
        '-',
        "'",
        '(',
        ')',
        '+',
        ',',
        '.',
        '/',
        ':',
        '=',
        '?',
        ';',
        '!',
        '*',
        '#',
        '@',
        '$',
        '_',
        '%',
        ...allowed,
      ],
      disallowed,
    }); // #x20 | #xD | #xA | [a-zA-Z0-9] | [-'()+,./:=?;!*#@$_%]
  }
}

export default PubidCharRule;
