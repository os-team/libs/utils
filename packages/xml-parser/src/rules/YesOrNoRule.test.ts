import { it } from 'node:test';
import assert from 'node:assert';
import YesOrNoRule from './YesOrNoRule.js';

const yesOrNoRule = new YesOrNoRule();

it('Should not match the incorrect literal', () => {
  const data = '_yee';
  const [isValid, nextPos] = yesOrNoRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should match the yes literal', () => {
  const data = '_yes';
  const [isValid, nextPos, res] = yesOrNoRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, true);
});

it('Should match the no literal', () => {
  const data = '_no';
  const [isValid, nextPos, res] = yesOrNoRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, false);
});
