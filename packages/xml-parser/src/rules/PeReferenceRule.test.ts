import { it } from 'node:test';
import assert from 'node:assert';
import PeReferenceRule from './PeReferenceRule.js';

const peReferenceRule = new PeReferenceRule();

it('Should not match because the prefix was skipped', () => {
  const data = '_a;';
  const [isValid, nextPos] = peReferenceRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should not match because the suffix was skipped', () => {
  const data = '_%a';
  const [isValid, nextPos] = peReferenceRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, data.length);
});

it('Should not match because the name is incorrect', () => {
  const data = '_%-;';
  const [isValid, nextPos] = peReferenceRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 2);
});

it('Should match the PE reference', () => {
  const data = '_%a;';
  const [isValid, nextPos, res] = peReferenceRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, '%a;');
});
