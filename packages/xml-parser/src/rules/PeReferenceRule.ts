import {
  AndRule,
  type DataRef,
  LiteralRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import NameRule from './NameRule.js';

/**
 * The parameter-entity reference.
 * See https://www.w3.org/TR/xml/#NT-PEReference
 */
class PeReferenceRule implements Rule<string> {
  private readonly rule: Rule<[string, string, string]>;

  public constructor() {
    const prefixRule = new LiteralRule('%'); // '%'
    const nameRule = new NameRule(); // Name
    const suffixRule = new LiteralRule(';'); // ';'
    this.rule = new AndRule([prefixRule, nameRule, suffixRule]); // '%' Name ';'
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res.join('')];
  }
}

export default PeReferenceRule;
