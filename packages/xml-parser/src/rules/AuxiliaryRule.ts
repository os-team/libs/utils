import type { DataRef, Rule, RuleTestResponse } from '@os-team/lexical-rules';

abstract class AuxiliaryRule implements Rule<undefined> {
  protected rule: Rule<string[]> | undefined;

  public test(ref: DataRef, pos: number): RuleTestResponse<undefined> {
    if (!this.rule) return [false, pos];
    const [isValid, nextPos] = this.rule.test(ref, pos);
    if (!isValid) return [false, nextPos];
    return [true, nextPos, undefined];
  }
}

export default AuxiliaryRule;
