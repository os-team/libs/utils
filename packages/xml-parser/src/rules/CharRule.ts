import {
  CharRule as BaseCharRule,
  type CharRuleConfig,
} from '@os-team/lexical-rules';

/**
 * An atomic unit of text.
 * See https://www.w3.org/TR/xml/#NT-Char
 */
class CharRule extends BaseCharRule {
  public constructor(config: CharRuleConfig = {}) {
    const { allowed = [], disallowed = [] } = config;
    super({
      allowed: [
        '\x09',
        '\x0A',
        '\x0D',
        ['\u0020', '\uD7FF'],
        ['\uE000', '\uFFFD'],
        ['\u{10000}', '\u{10FFFF}'],
        ...allowed,
      ],
      disallowed,
    }); // #x9 | #xA | #xD | [#x20-#xD7FF] | [#xE000-#xFFFD] | [#x10000-#x10FFFF]
  }
}

export default CharRule;
