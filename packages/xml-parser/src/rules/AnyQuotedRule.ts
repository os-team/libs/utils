import {
  type DataRef,
  OrRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import QuotedRule from './QuotedRule.js';

/**
 * The string in any quotes.
 */
class AnyQuotedRule<T> implements Rule<T> {
  private readonly rule: Rule<T>;

  public constructor(rule: Rule<T>) {
    const quotedRule = new QuotedRule(rule, '"');
    const aposQuotedRule = new QuotedRule(rule, "'");
    this.rule = new OrRule([quotedRule, aposQuotedRule]);
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<T> {
    return this.rule.test(ref, pos);
  }
}

export default AnyQuotedRule;
