import { it } from 'node:test';
import assert from 'node:assert';
import NumberRule from './NumberRule.js';

const numberRule = new NumberRule();

it('Should not match because there is no a digit', () => {
  const data = '_a';
  const [isValid, nextPos] = numberRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should match a digit', () => {
  const data = '_0';
  const [isValid, nextPos, res] = numberRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 0);
});

it('Should match a number', () => {
  const data = '_12';
  const [isValid, nextPos, res] = numberRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 12);
});
