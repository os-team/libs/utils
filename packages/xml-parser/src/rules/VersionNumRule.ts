import {
  AndRule,
  type DataRef,
  LiteralRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import NumberRule from './NumberRule.js';

/**
 * The version number in the XML declaration.
 * See https://www.w3.org/TR/xml/#NT-VersionNum
 */
class VersionNumRule implements Rule<string> {
  private readonly rule: Rule<[string, number]>;

  public constructor() {
    const majorNumberRule = new LiteralRule('1.'); // '1.'
    const minorNumberRule = new NumberRule(); // [0-9]+
    this.rule = new AndRule([majorNumberRule, minorNumberRule]); // '1.' [0-9]+
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res.join('')];
  }
}

export default VersionNumRule;
