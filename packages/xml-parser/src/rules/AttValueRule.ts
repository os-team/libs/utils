import {
  type DataRef,
  OrRule,
  RepetitionRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import CharRule from './CharRule.js';
import ReferenceRule from './ReferenceRule.js';
import QuotedRule from './QuotedRule.js';

/**
 * The value of attributes.
 * See https://www.w3.org/TR/xml/#NT-AttValue
 */
class AttValueRule implements Rule<string> {
  private readonly rule: Rule<string[]>;

  public constructor() {
    const charWithoutQuotRule = new CharRule({ disallowed: ['<', '&', '"'] }); // [^<&"]
    const charWithoutAposRule = new CharRule({ disallowed: ['<', '&', "'"] }); // [^<&']
    const referenceRule = new ReferenceRule(); // Reference
    const quotRule = new OrRule([charWithoutQuotRule, referenceRule]); // [^<&"] | Reference
    const aposRule = new OrRule([charWithoutAposRule, referenceRule]); // [^<&'] | Reference
    const anyQuotRule = new RepetitionRule(quotRule, 0); // ([^<&"] | Reference)*
    const anyAposRule = new RepetitionRule(aposRule, 0); // ([^<&'] | Reference)*
    const quotedAnyQuotRule = new QuotedRule(anyQuotRule, '"'); // '"' ([^<&"] | Reference)* '"'
    const quotedAnyAposRule = new QuotedRule(anyAposRule, "'"); // "'" ([^<&'] | Reference)* "'"
    this.rule = new OrRule([quotedAnyQuotRule, quotedAnyAposRule]); // '"' ([^<&"] | Reference)* '"' |  "'" ([^<&'] | Reference)* "'"
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res.join('')];
  }
}

export default AttValueRule;
