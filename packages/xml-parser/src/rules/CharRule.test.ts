import { it } from 'node:test';
import assert from 'node:assert';
import CharRule from './CharRule.js';

const charRule = new CharRule();

it('Should not match because the char is incorrect', () => {
  const data = '_\x0B';
  const [isValid, nextPos] = charRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should match a char', () => {
  const data = '_a';
  const [isValid, nextPos, res] = charRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, 2);
  assert.equal(res, 'a');
});
