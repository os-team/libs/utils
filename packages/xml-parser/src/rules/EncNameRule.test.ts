import { it } from 'node:test';
import assert from 'node:assert';
import EncNameRule from './EncNameRule.js';

const encNameRule = new EncNameRule();

it('Should not match because the first character is invalid', () => {
  const data = '_1';
  const [isValid, nextPos] = encNameRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should match the single-character encoding name', () => {
  const data = '_a';
  const [isValid, nextPos, res] = encNameRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'a');
});

it('Should match the encoding name', () => {
  const data = '_UTF-8';
  const [isValid, nextPos, res] = encNameRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'UTF-8');
});
