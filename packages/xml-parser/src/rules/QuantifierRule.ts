import {
  CharRule,
  type DataRef,
  MaybeRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';

export type Quantifier = '?' | '*' | '+' | undefined;

class QuantifierRule implements Rule<Quantifier> {
  private readonly rule: Rule<string | undefined>;

  public constructor() {
    const charRule = new CharRule({ allowed: ['?', '*', '+'] }); // '?' | '*' | '+'
    this.rule = new MaybeRule(charRule); // ('?' | '*' | '+')?
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<Quantifier> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid) return [false, nextPos];
    return [true, nextPos, res as Quantifier];
  }
}

export default QuantifierRule;
