import {
  type DataRef,
  OrRule,
  RepetitionRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import PubidCharRule from './PubidCharRule.js';
import QuotedRule from './QuotedRule.js';

/**
 * See https://www.w3.org/TR/xml/#NT-PubidLiteral
 */
class PubidLiteralRule implements Rule<string> {
  private readonly rule: Rule<string[]>;

  public constructor() {
    const pubidChar = new PubidCharRule(); // PubidChar
    const anyPubidChar = new RepetitionRule(pubidChar, 0); // PubidChar*
    const quotedAnyPubidChar = new QuotedRule(anyPubidChar, '"'); // '"' PubidChar* '"'
    const pubidCharWithoutApos = new PubidCharRule({ disallowed: ["'"] }); // PubidChar - "'"
    const anyPubidCharWithoutApos = new RepetitionRule(pubidCharWithoutApos, 0); // (PubidChar - "'")*
    const quotedAnyPubidCharWithoutApos = new QuotedRule(
      anyPubidCharWithoutApos,
      "'"
    ); // "'" (PubidChar - "'")* "'"
    this.rule = new OrRule([quotedAnyPubidChar, quotedAnyPubidCharWithoutApos]); // '"' PubidChar* '"' | "'" (PubidChar - "'")* "'"
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, `${res.join('')}`];
  }
}

export default PubidLiteralRule;
