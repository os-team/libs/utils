import { it } from 'node:test';
import assert from 'node:assert';
import AttValueRule from './AttValueRule.js';

const attValueRule = new AttValueRule();

it('Should not match because the first quotation mark was skipped', () => {
  const data = '_&a;"';
  const [isValid, nextPos] = attValueRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should not match because the second quotation mark was skipped', () => {
  const data = '_"&a;';
  const [isValid, nextPos] = attValueRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should not match because a string contains the "<" character', () => {
  const data = '_"ab<c"';
  const [isValid, nextPos] = attValueRule.test({ data }, 1);
  assert.equal(isValid, false);
  assert.equal(nextPos, 1);
});

it('Should match the reference in quotes', () => {
  const data = '_"abc&a;"';
  const [isValid, nextPos, res] = attValueRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'abc&a;');
});

it('Should match the reference in apostrophes', () => {
  const data = "_'abc&a;'";
  const [isValid, nextPos, res] = attValueRule.test({ data }, 1);
  assert.equal(isValid, true);
  assert.equal(nextPos, data.length);
  assert.equal(res, 'abc&a;');
});
