import { it } from 'node:test';
import assert from 'node:assert';
import { buildXml, parseXml, type Document } from './index.js';

it('Should build and parse an XML', () => {
  const data: Document = {
    '#prolog': {
      xml: {
        version: '1.0',
        encoding: 'UTF-8',
      },
    },
    root: {
      first: 'text',
      second: {
        '#attrs': { id: '1' },
        '#content': 'text',
      },
    },
  };
  const xml = buildXml(data);
  const parsedData = parseXml(xml, { include: 'ALL' });
  assert.deepStrictEqual(parsedData, data);
});

it('Should make the item node an array', () => {
  const data = '<root><item>item1</item></root>';
  const document = parseXml(data, { isArray: ['item'] });
  assert.deepStrictEqual(document, {
    root: {
      item: ['item1'],
    },
  });
});

it('Should transform tag names to camel case', () => {
  const data = '<Root><TagElement>text</TagElement></Root>';
  const document = parseXml(data, {
    tagName: (name) => name.replace(/^\w/, (c) => c.toLowerCase()),
  });
  assert.deepStrictEqual(document, {
    root: {
      tagElement: 'text',
    },
  });
});
