import {
  AndRule,
  type DataRef,
  LiteralRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import { type Parser } from './Parser.js';
import ContentSpecParser, { type ContentSpec } from './ContentSpecParser.js';
import SRule from '../rules/SRule.js';
import NameRule from '../rules/NameRule.js';

export interface ElementDecl {
  type: 'ELEMENT';
  name: string;
  content: ContentSpec;
}

/**
 * The element type declaration.
 * See https://www.w3.org/TR/xml/#NT-elementdecl
 */
class ElementDeclParser implements Parser<ElementDecl> {
  private readonly contentSpecParser: ContentSpecParser;

  private readonly rule: Rule<
    [string, undefined, string, undefined, ContentSpec, undefined, string]
  >;

  public constructor() {
    const prefixRule = new LiteralRule('<!ELEMENT');
    const sRule = new SRule('+'); // S
    const nameRule = new NameRule(); // Name
    this.contentSpecParser = new ContentSpecParser(); // contentspec
    const anySRule = new SRule('*'); // S?
    const suffixRule = new LiteralRule('>'); // '>'
    this.rule = new AndRule([
      prefixRule,
      sRule,
      nameRule,
      sRule,
      this.contentSpecParser,
      anySRule,
      suffixRule,
    ]); // '<!ELEMENT' S Name S contentspec S? '>'
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<ElementDecl> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    const [, , name, , content] = res;
    return [true, nextPos, { type: 'ELEMENT', name, content }];
  }

  public build(data: ElementDecl) {
    const content = this.contentSpecParser.build(data.content);
    return `<!ELEMENT ${data.name} ${content}>`;
  }
}

export default ElementDeclParser;
