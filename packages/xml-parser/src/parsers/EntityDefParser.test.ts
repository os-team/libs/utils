import { describe, it } from 'node:test';
import assert from 'node:assert';
import EntityDefParser from './EntityDefParser.js';

const entityDefParser = new EntityDefParser();

describe('test', () => {
  it('Should match the entity value', () => {
    const data = '_"abc"';
    const [isValid, nextPos, res] = entityDefParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, 'abc');
  });

  it('Should match the external ID without NData', () => {
    const data = '_SYSTEM "scheme.gif"';
    const [isValid, nextPos, res] = entityDefParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'SYSTEM_EXTERNAL_ID',
      systemValue: 'scheme.gif',
    });
  });

  it('Should match the external ID with NData', () => {
    const data = '_SYSTEM "scheme.gif" NDATA gif';
    const [isValid, nextPos, res] = entityDefParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'SYSTEM_EXTERNAL_ID',
      systemValue: 'scheme.gif',
      nData: 'gif',
    });
  });
});

describe('build', () => {
  it('Should return the entity value', () => {
    const res = entityDefParser.build('abc');
    assert.equal(res, '"abc"');
  });

  it('Should return the external ID without NData', () => {
    const res = entityDefParser.build({
      type: 'SYSTEM_EXTERNAL_ID',
      systemValue: 'scheme.gif',
    });
    assert.equal(res, 'SYSTEM "scheme.gif"');
  });

  it('Should return the external ID with NData', () => {
    const res = entityDefParser.build({
      type: 'SYSTEM_EXTERNAL_ID',
      systemValue: 'scheme.gif',
      nData: 'gif',
    });
    assert.equal(res, 'SYSTEM "scheme.gif" NDATA gif');
  });
});
