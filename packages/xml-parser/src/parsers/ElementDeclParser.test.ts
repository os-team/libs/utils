import { describe, it } from 'node:test';
import assert from 'node:assert';
import ElementDeclParser from './ElementDeclParser.js';

const elementDeclParser = new ElementDeclParser();

describe('test', () => {
  it('Should not match because the prefix is incorrect', () => {
    const data = '_<!ELEMEN p (#PCDATA|m|n)*>';
    const [isValid, nextPos] = elementDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 9);
  });

  it('Should not match because the suffix was skipped', () => {
    const data = '_<!ELEMENT p (#PCDATA|m|n)*';
    const [isValid, nextPos] = elementDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, data.length);
  });

  it('Should match the element type declaration', () => {
    const data = '_<!ELEMENT p (#PCDATA|m|n)*>';
    const [isValid, nextPos, res] = elementDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'ELEMENT',
      name: 'p',
      content: {
        type: 'MIXED',
        items: ['m', 'n'],
      },
    });
  });
});

describe('build', () => {
  it('Should return the element type declaration', () => {
    const res = elementDeclParser.build({
      type: 'ELEMENT',
      name: 'p',
      content: {
        type: 'MIXED',
        items: ['m', 'n'],
      },
    });
    assert.equal(res, '<!ELEMENT p (#PCDATA|m|n)*>');
  });
});
