import { describe, it } from 'node:test';
import assert from 'node:assert';
import EnumerationParser from './EnumerationParser.js';

const enumerationParser = new EnumerationParser();

describe('test', () => {
  it('Should not match because the opening parenthesis was skipped', () => {
    const data = '_one|two|three)';
    const [isValid, nextPos] = enumerationParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should not match because the closing parenthesis was skipped', () => {
    const data = '_(one|two|three';
    const [isValid, nextPos] = enumerationParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, data.length);
  });

  it('Should match the enumeration', () => {
    const data = '_(one | two|  three  )';
    const [isValid, nextPos, res] = enumerationParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'ENUMERATION',
      items: ['one', 'two', 'three'],
    });
  });
});

describe('build', () => {
  it('Should return the enumeration', () => {
    const res = enumerationParser.build({
      type: 'ENUMERATION',
      items: ['one', 'two', 'three'],
    });
    assert.equal(res, '(one|two|three)');
  });
});
