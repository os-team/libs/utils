import {
  AndRule,
  type DataRef,
  LiteralRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import SRule from '../rules/SRule.js';
import NameRule from '../rules/NameRule.js';
import type { Parser } from './Parser.js';
import PEDefParser, { type PEDef } from './PEDefParser.js';

export interface PEDecl {
  type: 'ENTITY';
  name: string;
  pe: PEDef;
}

/**
 * See https://www.w3.org/TR/xml/#NT-PEDecl
 */
class PEDeclParser implements Parser<PEDecl> {
  private readonly peDefParser: PEDefParser;

  private readonly rule: Rule<
    [
      string,
      undefined,
      string,
      undefined,
      string,
      undefined,
      PEDef,
      undefined,
      string,
    ]
  >;

  public constructor() {
    const prefixRule = new LiteralRule('<!ENTITY'); // '<!ENTITY'
    const sRule = new SRule('+'); // S
    const percentRule = new LiteralRule('%'); // '%'
    const nameRule = new NameRule(); // Name
    this.peDefParser = new PEDefParser(); // PEDef
    const anySRule = new SRule('*'); // S?
    const suffixRule = new LiteralRule('>'); // '>'
    this.rule = new AndRule([
      prefixRule,
      sRule,
      percentRule,
      sRule,
      nameRule,
      sRule,
      this.peDefParser,
      anySRule,
      suffixRule,
    ]); // '<!ENTITY' S '%' S Name S PEDef S? '>'
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<PEDecl> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    const [, , , , name, , pe] = res;
    return [true, nextPos, { type: 'ENTITY', name, pe }];
  }

  public build(data: PEDecl) {
    const pe = this.peDefParser.build(data.pe);
    return `<!ENTITY % ${data.name} ${pe}>`;
  }
}

export default PEDeclParser;
