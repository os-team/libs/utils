import { describe, it } from 'node:test';
import assert from 'node:assert';
import XMLDeclParser from './XMLDeclParser.js';

const xmlDeclParser = new XMLDeclParser();

describe('test', () => {
  it('Should not match because the prefix is incorrect', () => {
    const data = '_<?xm version="1.0"?>';
    const [isValid, nextPos] = xmlDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 5);
  });

  it('Should not match because the suffix is incorrect', () => {
    const data = '_<?xml version="1.0"?';
    const [isValid, nextPos] = xmlDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, data.length);
  });

  it('Should match the version number', () => {
    const data = '_<?xml version="1.0"?>';
    const [isValid, nextPos, res] = xmlDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { version: '1.0' });
  });

  it('Should match the version number and encoding name', () => {
    const data = '_<?xml version="1.0" encoding="UTF-8"?>';
    const [isValid, nextPos, res] = xmlDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { version: '1.0', encoding: 'UTF-8' });
  });

  it('Should match the version number and standalone declaration', () => {
    const data = '_<?xml version="1.0" standalone="yes"?>';
    const [isValid, nextPos, res] = xmlDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { version: '1.0', standalone: true });
  });

  it('Should match the version number, encoding name, and standalone declaration', () => {
    const data = '_<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
    const [isValid, nextPos, res] = xmlDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      version: '1.0',
      encoding: 'UTF-8',
      standalone: true,
    });
  });
});

describe('build', () => {
  it('Should return the XML declaration with the version number', () => {
    const res = xmlDeclParser.build({ version: '1.0' });
    assert.equal(res, '<?xml version="1.0"?>');
  });

  it('Should return the full XML declaration', () => {
    const res = xmlDeclParser.build({
      version: '1.0',
      encoding: 'UTF-8',
      standalone: true,
    });
    assert.equal(
      res,
      '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
    );
  });
});
