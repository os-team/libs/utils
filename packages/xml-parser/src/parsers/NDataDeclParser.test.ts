import { describe, it } from 'node:test';
import assert from 'node:assert';
import NDataDeclParser from './NDataDeclParser.js';

const nDataDeclParser = new NDataDeclParser();

describe('test', () => {
  it('Should not match because the prefix is incorrect', () => {
    const data = '_ NDAT gif';
    const [isValid, nextPos] = nDataDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 6);
  });

  it('Should match the NData name', () => {
    const data = '_ NDATA gif';
    const [isValid, nextPos, res] = nDataDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, 'gif');
  });
});

describe('build', () => {
  it('Should return the NData name', () => {
    const res = nDataDeclParser.build('gif');
    assert.equal(res, ' NDATA gif');
  });
});
