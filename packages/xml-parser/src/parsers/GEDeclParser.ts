import {
  AndRule,
  type DataRef,
  LiteralRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import SRule from '../rules/SRule.js';
import NameRule from '../rules/NameRule.js';
import type { Parser } from './Parser.js';
import EntityDefParser, { type EntityDef } from './EntityDefParser.js';

export interface GEDecl {
  type: 'ENTITY';
  name: string;
  entity: EntityDef;
}

/**
 * See https://www.w3.org/TR/xml/#NT-GEDecl
 */
class GEDeclParser implements Parser<GEDecl> {
  private readonly entityDefParser: EntityDefParser;

  private readonly rule: Rule<
    [string, undefined, string, undefined, EntityDef, undefined, string]
  >;

  public constructor() {
    const prefixRule = new LiteralRule('<!ENTITY'); // '<!ENTITY'
    const sRule = new SRule('+'); // S
    const nameRule = new NameRule(); // Name
    this.entityDefParser = new EntityDefParser(); // EntityDef
    const anySRule = new SRule('*'); // S?
    const suffixRule = new LiteralRule('>'); // '>'
    this.rule = new AndRule([
      prefixRule,
      sRule,
      nameRule,
      sRule,
      this.entityDefParser,
      anySRule,
      suffixRule,
    ]); // '<!ENTITY' S Name S EntityDef S? '>'
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<GEDecl> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    const [, , name, , entity] = res;
    return [true, nextPos, { type: 'ENTITY', name, entity }];
  }

  public build(data: GEDecl) {
    const entity = this.entityDefParser.build(data.entity);
    return `<!ENTITY ${data.name} ${entity}>`;
  }
}

export default GEDeclParser;
