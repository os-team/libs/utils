import { describe, it } from 'node:test';
import assert from 'node:assert';
import SdDeclParser from './SdDeclParser.js';

const sdDeclParser = new SdDeclParser();

describe('test', () => {
  it('Should not match because the first character is not a white space', () => {
    const data = '_standalone="yes"';
    const [isValid, nextPos] = sdDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should match the standalone declaration in quotes', () => {
    const data = '_ standalone="yes"';
    const [isValid, nextPos, res] = sdDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, true);
  });

  it('Should match the standalone declaration in apostrophes', () => {
    const data = "_ standalone='yes'";
    const [isValid, nextPos, res] = sdDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, true);
  });

  it('Should match the standalone declaration regardless of many white spaces', () => {
    const data = '_  standalone  = "no"';
    const [isValid, nextPos, res] = sdDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, false);
  });
});

describe('build', () => {
  it('Should return the yes literal', () => {
    const res = sdDeclParser.build(true);
    assert.equal(res, ' standalone="yes"');
  });

  it('Should return the no literal', () => {
    const res = sdDeclParser.build(false);
    assert.equal(res, ' standalone="no"');
  });
});
