import { describe, it } from 'node:test';
import assert from 'node:assert';
import NotationTypeParser from './NotationTypeParser.js';

const notationTypeParser = new NotationTypeParser();

describe('test', () => {
  it('Should not match because the notation literal is incorrect', () => {
    const data = '_NOTATIO (one|two|three)';
    const [isValid, nextPos] = notationTypeParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 8);
  });

  it('Should not match because the opening parenthesis was skipped', () => {
    const data = '_NOTATION one|two|three)';
    const [isValid, nextPos] = notationTypeParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 10);
  });

  it('Should not match because the closing parenthesis was skipped', () => {
    const data = '_NOTATION (one|two|three';
    const [isValid, nextPos] = notationTypeParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, data.length);
  });

  it('Should match the notation type', () => {
    const data = '_NOTATION (one | two|  three  )';
    const [isValid, nextPos, res] = notationTypeParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'NOTATION',
      items: ['one', 'two', 'three'],
    });
  });
});

describe('build', () => {
  it('Should return the notation type', () => {
    const res = notationTypeParser.build({
      type: 'NOTATION',
      items: ['one', 'two', 'three'],
    });
    assert.equal(res, 'NOTATION (one|two|three)');
  });
});
