import { describe, it } from 'node:test';
import assert from 'node:assert';
import DocumentParser from './DocumentParser.js';

const documentParser = new DocumentParser();

describe('test', () => {
  it('Should not match because the element is not found (no prolog)', () => {
    const data = '_';
    const [isValid, nextPos] = documentParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should not match because the element is not found (has prolog)', () => {
    const data = '_<?xml version="1.0" encoding="UTF-8"?>';
    const [isValid, nextPos] = documentParser.test(
      { data, options: { include: 'PROLOG' } },
      1
    );
    assert.equal(isValid, false);
    assert.equal(nextPos, data.length);
  });

  it('Should match the document only with an element', () => {
    const data = '_<tag>text</tag>';
    const [isValid, nextPos, res] = documentParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { tag: 'text' });
  });

  it('Should skip the prolog and misc because the include option is not specified', () => {
    const data =
      '_<?xml version="1.0" encoding="UTF-8"?><tag>text</tag><?xml-stylesheet type="text/xsl" href="style.xsl"?>';
    const [isValid, nextPos, res] = documentParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { tag: 'text' });
  });

  it('Should match the document with an element, prolog and misc', () => {
    const data =
      '_<?xml version="1.0" encoding="UTF-8"?><tag>text</tag><?xml-stylesheet type="text/xsl" href="style.xsl"?>';
    const [isValid, nextPos, res] = documentParser.test(
      { data, options: { include: 'PROLOG' } },
      1
    );
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      tag: 'text',
      '?xml-stylesheet': 'type="text/xsl" href="style.xsl"',
      '#prolog': {
        xml: {
          version: '1.0',
          encoding: 'UTF-8',
        },
      },
    });
  });
});

describe('build', () => {
  it('Should throw the error because there are 2 root elements', () => {
    assert.throws(() => documentParser.build({ tag: '1', tag2: '2' }));
  });

  it('Should return the document only with an element', () => {
    const res = documentParser.build({ tag: 'text' });
    assert.equal(res, '<tag>text</tag>');
  });

  it('Should return the document with an element, prolog and misc', () => {
    const res = documentParser.build({
      tag: 'text',
      '?xml-stylesheet': 'type="text/xsl" href="style.xsl"',
      '#prolog': {
        xml: {
          version: '1.0',
          encoding: 'UTF-8',
        },
      },
    });
    assert.equal(
      res,
      '<?xml version="1.0" encoding="UTF-8"?><tag>text</tag><?xml-stylesheet type="text/xsl" href="style.xsl"?>'
    );
  });
});
