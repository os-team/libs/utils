import {
  AndRule,
  type DataRef,
  LiteralRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import type { Parser } from './Parser.js';
import CharRule from '../rules/CharRule.js';
import AnyCharRule from '../rules/AnyCharRule.js';

/**
 * The CDATA section.
 * See https://www.w3.org/TR/xml/#NT-CDSect
 */
class CDSectParser implements Parser<string> {
  private readonly rule: Rule<[string, string, string]>;

  public constructor() {
    const cdStartRule = new LiteralRule('<![CDATA['); // '<![CDATA['
    const charRule = new CharRule(); // Char
    const cDataRule = new AnyCharRule(charRule, [']]>']); // (Char* - (Char* ']]>' Char*))
    const cdEndRule = new LiteralRule(']]>'); // ']]>'
    this.rule = new AndRule([cdStartRule, cDataRule, cdEndRule]); // CDStart CData CDEnd
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res[1]];
  }

  public build(data: string) {
    return `<![CDATA[${data}]]>`;
  }
}

export default CDSectParser;
