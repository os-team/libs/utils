import { describe, it } from 'node:test';
import assert from 'node:assert';
import MiscParser from './MiscParser.js';

const miscParser = new MiscParser();

describe('test', () => {
  it('Should match the comment', () => {
    const data = '_<!-- Comment -->';
    const [isValid, nextPos, res] = miscParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, undefined);
  });

  it('Should match the processing instruction', () => {
    const data = '_<?xml-stylesheet type="text/xsl" href="style.xsl"?>';
    const [isValid, nextPos, res] = miscParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, [
      'PI',
      'xml-stylesheet',
      'type="text/xsl" href="style.xsl"',
    ]);
  });

  it('Should match white spaces', () => {
    const data = '_\t \n';
    const [isValid, nextPos, res] = miscParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, undefined);
  });
});

describe('build', () => {
  it('Should return the processing instruction', () => {
    const res = miscParser.build([
      'PI',
      'xml-stylesheet',
      'type="text/xsl" href="style.xsl"',
    ]);
    assert.equal(res, '<?xml-stylesheet type="text/xsl" href="style.xsl"?>');
  });

  it('Should return an empty string', () => {
    const res = miscParser.build(undefined);
    assert.equal(res, '');
  });
});
