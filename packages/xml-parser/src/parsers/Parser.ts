import type { DataRef, RuleTestResponse, Rule } from '@os-team/lexical-rules';

export interface ParseOptions {
  /**
   * The callback that detects whether the node is an array.
   * @default undefined
   */
  isArray?: string[] | ((name: string) => boolean);
  /**
   * The callback used to redefine the tag name.
   * @default undefined
   */
  tagName?: (name: string) => string;
  /**
   * Parts of a document that must be included to the response.
   * @default undefined
   */
  include?: 'ATTRIBUTES' | 'PROLOG' | 'ALL';
}

export interface DataWithOptionsRef extends DataRef {
  options?: ParseOptions;
}

export interface Parser<T> extends Rule<T> {
  test: (ref: DataWithOptionsRef, pos: number) => RuleTestResponse<T>;
  build: (data: T) => string;
}
