import {
  type DataRef,
  OrRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import type { Parser } from './Parser.js';
import StringTypeParser, { type StringType } from './StringTypeParser.js';
import TokenizedTypeParser, {
  type TokenizedType,
} from './TokenizedTypeParser.js';
import EnumeratedTypeParser, {
  type EnumeratedType,
} from './EnumeratedTypeParser.js';

export type AttType = StringType | TokenizedType | EnumeratedType;

/**
 * The attribute type.
 * See https://www.w3.org/TR/xml/#NT-AttType
 */
class AttTypeParser implements Parser<AttType> {
  private readonly stringTypeParser: StringTypeParser;

  private readonly tokenizedTypeParser: TokenizedTypeParser;

  private readonly enumeratedTypeParser: EnumeratedTypeParser;

  private readonly rule: Rule<StringType | TokenizedType | EnumeratedType>;

  public constructor() {
    this.stringTypeParser = new StringTypeParser(); // StringType
    this.tokenizedTypeParser = new TokenizedTypeParser(); // TokenizedType
    this.enumeratedTypeParser = new EnumeratedTypeParser(); // EnumeratedType
    this.rule = new OrRule([
      this.stringTypeParser,
      this.tokenizedTypeParser,
      this.enumeratedTypeParser,
    ]); // StringType | TokenizedType | EnumeratedType
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<AttType> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res];
  }

  public build(data: AttType) {
    if (data.type === 'CDATA') {
      return this.stringTypeParser.build(data);
    }
    if (data.type !== 'NOTATION' && data.type !== 'ENUMERATION') {
      return this.tokenizedTypeParser.build(data);
    }
    return this.enumeratedTypeParser.build(data);
  }
}

export default AttTypeParser;
