import {
  type DataRef,
  LiteralRule,
  OrRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import type { Parser } from './Parser.js';

export interface TokenizedType {
  type:
    | 'ID'
    | 'IDREF'
    | 'IDREFS'
    | 'ENTITY'
    | 'ENTITIES'
    | 'NMTOKEN'
    | 'NMTOKENS';
}

/**
 * See https://www.w3.org/TR/xml/#NT-TokenizedType
 */
class TokenizedTypeParser implements Parser<TokenizedType> {
  private readonly rule: Rule<string>;

  public constructor() {
    const idRule = new LiteralRule('ID'); // 'ID'
    const idRefRule = new LiteralRule('IDREF'); // 'IDREF'
    const idRefsRule = new LiteralRule('IDREFS'); // 'IDREFS'
    const entityRule = new LiteralRule('ENTITY'); // 'ENTITY'
    const entitiesRule = new LiteralRule('ENTITIES'); // 'ENTITIES'
    const nmtokenRule = new LiteralRule('NMTOKEN'); // 'NMTOKEN'
    const nmtokensRule = new LiteralRule('NMTOKENS'); // 'NMTOKENS'
    this.rule = new OrRule([
      idRule,
      idRefRule,
      idRefsRule,
      entityRule,
      entitiesRule,
      nmtokenRule,
      nmtokensRule,
    ]); // 'ID'	| 'IDREF' | 'IDREFS' | 'ENTITY' | 'ENTITIES' | 'NMTOKEN' | 'NMTOKENS'
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<TokenizedType> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, { type: res } as TokenizedType];
  }

  public build(data: TokenizedType) {
    return data.type;
  }
}

export default TokenizedTypeParser;
