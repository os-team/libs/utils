import {
  type DataRef,
  OrRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import type { Parser } from './Parser.js';
import ElementDeclParser, { type ElementDecl } from './ElementDeclParser.js';
import AttlistDeclParser, { type AttlistDecl } from './AttlistDeclParser.js';
import EntityDeclParser, { type EntityDecl } from './EntityDeclParser.js';
import NotationDeclParser, { type NotationDecl } from './NotationDeclParser.js';
import CommentRule from '../rules/CommentRule.js';
import PIParser, { type PI } from './PIParser.js';

export type MarkupDecl =
  | ElementDecl
  | AttlistDecl
  | EntityDecl
  | NotationDecl
  | PI
  | undefined;

/**
 * The markup declaration.
 * See https://www.w3.org/TR/xml/#NT-markupdecl
 */
class MarkupDeclParser implements Parser<MarkupDecl> {
  private readonly elementDeclParser: ElementDeclParser;

  private readonly attlistDeclParser: AttlistDeclParser;

  private readonly entityDeclParser: EntityDeclParser;

  private readonly notationDeclParser: NotationDeclParser;

  private readonly piPraser: PIParser;

  private readonly rule: Rule<MarkupDecl>;

  public constructor() {
    this.elementDeclParser = new ElementDeclParser(); // elementdecl
    this.attlistDeclParser = new AttlistDeclParser(); // AttlistDecl
    this.entityDeclParser = new EntityDeclParser(); // EntityDecl
    this.notationDeclParser = new NotationDeclParser(); // NotationDecl
    this.piPraser = new PIParser(); // PI
    const commentRule = new CommentRule(); // Comment
    this.rule = new OrRule([
      this.elementDeclParser,
      this.attlistDeclParser,
      this.entityDeclParser,
      this.notationDeclParser,
      this.piPraser,
      commentRule,
    ]); // elementdecl | AttlistDecl | EntityDecl | NotationDecl | PI | Comment
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<MarkupDecl> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid) return [false, nextPos];
    return [true, nextPos, res];
  }

  public build(data: MarkupDecl) {
    if (data === undefined) return '';
    if (Array.isArray(data)) return this.piPraser.build(data);
    if (data.type === 'ELEMENT') return this.elementDeclParser.build(data);
    if (data.type === 'ATTLIST') return this.attlistDeclParser.build(data);
    if (data.type === 'ENTITY') return this.entityDeclParser.build(data);
    return this.notationDeclParser.build(data);
  }
}

export default MarkupDeclParser;
