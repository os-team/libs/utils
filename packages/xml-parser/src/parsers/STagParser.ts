import {
  AndRule,
  type DataRef,
  LiteralRule,
  RepetitionRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import type { Parser } from './Parser.js';
import SRule from '../rules/SRule.js';
import NameRule from '../rules/NameRule.js';
import AttributeParser, { type Attribute } from './AttributeParser.js';

export interface STag {
  type: 'START_TAG';
  name: string;
  attrs?: Record<string, string>;
}

/**
 * The start-tag for an element.
 * See https://www.w3.org/TR/xml/#NT-STag
 */
class STagParser implements Parser<STag> {
  private readonly attributeParser: AttributeParser;

  private readonly rule: Rule<
    [string, string, Array<[undefined, Attribute]>, undefined, string]
  >;

  public constructor() {
    const prefixRule = new LiteralRule('<'); // '<'
    const nameRule = new NameRule(); // Name
    const sRule = new SRule('+'); // S
    this.attributeParser = new AttributeParser(); // Attribute
    const sAndAttributeRule = new AndRule([sRule, this.attributeParser]); // S Attribute
    const anySAndAttributeRule = new RepetitionRule(sAndAttributeRule, 0); // (S Attribute)*
    const anySRule = new SRule('*'); // S?
    const suffixRule = new LiteralRule('>'); // '>'
    this.rule = new AndRule([
      prefixRule,
      nameRule,
      anySAndAttributeRule,
      anySRule,
      suffixRule,
    ]); // '<' Name (S Attribute)* S? '>'
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<STag> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];

    const [, name, rawAttributes] = res;
    const sTag: STag = { type: 'START_TAG', name };
    if (rawAttributes.length > 0) {
      sTag.attrs = rawAttributes.reduce(
        (acc, [, [key, value]]) => ({ ...acc, [key]: value }),
        {}
      );
    }

    return [true, nextPos, sTag];
  }

  public build(data: STag) {
    const attrs = Object.entries(data.attrs || {})
      .map((item) => ` ${this.attributeParser.build(item)}`)
      .join('');
    return `<${data.name}${attrs}>`;
  }
}

export default STagParser;
