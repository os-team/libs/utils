import {
  AndRule,
  type DataRef,
  LiteralRule,
  RepetitionRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import type { Parser } from './Parser.js';
import CpParser, { type Cp } from './CpParser.js';
import SRule from '../rules/SRule.js';

export interface Choice {
  type: 'CHOICE';
  items: Cp[];
}

/**
 * See https://www.w3.org/TR/xml/#NT-choice
 */
class ChoiceParser implements Parser<Choice> {
  private readonly cpParser: CpParser;

  private readonly rule: Rule<
    [
      string,
      undefined,
      Cp,
      Array<[undefined, string, undefined, Cp]>,
      undefined,
      string,
    ]
  >;

  public constructor() {
    const prefixRule = new LiteralRule('('); // '('
    const anySRule = new SRule('*'); // S?
    this.cpParser = new CpParser(); // cp
    const separatorRule = new LiteralRule('|'); // '|'
    const moreCpRule = new AndRule([
      anySRule,
      separatorRule,
      anySRule,
      this.cpParser,
    ]); // S? '|' S? cp
    const oneOrMoreMoreCpRule = new RepetitionRule(moreCpRule, 1); // ( S? '|' S? cp )+
    const suffixRule = new LiteralRule(')'); // ')'
    this.rule = new AndRule([
      prefixRule,
      anySRule,
      this.cpParser,
      oneOrMoreMoreCpRule,
      anySRule,
      suffixRule,
    ]); // '(' S? cp ( S? '|' S? cp )+ S? ')'
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<Choice> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];

    const [, , cp, moreCp] = res;
    const choice: Choice = {
      type: 'CHOICE',
      items: [cp, ...moreCp.map((item) => item[3])],
    };

    return [true, nextPos, choice];
  }

  public build(data: Choice) {
    return `(${data.items.map((item) => this.cpParser.build(item)).join('|')})`;
  }
}

export default ChoiceParser;
