import type { Parser } from './Parser.js';

/* eslint-disable @typescript-eslint/no-explicit-any */

interface Class<T> {
  new (...args: any[]): T;
}

/**
 * Creates an instance of the parser only before using it.
 */
function createLazyParser<T, P extends Parser<T>>(
  ParserClass: Class<P>,
  ...args: any[]
): P {
  let parser: P;

  const getParser = () => {
    if (!parser) parser = new ParserClass(...args);
    return parser;
  };

  return {
    test: (ref, pos) => getParser().test(ref, pos),
    build: (data) => getParser().build(data),
  } as P;
}

export default createLazyParser;
