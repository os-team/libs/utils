import { describe, it } from 'node:test';
import assert from 'node:assert';
import MixedParser from './MixedParser.js';

const mixedParser = new MixedParser();

describe('test', () => {
  it('Should match the mixed-content declaration with items', () => {
    const data = '_(#PCDATA|m|n)*';
    const [isValid, nextPos, res] = mixedParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'MIXED',
      items: ['m', 'n'],
    });
  });

  it('Should match the mixed-content declaration without items', () => {
    const data = '_(#PCDATA)';
    const [isValid, nextPos, res] = mixedParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { type: 'MIXED' });
  });
});

describe('build', () => {
  it('Should return the mixed-content declaration with items', () => {
    const res = mixedParser.build({
      type: 'MIXED',
      items: ['m', 'n'],
    });
    assert.equal(res, '(#PCDATA|m|n)*');
  });

  it('Should return the mixed-content declaration without items', () => {
    const res = mixedParser.build({ type: 'MIXED' });
    assert.equal(res, '(#PCDATA)');
  });
});
