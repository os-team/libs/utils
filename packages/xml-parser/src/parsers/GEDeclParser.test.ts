import { describe, it } from 'node:test';
import assert from 'node:assert';
import GEDeclParser from './GEDeclParser.js';

const geDeclParser = new GEDeclParser();

describe('test', () => {
  it('Should not match because the prefix is incorrect', () => {
    const data =
      '_<!ENTIT unpsd PUBLIC "//this/is/a/URI/me.gif" "me.gif" NDATA TeX>';
    const [isValid, nextPos] = geDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 8);
  });

  it('Should not match because the suffix is incorrect', () => {
    const data =
      '_<!ENTITY unpsd PUBLIC "//this/is/a/URI/me.gif" "me.gif" NDATA TeX';
    const [isValid, nextPos] = geDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, data.length);
  });

  it('Should match GEDecl', () => {
    const data =
      '_<!ENTITY unpsd PUBLIC "//this/is/a/URI/me.gif" "me.gif" NDATA TeX>';
    const [isValid, nextPos, res] = geDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'ENTITY',
      name: 'unpsd',
      entity: {
        type: 'PUBLIC_EXTERNAL_ID',
        pubidValue: '//this/is/a/URI/me.gif',
        systemValue: 'me.gif',
        nData: 'TeX',
      },
    });
  });
});

describe('build', () => {
  it('Should return GEDecl', () => {
    const res = geDeclParser.build({
      type: 'ENTITY',
      name: 'unpsd',
      entity: {
        type: 'PUBLIC_EXTERNAL_ID',
        pubidValue: '//this/is/a/URI/me.gif',
        systemValue: 'me.gif',
        nData: 'TeX',
      },
    });
    assert.equal(
      res,
      '<!ENTITY unpsd PUBLIC "//this/is/a/URI/me.gif" "me.gif" NDATA TeX>'
    );
  });
});
