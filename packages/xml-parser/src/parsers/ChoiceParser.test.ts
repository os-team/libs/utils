import { describe, it } from 'node:test';
import assert from 'node:assert';
import ChoiceParser from './ChoiceParser.js';

const choiceParser = new ChoiceParser();

describe('test', () => {
  it('Should not match because the prefix was skipped', () => {
    const data = '_one|two)';
    const [isValid, nextPos] = choiceParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should not match because the suffix was skipped', () => {
    const data = '_(one|two';
    const [isValid, nextPos] = choiceParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, data.length);
  });

  it('Should not match because the separator is incorrect', () => {
    const data = '_(one,two)';
    const [isValid, nextPos] = choiceParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 5);
  });

  it('Should not match because the number of items must be more than 1', () => {
    const data = '_(one)';
    const [isValid, nextPos] = choiceParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 5);
  });

  it('Should match the choice', () => {
    const data = '_(one| two?  )';
    const [isValid, nextPos, res] = choiceParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'CHOICE',
      items: ['one', ['two', '?']],
    });
  });
});

describe('build', () => {
  it('Should return the choice', () => {
    const res = choiceParser.build({
      type: 'CHOICE',
      items: ['one', ['two', '?']],
    });
    assert.equal(res, '(one|two?)');
  });
});
