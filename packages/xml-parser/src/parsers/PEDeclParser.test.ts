import { describe, it } from 'node:test';
import assert from 'node:assert';
import PEDeclParser from './PEDeclParser.js';

const peDeclParser = new PEDeclParser();

describe('test', () => {
  it('Should not match because the prefix is incorrect', () => {
    const data =
      '_<!ENTIT % xs-datatypes PUBLIC "-//W3C//DTD XSD 1.1 Datatypes//EN" "datatypes.dtd">';
    const [isValid, nextPos] = peDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 8);
  });

  it('Should not match because the suffix is incorrect', () => {
    const data =
      '_<!ENTITY % xs-datatypes PUBLIC "-//W3C//DTD XSD 1.1 Datatypes//EN" "datatypes.dtd"';
    const [isValid, nextPos] = peDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, data.length);
  });

  it('Should match PEDecl', () => {
    const data =
      '_<!ENTITY % xs-datatypes PUBLIC "-//W3C//DTD XSD 1.1 Datatypes//EN" "datatypes.dtd">';
    const [isValid, nextPos, res] = peDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'ENTITY',
      name: 'xs-datatypes',
      pe: {
        type: 'PUBLIC_EXTERNAL_ID',
        pubidValue: '-//W3C//DTD XSD 1.1 Datatypes//EN',
        systemValue: 'datatypes.dtd',
      },
    });
  });
});

describe('build', () => {
  it('Should return PEDecl', () => {
    const res = peDeclParser.build({
      type: 'ENTITY',
      name: 'xs-datatypes',
      pe: {
        type: 'PUBLIC_EXTERNAL_ID',
        pubidValue: '-//W3C//DTD XSD 1.1 Datatypes//EN',
        systemValue: 'datatypes.dtd',
      },
    });
    assert.equal(
      res,
      '<!ENTITY % xs-datatypes PUBLIC "-//W3C//DTD XSD 1.1 Datatypes//EN" "datatypes.dtd">'
    );
  });
});
