import { describe, it } from 'node:test';
import assert from 'node:assert';
import AttlistDeclParser from './AttlistDeclParser.js';

const attlistDeclParser = new AttlistDeclParser();

describe('test', () => {
  it('Should not match because the prefix is incorrect', () => {
    const data = '_<!ATTLIS abc name (one|two|three) #REQUIRED>';
    const [isValid, nextPos] = attlistDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 9);
  });

  it('Should not match because the suffix was skipped', () => {
    const data = '_<!ATTLIST abc name (one|two|three) #REQUIRED';
    const [isValid, nextPos] = attlistDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, data.length);
  });

  it('Should match the attribute-list declaration', () => {
    const data = '_<!ATTLIST abc name (one|two|three) #REQUIRED>';
    const [isValid, nextPos, res] = attlistDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'ATTLIST',
      name: 'abc',
      items: [
        {
          name: 'name',
          type: {
            type: 'ENUMERATION',
            items: ['one', 'two', 'three'],
          },
          default: {
            type: 'REQUIRED',
          },
        },
      ],
    });
  });
});

describe('build', () => {
  it('Should return the attribute-list declaration', () => {
    const res = attlistDeclParser.build({
      type: 'ATTLIST',
      name: 'abc',
      items: [
        {
          name: 'name',
          type: {
            type: 'ENUMERATION',
            items: ['one', 'two', 'three'],
          },
          default: {
            type: 'REQUIRED',
          },
        },
      ],
    });
    assert.equal(res, '<!ATTLIST abc name (one|two|three) #REQUIRED>');
  });
});
