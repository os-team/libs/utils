import { describe, it } from 'node:test';
import assert from 'node:assert';
import DoctypeDeclParser from './DoctypeDeclParser.js';

const doctypeDeclParser = new DoctypeDeclParser();

describe('test', () => {
  it('Should not match because the prefix is incorrect', () => {
    const data = '_<!DOCTYP html>';
    const [isValid, nextPos] = doctypeDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 9);
  });

  it('Should not match because the suffix was skipped', () => {
    const data = '_<!DOCTYPE html';
    const [isValid, nextPos] = doctypeDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, data.length);
  });

  it('Should match the document type definition', () => {
    const data = '_<!DOCTYPE html>';
    const [isValid, nextPos, res] = doctypeDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { name: 'html' });
  });

  it('Should match the document type definition with ExternalID and IntSubset', () => {
    const data =
      '_<!DOCTYPE message SYSTEM "myMessage.dtd" [\n' +
      '  <!ATTLIST message info CDATA #IMPLIED >\n' +
      ']>';
    const [isValid, nextPos, res] = doctypeDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      name: 'message',
      externalID: {
        type: 'SYSTEM_EXTERNAL_ID',
        systemValue: 'myMessage.dtd',
      },
      intSubset: [
        {
          type: 'ATTLIST',
          name: 'message',
          items: [
            {
              name: 'info',
              type: {
                type: 'CDATA',
              },
              default: {
                type: 'IMPLIED',
              },
            },
          ],
        },
      ],
    });
  });
});

describe('build', () => {
  it('Should return the document type definition', () => {
    const res = doctypeDeclParser.build({ name: 'html' });
    assert.equal(res, '<!DOCTYPE html>');
  });

  it('Should return the document type definition with ExternalID and IntSubset', () => {
    const res = doctypeDeclParser.build({
      name: 'message',
      externalID: {
        type: 'SYSTEM_EXTERNAL_ID',
        systemValue: 'myMessage.dtd',
      },
      intSubset: [
        {
          type: 'ATTLIST',
          name: 'message',
          items: [
            {
              name: 'info',
              type: {
                type: 'CDATA',
              },
              default: {
                type: 'IMPLIED',
              },
            },
          ],
        },
      ],
    });
    assert.equal(
      res,
      '<!DOCTYPE message SYSTEM "myMessage.dtd" [<!ATTLIST message info CDATA #IMPLIED>]>'
    );
  });
});
