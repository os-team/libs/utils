import { describe, it } from 'node:test';
import assert from 'node:assert';
import PEDefParser from './PEDefParser.js';

const peDefParser = new PEDefParser();

describe('test', () => {
  it('Should match the entity value', () => {
    const data = '_"abc"';
    const [isValid, nextPos, res] = peDefParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, 'abc');
  });

  it('Should match the external ID', () => {
    const data = '_SYSTEM "exam.dtd"';
    const [isValid, nextPos, res] = peDefParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'SYSTEM_EXTERNAL_ID',
      systemValue: 'exam.dtd',
    });
  });
});

describe('build', () => {
  it('Should return the entity value', () => {
    const res = peDefParser.build('abc');
    assert.equal(res, '"abc"');
  });

  it('Should return the external ID', () => {
    const res = peDefParser.build({
      type: 'SYSTEM_EXTERNAL_ID',
      systemValue: 'exam.dtd',
    });
    assert.equal(res, 'SYSTEM "exam.dtd"');
  });
});
