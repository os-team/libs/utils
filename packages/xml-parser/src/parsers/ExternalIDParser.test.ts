import { describe, it } from 'node:test';
import assert from 'node:assert';
import ExternalIDParser from './ExternalIDParser.js';

const externalIDParser = new ExternalIDParser();

describe('test', () => {
  it('Should not match because the prefix of the system external ID is incorrect', () => {
    const data = '_SYSTE "exam.dtd"';
    const [isValid, nextPos] = externalIDParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should not match because the prefix of the public external ID is incorrect', () => {
    const data =
      '_PUBLI "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"';
    const [isValid, nextPos] = externalIDParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should not match because the system literal of the public external ID is not found', () => {
    const data = '_PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"';
    const [isValid, nextPos] = externalIDParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should match the system external ID', () => {
    const data = '_SYSTEM "exam.dtd"';
    const [isValid, nextPos, res] = externalIDParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'SYSTEM_EXTERNAL_ID',
      systemValue: 'exam.dtd',
    });
  });

  it('Should match the public external ID', () => {
    const data =
      '_PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"';
    const [isValid, nextPos, res] = externalIDParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'PUBLIC_EXTERNAL_ID',
      pubidValue: '-//W3C//DTD XHTML 1.0 Transitional//EN',
      systemValue: 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd',
    });
  });
});

describe('build', () => {
  it('Should return the system external ID', () => {
    const res = externalIDParser.build({
      type: 'SYSTEM_EXTERNAL_ID',
      systemValue: 'exam.dtd',
    });
    assert.equal(res, 'SYSTEM "exam.dtd"');
  });

  it('Should return the public external ID', () => {
    const res = externalIDParser.build({
      type: 'PUBLIC_EXTERNAL_ID',
      pubidValue: '-//W3C//DTD XHTML 1.0 Transitional//EN',
      systemValue: 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd',
    });
    assert.equal(
      res,
      'PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"'
    );
  });
});
