import { describe, it } from 'node:test';
import assert from 'node:assert';
import STagParser from './STagParser.js';

const sTagParser = new STagParser();

describe('test', () => {
  it('Should not match because the prefix was skipped', () => {
    const data = '_tag>';
    const [isValid, nextPos] = sTagParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should not match because the suffix was skipped', () => {
    const data = '_<tag';
    const [isValid, nextPos] = sTagParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, data.length);
  });

  it('Should match the start-tag for an element without attributes', () => {
    const data = '_<tag>';
    const [isValid, nextPos, res] = sTagParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'START_TAG',
      name: 'tag',
    });
  });

  it('Should match the start-tag for an element with attributes', () => {
    const data = '_<tag attr1="abc" attr2="123">';
    const [isValid, nextPos, res] = sTagParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'START_TAG',
      name: 'tag',
      attrs: {
        attr1: 'abc',
        attr2: '123',
      },
    });
  });
});

describe('build', () => {
  it('Should return the start-tag for an element without attributes', () => {
    const res = sTagParser.build({
      type: 'START_TAG',
      name: 'tag',
    });
    assert.equal(res, '<tag>');
  });

  it('Should return the start-tag for an element with attributes', () => {
    const res = sTagParser.build({
      type: 'START_TAG',
      name: 'tag',
      attrs: {
        attr1: 'abc',
        attr2: '123',
      },
    });
    assert.equal(res, '<tag attr1="abc" attr2="123">');
  });
});
