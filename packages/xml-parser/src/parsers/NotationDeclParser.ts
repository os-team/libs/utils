import {
  AndRule,
  type DataRef,
  LiteralRule,
  OrRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import SRule from '../rules/SRule.js';
import NameRule from '../rules/NameRule.js';
import ExternalIDParser, { type ExternalID } from './ExternalIDParser.js';
import type { Parser } from './Parser.js';
import PublicIDParser, { type PublicID } from './PublicIDParser.js';

export interface NotationDecl {
  type: 'NOTATION';
  name: string;
  id: ExternalID | PublicID;
}

/**
 * The notation declaration.
 * See https://www.w3.org/TR/xml/#NT-NotationDecl
 */
class NotationDeclParser implements Parser<NotationDecl> {
  private readonly externalIDParser: ExternalIDParser;

  private readonly publicIDParser: PublicIDParser;

  private readonly rule: Rule<
    [
      string,
      undefined,
      string,
      undefined,
      ExternalID | PublicID,
      undefined,
      string,
    ]
  >;

  public constructor() {
    const prefixRule = new LiteralRule('<!NOTATION'); // '<!NOTATION'
    const sRule = new SRule('+'); // S
    const nameRule = new NameRule(); // Name
    this.externalIDParser = new ExternalIDParser(); // ExternalID
    this.publicIDParser = new PublicIDParser(); // PublicID
    const idRule = new OrRule([this.externalIDParser, this.publicIDParser]); // ExternalID | PublicID
    const anySRule = new SRule('*'); // S?
    const suffixRule = new LiteralRule('>'); // '>'
    this.rule = new AndRule([
      prefixRule,
      sRule,
      nameRule,
      sRule,
      idRule,
      anySRule,
      suffixRule,
    ]); // '<!NOTATION' S Name S (ExternalID | PublicID) S? '>'
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<NotationDecl> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    const [, , name, , id] = res;
    return [true, nextPos, { type: 'NOTATION', name, id }];
  }

  public build(data: NotationDecl) {
    const id =
      data.id.type === 'PUBLIC_ID'
        ? this.publicIDParser.build(data.id)
        : this.externalIDParser.build(data.id);
    return `<!NOTATION ${data.name} ${id}>`;
  }
}

export default NotationDeclParser;
