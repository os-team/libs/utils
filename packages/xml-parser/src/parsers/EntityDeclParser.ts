import {
  type DataRef,
  OrRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import type { Parser } from './Parser.js';
import GEDeclParser, { type GEDecl } from './GEDeclParser.js';
import PEDeclParser, { type PEDecl } from './PEDeclParser.js';

export type EntityDecl = GEDecl | PEDecl;

/**
 * The entity declaration.
 * See https://www.w3.org/TR/xml/#NT-EntityDecl
 */
class EntityDeclParser implements Parser<EntityDecl> {
  private readonly geDeclParser: GEDeclParser;

  private readonly peDeclParser: PEDeclParser;

  private readonly rule: Rule<GEDecl | PEDecl>;

  public constructor() {
    this.geDeclParser = new GEDeclParser(); // GEDecl
    this.peDeclParser = new PEDeclParser(); // PEDecl
    this.rule = new OrRule([this.geDeclParser, this.peDeclParser]); // GEDecl | PEDecl
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<EntityDecl> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res];
  }

  public build(data: EntityDecl) {
    return data['entity']
      ? this.geDeclParser.build(data as GEDecl)
      : this.peDeclParser.build(data as PEDecl);
  }
}

export default EntityDeclParser;
