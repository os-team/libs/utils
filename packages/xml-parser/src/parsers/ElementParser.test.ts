import { describe, it } from 'node:test';
import assert from 'node:assert';
import ElementParser from './ElementParser.js';

const elementParser = new ElementParser();

describe('test', () => {
  it('Should match the empty element tag without attributes', () => {
    const data = '_<tag />';
    const [isValid, nextPos, res] = elementParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { tag: '' });
  });

  it('Should skip attributes because the include option is not specified', () => {
    const data = '_<tag attr1="abc" attr2="123" />';
    const [isValid, nextPos, res] = elementParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      tag: '',
    });
  });

  it('Should match the empty element tag with attributes', () => {
    const data = '_<tag attr1="abc" attr2="123" />';
    const [isValid, nextPos, res] = elementParser.test(
      { data, options: { include: 'ATTRIBUTES' } },
      1
    );
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      tag: {
        '#content': '',
        '#attrs': {
          attr1: 'abc',
          attr2: '123',
        },
      },
    });
  });

  it('Should match the element tag without attributes', () => {
    const data = '_<tag>text</tag>';
    const [isValid, nextPos, res] = elementParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { tag: 'text' });
  });

  it('Should match the element tag with attributes', () => {
    const data = '_<tag attr1="abc" attr2="123">text</tag>';
    const [isValid, nextPos, res] = elementParser.test(
      { data, options: { include: 'ATTRIBUTES' } },
      1
    );
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      tag: {
        '#content': 'text',
        '#attrs': {
          attr1: 'abc',
          attr2: '123',
        },
      },
    });
  });

  it('Should match nested elements', () => {
    const data = '_<root><tag attr1="abc" attr2="123">text</tag></root>';
    const [isValid, nextPos, res] = elementParser.test(
      { data, options: { include: 'ATTRIBUTES' } },
      1
    );
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      root: {
        tag: {
          '#content': 'text',
          '#attrs': {
            attr1: 'abc',
            attr2: '123',
          },
        },
      },
    });
  });
});

describe('build', () => {
  it('Should throw the error because the root element is an array', () => {
    assert.throws(() => elementParser.build({ tag: ['item1', 'item2'] }));
  });

  it('Should return the empty element tag without attributes', () => {
    const res = elementParser.build({ tag: '' });
    assert.equal(res, '<tag/>');
  });

  it('Should return the empty element tag with attributes', () => {
    const res = elementParser.build({
      tag: {
        '#content': '',
        '#attrs': {
          attr1: 'abc',
          attr2: '123',
        },
      },
    });
    assert.equal(res, '<tag attr1="abc" attr2="123"/>');
  });

  it('Should return the element tag without attributes', () => {
    const res = elementParser.build({ tag: 'text' });
    assert.equal(res, '<tag>text</tag>');
  });

  it('Should return the element tag with attributes', () => {
    const res = elementParser.build({
      tag: {
        '#content': 'text',
        '#attrs': {
          attr1: 'abc',
          attr2: '123',
        },
      },
    });
    assert.equal(res, '<tag attr1="abc" attr2="123">text</tag>');
  });

  it('Should return nested elements', () => {
    const res = elementParser.build({
      root: {
        tag: {
          '#content': 'text',
          '#attrs': {
            attr1: 'abc',
            attr2: '123',
          },
        },
      },
    });
    assert.equal(res, '<root><tag attr1="abc" attr2="123">text</tag></root>');
  });

  it('Should return an array tag', () => {
    const res = elementParser.build({
      root: {
        item: ['item1', 'item2'],
      },
    });
    assert.equal(res, '<root><item>item1</item><item>item2</item></root>');
  });
});
