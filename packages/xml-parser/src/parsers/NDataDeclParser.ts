import {
  AndRule,
  type DataRef,
  LiteralRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import SRule from '../rules/SRule.js';
import NameRule from '../rules/NameRule.js';
import { type Parser } from './Parser.js';

/**
 * See https://www.w3.org/TR/xml/#NT-NDataDecl
 */
class NDataDeclParser implements Parser<string> {
  private readonly rule: Rule<[undefined, string, undefined, string]>;

  public constructor() {
    const sRule = new SRule('+'); // S
    const nDataRule = new LiteralRule('NDATA'); // 'NDATA'
    const nameRule = new NameRule(); // Name
    this.rule = new AndRule([sRule, nDataRule, sRule, nameRule]); // S 'NDATA' S Name
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res[3]];
  }

  public build(data: string) {
    return ` NDATA ${data}`;
  }
}

export default NDataDeclParser;
