import { describe, it } from 'node:test';
import assert from 'node:assert';
import TagParser from './TagParser.js';

const tagParser = new TagParser();

describe('test', () => {
  it('Should not match because the names of the start and end tags are not the same', () => {
    const data = '_<tag></tag2>';
    const [isValid, nextPos] = tagParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 11);
  });

  it('Should match the element tag without attributes', () => {
    const data = '_<tag>text</tag>';
    const [isValid, nextPos, res] = tagParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'TAG',
      name: 'tag',
      content: 'text',
    });
  });

  it('Should match the element tag with attributes', () => {
    const data = '_<tag attr1="abc" attr2="123">text</tag>';
    const [isValid, nextPos, res] = tagParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'TAG',
      name: 'tag',
      content: 'text',
      attrs: {
        attr1: 'abc',
        attr2: '123',
      },
    });
  });
});

describe('build', () => {
  it('Should return the element tag without attributes', () => {
    const res = tagParser.build({
      type: 'TAG',
      name: 'tag',
      content: 'text',
    });
    assert.equal(res, '<tag>text</tag>');
  });

  it('Should return the element tag with attributes', () => {
    const res = tagParser.build({
      type: 'TAG',
      name: 'tag',
      content: 'text',
      attrs: {
        attr1: 'abc',
        attr2: '123',
      },
    });
    assert.equal(res, '<tag attr1="abc" attr2="123">text</tag>');
  });

  it('Should return an empty string because the content is undefined and there are no attributes', () => {
    const res = tagParser.build({
      type: 'TAG',
      name: 'tag',
      content: undefined,
    });
    assert.equal(res, '');
  });

  it('Should return the empty tag because the content is undefined', () => {
    const res = tagParser.build({
      type: 'TAG',
      name: 'tag',
      content: undefined,
      attrs: {
        attr1: 'abc',
        attr2: '123',
      },
    });
    assert.equal(res, '<tag attr1="abc" attr2="123"></tag>');
  });
});
