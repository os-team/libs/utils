import { describe, it } from 'node:test';
import assert from 'node:assert';
import StringTypeParser from './StringTypeParser.js';

const stringTypeParser = new StringTypeParser();

describe('test', () => {
  it('Should not match because the literal is incorrect', () => {
    const data = '_CDAT';
    const [isValid, nextPos] = stringTypeParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, data.length);
  });

  it('Should match the string type', () => {
    const data = '_CDATA';
    const [isValid, nextPos, res] = stringTypeParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { type: 'CDATA' });
  });
});

describe('build', () => {
  it('Should return the string type', () => {
    const res = stringTypeParser.build({ type: 'CDATA' });
    assert.equal(res, 'CDATA');
  });
});
