import { describe, it } from 'node:test';
import assert from 'node:assert';
import AttributeParser from './AttributeParser.js';

const attributeParser = new AttributeParser();

describe('test', () => {
  it('Should not match because the equal sign was skipped', () => {
    const data = '_namevalue';
    const [isValid, nextPos] = attributeParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, data.length);
  });

  it('Should match the attribute', () => {
    const data = '_name=  "value"';
    const [isValid, nextPos, res] = attributeParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, ['name', 'value']);
  });
});

describe('build', () => {
  it('Should return the attribute', () => {
    const res = attributeParser.build(['name', 'value']);
    assert.equal(res, 'name="value"');
  });
});
