import { describe, it } from 'node:test';
import assert from 'node:assert';
import PIParser from './PIParser.js';

const piParser = new PIParser();

describe('test', () => {
  it('Should not match because the prefix is incorrect', () => {
    const data = '_<xml-stylesheet type="text/xsl" href="style.xsl"?>';
    const [isValid, nextPos] = piParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 2);
  });

  it('Should not match because the suffix is incorrect', () => {
    const data = '_<?xml-stylesheet type="text/xsl" href="style.xsl"?';
    const [isValid, nextPos] = piParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, data.length);
  });

  it('Should not match because the target of the processing instruction is xml', () => {
    const data = '_<?XmL type="text/xsl" href="style.xsl"?>';
    const [isValid, nextPos] = piParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 3);
  });

  it('Should match the processing instruction', () => {
    const data = '_<?xml-stylesheet type="text/xsl" href="style.xsl"?>_';
    const [isValid, nextPos, res] = piParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length - 1);
    assert.deepStrictEqual(res, [
      'PI',
      'xml-stylesheet',
      'type="text/xsl" href="style.xsl"',
    ]);
  });
});

describe('build', () => {
  it('Should throw the error because the target of the processing instruction is xml', () => {
    assert.throws(() => piParser.build(['PI', 'XmL']));
  });

  it('Should throw the error because the content of the processing instruction contains ?>', () => {
    assert.throws(() =>
      piParser.build([
        'PI',
        'xml-stylesheet',
        'type="text/xsl" ?>href="style.xsl"',
      ])
    );
  });

  it('Should return the processing instruction without content', () => {
    const res = piParser.build(['PI', 'xml-stylesheet']);
    assert.equal(res, '<?xml-stylesheet?>');
  });

  it('Should return the processing instruction with content', () => {
    const res = piParser.build([
      'PI',
      'xml-stylesheet',
      'type="text/xsl" href="style.xsl"',
    ]);
    assert.equal(res, '<?xml-stylesheet type="text/xsl" href="style.xsl"?>');
  });
});
