import { describe, it } from 'node:test';
import assert from 'node:assert';
import ETagParser from './ETagParser.js';

const eTagParser = new ETagParser('tag');

describe('test', () => {
  it('Should not match because the prefix is incorrect', () => {
    const data = '_<tag>';
    const [isValid, nextPos] = eTagParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 2);
  });

  it('Should not match because the suffix was skipped', () => {
    const data = '_</tag';
    const [isValid, nextPos] = eTagParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, data.length);
  });

  it('Should not match because the name is incorrect', () => {
    const data = '_</tag2>';
    const [isValid, nextPos] = eTagParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 6);
  });

  it('Should match the end-tag for an element', () => {
    const data = '_</tag>';
    const [isValid, nextPos, res] = eTagParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, undefined);
  });
});

describe('build', () => {
  it('Should return the end-tag for an element', () => {
    const res = eTagParser.build();
    assert.equal(res, '</tag>');
  });
});
