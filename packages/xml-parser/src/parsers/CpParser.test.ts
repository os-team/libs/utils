import { describe, it } from 'node:test';
import assert from 'node:assert';
import CpParser from './CpParser.js';

const cpParser = new CpParser();

describe('test', () => {
  it('Should match the name', () => {
    const data = '_name';
    const [isValid, nextPos, res] = cpParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, 'name');
  });

  it('Should match the name with quantifier', () => {
    const data = '_name*';
    const [isValid, nextPos, res] = cpParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, ['name', '*']);
  });

  it('Should match the choice', () => {
    const data = '_(one|two?)';
    const [isValid, nextPos, res] = cpParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'CHOICE',
      items: ['one', ['two', '?']],
    });
  });

  it('Should match the seq', () => {
    const data = '_(one,two?)';
    const [isValid, nextPos, res] = cpParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'SEQ',
      items: ['one', ['two', '?']],
    });
  });
});

describe('build', () => {
  it('Should return the name', () => {
    const res = cpParser.build('name');
    assert.equal(res, 'name');
  });

  it('Should return the name with quantifier', () => {
    const res = cpParser.build(['name', '*']);
    assert.equal(res, 'name*');
  });

  it('Should return the choice', () => {
    const res = cpParser.build({
      type: 'CHOICE',
      items: ['one', ['two', '?']],
    });
    assert.equal(res, '(one|two?)');
  });

  it('Should return the seq', () => {
    const res = cpParser.build({
      type: 'SEQ',
      items: ['one', ['two', '?']],
    });
    assert.equal(res, '(one,two?)');
  });
});
