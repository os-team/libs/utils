import {
  AndRule,
  type DataRef,
  LiteralRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import type { Parser } from './Parser.js';
import SRule from '../rules/SRule.js';
import EqRule from '../rules/EqRule.js';
import VersionNumRule from '../rules/VersionNumRule.js';
import AnyQuotedRule from '../rules/AnyQuotedRule.js';

/**
 * The version number.
 * See https://www.w3.org/TR/xml/#NT-VersionInfo
 */
class VersionInfoParser implements Parser<string> {
  private readonly rule: Rule<[undefined, string, undefined, string]>;

  public constructor() {
    const sRule = new SRule('+'); // S
    const versionRule = new LiteralRule('version'); // 'version'
    const eqRule = new EqRule(); // Eq
    const versionNumRule = new VersionNumRule(); // VersionNum
    const quotedVersionNumRule = new AnyQuotedRule(versionNumRule); // "'" VersionNum "'" | '"' VersionNum '"'
    this.rule = new AndRule([sRule, versionRule, eqRule, quotedVersionNumRule]); // S 'version' Eq ("'" VersionNum "'" | '"' VersionNum '"')
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<string> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res[3]];
  }

  public build(data: string) {
    if (!/^1\.[0-9]+$/.test(data)) {
      throw new Error('The version number in the XML declaration is incorrect');
    }
    return ` version="${data}"`;
  }
}

export default VersionInfoParser;
