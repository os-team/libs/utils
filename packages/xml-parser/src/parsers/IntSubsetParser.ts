import {
  type DataRef,
  OrRule,
  RepetitionRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import type { Parser } from './Parser.js';
import MarkupDeclParser, { type MarkupDecl } from './MarkupDeclParser.js';
import DeclSepRule from '../rules/DeclSepRule.js';

export type IntSubset = MarkupDecl[];

/**
 * The markup declaration.
 * See https://www.w3.org/TR/xml/#NT-markupdecl
 */
class IntSubsetParser implements Parser<IntSubset> {
  private readonly markupDeclParser: MarkupDeclParser;

  private readonly rule: Rule<Array<MarkupDecl | string | undefined>>;

  public constructor() {
    this.markupDeclParser = new MarkupDeclParser(); // markupdecl
    const declSepRule = new DeclSepRule(); // DeclSep
    const markupOrSepRule = new OrRule([this.markupDeclParser, declSepRule]); // markupdecl | DeclSep
    this.rule = new RepetitionRule(markupOrSepRule, 0); // (markupdecl | DeclSep)*
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<IntSubset> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];

    const markupDecls: IntSubset = [];
    res.forEach((item) => {
      if (typeof item === 'object') markupDecls.push(item);
    });

    return [true, nextPos, markupDecls];
  }

  public build(data: IntSubset) {
    return data.reduce(
      (acc, markupDecl) => `${acc}${this.markupDeclParser.build(markupDecl)}`,
      ''
    );
  }
}

export default IntSubsetParser;
