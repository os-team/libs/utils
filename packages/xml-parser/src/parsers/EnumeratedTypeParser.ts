import {
  type DataRef,
  OrRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import type { Parser } from './Parser.js';
import NotationTypeParser, { type NotationType } from './NotationTypeParser.js';
import EnumerationParser, { type Enumeration } from './EnumerationParser.js';

export type EnumeratedType = NotationType | Enumeration;

/**
 * The enumerated attribute type declaration.
 * See https://www.w3.org/TR/xml/#NT-EnumeratedType
 */
class EnumeratedTypeParser implements Parser<EnumeratedType> {
  private readonly notationTypeParser: NotationTypeParser;

  private readonly enumerationParser: EnumerationParser;

  private readonly rule: Rule<EnumeratedType>;

  public constructor() {
    this.notationTypeParser = new NotationTypeParser(); // NotationType
    this.enumerationParser = new EnumerationParser(); // Enumeration
    this.rule = new OrRule([this.notationTypeParser, this.enumerationParser]); // NotationType | Enumeration
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<EnumeratedType> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res];
  }

  public build(data: EnumeratedType) {
    return data.type === 'NOTATION'
      ? this.notationTypeParser.build(data)
      : this.enumerationParser.build(data);
  }
}

export default EnumeratedTypeParser;
