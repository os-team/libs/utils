import {
  type DataRef,
  LiteralRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import type { Parser } from './Parser.js';

export interface StringType {
  type: 'CDATA';
}

/**
 * See https://www.w3.org/TR/xml/#NT-StringType
 */
class StringTypeParser implements Parser<StringType> {
  private readonly rule: Rule<string>;

  public constructor() {
    this.rule = new LiteralRule('CDATA'); // 'CDATA'
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<StringType> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, { type: 'CDATA' }];
  }

  public build(data: StringType) {
    return data.type;
  }
}

export default StringTypeParser;
