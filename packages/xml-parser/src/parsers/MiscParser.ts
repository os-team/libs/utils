import {
  type DataRef,
  OrRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import type { Parser } from './Parser.js';
import SRule from '../rules/SRule.js';
import PIParser, { type PI } from './PIParser.js';
import CommentRule from '../rules/CommentRule.js';

export type Misc = PI | undefined;

/**
 * The misc.
 * See https://www.w3.org/TR/xml/#NT-Misc
 */
class MiscParser implements Parser<Misc> {
  private readonly piParser: PIParser;

  private readonly rule: Rule<Misc>;

  public constructor() {
    const commentRule = new CommentRule(); // Comment
    this.piParser = new PIParser(); // PI
    const sRule = new SRule('+'); // S
    this.rule = new OrRule([commentRule, this.piParser, sRule]); // Comment | PI | S
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<Misc> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid) return [false, nextPos];
    return [true, nextPos, res];
  }

  public build(data: Misc) {
    if (data === undefined) return '';
    return this.piParser.build(data);
  }
}

export default MiscParser;
