import {
  AndRule,
  type DataRef,
  LiteralRule,
  RepetitionRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import type { Parser } from './Parser.js';
import NameRule from '../rules/NameRule.js';
import SRule from '../rules/SRule.js';
import AttDefParser, { type AttDef } from './AttDefParser.js';

export interface AttlistDecl {
  type: 'ATTLIST';
  name: string;
  items: AttDef[];
}

/**
 * The attribute-list declaration.
 * See https://www.w3.org/TR/xml/#NT-AttlistDecl
 */
class AttlistDeclParser implements Parser<AttlistDecl> {
  private readonly attDefParser: AttDefParser;

  private readonly rule: Rule<
    [string, undefined, string, AttDef[], undefined, string]
  >;

  public constructor() {
    const prefixRule = new LiteralRule('<!ATTLIST'); // '<!ATTLIST'
    const sRule = new SRule('+'); // S
    const nameRule = new NameRule(); // Name
    this.attDefParser = new AttDefParser(); // AttDef
    const anyAttDefRule = new RepetitionRule(this.attDefParser, 0); // AttDef*
    const anySRule = new SRule('*'); // S?
    const suffixRule = new LiteralRule('>'); // '>'
    this.rule = new AndRule([
      prefixRule,
      sRule,
      nameRule,
      anyAttDefRule,
      anySRule,
      suffixRule,
    ]); // '<!ATTLIST' S Name AttDef* S? '>'
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<AttlistDecl> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    const [, , name, items] = res;
    return [true, nextPos, { type: 'ATTLIST', name, items }];
  }

  public build(data: AttlistDecl) {
    const attDefs = data.items.reduce(
      (acc, item) => `${acc}${this.attDefParser.build(item)}`,
      ''
    );
    return `<!ATTLIST ${data.name}${attDefs}>`;
  }
}

export default AttlistDeclParser;
