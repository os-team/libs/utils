import { describe, it } from 'node:test';
import assert from 'node:assert';
import MarkupDeclParser from './MarkupDeclParser.js';

const markupDeclParser = new MarkupDeclParser();

describe('test', () => {
  it('Should match the element declaration', () => {
    const data = '_<!ELEMENT p (#PCDATA|m|n)*>';
    const [isValid, nextPos, res] = markupDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'ELEMENT',
      name: 'p',
      content: {
        type: 'MIXED',
        items: ['m', 'n'],
      },
    });
  });

  it('Should match the attribute-list declaration', () => {
    const data = '_<!ATTLIST abc name (one|two|three) #REQUIRED>';
    const [isValid, nextPos, res] = markupDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'ATTLIST',
      name: 'abc',
      items: [
        {
          name: 'name',
          type: {
            type: 'ENUMERATION',
            items: ['one', 'two', 'three'],
          },
          default: {
            type: 'REQUIRED',
          },
        },
      ],
    });
  });

  it('Should match the entity declaration', () => {
    const data =
      '_<!ENTITY unpsd PUBLIC "//this/is/a/URI/me.gif" "me.gif" NDATA TeX>';
    const [isValid, nextPos, res] = markupDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'ENTITY',
      name: 'unpsd',
      entity: {
        type: 'PUBLIC_EXTERNAL_ID',
        pubidValue: '//this/is/a/URI/me.gif',
        systemValue: 'me.gif',
        nData: 'TeX',
      },
    });
  });

  it('Should match the notation declaration', () => {
    const data =
      '_<!NOTATION XMLSchemaStructures PUBLIC "structures" "http://www.w3.org/2001/XMLSchema.xsd">';
    const [isValid, nextPos, res] = markupDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'NOTATION',
      name: 'XMLSchemaStructures',
      id: {
        type: 'PUBLIC_EXTERNAL_ID',
        pubidValue: 'structures',
        systemValue: 'http://www.w3.org/2001/XMLSchema.xsd',
      },
    });
  });

  it('Should match the processing instruction', () => {
    const data = '_<?xml-stylesheet type="text/xsl" href="style.xsl"?>_';
    const [isValid, nextPos, res] = markupDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length - 1);
    assert.deepStrictEqual(res, [
      'PI',
      'xml-stylesheet',
      'type="text/xsl" href="style.xsl"',
    ]);
  });

  it('Should match the comment', () => {
    const data = '_<!-- Comm-ent -->';
    const [isValid, nextPos, res] = markupDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, undefined);
  });
});

describe('build', () => {
  it('Should return the element declaration', () => {
    const res = markupDeclParser.build({
      type: 'ELEMENT',
      name: 'p',
      content: {
        type: 'MIXED',
        items: ['m', 'n'],
      },
    });
    assert.equal(res, '<!ELEMENT p (#PCDATA|m|n)*>');
  });

  it('Should return the attribute-list declaration', () => {
    const res = markupDeclParser.build({
      type: 'ATTLIST',
      name: 'abc',
      items: [
        {
          name: 'name',
          type: {
            type: 'ENUMERATION',
            items: ['one', 'two', 'three'],
          },
          default: {
            type: 'REQUIRED',
          },
        },
      ],
    });
    assert.equal(res, '<!ATTLIST abc name (one|two|three) #REQUIRED>');
  });

  it('Should return the entity declaration', () => {
    const res = markupDeclParser.build({
      type: 'ENTITY',
      name: 'unpsd',
      entity: {
        type: 'PUBLIC_EXTERNAL_ID',
        pubidValue: '//this/is/a/URI/me.gif',
        systemValue: 'me.gif',
        nData: 'TeX',
      },
    });
    assert.equal(
      res,
      '<!ENTITY unpsd PUBLIC "//this/is/a/URI/me.gif" "me.gif" NDATA TeX>'
    );
  });

  it('Should return the notation declaration', () => {
    const res = markupDeclParser.build({
      type: 'NOTATION',
      name: 'XMLSchemaStructures',
      id: {
        type: 'PUBLIC_EXTERNAL_ID',
        pubidValue: 'structures',
        systemValue: 'http://www.w3.org/2001/XMLSchema.xsd',
      },
    });
    assert.equal(
      res,
      '<!NOTATION XMLSchemaStructures PUBLIC "structures" "http://www.w3.org/2001/XMLSchema.xsd">'
    );
  });

  it('Should return the processing instruction', () => {
    const res = markupDeclParser.build([
      'PI',
      'xml-stylesheet',
      'type="text/xsl" href="style.xsl"',
    ]);
    assert.equal(res, '<?xml-stylesheet type="text/xsl" href="style.xsl"?>');
  });
});
