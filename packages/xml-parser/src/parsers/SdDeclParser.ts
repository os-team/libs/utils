import {
  AndRule,
  type DataRef,
  LiteralRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import type { Parser } from './Parser.js';
import SRule from '../rules/SRule.js';
import EqRule from '../rules/EqRule.js';
import AnyQuotedRule from '../rules/AnyQuotedRule.js';
import YesOrNoRule from '../rules/YesOrNoRule.js';

/**
 * The standalone declaration.
 * See https://www.w3.org/TR/xml/#NT-SDDecl
 */
class SdDeclParser implements Parser<boolean> {
  private readonly rule: Rule<[undefined, string, undefined, boolean]>;

  public constructor() {
    const sRule = new SRule('+'); // S
    const standaloneRule = new LiteralRule('standalone'); // 'standalone'
    const eqRule = new EqRule(); // Eq
    const yesOrNoRule = new YesOrNoRule(); // 'yes' | 'no'
    const quotedYesOrNoRule = new AnyQuotedRule(yesOrNoRule); // (("'" ('yes' | 'no') "'") | ('"' ('yes' | 'no') '"'))
    this.rule = new AndRule([sRule, standaloneRule, eqRule, quotedYesOrNoRule]); // S 'standalone' Eq (("'" ('yes' | 'no') "'") | ('"' ('yes' | 'no') '"'))
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<boolean> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res[3]];
  }

  public build(data: boolean) {
    return ` standalone="${data ? 'yes' : 'no'}"`;
  }
}

export default SdDeclParser;
