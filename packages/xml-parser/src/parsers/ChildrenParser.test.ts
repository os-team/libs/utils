import { describe, it } from 'node:test';
import assert from 'node:assert';
import ChildrenParser from './ChildrenParser.js';

const childrenParser = new ChildrenParser();

describe('test', () => {
  it('Should match the choice', () => {
    const data = '_(one|two?)';
    const [isValid, nextPos, res] = childrenParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'CHOICE',
      items: ['one', ['two', '?']],
    });
  });

  it('Should match the choice with quantifier', () => {
    const data = '_(one|two?)*';
    const [isValid, nextPos, res] = childrenParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, [
      {
        type: 'CHOICE',
        items: ['one', ['two', '?']],
      },
      '*',
    ]);
  });

  it('Should match the seq', () => {
    const data = '_(one,two?)';
    const [isValid, nextPos, res] = childrenParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'SEQ',
      items: ['one', ['two', '?']],
    });
  });

  it('Should match the seq with quantifier', () => {
    const data = '_(one,two?)*';
    const [isValid, nextPos, res] = childrenParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, [
      {
        type: 'SEQ',
        items: ['one', ['two', '?']],
      },
      '*',
    ]);
  });

  it('Should match the choice with a seq', () => {
    const data = '_(one|(one,two+)?)*';
    const [isValid, nextPos, res] = childrenParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, [
      {
        type: 'CHOICE',
        items: [
          'one',
          [
            {
              type: 'SEQ',
              items: ['one', ['two', '+']],
            },
            '?',
          ],
        ],
      },
      '*',
    ]);
  });
});

describe('build', () => {
  it('Should return the choice', () => {
    const res = childrenParser.build({
      type: 'CHOICE',
      items: ['one', ['two', '?']],
    });
    assert.equal(res, '(one|two?)');
  });

  it('Should return the choice with quantifier', () => {
    const res = childrenParser.build([
      {
        type: 'CHOICE',
        items: ['one', ['two', '?']],
      },
      '*',
    ]);
    assert.equal(res, '(one|two?)*');
  });

  it('Should return the seq', () => {
    const res = childrenParser.build({
      type: 'SEQ',
      items: ['one', ['two', '?']],
    });
    assert.equal(res, '(one,two?)');
  });

  it('Should return the seq with quantifier', () => {
    const res = childrenParser.build([
      {
        type: 'SEQ',
        items: ['one', ['two', '?']],
      },
      '*',
    ]);
    assert.equal(res, '(one,two?)*');
  });

  it('Should return the choice with a seq', () => {
    const res = childrenParser.build([
      {
        type: 'CHOICE',
        items: [
          'one',
          [
            {
              type: 'SEQ',
              items: ['one', ['two', '+']],
            },
            '?',
          ],
        ],
      },
      '*',
    ]);
    assert.equal(res, '(one|(one,two+)?)*');
  });
});
