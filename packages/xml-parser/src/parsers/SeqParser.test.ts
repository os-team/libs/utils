import { describe, it } from 'node:test';
import assert from 'node:assert';
import SeqParser from './SeqParser.js';

const seqParser = new SeqParser();

describe('test', () => {
  it('Should not match because the prefix was skipped', () => {
    const data = '_one,two)';
    const [isValid, nextPos] = seqParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should not match because the suffix was skipped', () => {
    const data = '_(one,two';
    const [isValid, nextPos] = seqParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, data.length);
  });

  it('Should not match because the separator is incorrect', () => {
    const data = '_(one|two)';
    const [isValid, nextPos] = seqParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 5);
  });

  it('Should match the seq of 1 element', () => {
    const data = '_(one)';
    const [isValid, nextPos, res] = seqParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'SEQ',
      items: ['one'],
    });
  });

  it('Should match the seq of 2 elements', () => {
    const data = '_(one, two?  )';
    const [isValid, nextPos, res] = seqParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'SEQ',
      items: ['one', ['two', '?']],
    });
  });
});

describe('build', () => {
  it('Should return the seq of 1 element', () => {
    const res = seqParser.build({
      type: 'SEQ',
      items: ['one'],
    });
    assert.equal(res, '(one)');
  });

  it('Should return the seq of 2 elements', () => {
    const res = seqParser.build({
      type: 'SEQ',
      items: ['one', ['two', '?']],
    });
    assert.equal(res, '(one,two?)');
  });
});
