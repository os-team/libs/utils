import {
  AndRule,
  type DataRef,
  LiteralRule,
  RepetitionRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import SRule from '../rules/SRule.js';
import NmtokenRule from '../rules/NmtokenRule.js';
import type { Parser } from './Parser.js';

export interface Enumeration {
  type: 'ENUMERATION';
  items: string[];
}

/**
 * The enumeration attribute type declaration.
 * See https://www.w3.org/TR/xml/#NT-Enumeration
 */
class EnumerationParser implements Parser<Enumeration> {
  private readonly rule: Rule<
    [
      string,
      undefined,
      string,
      Array<[undefined, string, undefined, string]>,
      undefined,
      string,
    ]
  >;

  public constructor() {
    const prefixRule = new LiteralRule('('); // '('
    const anySRule = new SRule('*'); // S?
    const nmtokenRule = new NmtokenRule(); // Nmtoken
    const separatorRule = new LiteralRule('|'); // '|'
    const otherNmtokenRule = new AndRule([
      anySRule,
      separatorRule,
      anySRule,
      nmtokenRule,
    ]); // S? '|' S? Nmtoken
    const anyOtherNmtokenRule = new RepetitionRule(otherNmtokenRule, 0); // (S? '|' S? Nmtoken)*
    const suffixRule = new LiteralRule(')'); // ')'
    this.rule = new AndRule([
      prefixRule,
      anySRule,
      nmtokenRule,
      anyOtherNmtokenRule,
      anySRule,
      suffixRule,
    ]); // '(' S? Nmtoken (S? '|' S? Nmtoken)* S? ')'
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<Enumeration> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    const [, , token, rawTokens] = res;
    return [
      true,
      nextPos,
      {
        type: 'ENUMERATION',
        items: [token, ...rawTokens.map((item) => item[3])],
      },
    ];
  }

  public build(data: Enumeration) {
    return `(${data.items.join('|')})`;
  }
}

export default EnumerationParser;
