import { describe, it } from 'node:test';
import assert from 'node:assert';
import NotationDeclParser from './NotationDeclParser.js';

const notationDeclParser = new NotationDeclParser();

describe('test', () => {
  it('Should not match because the prefix is incorrect', () => {
    const data = '_<!NOTATIO vrml PUBLIC "VRML 1.0">';
    const [isValid, nextPos] = notationDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 10);
  });

  it('Should match the notation declaration with the public ID', () => {
    const data = '_<!NOTATION vrml PUBLIC "VRML 1.0">';
    const [isValid, nextPos, res] = notationDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'NOTATION',
      name: 'vrml',
      id: {
        type: 'PUBLIC_ID',
        pubidValue: 'VRML 1.0',
      },
    });
  });

  it('Should match the notation declaration with the system external ID', () => {
    const data = '_<!NOTATION png SYSTEM "image/png">';
    const [isValid, nextPos, res] = notationDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'NOTATION',
      name: 'png',
      id: {
        type: 'SYSTEM_EXTERNAL_ID',
        systemValue: 'image/png',
      },
    });
  });

  it('Should match the notation declaration with the public external ID', () => {
    const data =
      '_<!NOTATION XMLSchemaStructures PUBLIC "structures" "http://www.w3.org/2001/XMLSchema.xsd">';
    const [isValid, nextPos, res] = notationDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'NOTATION',
      name: 'XMLSchemaStructures',
      id: {
        type: 'PUBLIC_EXTERNAL_ID',
        pubidValue: 'structures',
        systemValue: 'http://www.w3.org/2001/XMLSchema.xsd',
      },
    });
  });
});

describe('build', () => {
  it('Should return the notation declaration with the public ID', () => {
    const res = notationDeclParser.build({
      type: 'NOTATION',
      name: 'vrml',
      id: {
        type: 'PUBLIC_ID',
        pubidValue: 'VRML 1.0',
      },
    });
    assert.equal(res, '<!NOTATION vrml PUBLIC "VRML 1.0">');
  });

  it('Should return the notation declaration with the system external ID', () => {
    const res = notationDeclParser.build({
      type: 'NOTATION',
      name: 'png',
      id: {
        type: 'SYSTEM_EXTERNAL_ID',
        systemValue: 'image/png',
      },
    });
    assert.equal(res, '<!NOTATION png SYSTEM "image/png">');
  });

  it('Should return the notation declaration with the public external ID', () => {
    const res = notationDeclParser.build({
      type: 'NOTATION',
      name: 'XMLSchemaStructures',
      id: {
        type: 'PUBLIC_EXTERNAL_ID',
        pubidValue: 'structures',
        systemValue: 'http://www.w3.org/2001/XMLSchema.xsd',
      },
    });
    assert.equal(
      res,
      '<!NOTATION XMLSchemaStructures PUBLIC "structures" "http://www.w3.org/2001/XMLSchema.xsd">'
    );
  });
});
