import { describe, it } from 'node:test';
import assert from 'node:assert';
import TokenizedTypeParser from './TokenizedTypeParser.js';

const tokenizedTypeParser = new TokenizedTypeParser();

describe('test', () => {
  it('Should not match because the literal is incorrect', () => {
    const data = '_ENTITIE';
    const [isValid, nextPos] = tokenizedTypeParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should match the tokenized type', () => {
    const data = '_ENTITIES';
    const [isValid, nextPos, res] = tokenizedTypeParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { type: 'ENTITIES' });
  });
});

describe('build', () => {
  it('Should return the tokenized type', () => {
    const res = tokenizedTypeParser.build({ type: 'ENTITIES' });
    assert.equal(res, 'ENTITIES');
  });
});
