import { describe, it } from 'node:test';
import assert from 'node:assert';
import AttTypeParser from './AttTypeParser.js';

const attTypeParser = new AttTypeParser();

describe('test', () => {
  it('Should match the string attribute type', () => {
    const data = '_CDATA';
    const [isValid, nextPos, res] = attTypeParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { type: 'CDATA' });
  });

  it('Should match the tokenized attribute type', () => {
    const data = '_ENTITIES';
    const [isValid, nextPos, res] = attTypeParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { type: 'ENTITIES' });
  });

  it('Should match the enumerated attribute type', () => {
    const data = '_(one|two|three)';
    const [isValid, nextPos, res] = attTypeParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'ENUMERATION',
      items: ['one', 'two', 'three'],
    });
  });
});

describe('build', () => {
  it('Should return the string attribute type', () => {
    const res = attTypeParser.build({ type: 'CDATA' });
    assert.equal(res, 'CDATA');
  });

  it('Should return the tokenized attribute type', () => {
    const res = attTypeParser.build({ type: 'ENTITIES' });
    assert.equal(res, 'ENTITIES');
  });

  it('Should return the enumerated attribute type', () => {
    const res = attTypeParser.build({
      type: 'ENUMERATION',
      items: ['one', 'two', 'three'],
    });
    assert.equal(res, '(one|two|three)');
  });
});
