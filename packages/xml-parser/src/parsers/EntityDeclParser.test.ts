import { describe, it } from 'node:test';
import assert from 'node:assert';
import EntityDeclParser from './EntityDeclParser.js';

const entityDeclParser = new EntityDeclParser();

describe('test', () => {
  it('Should match GEDecl', () => {
    const data =
      '_<!ENTITY unpsd PUBLIC "//this/is/a/URI/me.gif" "me.gif" NDATA TeX>';
    const [isValid, nextPos, res] = entityDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'ENTITY',
      name: 'unpsd',
      entity: {
        type: 'PUBLIC_EXTERNAL_ID',
        pubidValue: '//this/is/a/URI/me.gif',
        systemValue: 'me.gif',
        nData: 'TeX',
      },
    });
  });

  it('Should match PEDecl', () => {
    const data =
      '_<!ENTITY % xs-datatypes PUBLIC "-//W3C//DTD XSD 1.1 Datatypes//EN" "datatypes.dtd">';
    const [isValid, nextPos, res] = entityDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'ENTITY',
      name: 'xs-datatypes',
      pe: {
        type: 'PUBLIC_EXTERNAL_ID',
        pubidValue: '-//W3C//DTD XSD 1.1 Datatypes//EN',
        systemValue: 'datatypes.dtd',
      },
    });
  });
});

describe('build', () => {
  it('Should return GEDecl', () => {
    const res = entityDeclParser.build({
      type: 'ENTITY',
      name: 'unpsd',
      entity: {
        type: 'PUBLIC_EXTERNAL_ID',
        pubidValue: '//this/is/a/URI/me.gif',
        systemValue: 'me.gif',
        nData: 'TeX',
      },
    });
    assert.equal(
      res,
      '<!ENTITY unpsd PUBLIC "//this/is/a/URI/me.gif" "me.gif" NDATA TeX>'
    );
  });

  it('Should return PEDecl', () => {
    const res = entityDeclParser.build({
      type: 'ENTITY',
      name: 'xs-datatypes',
      pe: {
        type: 'PUBLIC_EXTERNAL_ID',
        pubidValue: '-//W3C//DTD XSD 1.1 Datatypes//EN',
        systemValue: 'datatypes.dtd',
      },
    });
    assert.equal(
      res,
      '<!ENTITY % xs-datatypes PUBLIC "-//W3C//DTD XSD 1.1 Datatypes//EN" "datatypes.dtd">'
    );
  });
});
