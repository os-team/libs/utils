import {
  AndRule,
  type DataRef,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import type { Parser } from './Parser.js';
import NameRule from '../rules/NameRule.js';
import EqRule from '../rules/EqRule.js';
import AttValueRule from '../rules/AttValueRule.js';

export type Attribute = [string, string]; // [name, value]

/**
 * The attribute.
 * See https://www.w3.org/TR/xml/#NT-Attribute
 */
class AttributeParser implements Parser<Attribute> {
  private readonly rule: Rule<[string, undefined, string]>;

  public constructor() {
    const nameRule = new NameRule(); // Name
    const eqRule = new EqRule(); // Eq
    const attValueRule = new AttValueRule(); // AttValue
    this.rule = new AndRule([nameRule, eqRule, attValueRule]); // Name Eq AttValue
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<Attribute> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    const [name, , value] = res;
    return [true, nextPos, [name, value]];
  }

  public build(data: Attribute) {
    const [name, value] = data;
    return `${name}="${value.replaceAll('"', "'")}"`;
  }
}

export default AttributeParser;
