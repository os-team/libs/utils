import {
  type DataRef,
  OrRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import ExternalIDParser, { type ExternalID } from './ExternalIDParser.js';
import type { Parser } from './Parser.js';
import EntityValueRule from '../rules/EntityValueRule.js';

export type PEDef = string | ExternalID;

/**
 * See https://www.w3.org/TR/xml/#NT-PEDef
 */
class PEDefParser implements Parser<PEDef> {
  private readonly externalIDParser: ExternalIDParser;

  private readonly rule: Rule<string | ExternalID>;

  public constructor() {
    const entityValueRule = new EntityValueRule(); // EntityValue
    this.externalIDParser = new ExternalIDParser(); // ExternalID
    this.rule = new OrRule([entityValueRule, this.externalIDParser]); // EntityValue | ExternalID
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<PEDef> {
    const [isValid, nextPos, res] = this.rule.test(ref, pos);
    if (!isValid || res === undefined) return [false, nextPos];
    return [true, nextPos, res];
  }

  public build(data: PEDef) {
    return typeof data === 'string'
      ? `"${data.replaceAll('"', "'")}"`
      : this.externalIDParser.build(data);
  }
}

export default PEDefParser;
