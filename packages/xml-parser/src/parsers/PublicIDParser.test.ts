import { describe, it } from 'node:test';
import assert from 'node:assert';
import PublicIDParser from './PublicIDParser.js';

const publicIDParser = new PublicIDParser();

describe('test', () => {
  it('Should not match because the prefix is incorrect', () => {
    const data = '_PUBLI "VRML 1.0"';
    const [isValid, nextPos] = publicIDParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 6);
  });

  it('Should match the public ID', () => {
    const data = '_PUBLIC "VRML 1.0"';
    const [isValid, nextPos, res] = publicIDParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'PUBLIC_ID',
      pubidValue: 'VRML 1.0',
    });
  });
});

describe('build', () => {
  it('Should return the public ID', () => {
    const res = publicIDParser.build({
      type: 'PUBLIC_ID',
      pubidValue: 'VRML 1.0',
    });
    assert.equal(res, 'PUBLIC "VRML 1.0"');
  });
});
