import { describe, it } from 'node:test';
import assert from 'node:assert';
import EnumeratedTypeParser from './EnumeratedTypeParser.js';

const enumeratedTypeParser = new EnumeratedTypeParser();

describe('test', () => {
  it('Should match the notation attribute type declaration', () => {
    const data = '_NOTATION (one|two|three)';
    const [isValid, nextPos, res] = enumeratedTypeParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'NOTATION',
      items: ['one', 'two', 'three'],
    });
  });

  it('Should match the enumeration attribute type declaration', () => {
    const data = '_(one|two|three)';
    const [isValid, nextPos, res] = enumeratedTypeParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'ENUMERATION',
      items: ['one', 'two', 'three'],
    });
  });
});

describe('build', () => {
  it('Should return the notation attribute type declaration', () => {
    const res = enumeratedTypeParser.build({
      type: 'NOTATION',
      items: ['one', 'two', 'three'],
    });
    assert.equal(res, 'NOTATION (one|two|three)');
  });

  it('Should match the enumeration attribute type declaration', () => {
    const res = enumeratedTypeParser.build({
      type: 'ENUMERATION',
      items: ['one', 'two', 'three'],
    });
    assert.equal(res, '(one|two|three)');
  });
});
