import { describe, it } from 'node:test';
import assert from 'node:assert';
import ContentParser from './ContentParser.js';

const contentParser = new ContentParser();

describe('test', () => {
  it('Should match a string', () => {
    const data = '_abc';
    const [isValid, nextPos, res] = contentParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, 'abc');
  });

  it('Should match a number', () => {
    const data = '_123';
    const [isValid, nextPos, res] = contentParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, 123);
  });

  it('Should match a boolean (true)', () => {
    const data = '_true';
    const [isValid, nextPos, res] = contentParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, true);
  });

  it('Should match a boolean (false)', () => {
    const data = '_false';
    const [isValid, nextPos, res] = contentParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, false);
  });

  it('Should match a null', () => {
    const data = '_null';
    const [isValid, nextPos, res] = contentParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, null);
  });

  it('Should match an undefined', () => {
    const data = '_undefined';
    const [isValid, nextPos, res] = contentParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, '');
  });

  it('Should match the element', () => {
    const data = '_<tag>abc</tag>';
    const [isValid, nextPos, res] = contentParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { tag: 'abc' });
  });

  it('Should match the reference', () => {
    const data = '_&entity;';
    const [isValid, nextPos, res] = contentParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, '&entity;');
  });

  it('Should match the CDATA section', () => {
    const data = '_<![CDATA[<sender>John Smith</sender>]]>';
    const [isValid, nextPos, res] = contentParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, '<sender>John Smith</sender>');
  });

  it('Should skip the processing instruction because the include option is not specified', () => {
    const data = '_<?xml-stylesheet type="text/xsl" href="style.xsl"?>';
    const [isValid, nextPos, res] = contentParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, '');
  });

  it('Should match the processing instruction', () => {
    const data = '_<?xml-stylesheet type="text/xsl" href="style.xsl"?>';
    const [isValid, nextPos, res] = contentParser.test(
      { data, options: { include: 'PROLOG' } },
      1
    );
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      '?xml-stylesheet': 'type="text/xsl" href="style.xsl"',
    });
  });

  it('Should match the comment', () => {
    const data = '_<!-- Comment -->';
    const [isValid, nextPos, res] = contentParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, '');
  });

  it('Should match multiple types of content', () => {
    const data =
      '_abc<tag>abc</tag>&entity;<![CDATA[<sender>John Smith</sender>]]><?xml-stylesheet type="text/xsl" href="style.xsl"?><!-- Comment -->';
    const [isValid, nextPos, res] = contentParser.test(
      { data, options: { include: 'PROLOG' } },
      1
    );
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      tag: 'abc',
      '?xml-stylesheet': 'type="text/xsl" href="style.xsl"',
      '#content': 'abc&entity;<sender>John Smith</sender>',
    });
  });

  it('Should match an array of the same elements', () => {
    const data = '_<item>item1</item><item>item2</item>';
    const [isValid, nextPos, res] = contentParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { item: ['item1', 'item2'] });
  });

  it('Should match an array with one element (isArray is an array)', () => {
    const data = '_<item>item1</item>';
    const [isValid, nextPos, res] = contentParser.test(
      { data, options: { isArray: ['item'] } },
      1
    );
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { item: ['item1'] });
  });

  it('Should match an array with one element (isArray is a function)', () => {
    const data = '_<item>item1</item>';
    const [isValid, nextPos, res] = contentParser.test(
      { data, options: { isArray: (name) => name === 'item' } },
      1
    );
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { item: ['item1'] });
  });
});

describe('build', () => {
  it('Should return a string', () => {
    const res = contentParser.build('abc');
    assert.equal(res, 'abc');
  });

  it('Should return a number', () => {
    const res = contentParser.build(123);
    assert.equal(res, '123');
  });

  it('Should return a boolean (true)', () => {
    const res = contentParser.build(true);
    assert.equal(res, 'true');
  });

  it('Should return a boolean (false)', () => {
    const res = contentParser.build(false);
    assert.equal(res, 'false');
  });

  it('Should return a null', () => {
    const res = contentParser.build(null);
    assert.equal(res, 'null');
  });

  it('Should return an undefined', () => {
    const res = contentParser.build(undefined);
    assert.equal(res, '');
  });

  it('Should return the element', () => {
    const res = contentParser.build({ tag: 'abc' });
    assert.equal(res, '<tag>abc</tag>');
  });

  it('Should return the reference', () => {
    const res = contentParser.build('&entity;');
    assert.equal(res, '&entity;');
  });

  it('Should return the CDATA section', () => {
    const res = contentParser.build('<sender>John Smith</sender>');
    assert.equal(res, '&lt;sender&gt;John Smith&lt;/sender&gt;');
  });

  it('Should return the processing instruction', () => {
    const res = contentParser.build({
      '?xml-stylesheet': 'type="text/xsl" href="style.xsl"',
    });
    assert.equal(res, '<?xml-stylesheet type="text/xsl" href="style.xsl"?>');
  });

  it('Should return multiple types of content', () => {
    const res = contentParser.build({
      tag: 'abc',
      '?xml-stylesheet': 'type="text/xsl" href="style.xsl"',
      '#content': 'abc&entity;<sender>John Smith</sender>',
    });
    assert.equal(
      res,
      'abc&entity;&lt;sender&gt;John Smith&lt;/sender&gt;<tag>abc</tag><?xml-stylesheet type="text/xsl" href="style.xsl"?>'
    );
  });

  it('Should return an array of the same elements', () => {
    const res = contentParser.build({ item: ['item1', 'item2'] });
    assert.equal(res, '<item>item1</item><item>item2</item>');
  });
});
