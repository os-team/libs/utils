import { describe, it } from 'node:test';
import assert from 'node:assert';
import PrologParser from './PrologParser.js';

const prologParser = new PrologParser();

describe('test', () => {
  it('Should match only the xml declaration', () => {
    const data = '_<?xml version="1.0" encoding="UTF-8"?>';
    const [isValid, nextPos, res] = prologParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      xml: {
        version: '1.0',
        encoding: 'UTF-8',
      },
    });
  });

  it('Should match only the misc', () => {
    const data = '_<?xml-stylesheet type="text/xsl" href="style.xsl"?>';
    const [isValid, nextPos, res] = prologParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      misc: [['PI', 'xml-stylesheet', 'type="text/xsl" href="style.xsl"']],
    });
  });

  it('Should match only the document type declaration', () => {
    const data = '_<!DOCTYPE html>';
    const [isValid, nextPos, res] = prologParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      doctype: {
        name: 'html',
      },
    });
  });

  it('Should match the complex prolog', () => {
    const data =
      '_<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="style.xsl"?><!DOCTYPE html><!-- Comment -->';
    const [isValid, nextPos, res] = prologParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      xml: {
        version: '1.0',
        encoding: 'UTF-8',
      },
      misc: [['PI', 'xml-stylesheet', 'type="text/xsl" href="style.xsl"']],
      doctype: {
        name: 'html',
      },
    });
  });
});

describe('build', () => {
  it('Should return only the xml declaration', () => {
    const res = prologParser.build({
      xml: {
        version: '1.0',
        encoding: 'UTF-8',
      },
    });
    assert.equal(res, '<?xml version="1.0" encoding="UTF-8"?>');
  });

  it('Should return only the misc', () => {
    const res = prologParser.build({
      misc: [['PI', 'xml-stylesheet', 'type="text/xsl" href="style.xsl"']],
    });
    assert.equal(res, '<?xml-stylesheet type="text/xsl" href="style.xsl"?>');
  });

  it('Should return only the document type declaration', () => {
    const res = prologParser.build({
      doctype: {
        name: 'html',
      },
    });
    assert.equal(res, '<!DOCTYPE html>');
  });

  it('Should return only the complex prolog', () => {
    const res = prologParser.build({
      xml: {
        version: '1.0',
        encoding: 'UTF-8',
      },
      misc: [['PI', 'xml-stylesheet', 'type="text/xsl" href="style.xsl"']],
      doctype: {
        name: 'html',
      },
    });
    assert.equal(
      res,
      '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="style.xsl"?><!DOCTYPE html>'
    );
  });
});
