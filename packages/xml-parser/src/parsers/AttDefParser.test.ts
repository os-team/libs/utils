import { describe, it } from 'node:test';
import assert from 'node:assert';
import AttDefParser from './AttDefParser.js';

const attDefParser = new AttDefParser();

describe('test', () => {
  it('Should match the attribute definition', () => {
    const data = '_ name (one|two|three) #REQUIRED';
    const [isValid, nextPos, res] = attDefParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      name: 'name',
      type: {
        type: 'ENUMERATION',
        items: ['one', 'two', 'three'],
      },
      default: {
        type: 'REQUIRED',
      },
    });
  });
});

describe('build', () => {
  it('Should return the attribute definition', () => {
    const res = attDefParser.build({
      name: 'name',
      type: {
        type: 'ENUMERATION',
        items: ['one', 'two', 'three'],
      },
      default: {
        type: 'REQUIRED',
      },
    });
    assert.equal(res, ' name (one|two|three) #REQUIRED');
  });
});
