import { describe, it } from 'node:test';
import assert from 'node:assert';
import DefaultDeclParser from './DefaultDeclParser.js';

const defaultDeclParser = new DefaultDeclParser();

describe('test', () => {
  it('Should not match because the literal is incorrect', () => {
    const data = '_#REQUIRE';
    const [isValid, nextPos] = defaultDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should not match because the attribute value was skipped', () => {
    const data = '_#FIXED';
    const [isValid, nextPos] = defaultDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should match the required attribute default declaration', () => {
    const data = '_#REQUIRED';
    const [isValid, nextPos, res] = defaultDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { type: 'REQUIRED' });
  });

  it('Should match the implied attribute default declaration', () => {
    const data = '_#IMPLIED';
    const [isValid, nextPos, res] = defaultDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { type: 'IMPLIED' });
  });

  it('Should match the fixed attribute default declaration', () => {
    const data = '_#FIXED "4.0"';
    const [isValid, nextPos, res] = defaultDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { type: 'FIXED', attValue: '4.0' });
  });

  it('Should match the fixed attribute default declaration without the prefix', () => {
    const data = '_"4.0"';
    const [isValid, nextPos, res] = defaultDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { type: 'FIXED', attValue: '4.0' });
  });
});

describe('build', () => {
  it('Should return the required attribute default declaration', () => {
    const res = defaultDeclParser.build({ type: 'REQUIRED' });
    assert.equal(res, '#REQUIRED');
  });

  it('Should return the implied attribute default declaration', () => {
    const res = defaultDeclParser.build({ type: 'IMPLIED' });
    assert.equal(res, '#IMPLIED');
  });

  it('Should return the fixed attribute default declaration', () => {
    const res = defaultDeclParser.build({ type: 'FIXED', attValue: '4."0' });
    assert.equal(res, '#FIXED "4.\'0"');
  });
});
