import { describe, it } from 'node:test';
import assert from 'node:assert';
import EncodingDeclParser from './EncodingDeclParser.js';

const encodingDeclParser = new EncodingDeclParser();

describe('test', () => {
  it('Should not match because the first character is not a white space', () => {
    const data = '_encoding="UTF-8"';
    const [isValid, nextPos] = encodingDeclParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should match the encoding name in quotes', () => {
    const data = '_ encoding="UTF-8"';
    const [isValid, nextPos, res] = encodingDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, 'UTF-8');
  });

  it('Should match the encoding name in apostrophes', () => {
    const data = "_ encoding='UTF-8'";
    const [isValid, nextPos, res] = encodingDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, 'UTF-8');
  });

  it('Should match the encoding name regardless of many white spaces', () => {
    const data = '_  encoding  = "UTF-8"';
    const [isValid, nextPos, res] = encodingDeclParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, 'UTF-8');
  });
});

describe('build', () => {
  it('Should throw the error because the encoding name is invalid', () => {
    assert.throws(() => encodingDeclParser.build('1'));
  });

  it('Should return the encoding name in the XML format', () => {
    const res = encodingDeclParser.build('UTF-8');
    assert.equal(res, ' encoding="UTF-8"');
  });
});
