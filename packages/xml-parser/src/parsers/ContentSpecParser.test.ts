import { describe, it } from 'node:test';
import assert from 'node:assert';
import ContentSpecParser from './ContentSpecParser.js';

const contentSpecParser = new ContentSpecParser();

describe('test', () => {
  it('Should match the empty content of an element', () => {
    const data = '_EMPTY';
    const [isValid, nextPos, res] = contentSpecParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { type: 'EMPTY' });
  });

  it('Should match the any content of an element', () => {
    const data = '_ANY';
    const [isValid, nextPos, res] = contentSpecParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, { type: 'ANY' });
  });

  it('Should match the mixed content of an element', () => {
    const data = '_(#PCDATA|m|n)*';
    const [isValid, nextPos, res] = contentSpecParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'MIXED',
      items: ['m', 'n'],
    });
  });

  it('Should match the custom content of an element', () => {
    const data = '_((one)?, (two)?)';
    const [isValid, nextPos, res] = contentSpecParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, {
      type: 'SEQ',
      items: [
        [
          {
            type: 'SEQ',
            items: ['one'],
          },
          '?',
        ],
        [
          {
            type: 'SEQ',
            items: ['two'],
          },
          '?',
        ],
      ],
    });
  });
});

describe('build', () => {
  it('Should return the empty content of an element', () => {
    const res = contentSpecParser.build({ type: 'EMPTY' });
    assert.equal(res, 'EMPTY');
  });

  it('Should return the any content of an element', () => {
    const res = contentSpecParser.build({ type: 'ANY' });
    assert.equal(res, 'ANY');
  });

  it('Should return the mixed content of an element', () => {
    const res = contentSpecParser.build({
      type: 'MIXED',
      items: ['m', 'n'],
    });
    assert.equal(res, '(#PCDATA|m|n)*');
  });

  it('Should return the custom content of an element', () => {
    const res = contentSpecParser.build({
      type: 'SEQ',
      items: [
        [
          {
            type: 'SEQ',
            items: ['one'],
          },
          '?',
        ],
        [
          {
            type: 'SEQ',
            items: ['two'],
          },
          '?',
        ],
      ],
    });
    assert.equal(res, '((one)?,(two)?)');
  });
});
