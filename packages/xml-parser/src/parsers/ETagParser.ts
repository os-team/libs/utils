import {
  AndRule,
  type DataRef,
  LiteralRule,
  type Rule,
  type RuleTestResponse,
} from '@os-team/lexical-rules';
import type { Parser } from './Parser.js';
import SRule from '../rules/SRule.js';

/**
 * The end-tag for an element.
 * See https://www.w3.org/TR/xml/#NT-ETag
 */
class ETagParser implements Parser<undefined> {
  private readonly name: string;

  private readonly rule: Rule<[string, string, undefined, string]>;

  public constructor(name: string) {
    this.name = name;
    const prefixRule = new LiteralRule('</'); // '</'
    const nameRule = new LiteralRule(name); // Name
    const anySRule = new SRule('*'); // S?
    const suffixRule = new LiteralRule('>'); // '>'
    this.rule = new AndRule([prefixRule, nameRule, anySRule, suffixRule]); // '</' Name S? '>'
  }

  public test(ref: DataRef, pos: number): RuleTestResponse<undefined> {
    const [isValid, nextPos] = this.rule.test(ref, pos);
    if (!isValid) return [false, nextPos];
    return [true, nextPos, undefined];
  }

  public build() {
    return `</${this.name}>`;
  }
}

export default ETagParser;
