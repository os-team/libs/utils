import { describe, it } from 'node:test';
import assert from 'node:assert';
import VersionInfoParser from './VersionInfoParser.js';

const versionInfoParser = new VersionInfoParser();

describe('test', () => {
  it('Should not match because the first character is not a white space', () => {
    const data = '_version="1.0"';
    const [isValid, nextPos] = versionInfoParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 1);
  });

  it('Should match the version number in quotes', () => {
    const data = '_ version="1.0"';
    const [isValid, nextPos, res] = versionInfoParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, '1.0');
  });

  it('Should match the version number in apostrophes', () => {
    const data = "_ version='1.0'";
    const [isValid, nextPos, res] = versionInfoParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, '1.0');
  });

  it('Should match the version number regardless of many white spaces', () => {
    const data = '_  version  = "1.1"';
    const [isValid, nextPos, res] = versionInfoParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.equal(res, '1.1');
  });
});

describe('build', () => {
  it('Should throw the error because the version number is invalid', () => {
    assert.throws(() => versionInfoParser.build('2.0'));
  });

  it('Should return the version number in the XML format', () => {
    const res = versionInfoParser.build('1.1');
    assert.equal(res, ' version="1.1"');
  });
});
