import { describe, it } from 'node:test';
import assert from 'node:assert';
import CDSectParser from './CDSectParser.js';

const cdSectParser = new CDSectParser();

describe('test', () => {
  it('Should not match because the prefix is incorrect', () => {
    const data = '_<![CDATA<sender>John Smith</sender>]]>';
    const [isValid, nextPos] = cdSectParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, 9);
  });

  it('Should not match because the suffix is incorrect', () => {
    const data = '_<![CDATA[<sender>John Smith</sender>]]';
    const [isValid, nextPos] = cdSectParser.test({ data }, 1);
    assert.equal(isValid, false);
    assert.equal(nextPos, data.length);
  });

  it('Should match the CDATA section', () => {
    const data = '_<![CDATA[<sender>John Smith</sender>]]>';
    const [isValid, nextPos, res] = cdSectParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, '<sender>John Smith</sender>');
  });
});

describe('build', () => {
  it('Should return the CDATA section', () => {
    const res = cdSectParser.build('<sender>John Smith</sender>');
    assert.equal(res, '<![CDATA[<sender>John Smith</sender>]]>');
  });
});
