import { describe, it } from 'node:test';
import assert from 'node:assert';
import IntSubsetParser from './IntSubsetParser.js';

const intSubsetParser = new IntSubsetParser();

describe('test', () => {
  it('Should match multiple markup declarations', () => {
    const data =
      '_<!ELEMENT p (#PCDATA|m|n)*>\n<!ATTLIST abc name (one|two|three) #REQUIRED>';
    const [isValid, nextPos, res] = intSubsetParser.test({ data }, 1);
    assert.equal(isValid, true);
    assert.equal(nextPos, data.length);
    assert.deepStrictEqual(res, [
      {
        type: 'ELEMENT',
        name: 'p',
        content: {
          type: 'MIXED',
          items: ['m', 'n'],
        },
      },
      {
        type: 'ATTLIST',
        name: 'abc',
        items: [
          {
            name: 'name',
            type: {
              type: 'ENUMERATION',
              items: ['one', 'two', 'three'],
            },
            default: {
              type: 'REQUIRED',
            },
          },
        ],
      },
    ]);
  });
});

describe('build', () => {
  it('Should return multiple markup declarations', () => {
    const res = intSubsetParser.build([
      {
        type: 'ELEMENT',
        name: 'p',
        content: {
          type: 'MIXED',
          items: ['m', 'n'],
        },
      },
      {
        type: 'ATTLIST',
        name: 'abc',
        items: [
          {
            name: 'name',
            type: {
              type: 'ENUMERATION',
              items: ['one', 'two', 'three'],
            },
            default: {
              type: 'REQUIRED',
            },
          },
        ],
      },
    ]);
    assert.equal(
      res,
      '<!ELEMENT p (#PCDATA|m|n)*><!ATTLIST abc name (one|two|three) #REQUIRED>'
    );
  });
});
