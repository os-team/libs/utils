import DocumentParser, { type Document } from './parsers/DocumentParser.js';
import { type ParseOptions } from './parsers/Parser.js';

export type { Document } from './parsers/DocumentParser.js';

const documentParser = new DocumentParser();

export const parseXml = <T extends Document>(
  xml: string,
  options: ParseOptions = {}
): T => {
  const [isValid, nextPos, res] = documentParser.test(
    { data: xml, options },
    0
  );

  if (!isValid || nextPos < xml.length - 1 || res === undefined) {
    const message =
      nextPos < xml.length
        ? `Unexpected token ${xml[nextPos]} in XML at position ${nextPos}`
        : `The XML is not well-formed`;
    throw new Error(message);
  }

  return res as T;
};

export const buildXml = (data: Document) => documentParser.build(data);
