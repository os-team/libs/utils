# @os-team/invoice [![NPM version](https://img.shields.io/npm/v/@os-team/invoice)](https://yarnpkg.com/package/@os-team/invoice) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/invoice)](https://bundlephobia.com/result?p=@os-team/invoice)

Generates an invoice and a certificate of completion in Russian as a PDF.

## Usage

### Install the package

Install the package using the following command:

```
yarn add @os-team/invoice
```

### Generating an invoice

```ts
import path from 'path';
import fs from 'node:fs/promises';
import { generateInvoice } from '@os-team/invoice';

const invoice = await generateInvoice({
  bankDetails: {
    beneficiaryBank: {
      name: 'АО «ТБанк»',
      bic: '044525974',
      correspondentAccount: '30101810145250000974',
    },
    beneficiary: {
      name: 'ООО «Название»',
      tin: '0000000000',
      rrc: '000000000',
      iban: '00000000000000000000',
    },
    purpose: `Оплата по счету ${number} от ${formattedDate} за лицензию на использование ПО для ЭВМ "Название". Без НДС.`,
  },
  number,
  date,
  participants: {
    seller: {
      title: 'Лицензиар',
      value:
        'ООО «Название», ИНН 0000000000, КПП 000000000, 190031, г. Санкт-Петербург, Московский проспект, д. 1',
    },
    buyer: {
      title: 'Лицензиат',
      value:
        'ИП Иванов Иван Иванович, ИНН 000000000000, 191186, г. Санкт-Петербург, ул. Гороховая, д. 6 литера А',
    },
  },
  products: [
    {
      name: 'Лицензия на ПО для ЭВМ "Название" на 12 месяцев за 1 пользователя.',
      quantity: 10,
      unit: 'шт',
      price: 1190000,
    },
    {
      name: 'Дополнительное пространство на диске 50 GB.',
      quantity: 1,
      unit: 'шт',
      price: 149000,
    },
  ],
  signatures: {
    director: {
      name: 'Петров А.Н.',
      position: 'Директор',
      image: {
        path: path.resolve(__dirname, './signatures/signature1.png'),
        position: { x: 0, y: -20 },
      },
      width: 80,
    },
    accountant: {
      name: 'Иванов Д.Ю.',
      position: 'Бухгалтер',
      image: {
        path: path.resolve(__dirname, './signatures/signature2.png'),
        position: { x: 0, y: -35 },
      },
      width: 80,
    },
  },
  after: {
    image: path.resolve(__dirname, './signatures/stamp.png'),
    width: 100,
    relativePosition: { x: 0, y: -50 },
    alignment: 'center',
  },
});

const filePath = path.resolve(__dirname, './invoice.pdf');
await fs.writeFile(filePath, invoice);
```

[Generated PDF file](src/pdfs/invoice.pdf)

### Generating a certificate of completion

```ts
const completionCertificate = await generateCompletionCertificate({
  number,
  date,
  participants: {
    seller: {
      title: 'Лицензиар',
      value:
        'ООО «Название», ИНН 0000000000, КПП 000000000, 190031, г. Санкт-Петербург, Московский проспект, д. 1',
    },
    buyer: {
      title: 'Лицензиат',
      value:
        'ИП Иванов Иван Иванович, ИНН 000000000000, 191186, г. Санкт-Петербург, ул. Гороховая, д. 6 литера А',
    },
  },
  basis: `счет-оферта № ${number} от ${formatDate(date)}`,
  products: [
    {
      name: 'Лицензия на ПО для ЭВМ "Название" на 12 месяцев за 1 пользователя.',
      quantity: 10,
      unit: 'шт',
      price: 1190000,
    },
    {
      name: 'Дополнительное пространство на диске 50 GB.',
      quantity: 1,
      unit: 'шт',
      price: 149000,
    },
  ],
  signatures: {
    seller: {
      name: 'Петров А.Н.',
      position: 'Директор',
      width: 80,
    },
    buyer: {
      name: 'Иванов И.И.',
      position: 'Руководитель',
      width: 80,
    },
  },
});

const filePath = path.resolve(__dirname, './completionCertificate.pdf');
await fs.writeFile(filePath, completionCertificate);
```

[Generated PDF file](src/pdfs/completionCertificate.pdf)
