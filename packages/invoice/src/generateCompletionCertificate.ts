import type { Content, TDocumentDefinitions } from 'pdfmake/interfaces.d.ts';
import PdfPrinter from 'pdfmake';
import { buffer } from 'node:stream/consumers';
import formatDate from './utils/formatDate.js';
import fonts from './fonts.js';
import getProductsTable from './utils/getProductsTable.js';

export interface Participant {
  title: string;
  value: string;
}

export interface Participants {
  seller: Participant;
  buyer: Participant;
}

export interface Product {
  name: string;
  quantity: number;
  unit: string;
  price: number;
}

export interface SignaturePosition {
  x?: number;
  y?: number;
}

export interface SignatureImage {
  path: string;
  position?: SignaturePosition;
}

export interface Signature {
  name: string;
  position: string;
  image?: SignatureImage;
  width?: number;
}

export interface Signatures {
  buyer: Signature;
  seller: Signature;
}

interface CompletionCertificateData {
  number: number;
  date: Date;
  participants: Participants;
  basis?: string;
  products: Product[];
  signatures: Signatures;
  vat?: number; // Percent
  before?: Content;
  beforeSignature?: Content;
  after?: Content;
}

const FONT_SIZE = 10;
const SIGNATURE_WIDTH = 150;

const generateCompletionCertificate = (data: CompletionCertificateData) => {
  const documentDefinition: TDocumentDefinitions = {
    content: [
      {
        text: `Акт сдачи-приемки № ${data.number} от ${formatDate(data.date)}`,
        style: 'title',
      },
      ...(data.before ? [data.before] : []),
      {
        table: {
          body: [
            [
              `${data.participants.seller.title}:`,
              data.participants.seller.value,
            ],
            [
              `${data.participants.buyer.title}:`,
              data.participants.buyer.value,
            ],
            ...(data.basis ? [[`Основание:`, data.basis]] : []),
          ],
        },
        layout: {
          hLineWidth: () => 0,
          vLineWidth: () => 0,
          paddingTop: () => 5,
          paddingBottom: () => 5,
        },
        style: 'sellerBuyerTable',
      },
      getProductsTable({
        products: data.products,
        vat: data.vat,
      }),
      {
        text: `Все вышеперечисленные услуги выполнены полностью и в срок. ${data.participants.buyer.title} претензий по объему, качеству и срокам оказания услуг не имеет.`,
        style: 'paragraph',
      },
      ...(data.beforeSignature ? [data.beforeSignature] : []),
      {
        columns: [
          {
            stack: [
              {
                text: data.participants.seller.title,
                bold: true,
              },
              `${data.signatures.seller.position} ${data.signatures.seller.name}`,
              {
                stack: [
                  {
                    canvas: [
                      {
                        type: 'line',
                        x1: 0,
                        y1: FONT_SIZE,
                        x2: data.signatures.seller.width || SIGNATURE_WIDTH,
                        y2: FONT_SIZE,
                        lineWidth: 1,
                        lineColor: 'black',
                      },
                    ],
                  },
                  ...(data.signatures.seller.image
                    ? [
                        {
                          image: data.signatures.seller.image.path,
                          width:
                            data.signatures.seller.width || SIGNATURE_WIDTH,
                          relativePosition:
                            data.signatures.seller.image.position,
                        },
                      ]
                    : []),
                ],
                marginTop: 50,
              },
            ],
            width: '*',
          },
          {
            stack: [
              {
                text: data.participants.buyer.title,
                bold: true,
              },
              `${data.signatures.buyer.position} ${data.signatures.buyer.name}`,
              {
                stack: [
                  {
                    canvas: [
                      {
                        type: 'line',
                        x1: 0,
                        y1: FONT_SIZE,
                        x2: data.signatures.buyer.width || SIGNATURE_WIDTH,
                        y2: FONT_SIZE,
                        lineWidth: 1,
                        lineColor: 'black',
                      },
                    ],
                  },
                  ...(data.signatures.buyer.image
                    ? [
                        {
                          image: data.signatures.buyer.image.path,
                          width: data.signatures.buyer.width || SIGNATURE_WIDTH,
                          relativePosition:
                            data.signatures.buyer.image.position,
                        },
                      ]
                    : []),
                ],
                marginTop: 50,
              },
            ],
            width: '*',
          },
        ],
        style: 'signatures',
      },
      ...(data.after ? [data.after] : []),
    ],
    defaultStyle: {
      fontSize: FONT_SIZE,
    },
    styles: {
      title: {
        fontSize: 18,
        bold: true,
        alignment: 'center',
      },
      paragraph: {
        marginTop: 15,
      },
      sellerBuyerTable: {
        marginTop: 10,
      },
      productsTable: {
        marginTop: 10,
      },
      signatures: {
        columnGap: 10,
        marginTop: 50,
      },
    },
  };

  const printer = new PdfPrinter(fonts);
  const pdfDoc = printer.createPdfKitDocument(documentDefinition);
  pdfDoc.end();

  return buffer(pdfDoc);
};

export default generateCompletionCertificate;
