import type { Content, TDocumentDefinitions } from 'pdfmake/interfaces.d.ts';
import PdfPrinter from 'pdfmake';
import { buffer } from 'node:stream/consumers';
import withSpaces from './utils/withSpaces.js';
import formatDate from './utils/formatDate.js';
import fonts from './fonts.js';
import getProductsTable, { type Product } from './utils/getProductsTable.js';

export interface BeneficiaryBank {
  name: string;
  bic: string;
  correspondentAccount: string;
}

export interface Beneficiary {
  name: string;
  tin: string; // INN
  rrc?: string; // KPP
  iban: string;
}

export interface BankDetails {
  beneficiaryBank: BeneficiaryBank;
  beneficiary: Beneficiary;
  purpose: string;
}

export interface Participant {
  title: string;
  value: string;
}

export interface Participants {
  seller: Participant;
  buyer: Participant;
}

export interface SignaturePosition {
  x?: number;
  y?: number;
}

export interface SignatureImage {
  path: string;
  position?: SignaturePosition;
}

export interface Signature {
  name: string;
  position: string;
  image?: SignatureImage;
  width?: number;
}

export interface Signatures {
  director: Signature;
  accountant: Signature;
}

export interface InvoiceData {
  title?: string;
  bankDetails: BankDetails;
  number: string | number;
  date: Date;
  participants: Participants;
  products: Product[];
  signatures: Signatures;
  vat?: number; // Percent
  before?: Content;
  beforeSignature?: Content;
  after?: Content;
}

const FONT_SIZE = 10;
const SIGNATURE_WIDTH = 80;

const generateInvoice = async (data: InvoiceData) => {
  const price = data.products.reduce(
    (acc, product) => acc + product.price * product.quantity,
    0
  );
  const vat = price * ((data.vat || 0) / 100);
  const totalPrice = price + vat;

  const qrData = {
    Name: data.bankDetails.beneficiary.name,
    PersonalAcc: data.bankDetails.beneficiary.iban,
    BankName: data.bankDetails.beneficiaryBank.name,
    BIC: data.bankDetails.beneficiaryBank.bic,
    CorrespAcc: data.bankDetails.beneficiaryBank.correspondentAccount,
    Sum: totalPrice,
    PayeeINN: data.bankDetails.beneficiary.tin,
    KPP: data.bankDetails.beneficiary.rrc,
    Purpose: data.bankDetails.purpose,
  };
  const qrString = `ST00012|${Object.entries(qrData)
    .filter(([, value]) => value !== undefined)
    .map(([key, value]) => `${key}=${value}`)
    .join('|')}`;

  const documentDefinition: TDocumentDefinitions = {
    content: [
      ...(data.before ? [data.before] : []),
      {
        table: {
          body: [
            [
              {
                text: data.bankDetails.beneficiaryBank.name,
                colSpan: 2,
                border: [true, true, true, false],
              },
              '',
              'БИК',
              {
                text: withSpaces(data.bankDetails.beneficiaryBank.bic, [4, 2]),
                noWrap: true,
                border: [true, true, true, false],
              },
              {
                qr: qrString,
                fit: 100,
                rowSpan: 5,
              },
            ],
            [
              {
                text: 'БАНК ПОЛУЧАТЕЛЯ',
                colSpan: 2,
                border: [true, false, true, true],
                style: 'hint',
              },
              '',
              { text: 'К/С №', noWrap: true },
              {
                text: withSpaces(
                  data.bankDetails.beneficiaryBank.correspondentAccount,
                  [3, 2, 3, 1, 4]
                ),
                noWrap: true,
                border: [true, false, true, true],
              },
              '',
            ],
            [
              {
                text: `ИНН ${data.bankDetails.beneficiary.tin}`,
                noWrap: true,
              },
              {
                text: `КПП ${data.bankDetails.beneficiary.rrc || ''}`,
                noWrap: true,
              },
              {
                text: 'Р/С №',
                noWrap: true,
                border: [true, true, true, false],
              },
              {
                text: withSpaces(
                  data.bankDetails.beneficiary.iban,
                  [3, 2, 3, 1, 4]
                ),
                noWrap: true,
                border: [true, true, true, false],
              },
              '',
            ],
            [
              {
                text: data.bankDetails.beneficiary.name,
                colSpan: 2,
                border: [true, true, true, false],
              },
              '',
              { text: '', border: [true, false, true, false] },
              { text: '', border: [true, false, true, false] },
              '',
            ],
            [
              {
                text: 'ПОЛУЧАТЕЛЬ',
                colSpan: 2,
                border: [true, false, true, true],
                style: 'hint',
              },
              '',
              { text: '', border: [true, false, true, true] },
              { text: '', border: [true, false, true, true] },
              '',
            ],
            [
              {
                text: data.bankDetails.purpose,
                colSpan: 5,
                border: [true, true, true, false],
              },
              '',
              '',
              '',
              '',
            ],
            [
              {
                text: 'НАЗНАЧЕНИЕ ПЛАТЕЖА',
                colSpan: 5,
                border: [true, false, true, true],
                style: 'hint',
              },
              '',
              '',
              '',
              '',
            ],
          ],
          widths: ['auto', 'auto', 'auto', '*', 'auto'],
        },
        layout: {
          hLineWidth: (i) => ([0, 2, 5, 7].includes(i) ? 2 : 1),
          vLineWidth: (i) => ([0, 4, 5].includes(i) ? 2 : 1),
        },
      },
      {
        text: `${data.title ? data.title : 'Счет на оплату'} № ${data.number} от ${formatDate(data.date)}`,
        style: 'title',
      },
      {
        table: {
          body: [
            [
              `${data.participants.seller.title}:`,
              data.participants.seller.value,
            ],
            [
              `${data.participants.buyer.title}:`,
              data.participants.buyer.value,
            ],
          ],
        },
        layout: {
          // hLineWidth: (i) => (i === 0 ? 2 : 0),
          hLineWidth: () => 0,
          vLineWidth: () => 0,
          paddingTop: () => 5,
          paddingBottom: () => 5,
        },
        style: 'sellerBuyerTable',
      },
      getProductsTable({
        products: data.products,
        vat: data.vat,
      }),
      ...(data.beforeSignature ? [data.beforeSignature] : []),
      {
        columns: [
          {
            columns: [
              {
                text: data.signatures.director.position,
                fontSize: FONT_SIZE,
                width: 'auto',
              },
              {
                stack: [
                  {
                    canvas: [
                      {
                        type: 'line',
                        x1: 0,
                        y1: FONT_SIZE,
                        x2: data.signatures.director.width || SIGNATURE_WIDTH,
                        y2: FONT_SIZE,
                        lineWidth: 1,
                        lineColor: 'black',
                      },
                    ],
                  },
                  ...(data.signatures.director.image
                    ? [
                        {
                          image: data.signatures.director.image.path,
                          width:
                            data.signatures.director.width || SIGNATURE_WIDTH,
                          relativePosition:
                            data.signatures.director.image.position,
                        },
                      ]
                    : []),
                ],
                width: 'auto',
              },
              {
                text: data.signatures.director.name,
                fontSize: FONT_SIZE,
                width: 'auto',
              },
            ],
            columnGap: 5,
            width: '*',
          },
          {
            columns: [
              {
                text: data.signatures.accountant.position,
                fontSize: FONT_SIZE,
                width: 'auto',
              },
              {
                stack: [
                  {
                    canvas: [
                      {
                        type: 'line',
                        x1: 0,
                        y1: FONT_SIZE,
                        x2: data.signatures.accountant.width || SIGNATURE_WIDTH,
                        y2: FONT_SIZE,
                        lineWidth: 1,
                        lineColor: 'black',
                      },
                    ],
                  },
                  ...(data.signatures.accountant.image
                    ? [
                        {
                          image: data.signatures.accountant.image.path,
                          width:
                            data.signatures.accountant.width || SIGNATURE_WIDTH,
                          relativePosition:
                            data.signatures.accountant.image.position,
                        },
                      ]
                    : []),
                ],
                width: 'auto',
              },
              {
                text: data.signatures.accountant.name,
                fontSize: FONT_SIZE,
                width: 'auto',
              },
            ],
            columnGap: 5,
            width: 'auto',
          },
        ],
        style: 'signatures',
      },
      ...(data.after ? [data.after] : []),
    ],
    styles: {
      hint: {
        fontSize: FONT_SIZE - 1,
        bold: true,
        color: 'gray',
      },
      title: {
        fontSize: 18,
        bold: true,
        marginTop: 20,
      },
      sellerBuyerTable: {
        marginTop: 10,
      },
      productsTable: {
        marginTop: 10,
      },
      signatures: {
        marginTop: 50,
      },
    },
    defaultStyle: {
      fontSize: FONT_SIZE,
      columnGap: 0,
    },
  };

  const printer = new PdfPrinter(fonts);
  const pdfDoc = printer.createPdfKitDocument(documentDefinition);
  pdfDoc.end();

  return buffer(pdfDoc);
};

export default generateInvoice;
