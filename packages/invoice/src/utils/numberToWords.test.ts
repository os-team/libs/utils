import { test } from 'node:test';
import assert from 'node:assert';
import numberToWords from './numberToWords.js';

test('0', () => {
  assert.equal(numberToWords(0), 'ноль');
});

test('9', () => {
  assert.equal(numberToWords(9), 'девять');
});

test('10', () => {
  assert.equal(numberToWords(10), 'десять');
});

test('11', () => {
  assert.equal(numberToWords(11), 'одиннадцать');
});

test('19', () => {
  assert.equal(numberToWords(19), 'девятнадцать');
});

test('20', () => {
  assert.equal(numberToWords(20), 'двадцать');
});

test('21', () => {
  assert.equal(numberToWords(21), 'двадцать один');
});

test('99', () => {
  assert.equal(numberToWords(99), 'девяносто девять');
});

test('100', () => {
  assert.equal(numberToWords(100), 'сто');
});

test('101', () => {
  assert.equal(numberToWords(101), 'сто один');
});

test('111', () => {
  assert.equal(numberToWords(111), 'сто одиннадцать');
});

test('999', () => {
  assert.equal(numberToWords(999), 'девятьсот девяносто девять');
});

test('1 000', () => {
  assert.equal(numberToWords(1000), 'одна тысяча');
});

test('1 001', () => {
  assert.equal(numberToWords(1001), 'одна тысяча один');
});

test('2 002', () => {
  assert.equal(numberToWords(2002), 'две тысячи два');
});

test('3 003', () => {
  assert.equal(numberToWords(3003), 'три тысячи три');
});

test('999 999', () => {
  assert.equal(
    numberToWords(999999),
    'девятьсот девяносто девять тысяч девятьсот девяносто девять'
  );
});

test('9 007 199 254 740 991', () => {
  assert.equal(
    numberToWords(9007199254740991),
    'девять квадриллионов семь триллионов сто девяносто девять миллиардов двести пятьдесят четыре миллиона семьсот сорок тысяч девятьсот девяносто один'
  );
});
