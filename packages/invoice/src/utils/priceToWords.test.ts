import { test } from 'node:test';
import assert from 'node:assert';
import priceToWords from './priceToWords.js';

test('1.00', () => {
  const res = priceToWords(100);
  assert.equal(res, 'один рубль 00 копеек');
});

test('1 001.10', () => {
  const res = priceToWords(100110);
  assert.equal(res, 'одна тысяча один рубль 10 копеек');
});

test('2 002.02', () => {
  const res = priceToWords(200202);
  assert.equal(res, 'две тысячи два рубля 02 копейки');
});
