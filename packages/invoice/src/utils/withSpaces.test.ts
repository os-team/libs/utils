import { test } from 'node:test';
import assert from 'node:assert';
import withSpaces from './withSpaces.js';

test('1234 56 789', () => {
  const res = withSpaces('123456789', [4, 2]);
  assert.equal(res, '1234 56 789');
});
