import { test } from 'node:test';
import assert from 'node:assert';
import formatPrice from './formatPrice.js';

test('12 345,00', () => {
  const res = formatPrice(1234500);
  assert.equal(res, '12 345,00');
});
