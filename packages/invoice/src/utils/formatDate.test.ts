import { test } from 'node:test';
import assert from 'node:assert';
import formatDate from './formatDate.js';

test('21.11.1950', () => {
  const res = formatDate(new Date('11-21-1950'));
  assert.equal(res, '21.11.1950');
});
