import pluralForms from '@os-team/plural-forms';
import numberToWords from './numberToWords.js';

const priceToWords = (value: number) => {
  const rubles = Math.floor(value / 100);
  const kopecks = value % 100;

  const rublesTitle = pluralForms({
    language: 'ru',
    forms: ['рубль', 'рубля', 'рублей'],
    number: rubles,
  });

  const kopecksTitle = pluralForms({
    language: 'ru',
    forms: ['копейка', 'копейки', 'копеек'],
    number: kopecks,
  });

  return `${numberToWords(rubles)} ${rublesTitle} ${kopecks.toString().padStart(2, '0')} ${kopecksTitle}`;
};

export default priceToWords;
