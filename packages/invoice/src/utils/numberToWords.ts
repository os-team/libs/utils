import pluralForms from '@os-team/plural-forms';

const numbers = [
  'ноль',
  'один',
  'два',
  'три',
  'четыре',
  'пять',
  'шесть',
  'семь',
  'восемь',
  'девять',
  'десять',
  'одиннадцать',
  'двенадцать',
  'тринадцать',
  'четырнадцать',
  'пятнадцать',
  'шестнадцать',
  'семнадцать',
  'восемнадцать',
  'девятнадцать',
];

const numbersWithThousands = ['одна', 'две'];

const tens = [
  'двадцать',
  'тридцать',
  'сорок',
  'пятьдесят',
  'шестьдесят',
  'семьдесят',
  'восемьдесят',
  'девяносто',
];

const hundreds = [
  'сто',
  'двести',
  'триста',
  'четыреста',
  'пятьсот',
  'шестьсот',
  'семьсот',
  'восемьсот',
  'девятьсот',
];

const forms = [
  ['тысяча', 'тысячи', 'тысяч'],
  ['миллион', 'миллиона', 'миллионов'],
  ['миллиард', 'миллиарда', 'миллиардов'],
  ['триллион', 'триллиона', 'триллионов'],
  ['квадриллион', 'квадриллиона', 'квадриллионов'],
];

const twoDigitsToWords = (value: number, isThousands: boolean) => {
  if (value < 0 || value > 99) {
    throw new Error(`The number "${value}" must be between 0 and 99`);
  }
  if (value < 20) {
    return value > 0 && value < 3 && isThousands
      ? numbersWithThousands[value - 1]
      : numbers[value];
  }
  const tensIndex = Math.floor(value / 10) - 2;
  const numbersIndex = value % 10;
  if (numbersIndex > 0 && numbersIndex < 3 && isThousands) {
    return `${tens[tensIndex]} ${numbersWithThousands[numbersIndex]}`;
  }
  return numbersIndex > 0
    ? `${tens[tensIndex]} ${numbers[numbersIndex]}`
    : tens[tensIndex];
};

const threeDigitsToWords = (value: number, isThousands: boolean) => {
  if (value < 0 || value > 999) {
    throw new Error(`The number "${value}" must be between 0 and 999`);
  }
  if (value < 100) return twoDigitsToWords(value, isThousands);
  const hundredsIndex = Math.floor(value / 100) - 1;
  const twoDigits = value % 100;
  return twoDigits > 0
    ? `${hundreds[hundredsIndex]} ${twoDigitsToWords(twoDigits, isThousands)}`
    : hundreds[hundredsIndex];
};

const numberToWords = (value: number) => {
  if (value < 0 || value > Number.MAX_SAFE_INTEGER) {
    throw new Error(
      `The number "${value}" must be between 0 and ${Number.MAX_SAFE_INTEGER}`
    );
  }
  const valueStr = value.toString();
  const tripletsCount = Math.ceil(valueStr.length / 3);
  let res = '';
  for (let i = 0; i < tripletsCount; i++) {
    const triplet = Math.floor(value / 10 ** (3 * i)) % 1000;
    if (triplet > 0 || i === tripletsCount - 1) {
      const words = threeDigitsToWords(triplet, i === 1);
      const suffix =
        i > 0
          ? pluralForms({
              language: 'ru',
              forms: forms[i - 1],
              number: triplet,
            })
          : '';
      res = `${words}${suffix.length > 0 ? ` ${suffix}` : ''}${res.length > 0 ? ` ${res}` : ''}`;
    }
  }
  return res;
};

export default numberToWords;
