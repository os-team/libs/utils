const withSpaces = (value: string, offsets: number[]) => {
  let res = '';
  let offset = 0;
  let index = 0;

  for (let i = 0; i < value.length; i++) {
    if (index < offsets.length && offsets[index] === offset) {
      res += ` ${value.charAt(i)}`;
      index++;
      offset = 1;
    } else {
      res += value.charAt(i);
      offset += 1;
    }
  }

  return res;
};

export default withSpaces;
