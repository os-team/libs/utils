import type { ContentTable } from 'pdfmake/interfaces.d.ts';
import formatPrice from './formatPrice.js';
import priceToWords from './priceToWords.js';

export interface Product {
  name: string;
  quantity: number;
  unit: string;
  price: number;
}

interface ProductsTable {
  products: Product[];
  vat?: number;
}

const getProductsTable = (data: ProductsTable): ContentTable => {
  const price = data.products.reduce(
    (acc, product) => acc + product.price * product.quantity,
    0
  );
  const vat = price * ((data.vat || 0) / 100);
  const totalPrice = price + vat;

  return {
    table: {
      body: [
        [
          { text: '№', noWrap: true, bold: true },
          { text: 'Товары (работы, услуги)', bold: true },
          { text: 'Кол-во', noWrap: true, bold: true },
          { text: 'Ед.', noWrap: true, bold: true },
          { text: 'Цена', noWrap: true, bold: true },
          { text: 'Сумма', noWrap: true, bold: true },
        ],
        ...data.products.map((product, index) => [
          { text: (index + 1).toString(), noWrap: true },
          product.name,
          { text: product.quantity.toString(), noWrap: true },
          { text: product.unit, noWrap: true },
          { text: formatPrice(product.price), noWrap: true },
          { text: formatPrice(product.price * product.quantity), noWrap: true },
        ]),
        [
          {
            text: 'Итого:',
            colSpan: 5,
            alignment: 'right',
            bold: true,
            noWrap: true,
            border: [false, false, false, false],
            marginTop: 5,
          },
          '',
          '',
          '',
          '',
          {
            text: formatPrice(price),
            alignment: 'right',
            bold: true,
            noWrap: true,
            border: [false, false, false, false],
            marginTop: 5,
          },
        ],
        [
          {
            text: 'В том числе НДС:',
            colSpan: 5,
            alignment: 'right',
            bold: true,
            noWrap: true,
            border: [false, false, false, false],
          },
          '',
          '',
          '',
          '',
          {
            text: vat > 0 ? formatPrice(vat) : 'без НДС',
            alignment: 'right',
            bold: true,
            noWrap: true,
            border: [false, false, false, false],
          },
        ],
        [
          {
            text: 'Всего к оплате:',
            colSpan: 5,
            alignment: 'right',
            bold: true,
            noWrap: true,
            border: [false, false, false, false],
          },
          '',
          '',
          '',
          '',
          {
            text: formatPrice(totalPrice),
            alignment: 'right',
            bold: true,
            noWrap: true,
            border: [false, false, false, false],
          },
        ],
        [
          {
            text: [
              {
                text: `Всего наименований ${data.products.length}, на сумму ${formatPrice(totalPrice)} руб.\n`,
              },
              {
                text: priceToWords(totalPrice).replace(/^./, (c) =>
                  c.toUpperCase()
                ),
                bold: true,
              },
            ],
            colSpan: 6,
            border: [false, false, false, true],
          },
          '',
          '',
          '',
          '',
          '',
        ],
      ],
    },
    layout: {
      hLineWidth: (i, node) =>
        i === 0 ||
        i === node.table.body.length - 4 ||
        i === node.table.body.length
          ? 2
          : 1,
      vLineWidth: (i, node) =>
        i === 0 || i === (node.table.widths || []).length ? 2 : 1,
    },
    style: 'productsTable',
  };
};

export default getProductsTable;
