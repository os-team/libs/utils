const addThousandsSeparator = (value: string, thousandsSeparator: string) => {
  const groups = value.match(/(-?[0-9]+)([.,][0-9]+)?/);
  if (!groups) return value;
  const [, integer, fractional] = groups;
  let res = integer.replace(
    /([0-9])(?=(?:[0-9]{3})+\b)/g,
    `$1${thousandsSeparator}`
  );
  if (fractional) res += fractional;
  return res;
};

const formatPrice = (value: number) => {
  const str = value.toString();
  const rubles = str.slice(0, -2);
  const kopecks = str.slice(-2);
  const formattedRubles = addThousandsSeparator(rubles, ' ');
  return `${formattedRubles},${kopecks}`;
};

export default formatPrice;
