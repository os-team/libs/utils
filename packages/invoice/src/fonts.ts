import path from 'path';
import { fileURLToPath } from 'node:url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const fonts = {
  Roboto: {
    normal: path.resolve(__dirname, './fonts/Roboto-Regular.ttf'),
    bold: path.resolve(__dirname, './fonts/Roboto-Medium.ttf'),
  },
};

export default fonts;
