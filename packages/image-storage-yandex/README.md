# @os-team/yandex-image-storage [![NPM version](https://img.shields.io/npm/v/@os-team/yandex-image-storage)](https://yarnpkg.com/package/@os-team/yandex-image-storage) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/yandex-image-storage)](https://bundlephobia.com/result?p=@os-team/yandex-image-storage)

Library for uploading images to the Yandex Cloud Storage.

Usually when you want to store an image you need:

1. Validate the file type (e.g. allow only JPG, PNG, WEBP image formats). The file type should be detected by the first bytes, not by the file name (e.g. using the [file-type](https://github.com/sindresorhus/file-type) library).
2. Validate the file size.
3. Convert the image to a specific extension (e.g. to JPG).
4. Upload an image in multiple sizes to use them on the frontend side in different places (e.g. a large avatar on the profile page, a small avatar in the header).
5. Crop some sizes of the image to use them, for example, in a list of blog posts.
6. Append a hash to the file name to avoid caching when updating the image.
7. Delete old images that have been replaced with new ones.

This library performs all these steps.

## Usage

Install the package using the following command:

```
yarn add @os-team/yandex-image-storage @os-team/yc-storage
```

### Example

```ts
import YandexStorage from '@os-team/yc-storage';
import YandexImageStorage from '@os-team/yandex-image-storage';

// Initialize the Yandex Cloud Storage
const storage = new YandexStorage({
  accessKeyId: 'accessKeyId',
  secretAccessKey: 'secretAccessKey',
  region: 'ru-central1-a',
});

// Initialize the Image Storage
const imageStorage = new YandexImageStorage({
  storage,
  bucket: 'my-bucket',
});

// Upload an image
const { name } = await imageStorage.upload({
  body: createReadStream('my-image.jpg'),
  name: 'name',
});

// Delete the image
await imageStorage.delete(name);
```

This library extends the [@os-team/image-storage](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/image-storage) lib, so see it for more information about uploading and deleting files.
