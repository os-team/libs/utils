import ImageStorage, {
  type Storage,
  type ImageStorageOptions,
} from '@os-team/image-storage';
import YandexStorage, { type YandexStorageOptions } from './YandexStorage.js';

export type YandexImageStorageOptions = ImageStorageOptions &
  YandexStorageOptions;

class YandexImageStorage extends ImageStorage {
  protected readonly storage: Storage;

  public constructor(options: YandexImageStorageOptions) {
    const { storage, ...rest } = options;
    super(rest);
    this.storage = new YandexStorage({ storage });
  }
}

export default YandexImageStorage;
