import { Readable } from 'stream';

const streamToBuffer = async (stream: Readable): Promise<Buffer> => {
  const chunks: Buffer[] = [];
  let totalLength = 0;

  await new Promise((resolve, reject) => {
    stream.on('data', (chunk) => {
      chunks.push(chunk);
      totalLength += chunk.length;
    });
    stream.on('error', reject);
    stream.on('end', resolve);
  });

  return Buffer.concat(chunks, totalLength);
};

export default streamToBuffer;
