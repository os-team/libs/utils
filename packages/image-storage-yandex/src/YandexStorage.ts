import type { Storage, StorageUploadOptions } from '@os-team/image-storage';
import YStorage from '@os-team/yc-storage';
import streamToBuffer from './streamToBuffer.js';

export interface YandexStorageOptions {
  /**
   * The instance of the Yandex Cloud Storage.
   */
  storage: YStorage;
}

class YandexStorage implements Storage {
  private readonly storage: YStorage;

  public constructor(options: YandexStorageOptions) {
    const { storage } = options;
    this.storage = storage;
  }

  public async list(bucket: string, prefix: string): Promise<string[]> {
    const { contents } = await this.storage.bucket.listObjects({
      bucket,
      prefix,
      maxKeys: 100,
    });

    const keys: string[] = [];
    contents.forEach(({ key }) => {
      if (key) keys.push(key);
    });

    return keys;
  }

  public async upload(options: StorageUploadOptions): Promise<void> {
    const { body, ...restOptions } = options;
    const bodyBuffer = await streamToBuffer(body);

    await this.storage.object.upload({
      body: bodyBuffer,
      ...restOptions,
    });
  }

  public async delete(bucket: string, key: string): Promise<void> {
    await this.storage.object.delete({ bucket, key });
  }

  public async deleteMultiple(bucket: string, keys: string[]): Promise<void> {
    await this.storage.object.deleteMultipleObjects({
      bucket,
      objects: keys.map((key) => ({ key })),
    });
  }
}

export default YandexStorage;
