import { type Transformer } from '../utils/TransformArgs.js';

const toUpperCase: Transformer = (value) =>
  typeof value === 'string' ? value.toUpperCase() : value;

export default toUpperCase;
