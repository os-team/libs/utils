import { type Transformer } from '../utils/TransformArgs.js';

const unique: Transformer = (value) =>
  Array.isArray(value) ? [...new Set(value)] : value;

export default unique;
