import { type Transformer } from '../utils/TransformArgs.js';

const trim: Transformer = (value) =>
  typeof value === 'string' ? value.trim() : value;

export default trim;
