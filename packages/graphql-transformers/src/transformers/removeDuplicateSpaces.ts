import { type Transformer } from '../utils/TransformArgs.js';

const removeDuplicateSpaces: Transformer = (value) =>
  typeof value === 'string' ? value.replace(/\s{2,}/g, ' ') : value;

export default removeDuplicateSpaces;
