import { type Transformer } from '../utils/TransformArgs.js';

const capitalizeTheFirstLetter: Transformer = (value) =>
  typeof value === 'string'
    ? value.replace(/^\w/, (c) => c.toUpperCase())
    : value;

export default capitalizeTheFirstLetter;
