import { type Transformer } from '../utils/TransformArgs.js';

const toLowerCase: Transformer = (value) =>
  typeof value === 'string' ? value.toLowerCase() : value;

export default toLowerCase;
