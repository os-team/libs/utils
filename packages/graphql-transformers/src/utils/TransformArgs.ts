import {
  createMethodMiddlewareDecorator,
  type ResolverData,
} from 'type-graphql';

/* eslint-disable @typescript-eslint/no-explicit-any */

export type Transformer<T = any> = (value: T, data: ResolverData) => T;

export interface TransformArgsOptions {
  arg?: string;
}

const TransformArgs = (
  transformersMap: Record<string, Transformer[]>,
  options: TransformArgsOptions = {}
): MethodDecorator => {
  const { arg } = options;

  return createMethodMiddlewareDecorator<any>((data, next) => {
    const { args } = data;
    const input = arg ? args[arg] : args;

    // Check if the input is an object
    if (typeof input !== 'object') {
      throw new Error(
        `The ${arg ? `${arg} argument` : 'args'} must be an object`
      );
    }

    const transformedInput = Object.entries(transformersMap).reduce(
      (acc, [key, transformers]) => ({
        ...acc,
        [key]: transformers.reduce(
          (res, transformer) => transformer(res, data),
          input[key]
        ),
      }),
      input
    );

    // Replace the arguments
    if (arg) data.args[arg] = transformedInput;
    else data.args = transformedInput;

    return next();
  });
};

export default TransformArgs;
