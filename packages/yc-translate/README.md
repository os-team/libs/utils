# @os-team/yc-translate [![NPM version](https://img.shields.io/npm/v/@os-team/yc-translate)](https://yarnpkg.com/package/@os-team/yc-translate) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/yc-translate)](https://bundlephobia.com/result?p=@os-team/yc-translate)

Translates the text to the specific language by Yandex.Cloud.

Unlike the `yandex-cloud` library, this library uses the [API key](https://cloud.yandex.com/docs/iam/concepts/authorization/api-key) for authorization in the Yandex.Cloud.
If you want to use either the IAM token or the OAuth token for authorization, use the `yandex-cloud` library.

## Usage

Install the package using the following command:

```
yarn add @os-team/yc-translate
```

Simple usage:

```ts
import createTranslator from '@os-team/yc-translate';

const translator = createTranslator('API_KEY');
const translation = translator.translate('apple', 'ru'); // яблоко
```
