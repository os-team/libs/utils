import fetch from 'node-fetch';

const isObject = (value: unknown): value is object =>
  typeof value === 'object' && !Array.isArray(value) && value !== null;

interface Response {
  translations: Array<{ text: string }>;
}

const isResponse = (value: unknown): value is Response =>
  isObject(value) &&
  'translations' in value &&
  Array.isArray(value.translations) &&
  value.translations.every(
    (item) => isObject(item) && 'text' in item && typeof item.text === 'string'
  );

export interface YandexCloudTranslator<T> {
  translate: (input: T, to: string) => Promise<T>;
}

const createTranslator = <T = string | string[]>(
  apiKey: string
): YandexCloudTranslator<T> => ({
  translate: async (input, to) => {
    const texts = Array.isArray(input) ? input : [input];

    const response = await fetch(
      'https://translate.api.cloud.yandex.net/translate/v2/translate',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Api-Key ${apiKey}`,
        },
        body: JSON.stringify({
          texts,
          targetLanguageCode: to,
        }),
      }
    );

    const res = await response.json();

    if (!isResponse(res)) {
      throw new Error(
        isObject(res) && 'message' in res && typeof res.message === 'string'
          ? res.message
          : 'Translations are not found'
      );
    }

    if (Array.isArray(input)) {
      return res.translations.map((item) => item.text);
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return res.translations[0].text as any;
  },
});

export default createTranslator;
