/* eslint-disable @typescript-eslint/no-explicit-any */

export const isObject = (value: any) =>
  typeof value === 'object' && value !== null;

export const isString = (value: any) => typeof value === 'string';

export const isRawDraftContentState = (value: any) =>
  isObject(value) && Array.isArray(value.blocks);

export const isRawDraftContentBlock = (value: any) =>
  isObject(value) &&
  isString(value.key) &&
  isString(value.type) &&
  isString(value.text);

export const isRawDraftContentRange = (value: any) =>
  isObject(value) &&
  Number.isInteger(value.offset) &&
  Number.isInteger(value.length);

export const isRawDraftContentEntity = (value: any) =>
  isObject(value) &&
  isString(value.type) &&
  (isObject(value.data) || value.data === undefined);
