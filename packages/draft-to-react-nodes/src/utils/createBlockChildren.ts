import React from 'react';
import getRanges, { RangeType } from './getRanges.js';
import createInlineStyleElement, {
  type InlineStyleRenderer,
} from './createInlineStyleElement.js';
import { isRawDraftContentEntity } from './validators.js';

interface Entity {
  key: number;
  type: string;
  text: string;
  data?: Record<string, any>; // eslint-disable-line @typescript-eslint/no-explicit-any
}

export type EntityRenderer = (entity: Entity) => React.ReactNode;

let key = 0;

interface Props {
  block: any; // eslint-disable-line @typescript-eslint/no-explicit-any
  entityMap: any; // eslint-disable-line @typescript-eslint/no-explicit-any
  entityRenderer?: EntityRenderer;
  inlineStyleRenderer?: InlineStyleRenderer;
}

const createBlockChildren = (props: Props): React.ReactNode => {
  const {
    block,
    entityMap,
    entityRenderer = () => null,
    inlineStyleRenderer = () => null,
  } = props;

  const ranges = getRanges(block);

  // Returns plain text if the block has no inline styles and entities
  if (ranges.length === 0) return block.text;

  const nodes: React.ReactNode[] = [];
  let cursor = 0;

  ranges.forEach((range) => {
    // Cut the text between cursor and range offset
    if (range.offset - cursor > 0) {
      nodes.push(block.text.slice(cursor, range.offset));
    }

    // Cut the text from range
    const text = block.text.slice(range.offset, range.offset + range.length);

    // Add the inline style element
    if (range.type === RangeType.INLINE_STYLE) {
      const inlineStyleElement = createInlineStyleElement(
        {
          key,
          text,
          style: range.style,
        },
        inlineStyleRenderer
      );
      nodes.push(inlineStyleElement || text);
      key += 1;
    }

    // Add the entity element
    if (range.type === RangeType.ENTITY) {
      const entity = entityMap[range.key];
      if (isRawDraftContentEntity(entity)) {
        const entityElement = entityRenderer({
          key,
          text,
          type: entity.type,
          data: entity.data,
        });
        nodes.push(entityElement || text);
        key += 1;
      } else {
        nodes.push(text);
      }
    }

    // Update cursor
    cursor = range.offset + range.length;
  });

  // Cut the text from cursor to the end
  if (block.text.length > cursor) {
    nodes.push(block.text.slice(cursor));
  }

  return nodes;
};

export default createBlockChildren;
