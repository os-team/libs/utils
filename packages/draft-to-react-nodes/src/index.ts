import React from 'react';
import {
  isObject,
  isRawDraftContentBlock,
  isRawDraftContentState,
} from './utils/validators.js';
import createBlockElement, {
  type BlockRenderer,
} from './utils/createBlockElement.js';
import { type EntityRenderer } from './utils/createBlockChildren.js';
import { type InlineStyleRenderer } from './utils/createInlineStyleElement.js';

export interface Options {
  value: any; // eslint-disable-line @typescript-eslint/no-explicit-any
  entityRenderer?: EntityRenderer;
  blockRenderer?: BlockRenderer;
  inlineStyleRenderer?: InlineStyleRenderer;
}

const draftToReactNodes = (options: Options): React.ReactNode => {
  const { value, entityRenderer, blockRenderer, inlineStyleRenderer } = options;
  if (!isRawDraftContentState(value)) return null;

  const nodes: React.ReactNode[] = [];
  let ulNodes: React.ReactNode[] = [];
  let olNodes: React.ReactNode[] = [];
  let key = 0;

  value.blocks.forEach((block) => {
    if (!isRawDraftContentBlock(block)) return;

    // Add an unordered list
    if (block.type !== 'unordered-list-item' && ulNodes.length > 0) {
      nodes.push(React.createElement('ul', { key }, ulNodes));
      ulNodes = [];
      key += 1;
    }

    // Add an ordered list
    if (block.type !== 'ordered-list-item' && olNodes.length > 0) {
      nodes.push(React.createElement('ol', { key }, olNodes));
      olNodes = [];
      key += 1;
    }

    const entityMap = isObject(value.entityMap) ? value.entityMap : {};
    const element = createBlockElement({
      block,
      entityMap,
      entityRenderer,
      blockRenderer,
      inlineStyleRenderer,
    });

    if (block.type === 'unordered-list-item') ulNodes.push(element);
    else if (block.type === 'ordered-list-item') olNodes.push(element);
    else nodes.push(element);
  });

  // Add an unordered list located at the end
  if (ulNodes.length > 0) {
    nodes.push(React.createElement('ul', { key }, ulNodes));
  }

  // Add an ordered list  located at the end
  if (olNodes.length > 0) {
    nodes.push(React.createElement('ol', { key }, olNodes));
  }

  return nodes;
};

export * from './utils/createBlockChildren.js';
export * from './utils/createBlockElement.js';
export * from './utils/createInlineStyleElement.js';

export default draftToReactNodes;
