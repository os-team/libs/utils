import { type RelayNetworkMiddleware } from '@os-team/relay-network-creator';
import * as DeviceInfo from 'react-native-device-info';

const appUserAgent: RelayNetworkMiddleware = (next) => async (req) => {
  const appInfo = `${DeviceInfo.getBundleId()}/${DeviceInfo.getVersion()}.${DeviceInfo.getBuildNumber()}`;
  const deviceInfoList = [
    DeviceInfo.getDeviceType(),
    `${DeviceInfo.getSystemName()}/${DeviceInfo.getSystemVersion()}`,
    DeviceInfo.getBuildIdSync(),
    DeviceInfo.getManufacturerSync(),
    DeviceInfo.getBrand(),
    DeviceInfo.getModel(),
    DeviceInfo.getDeviceId(),
    DeviceInfo.getDeviceNameSync(),
    await DeviceInfo.getUniqueId(),
  ];

  req.headers['User-Agent'] = encodeURI(
    `App ${appInfo} (${deviceInfoList.join('; ')})`
  );

  return next(req);
};

export default appUserAgent;
