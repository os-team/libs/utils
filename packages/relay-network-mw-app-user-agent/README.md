# @os-team/relay-network-mw-app-user-agent [![NPM version](https://img.shields.io/npm/v/@os-team/relay-network-mw-app-user-agent)](https://yarnpkg.com/package/@os-team/relay-network-mw-app-user-agent) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/relay-network-mw-app-user-agent)](https://bundlephobia.com/result?p=@os-team/relay-network-mw-app-user-agent)

The middleware for @os-team/relay-network-creator to include more detailed app's user agent in each request made from React Native.

### Examples of user agents

| OS      | Without the middleware                      | With the middleware                                                                                                                           |
| ------- | ------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------- |
| Android | okhttp/3.12.12                              | App com.myproject/1.0.1 (Handset; Android/10; QP1A.190711.020; Xiaomi; Redmi; M2006C3MNG; angelican; Android Bluedroid; 12a345b678c90d12)     |
| iOS     | MyProject/1 CFNetwork/978.0.7 Darwin/18.7.0 | App com.myproject/1.0.1 (Handset; iOS/12.5.4; 16H50; Apple; Apple; iPhone 6; iPhone7,2; Becca's iPhone; 12ABCDE3-45FG-678H-I90J-K1L2MN34O567) |

Detailed user agents can be useful, for example, if you want to show the user his active sessions with information about devices.
You can parse these user agents using the [@os-team/app-ua-parser](https://gitlab.com/os-team/libs/utils/-/tree/main/packages/app-ua-parser) library.

## Usage

### Step 1. Install the package

Firstly, install the [react-native-device-info](https://github.com/react-native-device-info/react-native-device-info) library.

Then install the package using the following command:

```
yarn add @os-team/relay-network-mw-app-user-agent
```

### Step 2. Add the middleware

```ts
import createRelayNetwork from '@os-team/relay-network-creator';
import appUserAgent from '@os-team/relay-network-mw-app-user-agent';

createRelayNetwork({
  url: 'https://api.domain.com/graphql',
  middlewares: [appUserAgent],
});
```
