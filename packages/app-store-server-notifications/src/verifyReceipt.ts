import fetch from 'node-fetch';
import type {
  LatestReceiptInfo,
  PendingRenewalInfo,
  Receipt,
} from './types.js';

export interface VerifyReceiptOptions {
  /**
   * The Base64-encoded receipt data.
   */
  receiptData: string;
  /**
   * Your app’s shared secret, which is a hexadecimal string.
   */
  password?: string;
  /**
   * Set this value to true for the response to include only the latest renewal
   * transaction for any subscriptions. Use this field only for app receipts
   * that contain auto-renewable subscriptions.
   */
  excludeOldTransactions?: boolean;
}

export interface VerifyReceiptResponse {
  /**
   * The environment for which the receipt was generated.
   */
  environment: 'Sandbox' | 'Production';
  /**
   * An indicator that an error occurred during the request.
   * A value of 1 indicates a temporary issue; retry validation for this receipt
   * at a later time. A value of 0 indicates an unresolvable issue; do not retry
   * validation for this receipt. Only applicable to status codes 21100-21199.
   */
  'is-retryable'?: boolean;
  /**
   * The latest Base64 encoded app receipt.
   * Only returned for receipts that contain auto-renewable subscriptions.
   */
  latest_receipt?: string;
  /**
   * An array that contains all in-app purchase transactions. This excludes
   * transactions for consumable products that have been marked as finished by
   * your app. Only returned for receipts that contain auto-renewable
   * subscriptions.
   */
  latest_receipt_info?: LatestReceiptInfo[];
  /**
   * In the JSON file, an array where each element contains the pending renewal
   * information for each auto-renewable subscription identified by the product_id.
   * Only returned for app receipts that contain auto-renewable subscriptions.
   */
  pending_renewal_info?: PendingRenewalInfo[];
  /**
   * A JSON representation of the receipt that was sent for verification.
   */
  receipt: Receipt;
  /**
   * Either 0 if the receipt is valid, or a status code if there is an error.
   * The status code reflects the status of the app receipt as a whole.
   */
  status:
    | 0 // The receipt is valid
    | 21000 // The request to the App Store was not made using the HTTP POST request method
    | 21002 // The data in the receipt-data property was malformed or the service experienced a temporary issue. Try again.
    | 21003 // The receipt could not be authenticated
    | 21004 // The shared secret you provided does not match the shared secret on file for your account
    | 21005 // The receipt server was temporarily unable to provide the receipt. Try again.
    | 21006 // This receipt is valid but the subscription has expired. When this status code is returned to your server, the receipt data is also decoded and returned as part of the response. Only returned for iOS 6-style transaction receipts for auto-renewable subscriptions.
    | 21007 // This receipt is from the test environment, but it was sent to the production environment for verification
    | 21008 // This receipt is from the production environment, but it was sent to the test environment for verification
    | 21009 // Internal data access error. Try again later.
    | 21010 // The user account cannot be found or has been deleted
    | number; // Status codes 21100-21199 are various internal data access errors
}

export class VerifyReceiptError extends Error {
  public readonly status: number;

  public constructor(status: number) {
    super(`An error occurred while verifying the receipt. Status: ${status}.`);
    this.status = status;
  }
}

const isErrorStatus = (status: number) => status > 0 && status !== 21006;

const makeRequest = async (
  options: VerifyReceiptOptions,
  isSandbox: boolean,
  ignoreErrors = false
): Promise<VerifyReceiptResponse> => {
  const { receiptData, password, excludeOldTransactions } = options;

  const response = await fetch(
    `https://${isSandbox ? 'sandbox' : 'buy'}.itunes.apple.com/verifyReceipt`,
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        'receipt-data': receiptData,
        password,
        'exclude-old-transactions': excludeOldTransactions,
      }),
    }
  );
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const res = (await response.json()) as any;

  if (!ignoreErrors && isErrorStatus(res.status)) {
    throw new VerifyReceiptError(res.status);
  }

  return res as VerifyReceiptResponse;
};

/**
 * Sends a receipt to the App Store for verification.
 */
const verifyReceipt = async (
  options: VerifyReceiptOptions,
  environment: 'sandbox' | 'production' | 'auto' = 'auto'
): Promise<VerifyReceiptResponse> => {
  if (environment !== 'auto') {
    return makeRequest(options, environment === 'sandbox');
  }

  let res = await makeRequest(options, false, true);

  if (res.status === 21007) {
    res = await makeRequest(options, true);
  } else if (isErrorStatus(res.status)) {
    throw new VerifyReceiptError(res.status);
  }

  return res;
};

export default verifyReceipt;
