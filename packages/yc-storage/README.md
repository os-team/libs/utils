# @os-team/yc-storage [![NPM version](https://img.shields.io/npm/v/@os-team/yc-storage)](https://yarnpkg.com/package/@os-team/yc-storage) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/yc-storage)](https://bundlephobia.com/result?p=@os-team/yc-storage)

Yandex Cloud Object Storage API. All methods described in the official documentation are implemented (buckets, objects, multipart upload, static website hosting, CORS, lifecycles, ACL, bucket policy).

## Usage

Install the package using the following command:

```
yarn add @os-team/yc-storage
```

### Simple example

```js
import YandexStorage from '@os-team/yc-storage';

const yandexStorage = new YandexStorage({
  accessKeyId: 'accessKeyId',
  secretAccessKey: 'secretAccessKey',
  region: 'ru-central1-a',
});

const res = await yandexStorage.bucket.listObjects({
  bucket: 'my-bucket',
});
```

To get an access key ID and a secret access key, create a new [service account](https://cloud.yandex.com/en/docs/iam/quickstart-sa#create-sa) (e.g. with the name of your app), click the line with its name, click `Create new key` and `Create static access key`.

### Uploading a file

To upload a file using a buffer, use the `object.upload` method, which uploads the entire file at once.
This method is recommended, if the file size is up to 100 MB.

```ts
const res = await yandexStorage.object.upload({
  bucket: 'my-bucket',
  key: 'image.png',
  body: fs.readFileSync(filePath), // Must be a buffer
  contentType: 'image/png',
});
```

To upload a file using a stream, use the `multipart.upload` method, which uploads the file in parts (see more [the general procedure for multipart upload](https://cloud.yandex.com/en/docs/storage/s3/api-ref/multipart)).
This method is recommended, if the file size exceeds 100 MB.

```ts
const res = await yandexStorage.multipart.upload({
  bucket: 'testabc',
  key: 'big.png',
  body: fs.createReadStream(filePath), // Must be a stream
  contentType: 'image/png',
});
```

---

It makes no sense to describe all the methods in this README, because the library fully implements all the methods of the Yandex Cloud Storage API described in the [official documentation](https://cloud.yandex.com/en/docs/storage/s3/api-ref/).
