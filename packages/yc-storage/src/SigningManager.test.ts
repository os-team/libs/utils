import { test } from 'node:test';
import assert from 'node:assert';
import SigningManager, { type CanonicalRequest } from './SigningManager.js';

test('hash', () => {
  // The example is taken from https://cloud.yandex.ru/docs/storage/concepts/pre-signed-urls#example-for-object-download
  const res = SigningManager.hash(
    'GET\n' +
      '/example-bucket/object-for-share.txt\n' +
      'X-Amz-Algorithm=AWS4-HMAC-SHA256&' +
      'X-Amz-Credential=JK38EXAMPLEAKDID8%2F20190801%2Fru-central1%2Fs3%2Faws4_request&' +
      'X-Amz-Date=20190801T000000Z&' +
      'X-Amz-Expires=3600&' +
      'X-Amz-SignedHeaders=host\n' +
      'host:storage.yandexcloud.net\n' +
      '\n' +
      'host\n' +
      'UNSIGNED-PAYLOAD'
  );

  assert.equal(
    res,
    '2d2b4efefa9072d90a646afbc0fbaef4618c81396b216969ddfc2869db5aa356'
  );
});

test('sign', () => {
  // The example is taken from https://cloud.yandex.ru/docs/storage/concepts/pre-signed-urls#example-for-object-download
  const stringToSign =
    'AWS4-HMAC-SHA256\n' +
    '20190801T000000Z\n' +
    '20190801/ru-central1/s3/aws4_request\n' +
    '2d2b4efefa9072d90a646afbc0fbaef4618c81396b216969ddfc2869db5aa356';
  const signingKey = SigningManager.sign(
    SigningManager.sign(
      SigningManager.sign(
        SigningManager.sign('AWS4ExamP1eSecReTKeykdokKK38800', '20190801'),
        'ru-central1'
      ),
      's3'
    ),
    'aws4_request'
  );
  const res = SigningManager.sign(signingKey, stringToSign).toString('hex');

  assert.equal(
    res,
    '56bdf53a1f10c078c2b4fb5a26cefa670b3ea796567d85489135cf33e77783f0'
  );
});

test('dateToString', () => {
  const date = new Date('2013-05-24');
  const strDate = SigningManager.dateToString(date);
  assert.equal(strDate, '20130524');
});

test('dateToISO8601', () => {
  const date = new Date('2019-08-01');
  const strDate = SigningManager.dateToISO8601(date);
  assert.equal(strDate, '20190801T000000Z');
});

test('getQueryString', () => {
  const query = { one: '1', two: '#' };
  const queryString = SigningManager.getQueryString(query);
  assert.equal(queryString, 'one=1&two=%23');
});

test('createSignature', () => {
  const signingManager = new SigningManager({
    accessKeyId: 'JK38EXAMPLEAKDID8',
    secretAccessKey: 'ExamP1eSecReTKeykdokKK38800',
    region: 'ru-central1' as any, // eslint-disable-line @typescript-eslint/no-explicit-any
  });

  const req: CanonicalRequest = {
    method: 'GET',
    uri: '/example-bucket/object-for-share.txt',
    headers: {
      Host: 'storage.yandexcloud.net',
    },
  };
  const date = new Date('2019-08-01');
  const signature = signingManager.createSignature(req, date);

  assert.equal(
    signature,
    '402e19d3f467f59cdba71ba3ac66154e6af1c2ef8dc0781b103a0ba4d8a2f1d6'
  );
});

test('createAuthString', () => {
  const signingManager = new SigningManager({
    accessKeyId: 'JK38EXAMPLEAKDID8',
    secretAccessKey: 'ExamP1eSecReTKeykdokKK38800',
    region: 'ru-central1' as any, // eslint-disable-line @typescript-eslint/no-explicit-any
  });

  const req: CanonicalRequest = {
    method: 'GET',
    uri: '/example-bucket/object-for-share.txt',
    headers: {
      Host: 'storage.yandexcloud.net',
    },
  };
  const date = new Date('2019-08-01');
  const authString = signingManager.createAuthString(req, date);

  assert.equal(
    authString,
    'AWS4-HMAC-SHA256 ' +
      'Credential=JK38EXAMPLEAKDID8/20190801/ru-central1/s3/aws4_request,' +
      'SignedHeaders=host,' +
      'Signature=402e19d3f467f59cdba71ba3ac66154e6af1c2ef8dc0781b103a0ba4d8a2f1d6'
  );
});
