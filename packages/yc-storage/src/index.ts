import * as crypto from 'crypto';
import { type BinaryLike } from 'crypto';
import { buildXml, parseXml } from '@os-team/xml-parser';
import NotFoundError from './errors/NotFoundError.js';
import YCStorageError from './errors/YCStorageError.js';
import MultipartUploadManager from './MultipartUploadManager.js';
import MultipartUploadStream from './MultipartUploadStream.js';
import SigningManager, {
  type CanonicalRequest,
  type SigningManagerOptions,
} from './SigningManager.js';
import {
  type AccessControlPolicy,
  type ACL,
  type ACLBucketGetACLParams,
  type ACLBucketPutACLParams,
  type ACLObjectGetACLParams,
  type ACLObjectPutACLParams,
} from './types/acl.js';
import {
  type BucketCreateParams,
  type BucketDeleteBucketEncryptionParams,
  type BucketDeleteBucketParams,
  type BucketGetBucketEncryptionParams,
  type BucketGetBucketLoggingParams,
  type BucketGetBucketVersioningParams,
  type BucketGetMetaParams,
  type BucketListObjectsParams,
  type BucketListObjectVersionsParams,
  type BucketLoggingStatus,
  type BucketPutBucketEncryptionParams,
  type BucketPutBucketLoggingParams,
  type BucketPutBucketVersioningParams,
  type ListAllMyBucketsResult,
  type ListBucketResult,
  type ListVersionsResult,
  type ServerSideEncryptionConfiguration,
  type SSEAlgorithm,
  type StorageClass,
  type VersioningConfiguration,
} from './types/bucket.js';
import {
  type CORSConfiguration,
  type CORSDeleteParams,
  type CORSGetParams,
  type CORSUploadParams,
} from './types/cors.js';
import {
  type LifecycleConfiguration,
  type LifecyclesDeleteParams,
  type LifecyclesGetParams,
  type LifecyclesUploadParams,
} from './types/lifecycles.js';
import {
  type CompleteMultipartUploadResult,
  type InitiateMultipartUploadResult,
  type ListMultipartUploadsResult,
  type ListPartsResult,
  type MultipartAbortUploadParams,
  type MultipartCompleteUploadParams,
  type MultipartCopyPartParams,
  type MultipartListPartsParams,
  type MultipartListUploadsParams,
  type MultipartStartUploadParams,
  type MultipartUploadParams,
  type MultipartUploadPartParams,
  type MultipartUploadPartResponse,
} from './types/multipart.js';
import {
  type CopyObjectResult,
  type DeleteResult,
  type ObjectCopyParams,
  type ObjectCopyResponse,
  type ObjectDeleteMultipleObjectsParams,
  type ObjectDeleteParams,
  type ObjectGetObjectMetaParams,
  type ObjectGetObjectMetaResponse,
  type ObjectGetParams,
  type ObjectGetResponse,
  type ObjectOptionsParams,
  type ObjectOptionsResponse,
  type ObjectSelectObjectContentParams,
  type ObjectUploadParams,
  type SelectObjectContentPayload,
} from './types/object.js';
import {
  type Policy,
  type PolicyDeleteParams,
  type PolicyGetParams,
  type PolicyPutParams,
} from './types/policy.js';
import {
  type WebsiteConfiguration,
  type WebsiteDeleteParams,
  type WebsiteGetParams,
  type WebsiteUploadParams,
} from './types/website.js';

const YC_STORAGE_HOST = 'storage.yandexcloud.net';

export type YandexStorageOptions = SigningManagerOptions;

interface Request extends CanonicalRequest {
  arrayTags?: string[];
  includeAttributes?: boolean;
  format?: 'XML' | 'JSON' | 'BUFFER';
}

interface Response {
  headers: Headers;
  body: any; // eslint-disable-line @typescript-eslint/no-explicit-any
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const toStringRecord = (obj: Record<string, any>): Record<string, string> => {
  const res: Record<string, string> = {};
  Object.entries(obj).forEach(([key, value]) => {
    if (value !== undefined) res[key] = value.toString();
  });
  return res;
};

const SIZE_5MB = 5 * 1024 * 1024;
const SIZE_20MB = 20 * 1024 * 1024;

class YandexStorage {
  private signingManager: SigningManager;

  public constructor(options: YandexStorageOptions) {
    this.signingManager = new SigningManager(options);
  }

  public readonly bucket = {
    /**
     * Returns a list of all buckets owned by the authenticated sender of the request.
     */
    listBuckets: async (): Promise<ListAllMyBucketsResult> => {
      const { body } = await this.makeRequest({
        method: 'GET',
        uri: '/',
        arrayTags: ['Bucket'],
      });

      const result = body.ListAllMyBucketsResult;

      return {
        buckets: result.Buckets
          ? result.Buckets.Bucket.map((item) => ({
              creationDate: item.CreationDate,
              name: item.Name,
            }))
          : [],
        owner: {
          displayName: result.Owner.DisplayName,
          id: result.Owner.ID,
        },
      };
    },

    /**
     * Returns some or all (up to 1,000) of the objects in a bucket with each request.
     */
    listObjects: async (
      params: BucketListObjectsParams
    ): Promise<ListBucketResult> => {
      const {
        bucket,
        continuationToken,
        delimiter,
        encodingType,
        maxKeys,
        prefix,
        startAfter,
      } = params;

      const queryParams = toStringRecord({
        'continuation-token': continuationToken,
        delimiter,
        'encoding-type': encodingType,
        'max-keys': maxKeys,
        prefix,
        'start-after': startAfter,
      });

      const { body } = await this.makeRequest({
        method: 'GET',
        uri: `/${bucket}`,
        query: {
          'list-type': '2',
          ...queryParams,
        },
        arrayTags: ['Contents', 'CommonPrefixes'],
      });

      const result = body.ListBucketResult;

      return {
        commonPrefixes: result.CommonPrefixes
          ? result.CommonPrefixes.map((item) => ({
              prefix: item.Prefix,
            }))
          : undefined,
        contents: (result.Contents || []).map((item) => ({
          eTag: item.ETag,
          key: item.Key,
          lastModified: item.LastModified,
          owner: item.Owner
            ? {
                displayName: item.Owner.DisplayName,
                id: item.Owner.ID,
              }
            : undefined,
          size: item.Size,
          storageClass: item.StorageClass,
        })),
        continuationToken: result.ContinuationToken,
        delimiter: result.Delimiter,
        encodingType: result.EncodingType,
        isTruncated: result.IsTruncated,
        keyCount: result.KeyCount,
        maxKeys: result.MaxKeys,
        name: result.Name,
        nextContinuationToken: result.NextContinuationToken,
        prefix: result.Prefix,
        startAfter: result.StartAfter,
      };
    },

    /**
     * Returns metadata about all versions of the objects in a bucket.
     */
    listObjectVersions: async (
      params: BucketListObjectVersionsParams
    ): Promise<ListVersionsResult> => {
      const {
        bucket,
        delimiter,
        encodingType,
        keyMarker,
        maxKeys,
        prefix,
        versionIdMarker,
      } = params;

      const queryParams = toStringRecord({
        delimiter,
        'encoding-type': encodingType,
        'key-marker': keyMarker,
        'max-keys': maxKeys,
        prefix,
        'version-id-marker': versionIdMarker,
      });

      const { body } = await this.makeRequest({
        method: 'GET',
        uri: `/${bucket}`,
        query: {
          versions: '',
          ...queryParams,
        },
        arrayTags: ['Version', 'DeleteMarker', 'CommonPrefixes'],
      });

      const result = body.ListVersionsResult;

      return {
        commonPrefixes: result.CommonPrefixes
          ? result.CommonPrefixes.map((item) => ({
              prefix: item.Prefix,
            }))
          : undefined,
        deleteMarker: result.DeleteMarker
          ? result.DeleteMarker.map((item) => ({
              isLatest: item.IsLatest,
              key: item.Key,
              lastModified: item.LastModified,
              owner: item.Owner
                ? {
                    displayName: item.Owner.DisplayName,
                    id: item.Owner.ID,
                  }
                : undefined,
              versionId: item.VersionId,
            }))
          : undefined,
        delimiter: result.Delimiter,
        encodingType: result.EncodingType,
        isTruncated: result.IsTruncated,
        keyMarker: result.KeyMarker,
        maxKeys: result.MaxKeys,
        name: result.Name,
        nextKeyMarker: result.NextKeyMarker,
        nextVersionIdMarker: result.NextVersionIdMarker,
        prefix: result.Prefix,
        versions: result.Version
          ? result.Version.map((item) => ({
              eTag: item.ETag,
              isLatest: item.IsLatest,
              key: item.Key,
              lastModified: item.LastModified,
              owner: item.Owner
                ? {
                    displayName: item.Owner.DisplayName,
                    id: item.Owner.ID,
                  }
                : undefined,
              size: item.Size,
              storageClass: item.StorageClass,
              versionId: item.VersionId,
            }))
          : [],
        versionIdMarker: result.VersionIdMarker,
      };
    },

    /**
     * Creates a new S3 bucket.
     */
    create: async (params: BucketCreateParams): Promise<void> => {
      const { bucket, acl = {} } = params;

      const headers = YandexStorage.getACLHeaders(acl);

      await this.makeRequest({
        method: 'PUT',
        uri: `/${bucket}`,
        headers,
      });
    },

    /**
     * Deletes the S3 bucket.
     */
    deleteBucket: async (params: BucketDeleteBucketParams): Promise<void> => {
      const { bucket } = params;

      await this.makeRequest({
        method: 'DELETE',
        uri: `/${bucket}`,
      });
    },

    /**
     * Returns the bucket's metadata or an error.
     */
    getMeta: async (params: BucketGetMetaParams): Promise<void> => {
      const { bucket } = params;

      await this.makeRequest({
        method: 'HEAD',
        uri: `/${bucket}`,
      });
    },

    /**
     * Returns the default encryption configuration for an Amazon S3 bucket.
     */
    getBucketEncryption: async (
      params: BucketGetBucketEncryptionParams
    ): Promise<ServerSideEncryptionConfiguration> => {
      const { bucket } = params;

      const { body } = await this.makeRequest({
        method: 'GET',
        uri: `/${bucket}`,
        query: { encryption: '' },
        arrayTags: ['Rule'],
      });

      const result = body.ServerSideEncryptionConfiguration;

      return {
        rules: result.Rule.map((item) => ({
          applyServerSideEncryptionByDefault: {
            kmsMasterKeyId:
              item.ApplyServerSideEncryptionByDefault.KMSMasterKeyID,
            sseAlgorithm: item.ApplyServerSideEncryptionByDefault.SSEAlgorithm,
          },
        })),
      };
    },

    /**
     * This action uses the encryption subresource to configure default encryption and Amazon S3 Bucket Key
     * for an existing bucket.
     */
    putBucketEncryption: async (
      params: BucketPutBucketEncryptionParams
    ): Promise<void> => {
      const { bucket, rules } = params;

      const xml = buildXml({
        '#prolog': {
          xml: {
            version: '1.0',
            encoding: 'UTF-8',
          },
        },
        ServerSideEncryptionConfiguration: {
          '#attrs': { xmlns: 'http://s3.amazonaws.com/doc/2006-03-01/' },
          '#content': {
            Rule: rules.map((item) => ({
              ApplyServerSideEncryptionByDefault: {
                KMSMasterKeyID:
                  item.applyServerSideEncryptionByDefault.kmsMasterKeyId,
                SSEAlgorithm:
                  item.applyServerSideEncryptionByDefault.sseAlgorithm,
              },
            })),
          },
        },
      });

      await this.makeRequest({
        method: 'PUT',
        uri: `/${bucket}`,
        query: { encryption: '' },
        headers: {
          'Content-Length': xml.length.toString(),
          'Content-MD5': YandexStorage.md5(xml),
        },
        arrayTags: ['Rule'],
        payload: xml,
      });
    },

    /**
     * This implementation of the DELETE action resets the default encryption for the bucket as Amazon S3-managed keys.
     */
    deleteBucketEncryption: async (
      params: BucketDeleteBucketEncryptionParams
    ): Promise<void> => {
      const { bucket } = params;

      await this.makeRequest({
        method: 'DELETE',
        uri: `/${bucket}`,
        query: { encryption: '' },
      });
    },

    /**
     * Returns the versioning state of a bucket.
     */
    getBucketVersioning: async (
      params: BucketGetBucketVersioningParams
    ): Promise<VersioningConfiguration> => {
      const { bucket } = params;

      const { body } = await this.makeRequest({
        method: 'GET',
        uri: `/${bucket}`,
        query: { versioning: '' },
      });

      const result = body.VersioningConfiguration;

      return {
        status: result ? result.Status : 'Suspended',
      };
    },

    /**
     * Sets the versioning state of an existing bucket.
     */
    putBucketVersioning: async (
      params: BucketPutBucketVersioningParams
    ): Promise<void> => {
      const { bucket } = params;

      const xml = buildXml({
        '#prolog': {
          xml: {
            version: '1.0',
            encoding: 'UTF-8',
          },
        },
        VersioningConfiguration: {
          '#attrs': { xmlns: 'http://s3.amazonaws.com/doc/2006-03-01/' },
          '#content': {
            Status: params.status,
          },
        },
      });

      await this.makeRequest({
        method: 'PUT',
        uri: `/${bucket}`,
        query: { versioning: '' },
        headers: {
          'Content-Length': xml.length.toString(),
          'Content-MD5': YandexStorage.md5(xml),
        },
        payload: xml,
      });
    },

    /**
     * Returns the logging status of a bucket and the permissions users have to view and modify that status.
     */
    getBucketLogging: async (
      params: BucketGetBucketLoggingParams
    ): Promise<BucketLoggingStatus> => {
      const { bucket } = params;

      const { body } = await this.makeRequest({
        method: 'GET',
        uri: `/${bucket}`,
        query: { logging: '' },
      });

      const result = body.BucketLoggingStatus;

      return {
        enabled: result
          ? {
              targetBucket: result.LoggingEnabled.TargetBucket,
              targetPrefix: result.LoggingEnabled.TargetPrefix,
            }
          : false,
      };
    },

    /**
     * Set the logging parameters for a bucket and to specify permissions for who can view and modify the logging
     * parameters.
     */
    putBucketLogging: async (
      params: BucketPutBucketLoggingParams
    ): Promise<void> => {
      const { bucket, enabled } = params;

      const xml = buildXml({
        '#prolog': {
          xml: {
            version: '1.0',
            encoding: 'UTF-8',
          },
        },
        BucketLoggingStatus: {
          '#attrs': { xmlns: 'http://s3.amazonaws.com/doc/2006-03-01/' },
          '#content': enabled
            ? {
                LoggingEnabled: {
                  TargetBucket: enabled.targetBucket,
                  TargetPrefix: enabled.targetPrefix,
                },
              }
            : '',
        },
      });

      await this.makeRequest({
        method: 'PUT',
        uri: `/${bucket}`,
        query: { logging: '' },
        headers: {
          'Content-Length': xml.length.toString(),
          'Content-MD5': YandexStorage.md5(xml),
        },
        payload: xml,
      });
    },
  };

  public readonly object = {
    /**
     * Retrieves objects from Amazon S3.
     */
    get: async (params: ObjectGetParams): Promise<ObjectGetResponse> => {
      const {
        bucket,
        key,
        ifMatch,
        ifModifiedSince,
        ifNoneMatch,
        ifUnmodifiedSince,
        range,
        responseCacheControl,
        responseContentDisposition,
        responseContentEncoding,
        responseContentLanguage,
        responseContentType,
        responseExpires,
        versionId,
      } = params;

      const reqHeaders = toStringRecord({
        'If-Match': ifMatch,
        'If-Modified-Since': ifModifiedSince,
        'If-None-Match': ifNoneMatch,
        'If-Unmodified-Since': ifUnmodifiedSince,
        Range: range,
      });

      const query = toStringRecord({
        'response-cache-control': responseCacheControl,
        'response-content-disposition': responseContentDisposition,
        'response-content-encoding': responseContentEncoding,
        'response-content-language': responseContentLanguage,
        'response-content-type': responseContentType,
        'response-expires': responseExpires,
        versionId,
      });

      const { headers, body } = await this.makeRequest({
        method: 'GET',
        uri: `/${bucket}/${key}`,
        query,
        headers: reqHeaders,
        format: 'BUFFER',
      });

      const meta = YandexStorage.getMetaFromHeaders(headers);
      const storageClass = headers.get('X-Amz-Storage-Class') || undefined;
      const kmsMasterKeyId = headers.get(
        'X-Amz-Server-Side-Encryption-Aws-Kms-Key-Id'
      );
      const sseAlgorithm = headers.get('X-Amz-Server-Side-Encryption');

      return {
        buffer: body,
        meta: Object.keys(meta).length > 0 ? meta : undefined,
        storageClass: storageClass as StorageClass | undefined,
        serverSideEncryption:
          kmsMasterKeyId && sseAlgorithm
            ? {
                kmsMasterKeyId,
                sseAlgorithm: sseAlgorithm as SSEAlgorithm,
              }
            : undefined,
      };
    },

    /**
     * Returns object metadata.
     */
    getObjectMeta: async (
      params: ObjectGetObjectMetaParams
    ): Promise<ObjectGetObjectMetaResponse> => {
      const {
        bucket,
        key,
        ifMatch,
        ifModifiedSince,
        ifNoneMatch,
        ifUnmodifiedSince,
        range,
      } = params;

      const reqHeaders = toStringRecord({
        'If-Match': ifMatch,
        'If-Modified-Since': ifModifiedSince,
        'If-None-Match': ifNoneMatch,
        'If-Unmodified-Since': ifUnmodifiedSince,
        Range: range,
      });

      const { headers } = await this.makeRequest({
        method: 'HEAD',
        uri: `/${bucket}/${key}`,
        headers: reqHeaders,
      });

      const contentType = headers.get('Content-Type');
      const contentLength = headers.get('Content-Length');
      const meta = YandexStorage.getMetaFromHeaders(headers);
      const storageClass = headers.get('X-Amz-Storage-Class');

      return {
        contentType: contentType || '',
        contentLength: Number(contentLength) || 0,
        meta: Object.keys(meta).length > 0 ? meta : undefined,
        storageClass: (storageClass || undefined) as StorageClass | undefined,
      };
    },

    /**
     * Checks whether a CORS request to an object can be made.
     */
    options: async (
      params: ObjectOptionsParams
    ): Promise<ObjectOptionsResponse> => {
      const { bucket, key, origin, requestMethod, requestHeaders } = params;

      const reqHeaders = toStringRecord({
        Origin: origin,
        'Access-Control-Request-Method': requestMethod,
        'Access-Control-Request-Headers':
          requestHeaders && requestHeaders.length > 0
            ? requestHeaders.join(', ')
            : undefined,
      });

      const { headers } = await this.makeRequest({
        method: 'OPTIONS',
        uri: `/${bucket}/${key}`,
        headers: reqHeaders,
      });

      const allowOrigin = headers.get('Access-Control-Allow-Origin') || '';
      const maxAge = headers.get('Access-Control-Max-Age');
      const allowMethods = headers.get('Access-Control-Allow-Methods') || '';
      const allowHeaders = headers.get('Access-Control-Allow-Headers');
      const exposeHeaders = headers.get('Access-Control-Expose-Headers');

      return {
        allowOrigin,
        maxAge: maxAge ? Number(maxAge) : 0,
        allowMethods: allowMethods.split(', '),
        allowHeaders: allowHeaders ? allowHeaders.split(', ') : undefined,
        exposeHeaders: exposeHeaders ? exposeHeaders.split(', ') : undefined,
      };
    },

    /**
     * This action filters the contents of an Amazon S3 object based on a simple structured query language (SQL)
     * statement.
     */
    selectObjectContent: async (
      params: ObjectSelectObjectContentParams
    ): Promise<SelectObjectContentPayload> => {
      const {
        bucket,
        key,
        expression,
        expressionType,
        inputSerialization: input,
        outputSerialization: output,
        requestProgress,
        scanRange,
      } = params;

      const xml = buildXml({
        '#prolog': {
          xml: {
            version: '1.0',
            encoding: 'UTF-8',
          },
        },
        SelectObjectContentRequest: {
          '#attrs': { xmlns: 'http://s3.amazonaws.com/doc/2006-03-01/' },
          '#content': {
            Expression: expression,
            ExpressionType: expressionType,
            InputSerialization: {
              CompressionType: input.compressionType,
              CSV: input.csv
                ? {
                    AllowQuotedRecordDelimiter:
                      input.csv.allowQuotedRecordDelimiter,
                    Comments: input.csv.comments,
                    FieldDelimiter: input.csv.fieldDelimiter,
                    FileHeaderInfo: input.csv.fileHeaderInfo,
                    QuoteCharacter: input.csv.quoteCharacter,
                    QuoteEscapeCharacter: input.csv.quoteEscapeCharacter,
                    RecordDelimiter: input.csv.recordDelimiter,
                  }
                : undefined,
              JSON: input.json ? { Type: input.json.type } : undefined,
              Parquet: input.parquet,
            },
            OutputSerialization: {
              CSV: output.csv
                ? {
                    FieldDelimiter: output.csv.fieldDelimiter,
                    QuoteCharacter: output.csv.quoteCharacter,
                    QuoteEscapeCharacter: output.csv.quoteEscapeCharacter,
                    QuoteFields: output.csv.quoteFields,
                    RecordDelimiter: output.csv.recordDelimiter,
                  }
                : undefined,
              JSON: output.json
                ? { RecordDelimiter: output.json.recordDelimiter }
                : undefined,
            },
          },
          RequestProgress: requestProgress
            ? { Enabled: requestProgress.enabled }
            : undefined,
          ScanRange: scanRange
            ? {
                End: scanRange.end,
                Start: scanRange.start,
              }
            : undefined,
        },
      });

      const { body } = await this.makeRequest({
        method: 'POST',
        uri: `/${bucket}/${key}`,
        query: {
          select: '',
          'select-type': '2',
        },
        headers: {
          'Content-Length': xml.length.toString(),
          'Content-MD5': YandexStorage.md5(xml),
        },
        payload: xml,
      });

      const result = body.Payload;

      return {
        cont: result.Cont,
        end: result.End,
        progress: {
          details: result.Progress.Details
            ? {
                bytesProcessed: result.Progress.Details.BytesProcessed,
                bytesReturned: result.Progress.Details.BytesReturned,
                bytesScanned: result.Progress.Details.BytesScanned,
              }
            : undefined,
        },
        records: {
          payload: result.Records.Payload,
        },
        stats: {
          details: result.Stats.Details
            ? {
                bytesProcessed: result.Stats.Details.BytesProcessed,
                bytesReturned: result.Stats.Details.BytesReturned,
                bytesScanned: result.Stats.Details.BytesScanned,
              }
            : undefined,
        },
      };
    },

    /**
     * Adds an object and its metadata to a bucket.
     */
    upload: async (params: ObjectUploadParams): Promise<void> => {
      const {
        bucket,
        key,
        body,
        contentType,
        meta,
        storageClass,
        serverSideEncryption: sse,
        acl = {},
      } = params;

      const headers = toStringRecord({
        'Content-Length': body.byteLength.toString(),
        'Content-MD5': YandexStorage.md5(body),
        'Content-Type': contentType,
        'X-Amz-Storage-Class': storageClass,
        'X-Amz-Server-Side-Encryption': sse ? sse.sseAlgorithm : undefined,
        'X-Amz-Server-Side-Encryption-Aws-Kms-Key-Id': sse
          ? sse.kmsMasterKeyId
          : undefined,
        ...YandexStorage.getACLHeaders(acl),
      });
      if (meta) {
        Object.entries(meta).forEach(([metaKey, metaValue]) => {
          headers[`X-Amz-Meta-${metaKey}`] = metaValue;
        });
      }

      await this.makeRequest({
        method: 'PUT',
        uri: `/${bucket}/${key}`,
        headers,
        payload: body,
      });
    },

    /**
     * Creates a copy of an object that is already stored in Amazon S3.
     */
    copy: async (params: ObjectCopyParams): Promise<ObjectCopyResponse> => {
      const {
        bucket,
        key,
        copySource,
        ifMatch,
        ifModifiedSince,
        ifNoneMatch,
        ifUnmodifiedSince,
        meta,
        storageClass,
        serverSideEncryption: sse,
        acl = {},
      } = params;

      const reqHeaders = toStringRecord({
        'X-Amz-Copy-Source': copySource,
        'X-Amz-Copy-Source-If-Match': ifMatch,
        'X-Amz-Copy-Source-If-Modified-Since': ifModifiedSince,
        'X-Amz-Copy-Source-If-None-Match': ifNoneMatch,
        'X-Amz-Copy-Source-If-Unmodified-Since': ifUnmodifiedSince,
        'X-Amz-Metadata-Directive': meta ? 'REPLACE' : 'COPY',
        'X-Amz-Storage-Class': storageClass,
        'X-Amz-Server-Side-Encryption': sse ? sse.sseAlgorithm : undefined,
        'X-Amz-Server-Side-Encryption-Aws-Kms-Key-Id': sse
          ? sse.kmsMasterKeyId
          : undefined,
        ...YandexStorage.getACLHeaders(acl),
      });
      if (meta) {
        Object.entries(meta).forEach(([metaKey, metaValue]) => {
          reqHeaders[`X-Amz-Meta-${metaKey}`] = metaValue;
        });
      }

      const { headers, body } = await this.makeRequest({
        method: 'PUT',
        uri: `/${bucket}/${key}`,
        headers: reqHeaders,
      });

      const result = body.CopyObjectResult;
      const resStorageClass = headers.get('X-Amz-Storage-Class') || undefined;

      return {
        eTag: result.ETag,
        lastModified: result.LastModified,
        storageClass: resStorageClass as StorageClass | undefined,
      };
    },

    /**
     * Removes the null version (if there is one) of an object and inserts a delete marker, which becomes
     * the latest version of the object.
     */
    delete: async (params: ObjectDeleteParams): Promise<void> => {
      const { bucket, key, versionId } = params;

      const query = toStringRecord({
        versionId,
      });

      await this.makeRequest({
        method: 'DELETE',
        uri: `/${bucket}/${key}`,
        query,
      });
    },

    /**
     * This action enables you to delete multiple objects from a bucket using a single HTTP request.
     */
    deleteMultipleObjects: async (
      params: ObjectDeleteMultipleObjectsParams
    ): Promise<DeleteResult> => {
      const { bucket, objects } = params;

      const xml = buildXml({
        '#prolog': {
          xml: {
            version: '1.0',
            encoding: 'UTF-8',
          },
        },
        Delete: {
          Quiet: 'false',
          Object: objects.map((item) => ({
            Key: item.key,
            VersionId: item.versionId,
          })),
        },
      });

      const { body } = await this.makeRequest({
        method: 'POST',
        uri: `/${bucket}`,
        query: { delete: '' },
        headers: {
          'Content-Length': xml.length.toString(),
          'Content-MD5': YandexStorage.md5(xml),
        },
        arrayTags: ['Deleted', 'Error'],
        payload: xml,
      });

      const result = body.DeleteResult;

      return {
        deleted: result.Deleted
          ? result.Deleted.map((item) => ({
              deleteMarker: item.DeleteMarker,
              deleteMarkerVersionId: item.DeleteMarkerVersionId,
              key: item.Key,
              versionId: item.VersionId,
            }))
          : undefined,
        errors: result.Error
          ? result.Error.map((item) => ({
              code: item.Code,
              key: item.Key,
              message: item.Message,
              versionId: item.VersionId,
            }))
          : undefined,
      };
    },
  };

  public readonly multipart = {
    /**
     * This action initiates a multipart upload and returns an upload ID.
     */
    startUpload: async (
      params: MultipartStartUploadParams
    ): Promise<InitiateMultipartUploadResult> => {
      const {
        bucket,
        key,
        contentType,
        meta,
        storageClass,
        serverSideEncryption: sse,
        acl = {},
      } = params;

      const headers = toStringRecord({
        'Content-Type': contentType,
        'X-Amz-Storage-Class': storageClass,
        'X-Amz-Server-Side-Encryption': sse ? sse.sseAlgorithm : undefined,
        'X-Amz-Server-Side-Encryption-Aws-Kms-Key-Id': sse
          ? sse.kmsMasterKeyId
          : undefined,
        ...YandexStorage.getACLHeaders(acl),
      });
      if (meta) {
        Object.entries(meta).forEach(([metaKey, metaValue]) => {
          headers[`X-Amz-Meta-${metaKey}`] = metaValue;
        });
      }

      const { body } = await this.makeRequest({
        method: 'POST',
        uri: `/${bucket}/${key}`,
        query: { uploads: '' },
        headers,
      });

      const result = body.InitiateMultipartUploadResult;

      return {
        bucket: result.Bucket,
        key: result.Key,
        uploadId: result.UploadId,
      };
    },

    /**
     * Uploads a part in a multipart upload.
     */
    uploadPart: async (
      params: MultipartUploadPartParams
    ): Promise<MultipartUploadPartResponse> => {
      const {
        bucket,
        key,
        partNumber,
        uploadId,
        body,
        serverSideEncryption: sse,
      } = params;

      const query = toStringRecord({ partNumber, uploadId });
      const reqHeaders = toStringRecord({
        'Content-Length': body.byteLength.toString(),
        'Content-MD5': YandexStorage.md5(body),
        'X-Amz-Server-Side-Encryption': sse ? sse.sseAlgorithm : undefined,
        'X-Amz-Server-Side-Encryption-Aws-Kms-Key-Id': sse
          ? sse.kmsMasterKeyId
          : undefined,
      });

      const { headers } = await this.makeRequest({
        method: 'PUT',
        uri: `/${bucket}/${key}`,
        query,
        headers: reqHeaders,
        payload: body,
      });

      return {
        eTag: headers.get('ETag') || '',
      };
    },

    /**
     * Uploads a part by copying data from an existing object as data source.
     */
    copyPart: async (
      params: MultipartCopyPartParams
    ): Promise<CopyObjectResult> => {
      const {
        bucket,
        key,
        partNumber,
        uploadId,
        copySource,
        sourceRange,
        ifMatch,
        ifModifiedSince,
        ifNoneMatch,
        ifUnmodifiedSince,
        serverSideEncryption: sse,
      } = params;

      const query = toStringRecord({ partNumber, uploadId });
      const headers = toStringRecord({
        'X-Amz-Copy-Source': copySource,
        'X-Amz-Copy-Source-Range': sourceRange,
        'X-Amz-Copy-Source-If-Match': ifMatch,
        'X-Amz-Copy-Source-If-Modified-Since': ifModifiedSince,
        'X-Amz-Copy-Source-If-None-Match': ifNoneMatch,
        'X-Amz-Copy-Source-If-Unmodified-Since': ifUnmodifiedSince,
        'X-Amz-Server-Side-Encryption': sse ? sse.sseAlgorithm : undefined,
        'X-Amz-Server-Side-Encryption-Aws-Kms-Key-Id': sse
          ? sse.kmsMasterKeyId
          : undefined,
      });

      const { body } = await this.makeRequest({
        method: 'PUT',
        uri: `/${bucket}/${key}`,
        query,
        headers,
      });

      const result = body.CopyPartResult;

      return {
        eTag: result.ETag,
        lastModified: result.LastModified,
      };
    },

    /**
     * Lists the parts that have been uploaded for a specific multipart upload.
     */
    listParts: async (
      params: MultipartListPartsParams
    ): Promise<ListPartsResult> => {
      const { bucket, key, uploadId, maxParts, partNumberMarker } = params;

      const query = toStringRecord({
        uploadId,
        'max-parts': maxParts,
        'part-number-marker': partNumberMarker,
      });

      const { body } = await this.makeRequest({
        method: 'GET',
        uri: `/${bucket}/${key}`,
        query,
        arrayTags: ['Part'],
      });

      const result = body.ListPartsResult;

      return {
        bucket: result.Bucket,
        initiator: {
          displayName: result.Initiator.DisplayName,
          id: result.Initiator.ID,
        },
        isTruncated: result.IsTruncated,
        key: result.Key,
        maxParts: result.MaxParts,
        nextPartNumberMarker: result.NextPartNumberMarker,
        owner: result.Owner
          ? {
              displayName: result.Owner.DisplayName,
              id: result.Owner.ID,
            }
          : undefined,
        parts: result.Part
          ? result.Part.map((part) => ({
              eTag: part.ETag,
              lastModified: part.LastModified,
              partNumber: part.PartNumber,
              size: part.Size,
            }))
          : [],
        partNumberMarker: result.PartNumberMarker,
        storageClass: result.StorageClass,
        uploadId: result.UploadId,
      };
    },

    /**
     * This action aborts a multipart upload.
     */
    abortUpload: async (params: MultipartAbortUploadParams): Promise<void> => {
      const { bucket, key, uploadId } = params;

      const query = { uploadId };

      await this.makeRequest({
        method: 'DELETE',
        uri: `/${bucket}/${key}`,
        query,
      });
    },

    /**
     * Completes a multipart upload by assembling previously uploaded parts.
     */
    completeUpload: async (
      params: MultipartCompleteUploadParams
    ): Promise<CompleteMultipartUploadResult> => {
      const { bucket, key, uploadId, parts } = params;

      const query = { uploadId };

      const xml = buildXml({
        '#prolog': {
          xml: {
            version: '1.0',
            encoding: 'UTF-8',
          },
        },
        CompleteMultipartUpload: {
          '#attrs': { xmlns: 'http://s3.amazonaws.com/doc/2006-03-01/' },
          '#content': {
            Part: parts.map((part) => ({
              ETag: part.eTag,
              PartNumber: part.partNumber,
            })),
          },
        },
      });

      const { body } = await this.makeRequest({
        method: 'POST',
        uri: `/${bucket}/${key}`,
        query,
        headers: {
          'Content-Length': xml.length.toString(),
          'Content-MD5': YandexStorage.md5(xml),
        },
        payload: xml,
      });

      const result = body.CompleteMultipartUploadResult;

      return {
        bucket: result.Bucket,
        eTag: result.ETag,
        key: result.Key,
        location: result.Location,
      };
    },

    /**
     * This action lists in-progress multipart uploads.
     */
    listUploads: async (
      params: MultipartListUploadsParams
    ): Promise<ListMultipartUploadsResult> => {
      const {
        bucket,
        delimiter,
        encodingType,
        keyMarker,
        maxUploads,
        prefix,
        uploadIdMarker,
      } = params;

      const query = toStringRecord({
        delimiter,
        'encoding-type': encodingType,
        'key-marker': keyMarker,
        'max-uploads': maxUploads,
        prefix,
        'upload-id-marker': uploadIdMarker,
      });

      const { body } = await this.makeRequest({
        method: 'GET',
        uri: `/${bucket}`,
        query: { uploads: '', ...query },
        arrayTags: ['Upload', 'CommonPrefixes'],
      });

      const result = body.ListMultipartUploadsResult;

      return {
        bucket: result.Bucket,
        commonPrefixes: result.CommonPrefixes
          ? result.CommonPrefixes.map((item) => ({
              prefix: item.Prefix,
            }))
          : undefined,
        delimiter: result.Delimiter,
        encodingType: result.EncodingType,
        isTruncated: result.IsTruncated,
        keyMarker: result.KeyMarker,
        maxUploads: result.MaxUploads,
        nextKeyMarker: result.NextKeyMarker,
        nextUploadIdMarker: result.NextUploadIdMarker,
        prefix: result.Prefix,
        uploads: result.Upload
          ? result.Upload.map((item) => ({
              initiated: item.Initiated,
              initiator: item.Initiator
                ? {
                    displayName: item.Initiator.DisplayName,
                    id: item.Initiator.ID,
                  }
                : undefined,
              key: item.Key,
              owner: item.Owner
                ? {
                    displayName: item.Owner.DisplayName,
                    id: item.Owner.ID,
                  }
                : undefined,
              storageClass: item.StorageClass,
              uploadId: item.UploadId,
            }))
          : [],
        uploadIdMarker: result.UploadIdMarker,
      };
    },

    /**
     * Adds a large object and its metadata to a bucket using the multipart upload.
     */
    upload: async (
      params: MultipartUploadParams
    ): Promise<CompleteMultipartUploadResult> => {
      const {
        bucket,
        key,
        body: readStream,
        serverSideEncryption,
        concurrentParts = 4,
        partSize = SIZE_5MB,
        bufferSize = SIZE_20MB,
        ...restStartUploadParams
      } = params;

      let errorBeforeStart: Error | undefined;
      readStream.on('error', (error) => {
        errorBeforeStart = error;
      });

      const { uploadId } = await this.multipart.startUpload({
        bucket,
        key,
        serverSideEncryption,
        ...restStartUploadParams,
      });

      if (errorBeforeStart) {
        await this.multipart.abortUpload({ bucket, key, uploadId });
        throw errorBeforeStart;
      }

      return new Promise((resolve, reject) => {
        let response: CompleteMultipartUploadResult;
        const uploadManager = new MultipartUploadManager({
          uploadPart: (part, partNumber) =>
            this.multipart.uploadPart({
              bucket,
              key,
              uploadId,
              partNumber,
              body: part,
              serverSideEncryption,
            }),
          completeUpload: async (parts) => {
            response = await this.multipart.completeUpload({
              bucket,
              key,
              uploadId,
              parts,
            });
            return response;
          },
          maxThreads: concurrentParts,
        });

        const uploadStream = new MultipartUploadStream({
          uploadManager,
          partSize,
          bufferSize,
        });

        readStream.on('data', (chunk) => {
          const result = uploadStream.write(chunk);
          if (!result) readStream.pause();
        });

        uploadStream.on('drain', () => readStream.resume());
        readStream.on('end', () => uploadStream.end());

        const errorHandler = (error: Error) => {
          this.multipart
            .abortUpload({ bucket, key, uploadId })
            .then(() => reject(error))
            .catch(() => reject(error));
        };
        readStream.on('error', errorHandler);
        uploadStream.on('error', errorHandler);
        uploadStream.on('close', () => resolve(response));
      });
    },
  };

  public readonly website = {
    /**
     * Returns the website configuration for a bucket.
     */
    get: async (params: WebsiteGetParams): Promise<WebsiteConfiguration> => {
      const { bucket } = params;

      const { body } = await this.makeRequest({
        method: 'GET',
        uri: `/${bucket}`,
        query: { website: '' },
      });

      const result = body.WebsiteConfiguration;

      return {
        errorDocument: result.ErrorDocument
          ? { key: result.ErrorDocument.Key }
          : undefined,
        indexDocument: result.IndexDocument
          ? { suffix: result.IndexDocument.Suffix }
          : undefined,
        redirectAllRequestsTo: result.RedirectAllRequestsTo
          ? {
              hostName: result.RedirectAllRequestsTo.HostName,
              protocol: result.RedirectAllRequestsTo.Protocol,
            }
          : undefined,
        routingRules: result.RoutingRules
          ? {
              condition: result.RoutingRules.Condition
                ? {
                    httpErrorCodeReturnedEquals:
                      result.RoutingRules.Condition.HttpErrorCodeReturnedEquals,
                    keyPrefixEquals:
                      result.RoutingRules.Condition.KeyPrefixEquals,
                  }
                : undefined,
              redirect: {
                hostName: result.RoutingRules.Redirect.HostName,
                httpRedirectCode: result.RoutingRules.Redirect.HttpRedirectCode,
                protocol: result.RoutingRules.Redirect.Protocol,
                replaceKeyPrefixWith:
                  result.RoutingRules.Redirect.ReplaceKeyPrefixWith,
                replaceKeyWith: result.RoutingRules.Redirect.ReplaceKeyWith,
              },
            }
          : undefined,
      };
    },

    /**
     * Sets the configuration of the website that is specified in the website subresource.
     */
    upload: async (params: WebsiteUploadParams): Promise<void> => {
      const {
        bucket,
        errorDocument,
        indexDocument,
        redirectAllRequestsTo,
        routingRules,
      } = params;

      const xml = buildXml({
        '#prolog': {
          xml: {
            version: '1.0',
            encoding: 'UTF-8',
          },
        },
        WebsiteConfiguration: {
          '#attrs': { xmlns: 'http://s3.amazonaws.com/doc/2006-03-01/' },
          '#content': {
            ErrorDocument: errorDocument
              ? { Key: errorDocument.key }
              : undefined,
            IndexDocument: indexDocument
              ? { Suffix: indexDocument.suffix }
              : undefined,
            RedirectAllRequestsTo: redirectAllRequestsTo
              ? {
                  HostName: redirectAllRequestsTo.hostName,
                  Protocol: redirectAllRequestsTo.protocol,
                }
              : undefined,
            RoutingRules: routingRules
              ? {
                  Condition: routingRules.condition
                    ? {
                        HttpErrorCodeReturnedEquals:
                          routingRules.condition.httpErrorCodeReturnedEquals,
                        KeyPrefixEquals: routingRules.condition.keyPrefixEquals,
                      }
                    : undefined,
                  Redirect: {
                    HostName: routingRules.redirect.hostName,
                    HttpRedirectCode: routingRules.redirect.httpRedirectCode,
                    Protocol: routingRules.redirect.protocol,
                    ReplaceKeyPrefixWith:
                      routingRules.redirect.replaceKeyPrefixWith,
                    ReplaceKeyWith: routingRules.redirect.replaceKeyWith,
                  },
                }
              : undefined,
          },
        },
      });

      await this.makeRequest({
        method: 'PUT',
        uri: `/${bucket}`,
        query: { website: '' },
        headers: {
          'Content-Length': xml.length.toString(),
          'Content-MD5': YandexStorage.md5(xml),
        },
        payload: xml,
      });
    },

    /**
     * This action removes the website configuration for a bucket.
     */
    delete: async (params: WebsiteDeleteParams): Promise<void> => {
      const { bucket } = params;

      await this.makeRequest({
        method: 'DELETE',
        uri: `/${bucket}`,
        query: { website: '' },
      });
    },
  };

  public readonly cors = {
    /**
     * Returns the Cross-Origin Resource Sharing (CORS) configuration information set for the bucket.
     */
    get: async (params: CORSGetParams): Promise<CORSConfiguration> => {
      const { bucket } = params;

      const { body } = await this.makeRequest({
        method: 'GET',
        uri: `/${bucket}`,
        query: { cors: '' },
        arrayTags: [
          'CORSRule',
          'AllowedHeader',
          'AllowedMethod',
          'AllowedOrigin',
          'ExposeHeader',
        ],
      });

      const result = body.CORSConfiguration;

      return {
        rules: result.CORSRule.map((item) => ({
          allowedHeaders: item.AllowedHeader,
          allowedMethods: item.AllowedMethod,
          allowedOrigins: item.AllowedOrigin,
          exposeHeaders: item.ExposeHeader,
          id: item.ID,
          maxAgeSeconds: item.MaxAgeSeconds,
        })),
      };
    },

    /**
     * Sets the cors configuration for your bucket.
     */
    upload: async (params: CORSUploadParams): Promise<void> => {
      const { bucket, rules } = params;

      const xml = buildXml({
        '#prolog': {
          xml: {
            version: '1.0',
            encoding: 'UTF-8',
          },
        },
        CORSConfiguration: {
          '#attrs': { xmlns: 'http://s3.amazonaws.com/doc/2006-03-01/' },
          '#content': {
            CORSRule: rules.map((rule) => ({
              AllowedHeader: rule.allowedHeaders,
              AllowedMethod: rule.allowedMethods,
              AllowedOrigin: rule.allowedOrigins,
              ExposeHeader: rule.exposeHeaders,
              ID: rule.id,
              MaxAgeSeconds: rule.maxAgeSeconds,
            })),
          },
        },
      });

      await this.makeRequest({
        method: 'PUT',
        uri: `/${bucket}`,
        query: { cors: '' },
        headers: {
          'Content-Length': xml.length.toString(),
          'Content-MD5': YandexStorage.md5(xml),
        },
        payload: xml,
      });
    },

    /**
     * Deletes the cors configuration information set for the bucket.
     */
    delete: async (params: CORSDeleteParams): Promise<void> => {
      const { bucket } = params;

      await this.makeRequest({
        method: 'DELETE',
        uri: `/${bucket}`,
        query: { cors: '' },
      });
    },
  };

  public readonly lifecycles = {
    /**
     * Returns the lifecycle configuration information set on the bucket.
     */
    get: async (
      params: LifecyclesGetParams
    ): Promise<LifecycleConfiguration> => {
      const { bucket } = params;

      const { body } = await this.makeRequest({
        method: 'GET',
        uri: `/${bucket}`,
        query: { lifecycle: '' },
        arrayTags: ['Rule', 'Tag', 'NoncurrentVersionTransition', 'Transition'],
      });

      const result = body.LifecycleConfiguration;

      return {
        rules: result.Rule.map((rule) => ({
          abortIncompleteMultipartUpload: rule.AbortIncompleteMultipartUpload
            ? {
                daysAfterInitiation:
                  rule.AbortIncompleteMultipartUpload.DaysAfterInitiation,
              }
            : undefined,
          expiration: rule.Expiration
            ? {
                date: rule.Expiration.Date,
                days: rule.Expiration.Days,
                expiredObjectDeleteMarker:
                  rule.Expiration.ExpiredObjectDeleteMarker,
              }
            : undefined,
          filter: rule.Filter
            ? {
                and: rule.Filter.And
                  ? {
                      objectSizeGreaterThan:
                        rule.Filter.And.ObjectSizeGreaterThan,
                      objectSizeLessThan: rule.Filter.And.ObjectSizeLessThan,
                      prefix: rule.Filter.And.Prefix,
                      tags: rule.Filter.And.Tags
                        ? rule.Filter.And.Tags.map((tag) => ({
                            key: tag.Key,
                            value: tag.Value,
                          }))
                        : undefined,
                    }
                  : undefined,
                objectSizeGreaterThan: rule.Filter.ObjectSizeGreaterThan,
                objectSizeLessThan: rule.Filter.ObjectSizeLessThan,
                prefix: rule.Filter.Prefix,
                tag: rule.Filter.Tag
                  ? {
                      key: rule.Filter.Tag[0].Key,
                      value: rule.Filter.Tag[0].Value,
                    }
                  : undefined,
              }
            : undefined,
          id: rule.ID,
          noncurrentVersionExpiration: rule.NoncurrentVersionExpiration
            ? {
                newerNoncurrentVersions:
                  rule.NoncurrentVersionExpiration.NewerNoncurrentVersions,
                noncurrentDays: rule.NoncurrentVersionExpiration.NoncurrentDays,
              }
            : undefined,
          noncurrentVersionTransitions: rule.NoncurrentVersionTransition
            ? rule.NoncurrentVersionTransition.map((item) => ({
                newerNoncurrentVersions: item.NewerNoncurrentVersions,
                noncurrentDays: item.NoncurrentDays,
                storageClass: item.StorageClass,
              }))
            : undefined,
          prefix: rule.Prefix,
          status: rule.Status,
          transitions: rule.Transition
            ? rule.Transition.map((item) => ({
                date: item.Date,
                days: item.Days,
                storageClass: item.StorageClass,
              }))
            : undefined,
        })),
      };
    },

    /**
     * Creates a new lifecycle configuration for the bucket or replaces an existing lifecycle configuration.
     */
    upload: async (params: LifecyclesUploadParams): Promise<void> => {
      const { bucket, rules } = params;

      const xml = buildXml({
        '#prolog': {
          xml: {
            version: '1.0',
            encoding: 'UTF-8',
          },
        },
        LifecycleConfiguration: {
          '#attrs': { xmlns: 'http://s3.amazonaws.com/doc/2006-03-01/' },
          '#content': {
            Rule: rules.map((rule) => ({
              AbortIncompleteMultipartUpload:
                rule.abortIncompleteMultipartUpload
                  ? {
                      DaysAfterInitiation:
                        rule.abortIncompleteMultipartUpload.daysAfterInitiation,
                    }
                  : undefined,
              Expiration: rule.expiration
                ? {
                    Date: rule.expiration.date,
                    Days: rule.expiration.days,
                    ExpiredObjectDeleteMarker:
                      rule.expiration.expiredObjectDeleteMarker,
                  }
                : undefined,
              Filter: rule.filter
                ? {
                    And: rule.filter.and
                      ? {
                          ObjectSizeGreaterThan:
                            rule.filter.and.objectSizeGreaterThan,
                          ObjectSizeLessThan:
                            rule.filter.and.objectSizeLessThan,
                          Prefix: rule.filter.and.prefix,
                          Tag: rule.filter.and.tags
                            ? rule.filter.and.tags.map((tag) => ({
                                Key: tag.key,
                                Value: tag.value,
                              }))
                            : undefined,
                        }
                      : undefined,
                    ObjectSizeGreaterThan: rule.filter.objectSizeGreaterThan,
                    ObjectSizeLessThan: rule.filter.objectSizeLessThan,
                    Prefix: rule.filter.prefix,
                    Tag: rule.filter.tag
                      ? {
                          Key: rule.filter.tag.key,
                          Value: rule.filter.tag.value,
                        }
                      : undefined,
                  }
                : undefined,
              ID: rule.id,
              NoncurrentVersionExpiration: rule.noncurrentVersionExpiration
                ? {
                    NewerNoncurrentVersions:
                      rule.noncurrentVersionExpiration.newerNoncurrentVersions,
                    NoncurrentDays:
                      rule.noncurrentVersionExpiration.noncurrentDays,
                  }
                : undefined,
              NoncurrentVersionTransition: rule.noncurrentVersionTransitions
                ? rule.noncurrentVersionTransitions.map((item) => ({
                    NewerNoncurrentVersions: item.newerNoncurrentVersions,
                    NoncurrentDays: item.noncurrentDays,
                    StorageClass: item.storageClass,
                  }))
                : undefined,
              Prefix: rule.prefix,
              Status: rule.status,
              Transition: rule.transitions
                ? rule.transitions.map((item) => ({
                    Date: item.date,
                    Days: item.days,
                    StorageClass: item.storageClass,
                  }))
                : undefined,
            })),
          },
        },
      });

      await this.makeRequest({
        method: 'PUT',
        uri: `/${bucket}`,
        query: { lifecycle: '' },
        headers: {
          'Content-Length': xml.length.toString(),
          'Content-MD5': YandexStorage.md5(xml),
        },
        payload: xml,
      });
    },

    /**
     * Deletes the lifecycle configuration from the specified bucket.
     */
    delete: async (params: LifecyclesDeleteParams): Promise<void> => {
      const { bucket } = params;

      await this.makeRequest({
        method: 'DELETE',
        uri: `/${bucket}`,
        query: { lifecycle: '' },
      });
    },
  };

  public readonly acl = {
    /**
     * This implementation of the GET action uses the acl subresource to return the access control list (ACL) of
     * a bucket.
     */
    bucketGetAcl: async (
      params: ACLBucketGetACLParams
    ): Promise<AccessControlPolicy> => {
      const { bucket } = params;

      const { body } = await this.makeRequest({
        method: 'GET',
        uri: `/${bucket}`,
        query: { acl: '' },
        arrayTags: ['Grant'],
        includeAttributes: true,
      });

      const result = body.AccessControlPolicy['#content'];

      return {
        grants:
          result.AccessControlList && result.AccessControlList.Grant
            ? result.AccessControlList.Grant.map((item) => ({
                grantee:
                  item.Grantee &&
                  item.Grantee['#content'] &&
                  item.Grantee['#attrs']
                    ? {
                        displayName: item.Grantee['#content'].DisplayName,
                        id: item.Grantee['#content'].ID,
                        type: item.Grantee['#attrs']['xsi:type'],
                        uri: item.Grantee['#content'].URI,
                      }
                    : undefined,
                permission: item.Permission,
              }))
            : [],
        owner: {
          displayName: result.Owner.DisplayName,
          id: result.Owner.ID,
        },
      };
    },

    /**
     * Sets the permissions on an existing bucket using access control lists (ACL).
     */
    bucketPutAcl: async (params: ACLBucketPutACLParams): Promise<void> => {
      const { bucket, acl } = params;

      const headers = YandexStorage.getACLHeaders(acl);

      await this.makeRequest({
        method: 'PUT',
        uri: `/${bucket}`,
        query: { acl: '' },
        headers,
      });
    },

    /**
     * Returns the access control list (ACL) of an object.
     */
    objectGetAcl: async (
      params: ACLObjectGetACLParams
    ): Promise<AccessControlPolicy> => {
      const { bucket, key } = params;

      const { body } = await this.makeRequest({
        method: 'GET',
        uri: `/${bucket}/${key}`,
        query: { acl: '' },
        arrayTags: ['Grant'],
        includeAttributes: true,
      });

      const result = body.AccessControlPolicy['#content'];

      return {
        grants:
          result.AccessControlList && result.AccessControlList.Grant
            ? result.AccessControlList.Grant.map((item) => ({
                grantee:
                  item.Grantee &&
                  item.Grantee['#content'] &&
                  item.Grantee['#attrs']
                    ? {
                        displayName: item.Grantee['#content'].DisplayName,
                        id: item.Grantee['#content'].ID,
                        type: item.Grantee['#attrs']['xsi:type'],
                        uri: item.Grantee['#content'].URI,
                      }
                    : undefined,
                permission: item.Permission,
              }))
            : [],
        owner: {
          displayName: result.Owner.DisplayName,
          id: result.Owner.ID,
        },
      };
    },

    /**
     * Uses the acl subresource to set the access control list (ACL) permissions for a new or existing object in
     * an S3 bucket.
     */
    objectPutAcl: async (params: ACLObjectPutACLParams): Promise<void> => {
      const { bucket, key, acl } = params;

      const headers = YandexStorage.getACLHeaders(acl);

      await this.makeRequest({
        method: 'PUT',
        uri: `/${bucket}/${key}`,
        query: { acl: '' },
        headers,
      });
    },
  };

  public readonly policy = {
    /**
     * Returns the policy of a specified bucket.
     */
    get: async (params: PolicyGetParams): Promise<Policy> => {
      const { bucket } = params;

      const { body } = await this.makeRequest({
        method: 'GET',
        uri: `/${bucket}`,
        query: { policy: '' },
        format: 'JSON',
      });

      const statements = Array.isArray(body.Statement)
        ? body.Statement
        : [body.Statement];

      return {
        version: body.Version,
        id: body.Id,
        statements: statements.map((statement) => ({
          sid: statement.Sid,
          effect: statement.Effect,
          principal: statement.Principal,
          notPrincipal: statement.NotPrincipal,
          action: statement.Action,
          notAction: statement.NotAction,
          resource: statement.Resource,
          notResource: statement.NotResource,
          condition: statement.Condition,
        })),
      };
    },

    /**
     * Applies an Amazon S3 bucket policy to an Amazon S3 bucket.
     */
    put: async (params: PolicyPutParams): Promise<void> => {
      const { bucket, version, id, statements } = params;

      const json = JSON.stringify({
        Version: version,
        Id: id,
        Statement: statements.map((statement) => ({
          Sid: statement.sid,
          Effect: statement.effect,
          Principal: statement.principal,
          NotPrincipal: statement.notPrincipal,
          Action: statement.action,
          NotAction: statement.notAction,
          Resource: statement.resource,
          NotResource: statement.notResource,
          Condition: statement.condition,
        })),
      });

      await this.makeRequest({
        method: 'PUT',
        uri: `/${bucket}`,
        query: { policy: '' },
        headers: {
          'Content-Length': json.length.toString(),
          'Content-MD5': YandexStorage.md5(json),
        },
        payload: json,
      });
    },

    /**
     * This implementation of the DELETE action uses the policy subresource to delete the policy of a specified bucket.
     */
    delete: async (params: PolicyDeleteParams): Promise<void> => {
      const { bucket } = params;

      await this.makeRequest({
        method: 'DELETE',
        uri: `/${bucket}`,
        query: { policy: '' },
      });
    },
  };

  private static getACLHeaders(acl: ACL) {
    if (typeof acl === 'string') {
      return { 'X-Amz-Acl': acl };
    }
    if (typeof acl === 'object') {
      return Object.entries(acl).reduce((acc, [key, value]) => {
        const headerPart = key
          .replace(/[A-Z]/g, (c) => `-${c}`)
          .replace(/^\w/, (c) => c.toUpperCase());
        return {
          ...acc,
          [`X-Amz-Grant-${headerPart}`]: value
            .map((item) =>
              item.startsWith('http://') ? `uri=${item}` : `id=${item}`
            )
            .join(', '),
        };
      }, {});
    }
    return {};
  }

  private static getMetaFromHeaders(headers: Headers) {
    const meta: Record<string, string> = {};
    headers.forEach((value, name) => {
      if (name.toLowerCase().startsWith('x-amz-meta-')) {
        const metaKey = name.slice('x-amz-meta-'.length);
        meta[metaKey] = value;
      }
    });
    return meta;
  }

  private static md5(data: BinaryLike) {
    return crypto.createHash('md5').update(data).digest('base64');
  }

  private async makeRequest(req: Request): Promise<Response> {
    const {
      method,
      uri,
      query = {},
      headers = {},
      payload,
      arrayTags = [],
      includeAttributes = false,
      format = 'XML',
    } = req;
    const date = new Date();

    const allHeaders: Record<string, string> = {
      ...headers,
      Host: YC_STORAGE_HOST,
      'X-Amz-Date': SigningManager.dateToISO8601(date),
      'X-Amz-Content-Sha256': SigningManager.hash(payload || ''),
    };

    const authString = this.signingManager.createAuthString(
      { method, uri, query, headers: allHeaders, payload },
      date
    );

    const queryString = SigningManager.getQueryString(query);
    const queryStringUri = queryString ? `?${queryString}` : '';
    const url = `https://${YC_STORAGE_HOST}${uri}${queryStringUri}`;

    const response = await fetch(url, {
      method,
      headers: {
        ...allHeaders,
        Authorization: authString,
      },
      body: payload as any, // eslint-disable-line
    });

    if (response.status === 404) throw new NotFoundError();

    if (response.status >= 300) {
      const textRes = await response.text();
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const res = parseXml<any>(textRes);
      throw new YCStorageError({
        code: res.Error.Code,
        message: res.Error.Message,
        resource: res.Error.Resource,
        requestId: res.Error.RequestId,
      });
    }

    if (format === 'BUFFER') {
      return {
        headers: response.headers,
        body: await response.arrayBuffer(),
      };
    }

    if (format === 'JSON') {
      return {
        headers: response.headers,
        body: await response.json(),
      };
    }

    const textRes = await response.text();
    if (textRes === '') {
      return {
        headers: response.headers,
        body: undefined,
      };
    }

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const body = parseXml<any>(textRes, {
      isArray: arrayTags,
      include: includeAttributes ? 'ATTRIBUTES' : undefined,
    });
    return {
      headers: response.headers,
      body,
    };
  }
}

export * from './types/acl.js';
export * from './types/base.js';
export * from './types/bucket.js';
export * from './types/cors.js';
export * from './types/lifecycles.js';
export * from './types/multipart.js';
export * from './types/object.js';
export * from './types/policy.js';
export * from './types/website.js';

export default YandexStorage;
