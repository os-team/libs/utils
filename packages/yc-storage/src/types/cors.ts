import { type WithBucketParam } from './base.js';

export interface CORSRule {
  /**
   * Headers that are specified in the Access-Control-Request-Headers header. These headers are allowed in a preflight
   * OPTIONS request. In response to any preflight OPTIONS request, Amazon S3 returns any requested headers that
   * are allowed.
   */
  allowedHeaders?: string[];
  /**
   * An HTTP method that you allow the origin to execute. Valid values are GET, PUT, HEAD, POST, and DELETE.
   */
  allowedMethods: string[];
  /**
   * One or more origins you want customers to be able to access the bucket from.
   */
  allowedOrigins: string[];
  /**
   * One or more headers in the response that you want customers to be able to access from their applications
   * (for example, from a JavaScript XMLHttpRequest object).
   */
  exposeHeaders?: string[];
  /**
   * Unique identifier for the rule. The value cannot be longer than 255 characters.
   */
  id?: string;
  /**
   * The time in seconds that your browser is to cache the preflight response for the specified resource.
   */
  maxAgeSeconds?: number;
}

export interface CORSConfiguration {
  /**
   * A set of origins and methods (cross-origin access that you want to allow).
   * You can add up to 100 rules to the configuration.
   */
  rules: CORSRule[];
}

export type CORSGetParams = WithBucketParam;
export type CORSUploadParams = WithBucketParam & CORSConfiguration;
export type CORSDeleteParams = WithBucketParam;
