import { type WithBucketParam } from './base.js';

export type WebsiteGetParams = WithBucketParam;

export interface ErrorDocument {
  /**
   * The object key name to use when a 4XX class error occurs.
   */
  key: string;
}

export interface IndexDocument {
  /**
   * A suffix that is appended to a request that is for a directory on the website endpoint (for example, if the suffix
   * is index.html and you make a request to samplebucket/images/ the data that is returned will be for the object with
   * the key name images/index.html) The suffix must not be empty and must not include a slash character.
   */
  suffix: string;
}

export type Protocol = 'http' | 'https';

export interface RedirectAllRequestsTo {
  /**
   * Name of the host where requests are redirected.
   */
  hostName: string;
  /**
   * Protocol to use when redirecting requests. The default is the protocol that is used in the original request.
   */
  protocol?: Protocol;
}

export interface Condition {
  /**
   * The HTTP error code when the redirect is applied. In the event of an error, if the error code equals this value,
   * then the specified redirect is applied. Required when parent element Condition is specified and sibling
   * KeyPrefixEquals is not specified. If both are specified, then both must be true for the redirect to be applied.
   */
  httpErrorCodeReturnedEquals?: string;
  /**
   * The object key name prefix when the redirect is applied. For example, to redirect requests for ExamplePage.html,
   * the key prefix will be ExamplePage.html. To redirect request for all pages with the prefix docs/, the key prefix
   * will be /docs, which identifies all objects in the docs/ folder. Required when the parent element Condition is
   * specified and sibling HttpErrorCodeReturnedEquals is not specified. If both conditions are specified, both must be
   * true for the redirect to be applied.
   */
  keyPrefixEquals?: string;
}

export interface Redirect {
  /**
   * The host name to use in the redirect request.
   */
  hostName?: string;
  /**
   * The HTTP redirect code to use on the response. Not required if one of the siblings is present.
   */
  httpRedirectCode?: string;
  /**
   * Protocol to use when redirecting requests. The default is the protocol that is used in the original request.
   */
  protocol?: Protocol;
  /**
   * The object key prefix to use in the redirect request. For example, to redirect requests for all pages with prefix
   * docs/ (objects in the docs/ folder) to documents/, you can set a condition block with KeyPrefixEquals set to docs/
   * and in the Redirect set ReplaceKeyPrefixWith to /documents. Not required if one of the siblings is present.
   * Can be present only if ReplaceKeyWith is not provided.
   */
  replaceKeyPrefixWith?: string;
  /**
   * The specific object key to use in the redirect request. For example, redirect request to error.html.
   * Not required if one of the siblings is present. Can be present only if ReplaceKeyPrefixWith is not provided.
   */
  replaceKeyWith?: string;
}

export interface RoutingRules {
  /**
   * A container for describing a condition that must be met for the specified redirect to apply. For example,
   * 1. If request is for pages in the /docs folder, redirect to the /documents folder.
   * 2. If request results in HTTP error 4xx, redirect request to another host where you might process the error.
   */
  condition?: Condition;
  /**
   * Container for redirect information. You can redirect requests to another host, to another page, or with another
   * protocol. In the event of an error, you can specify a different error code to return.
   */
  redirect: Redirect;
}

export interface WebsiteConfiguration {
  /**
   * The object key name of the website error document to use for 4XX class errors.
   */
  errorDocument?: ErrorDocument;
  /**
   * The name of the index document for the website (for example index.html).
   */
  indexDocument?: IndexDocument;
  /**
   * Specifies the redirect behavior of all requests to a website endpoint of an Amazon S3 bucket.
   */
  redirectAllRequestsTo?: RedirectAllRequestsTo;
  /**
   * Rules that define when a redirect is applied and the redirect behavior.
   */
  routingRules?: RoutingRules;
}

export type WebsiteUploadParams = WithBucketParam & WebsiteConfiguration;

export type WebsiteDeleteParams = WithBucketParam;
