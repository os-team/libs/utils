export interface WithBucketParam {
  /**
   * The bucket name.
   */
  bucket: string;
}

export interface WithObjectParam extends WithBucketParam {
  /**
   * The object key.
   */
  key: string;
}

export interface Owner {
  /**
   * Container for the display name of the owner.
   */
  displayName?: string;
  /**
   * Container for the ID of the owner.
   */
  id?: string;
}
