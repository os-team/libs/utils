import {
  type Owner,
  type WithBucketParam,
  type WithObjectParam,
} from './base.js';

export type CannedACL =
  | 'private'
  | 'bucket-owner-full-control'
  | 'public-read'
  | 'public-read-write'
  | 'authenticated-read';

export enum PredefinedGroups {
  AuthenticatedUsers = 'http://acs.amazonaws.com/groups/global/AuthenticatedUsers',
  AllUsers = 'http://acs.amazonaws.com/groups/global/AllUsers',
}

export type ID = string;
export type ACLUnit = PredefinedGroups | ID;

export interface CustomACL {
  /**
   * Allows grantee to list the objects in the bucket.
   */
  read?: ACLUnit[];
  /**
   * Allows grantee to create new objects in the bucket.
   */
  write?: ACLUnit[];
  /**
   * Allows grantee to read the bucket ACL.
   */
  readAcp?: ACLUnit[];
  /**
   * Allows grantee to write the ACL for the applicable bucket.
   */
  writeAcp?: ACLUnit[];
  /**
   * Allows grantee the read, write, read ACP, and write ACP permissions on the bucket.
   */
  fullControl?: ACLUnit[];
}

export type ACL = CannedACL | CustomACL;

export type GranteeType = 'CanonicalUser' | 'Group';

export interface Grantee {
  /**
   * Screen name of the grantee.
   */
  displayName?: string;
  /**
   * The canonical user ID of the grantee.
   */
  id?: string;
  /**
   * Type of grantee.
   */
  type: GranteeType;
  /**
   * URI of the grantee group.
   */
  uri?: string;
}

export type Permission =
  | 'FULL_CONTROL'
  | 'WRITE'
  | 'WRITE_ACP'
  | 'READ'
  | 'READ_ACP';

export interface Grant {
  /**
   * The person being granted permissions.
   */
  grantee?: Grantee;
  /**
   * Specifies the permission given to the grantee.
   */
  permission?: Permission;
}

export interface AccessControlPolicy {
  /**
   * A list of grants.
   */
  grants: Grant[];
  /**
   * Container for the bucket owner's display name and ID.
   */
  owner: Owner;
}

export type ACLBucketGetACLParams = WithBucketParam;

export interface ACLBucketPutACLParams extends WithBucketParam {
  acl: ACL;
}

export type ACLObjectGetACLParams = WithObjectParam;

export interface ACLObjectPutACLParams extends WithObjectParam {
  acl: ACL;
}
