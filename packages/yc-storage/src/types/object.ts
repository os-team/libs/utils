import {
  type ApplyServerSideEncryptionByDefault,
  type StorageClass,
} from './bucket.js';
import { type ACL } from './acl.js';
import { type WithBucketParam, type WithObjectParam } from './base.js';

export interface ObjectGetParams extends WithObjectParam {
  /**
   * Return the object only if its entity tag (ETag) is the same as the one specified;
   * otherwise, return a 412 (precondition failed) error.
   */
  ifMatch?: string;
  /**
   * Return the object only if it has been modified since the specified time;
   * otherwise, return a 304 (not modified) error.
   */
  ifModifiedSince?: string;
  /**
   * Return the object only if its entity tag (ETag) is different from the one specified;
   * otherwise, return a 304 (not modified) error.
   */
  ifNoneMatch?: string;
  /**
   * Return the object only if it has not been modified since the specified time;
   * otherwise, return a 412 (precondition failed) error.
   */
  ifUnmodifiedSince?: string;
  /**
   * Downloads the specified range bytes of an object.
   */
  range?: string;
  /**
   * Sets the Cache-Control header of the response.
   */
  responseCacheControl?: string;
  /**
   * Sets the Content-Disposition header of the response
   */
  responseContentDisposition?: string;
  /**
   * Sets the Content-Encoding header of the response.
   */
  responseContentEncoding?: string;
  /**
   * Sets the Content-Language header of the response.
   */
  responseContentLanguage?: string;
  /**
   * Sets the Content-Type header of the response.
   */
  responseContentType?: string;
  /**
   * Sets the Expires header of the response.
   */
  responseExpires?: string;
  /**
   * VersionId used to reference a specific version of the object.
   */
  versionId?: string;
}

export interface ObjectGetResponse {
  /**
   * The content of the file.
   */
  buffer: Buffer;
  /**
   * User-defined object metadata.
   */
  meta?: Record<string, string>;
  /**
   * Object storage class.
   */
  storageClass?: StorageClass;
  /**
   * The server-side encryption algorithm used when storing this object.
   */
  serverSideEncryption?: ApplyServerSideEncryptionByDefault;
}

export interface ObjectGetObjectMetaParams extends WithObjectParam {
  /**
   * Return the object only if its entity tag (ETag) is the same as the one specified;
   * otherwise, return a 412 (precondition failed) error.
   */
  ifMatch?: string;
  /**
   * Return the object only if it has been modified since the specified time;
   * otherwise, return a 304 (not modified) error.
   */
  ifModifiedSince?: string;
  /**
   * Return the object only if its entity tag (ETag) is different from the one specified;
   * otherwise, return a 304 (not modified) error.
   */
  ifNoneMatch?: string;
  /**
   * Return the object only if it has not been modified since the specified time;
   * otherwise, return a 412 (precondition failed) error.
   */
  ifUnmodifiedSince?: string;
  /**
   * Downloads the specified range bytes of an object.
   */
  range?: string;
}

export interface ObjectGetObjectMetaResponse {
  /**
   * A standard MIME type describing the format of the object data.
   */
  contentType: string;
  /**
   * Size of the body in bytes.
   */
  contentLength: number;
  /**
   * User-defined object metadata.
   */
  meta?: Record<string, string>;
  /**
   * Object storage class.
   */
  storageClass?: StorageClass;
}

export interface ObjectOptionsParams extends WithObjectParam {
  /**
   * Request source domain.
   */
  origin: string;
  /**
   * HTTP method to be used to send a request to a resource.
   */
  requestMethod: string;
  /**
   * List of headers to be sent in a subsequent request to the object.
   */
  requestHeaders?: string[];
}

export interface ObjectOptionsResponse {
  /**
   * The domain that was passed in the Origin request header.
   */
  allowOrigin: string;
  /**
   * Allowed response caching time (in seconds).
   */
  maxAge: number;
  /**
   * Allowed request methods.
   */
  allowMethods: string[];
  /**
   * List of HTTP headers that can be used in a subsequent request to the object.
   */
  allowHeaders?: string[];
  /**
   * List of HTTP headers that the JavaScript client receives.
   */
  exposeHeaders?: string[];
}

export type ExpressionType = 'SQL';
export type CompressionType = 'NONE' | 'GZIP' | 'BZIP2';
export type FileHeaderInfo = 'NONE' | 'IGNORE' | 'USE';

export interface CSVInput {
  /**
   * Specifies that CSV field values may contain quoted record delimiters and such records should be allowed.
   * Default value is FALSE. Setting this value to TRUE may lower performance.
   */
  allowQuotedRecordDelimiter?: boolean;
  /**
   * A single character used to indicate that a row should be ignored when the character is present at the start of
   * that row. You can specify any character to indicate a comment line.
   */
  comments?: string;
  /**
   * A single character used to separate individual fields in a record. You can specify an arbitrary delimiter.
   */
  fieldDelimiter?: string;
  /**
   * Describes the first line of input.
   */
  fileHeaderInfo?: FileHeaderInfo;
  /**
   * A single character used for escaping when the field delimiter is part of the value. For example, if the value
   * is a, b, Amazon S3 wraps this field value in quotation marks, as follows: " a , b ".
   */
  quoteCharacter?: string;
  /**
   * A single character used for escaping the quotation mark character inside an already escaped value. For example,
   * the value """ a , b """ is parsed as " a , b ".
   */
  quoteEscapeCharacter?: string;
  /**
   * A single character used to separate individual records in the input. Instead of the default value, you can specify
   * an arbitrary delimiter.
   */
  recordDelimiter?: string;
}

export type JSONInputType = 'DOCUMENT' | 'LINES';

export interface JSONInput {
  /**
   * The type of JSON. Valid values: Document, Lines.
   */
  type?: JSONInputType;
}

export type ParquetInput = any; // eslint-disable-line @typescript-eslint/no-explicit-any

export interface InputSerialization {
  /**
   * Specifies object's compression format. Valid values: NONE, GZIP, BZIP2. Default Value: NONE.
   */
  compressionType?: CompressionType;
  /**
   * Describes the serialization of a CSV-encoded object.
   */
  csv?: CSVInput;
  /**
   * Specifies JSON as object's input serialization format.
   */
  json?: JSONInput;
  /**
   * Specifies Parquet as object's input serialization format.
   */
  parquet?: ParquetInput;
}

export type QuoteFields = 'ALWAYS' | 'ASNEEDED';

export interface CSVOutput {
  /**
   * The value used to separate individual fields in a record. You can specify an arbitrary delimiter.
   */
  fieldDelimiter?: string;
  /**
   * A single character used for escaping when the field delimiter is part of the value. For example, if the value
   * is a, b, Amazon S3 wraps this field value in quotation marks, as follows: " a , b ".
   */
  quoteCharacter?: string;
  /**
   * The single character used for escaping the quote character inside an already escaped value.
   */
  quoteEscapeCharacter?: string;
  /**
   * Indicates whether to use quotation marks around output fields.
   */
  quoteFields?: QuoteFields;
  /**
   * A single character used to separate individual records in the output. Instead of the default value, you can specify
   * an arbitrary delimiter.
   */
  recordDelimiter?: string;
}

export interface JSONOutput {
  /**
   * The value used to separate individual records in the output. If no value is specified, Amazon S3 uses a newline
   * character ('\n').
   */
  recordDelimiter?: string;
}

export interface OutputSerialization {
  /**
   * Describes the serialization of CSV-encoded Select results.
   */
  csv?: CSVOutput;
  /**
   * Specifies JSON as request's output serialization format.
   */
  json?: JSONOutput;
}

export interface RequestProgress {
  /**
   * Specifies whether periodic QueryProgress frames should be sent. Default value: FALSE.
   */
  enabled?: boolean;
}

export interface ScanRange {
  /**
   * Specifies the end of the byte range. This parameter is optional. Valid values: non-negative integers.
   * The default value is one less than the size of the object being queried. If only the End parameter is supplied,
   * it is interpreted to mean scan the last N bytes of the file.
   * For example, <scanrange><end>50</end></scanrange> means scan the last 50 bytes.
   */
  end?: number;
  /**
   * Specifies the start of the byte range. This parameter is optional. Valid values: non-negative integers.
   * The default value is 0. If only start is supplied, it means scan from that point to the end of the file.
   * For example, <scanrange><start>50</start></scanrange> means scan from byte 50 until the end of the file.
   */
  start?: number;
}

export interface ObjectSelectObjectContentParams extends WithObjectParam {
  /**
   * The expression that is used to query the object.
   */
  expression: string;
  /**
   * The type of the provided expression (for example, SQL).
   */
  expressionType: ExpressionType;
  /**
   * Describes the format of the data in the object that is being queried.
   */
  inputSerialization: InputSerialization;
  /**
   * Describes the format of the data that you want Amazon S3 to return in response.
   */
  outputSerialization: OutputSerialization;
  /**
   * Specifies if periodic request progress information should be enabled.
   */
  requestProgress?: RequestProgress;
  /**
   * Specifies the byte range of the object to get the records from. A record is processed when its first byte is
   * contained by the range. This parameter is optional, but when specified, it must not be empty. See RFC 2616,
   * Section 14.35.1 about how to specify the start and end of the range.
   */
  scanRange?: ScanRange;
}

export interface Progress {
  /**
   * The current number of uncompressed object bytes processed.
   */
  bytesProcessed?: number;
  /**
   * The current number of bytes of records payload data returned.
   */
  bytesReturned?: number;
  /**
   * The current number of object bytes scanned.
   */
  bytesScanned?: number;
}

export interface ProgressEvent {
  /**
   * The Progress event details.
   */
  details?: Progress;
}

export interface RecordsEvent {
  /**
   * The byte array of partial, one or more result records.
   */
  payload?: any; // eslint-disable-line @typescript-eslint/no-explicit-any
}

export interface Stats {
  /**
   * The total number of uncompressed object bytes processed.
   */
  bytesProcessed?: number;
  /**
   * The total number of bytes of records payload data returned.
   */
  bytesReturned?: number;
  /**
   * The total number of object bytes scanned.
   */
  bytesScanned?: number;
}

export interface StatsEvent {
  /**
   * The Stats event details.
   */
  details?: Stats;
}

export interface SelectObjectContentPayload {
  /**
   * The Continuation Event.
   */
  cont: any; // eslint-disable-line @typescript-eslint/no-explicit-any
  /**
   * The End Event.
   */
  end: any; // eslint-disable-line @typescript-eslint/no-explicit-any
  /**
   * The Progress Event.
   */
  progress: ProgressEvent;
  /**
   * The Records Event.
   */
  records: RecordsEvent;
  /**
   * The Stats Event.
   */
  stats: StatsEvent;
}

export interface ObjectUploadParams extends WithObjectParam {
  /**
   * The content of the file.
   */
  body: Buffer;
  /**
   * Data type in a request.
   * See https://en.wikipedia.org/wiki/Media_type
   */
  contentType?: string;
  /**
   * User-defined object metadata.
   */
  meta?: Record<string, string>;
  /**
   * Object storage class.
   */
  storageClass?: StorageClass;
  /**
   * The server-side encryption algorithm used when storing this object.
   */
  serverSideEncryption?: ApplyServerSideEncryptionByDefault;
  /**
   * Access control list.
   */
  acl?: ACL;
}

export interface ObjectCopyParams extends WithObjectParam {
  /**
   * Specifies the source object for the copy operation.
   * Specify the name of the source bucket and the key of the source object, separated by a slash (/).
   */
  copySource: string;
  /**
   * Copies the object if its entity tag (ETag) matches the specified tag.
   */
  ifMatch?: string;
  /**
   * Copies the object if it has been modified since the specified time.
   */
  ifModifiedSince?: string;
  /**
   * Copies the object if its entity tag (ETag) is different than the specified ETag.
   */
  ifNoneMatch?: string;
  /**
   * Copies the object if it hasn't been modified since the specified time.
   */
  ifUnmodifiedSince?: string;
  /**
   * User-defined object metadata.
   */
  meta?: Record<string, string>;
  /**
   * Object storage class.
   */
  storageClass?: StorageClass;
  /**
   * The server-side encryption algorithm used when storing this object.
   */
  serverSideEncryption?: ApplyServerSideEncryptionByDefault;
  /**
   * Access control list.
   */
  acl?: ACL;
}

export interface CopyObjectResult {
  /**
   * Returns the ETag of the new object. The ETag reflects only changes to the contents of an object, not its metadata.
   */
  eTag: string;
  /**
   * Creation date of the object.
   */
  lastModified: string;
}

export interface ObjectCopyResponse extends CopyObjectResult {
  /**
   * Object storage class.
   */
  storageClass?: StorageClass;
}

export interface ObjectDeleteParams extends WithObjectParam {
  /**
   * VersionId used to reference a specific version of the object.
   */
  versionId?: string;
}

export interface ObjectIdentifier {
  /**
   * Key name of the object.
   */
  key: string;
  /**
   * VersionId for the specific version of the object to delete.
   */
  versionId?: string;
}

export interface ObjectDeleteMultipleObjectsParams extends WithBucketParam {
  /**
   * The objects to delete.
   */
  objects: ObjectIdentifier[];
}

export interface DeletedObject {
  /**
   * Specifies whether the versioned object that was permanently deleted was (true) or was not (false) a delete marker.
   * In a simple DELETE, this header indicates whether (true) or not (false) a delete marker was created.
   */
  deleteMarker?: boolean;
  /**
   * The version ID of the delete marker created as a result of the DELETE operation. If you delete a specific object
   * version, the value returned by this header is the version ID of the object version deleted.
   */
  deleteMarkerVersionId?: string;
  /**
   * The name of the deleted object.
   */
  key?: string;
  /**
   * The version ID of the deleted object.
   */
  versionId?: string;
}

export interface Error {
  /**
   * The error code is a string that uniquely identifies an error condition. It is meant to be read and understood
   * by programs that detect and handle errors by type.
   */
  code?: string;
  /**
   * The error key.
   */
  key?: string;
  /**
   * The error message contains a generic description of the error condition in English. It is intended for a human
   * audience. Simple programs display the message directly to the end user if they encounter an error condition
   * they don't know how or don't care to handle. Sophisticated programs with more exhaustive error handling and
   * proper internationalization are more likely to ignore the error message.
   */
  message?: string;
  /**
   * The version ID of the error.
   */
  versionId?: string;
}

export interface DeleteResult {
  /**
   * Container element for a successful delete. It identifies the object that was successfully deleted.
   */
  deleted?: DeletedObject[];
  /**
   * Container for a failed delete action that describes the object that Amazon S3 attempted to delete and the error
   * it encountered.
   */
  errors?: Error[];
}
