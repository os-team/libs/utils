import { type WithBucketParam } from './base.js';

export type PolicyEffect = 'Allow' | 'Deny';
export type PrincipalKey = 'AWS' | 'Federated' | 'Service' | 'CanonicalUser';
export type Principal = '*' | Record<PrincipalKey, string | string[]>;
export type Action = '*' | string | string[];
export type Resource = '*' | string | string[];
export type ConditionValue = string | number | boolean | string[];
export type PolicyCondition = Record<string, Record<string, ConditionValue>>;

export interface PolicyStatement {
  /**
   * Rule ID.
   */
  sid?: string;
  /**
   * Specifies whether the requested action is denied or allowed.
   */
  effect: PolicyEffect;
  /**
   * ID of the recipient of the requested permission.
   */
  principal?: Principal;
  /**
   * ID of the entity that won't receive the requested permission.
   */
  notPrincipal?: Principal;
  /**
   * Determines the action to be executed when the policy is triggered.
   */
  action?: Action;
  /**
   * Determines the action that won't be executed when the policy is triggered.
   */
  notAction?: Action;
  /**
   * Specifies the resource that the action will be performed on.
   */
  resource?: Resource;
  /**
   * Specifies the resource that won't be affected by the action.
   */
  notResource?: Resource;
  /**
   * Condition that will be checked.
   */
  condition?: PolicyCondition;
}

export interface Policy {
  /**
   * Access policy description version.
   */
  version?: string;
  /**
   * General information about the policy. Some Yandex Cloud services require the uniqueness of this value.
   */
  id?: string;
  /**
   * Access policy rules.
   */
  statements: PolicyStatement[];
}

export type PolicyGetParams = WithBucketParam;
export type PolicyPutParams = WithBucketParam & Policy;
export type PolicyDeleteParams = WithBucketParam;
