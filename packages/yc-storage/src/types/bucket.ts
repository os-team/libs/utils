import { type ACL } from './acl.js';
import { type Owner, type WithBucketParam } from './base.js';

export interface Bucket {
  /**
   * Date the bucket was created. This date can change when making changes to your bucket, such as editing its bucket
   * policy.
   */
  creationDate?: string;
  /**
   * The name of the bucket.
   */
  name?: string;
}

export interface ListAllMyBucketsResult {
  /**
   * The list of buckets owned by the requester.
   */
  buckets: Bucket[];
  /**
   * The owner of the buckets listed.
   */
  owner: Owner;
}

export type EncodingType = 'url';

export interface BucketListObjectsParams extends WithBucketParam {
  /**
   * ContinuationToken indicates Amazon S3 that the list is being continued on this bucket with a token.
   * ContinuationToken is obfuscated and is not a real key.
   */
  continuationToken?: string;
  /**
   * A delimiter is a character you use to group keys.
   */
  delimiter?: string;
  /**
   * Encoding type used by Amazon S3 to encode object keys in the response.
   */
  encodingType?: EncodingType;
  /**
   * Sets the maximum number of keys returned in the response. By default the action returns up to 1,000 key names.
   * The response might contain fewer keys but will never contain more.
   */
  maxKeys?: number;
  /**
   * Limits the response to keys that begin with the specified prefix.
   */
  prefix?: string;
  /**
   * StartAfter is where you want Amazon S3 to start listing from. Amazon S3 starts listing after this specified key.
   * StartAfter can be any key in the bucket.
   */
  startAfter?: string;
}

export type StorageClass = 'STANDARD' | 'COLD';

export interface Object {
  /**
   * The entity tag is a hash of the object. The ETag reflects changes only to the contents of an object, not its metadata.
   */
  eTag?: string;
  /**
   * The name that you assign to an object. You use the object key to retrieve the object.
   */
  key?: string;
  /**
   * Creation date of the object.
   */
  lastModified?: string;
  /**
   * The owner of the object
   */
  owner?: Owner;
  /**
   * Size in bytes of the object
   */
  size?: number;
  /**
   * The class of storage used to store the object.
   */
  storageClass?: StorageClass;
}

export interface CommonPrefix {
  /**
   * Container for the specified common prefix.
   */
  prefix?: string;
}

export interface ListBucketResult {
  /**
   * All of the keys (up to 1,000) rolled up into a common prefix count as a single return when calculating the number
   * of returns.
   * A response can contain CommonPrefixes only if you specify a delimiter.
   * CommonPrefixes contains all (if there are any) keys between Prefix and the next occurrence of the string specified
   * by a delimiter.
   * CommonPrefixes lists keys that act like subdirectories in the directory specified by Prefix.
   * For example, if the prefix is notes/ and the delimiter is a slash (/) as in notes/summer/july, the common prefix
   * is notes/summer/. All of the keys that roll up into a common prefix count as a single return when calculating
   * the number of returns.
   */
  commonPrefixes?: CommonPrefix[];
  /**
   * Metadata about each object returned.
   */
  contents: Object[];
  /**
   * If ContinuationToken was sent with the request, it is included in the response.
   */
  continuationToken?: string;
  /**
   * Causes keys that contain the same string between the prefix and the first occurrence of the delimiter to be
   * rolled up into a single result element in the CommonPrefixes collection. These rolled-up keys are not returned
   * elsewhere in the response. Each rolled-up result counts as only one return against the MaxKeys value.
   */
  delimiter?: string;
  /**
   * Encoding type used by Amazon S3 to encode object key names in the XML response.
   * If you specify the encoding-type request parameter, Amazon S3 includes this element in the response, and returns
   * encoded key name values in the following response elements: Delimiter, Prefix, Key, and StartAfter.
   */
  encodingType?: EncodingType;
  /**
   * Set to false if all of the results were returned. Set to true if more keys are available to return. If the number
   * of results exceeds that specified by MaxKeys, all of the results might not be returned.
   */
  isTruncated: boolean;
  /**
   * KeyCount is the number of keys returned with this request. KeyCount will always be less than or equals to
   * MaxKeys field. Say you ask for 50 keys, your result will include less than equals 50 keys
   */
  keyCount: number;
  /**
   * Sets the maximum number of keys returned in the response. By default the action returns up to 1,000 key names.
   * The response might contain fewer keys but will never contain more.
   */
  maxKeys: number;
  /**
   * The bucket name.
   */
  name: string;
  /**
   * NextContinuationToken is sent when isTruncated is true, which means there are more keys in the bucket that can be
   * listed. The next list requests to Amazon S3 can be continued with this NextContinuationToken.
   * NextContinuationToken is obfuscated and is not a real key
   */
  nextContinuationToken?: string;
  /**
   * Keys that begin with the indicated prefix.
   */
  prefix: string;
  /**
   * If StartAfter was sent with the request, it is included in the response.
   */
  startAfter?: string;
}

export interface BucketListObjectVersionsParams extends WithBucketParam {
  /**
   * A delimiter is a character that you specify to group keys. All keys that contain the same string between
   * the prefix and the first occurrence of the delimiter are grouped under a single result element in CommonPrefixes.
   * These groups are counted as one result against the max-keys limitation. These keys are not returned elsewhere
   * in the response.
   */
  delimiter?: string;
  /**
   * Requests Amazon S3 to encode the object keys in the response and specifies the encoding method to use.
   * An object key may contain any Unicode character; however, XML 1.0 parser cannot parse some characters,
   * such as characters with an ASCII value from 0 to 10. For characters that are not supported in XML 1.0,
   * you can add this parameter to request that Amazon S3 encode the keys in the response.
   */
  encodingType?: EncodingType;
  /**
   * Specifies the key to start with when listing objects in a bucket.
   */
  keyMarker?: string;
  /**
   * Sets the maximum number of keys returned in the response. By default the action returns up to 1,000 key names.
   * The response might contain fewer keys but will never contain more. If additional keys satisfy the search criteria,
   * but were not returned because max-keys was exceeded, the response contains <isTruncated>true</isTruncated>.
   * To return the additional keys, see key-marker and version-id-marker.
   */
  maxKeys?: number;
  /**
   * Use this parameter to select only those keys that begin with the specified prefix. You can use prefixes
   * to separate a bucket into different groupings of keys. (You can think of using prefix to make groups in
   * the same way you'd use a folder in a file system.) You can use prefix with delimiter to roll up numerous objects
   * into a single result under CommonPrefixes.
   */
  prefix?: string;
  /**
   * Specifies the object version you want to start listing from.
   */
  versionIdMarker?: string;
}

export interface DeleteMarkerEntry {
  /**
   * Specifies whether the object is (true) or is not (false) the latest version of an object.
   */
  isLatest?: boolean;
  /**
   * The object key.
   */
  key?: string;
  /**
   * Date and time the object was last modified.
   */
  lastModified?: string;
  /**
   * The account that created the delete marker.
   */
  owner?: Owner;
  /**
   * Version ID of an object.
   */
  versionId?: string;
}

export interface ObjectVersion {
  /**
   * The entity tag is an MD5 hash of that version of the object.
   */
  eTag?: string;
  /**
   * Specifies whether the object is (true) or is not (false) the latest version of an object.
   */
  isLatest?: boolean;
  /**
   * The object key.
   */
  key?: string;
  /**
   * Date and time the object was last modified.
   */
  lastModified?: string;
  /**
   * Specifies the owner of the object.
   */
  owner?: Owner;
  /**
   * Size in bytes of the object.
   */
  size?: number;
  /**
   * The class of storage used to store the object.
   */
  storageClass?: StorageClass;
  /**
   * Version ID of an object.
   */
  versionId?: string;
}

export interface ListVersionsResult {
  /**
   * All of the keys rolled up into a common prefix count as a single return when calculating the number of returns.
   */
  commonPrefixes?: CommonPrefix[];
  /**
   * Container for an object that is a delete marker.
   */
  deleteMarker?: DeleteMarkerEntry[];
  /**
   * The delimiter grouping the included keys. A delimiter is a character that you specify to group keys.
   * All keys that contain the same string between the prefix and the first occurrence of the delimiter are grouped
   * under a single result element in CommonPrefixes. These groups are counted as one result against the max-keys
   * limitation. These keys are not returned elsewhere in the response.
   */
  delimiter?: string;
  /**
   * Encoding type used by Amazon S3 to encode object key names in the XML response.
   * If you specify encoding-type request parameter, Amazon S3 includes this element in the response, and returns
   * encoded key name values in the following response elements: KeyMarker, NextKeyMarker, Prefix, Key, and Delimiter.
   */
  encodingType?: EncodingType;
  /**
   * A flag that indicates whether Amazon S3 returned all of the results that satisfied the search criteria.
   * If your results were truncated, you can make a follow-up paginated request using the NextKeyMarker and
   * NextVersionIdMarker response parameters as a starting place in another request to return the rest of the results.
   */
  isTruncated: boolean;
  /**
   * Marks the last key returned in a truncated response.
   */
  keyMarker: string;
  /**
   * Specifies the maximum number of objects to return.
   */
  maxKeys: number;
  /**
   * The bucket name.
   */
  name: string;
  /**
   * When the number of responses exceeds the value of MaxKeys, NextKeyMarker specifies the first key not returned
   * that satisfies the search criteria. Use this value for the key-marker request parameter in a subsequent request.
   */
  nextKeyMarker?: string;
  /**
   * When the number of responses exceeds the value of MaxKeys, NextVersionIdMarker specifies the first object version
   * not returned that satisfies the search criteria. Use this value for the version-id-marker request parameter in
   * a subsequent request.
   */
  nextVersionIdMarker?: string;
  /**
   * Selects objects that start with the value supplied by this parameter.
   */
  prefix: string;
  /**
   * Container for version information.
   */
  versions: ObjectVersion[];
  /**
   * Marks the last version of the key returned in a truncated response.
   */
  versionIdMarker: string;
}

export interface BucketCreateParams extends WithBucketParam {
  /**
   * Access control list.
   */
  acl?: ACL;
}

export type BucketDeleteBucketParams = WithBucketParam;
export type BucketGetMetaParams = WithBucketParam;
export type BucketGetBucketEncryptionParams = WithBucketParam;

export type SSEAlgorithm = 'aws:kms';

export interface ApplyServerSideEncryptionByDefault {
  /**
   * AWS Key Management Service (KMS) customer AWS KMS key ID to use for the default encryption.
   */
  kmsMasterKeyId: string;
  /**
   * Server-side encryption algorithm to use for the default encryption.
   */
  sseAlgorithm: SSEAlgorithm;
}

export interface SSEConfigurationRule {
  /**
   * Sets default encryption for the object, if other encryption parameters are omitted in the request.
   */
  applyServerSideEncryptionByDefault: ApplyServerSideEncryptionByDefault;
}

export interface ServerSideEncryptionConfiguration {
  /**
   * Container for information about a particular server-side encryption configuration rule.
   */
  rules: SSEConfigurationRule[];
}

export type BucketPutBucketEncryptionParams = WithBucketParam &
  ServerSideEncryptionConfiguration;
export type BucketDeleteBucketEncryptionParams = WithBucketParam;
export type BucketGetBucketVersioningParams = WithBucketParam;

export type VersioningStatus = 'Enabled' | 'Suspended';

export interface VersioningConfiguration {
  /**
   * The versioning state of the bucket.
   */
  status: VersioningStatus;
}

export type BucketPutBucketVersioningParams = WithBucketParam &
  VersioningConfiguration;
export type BucketGetBucketLoggingParams = WithBucketParam;

export interface LoggingEnabled {
  /**
   * Specifies the bucket where you want Amazon S3 to store server access logs. You can have your logs delivered to
   * any bucket that you own, including the same bucket that is being logged. You can also configure multiple buckets
   * to deliver their logs to the same target bucket. In this case, you should choose a different TargetPrefix for
   * each source bucket so that the delivered log files can be distinguished by key.
   */
  targetBucket: string;
  /**
   * A prefix for all log object keys. If you store log files from multiple Amazon S3 buckets in a single bucket,
   * you can use a prefix to distinguish which log files came from which bucket.
   */
  targetPrefix: string;
}

export interface BucketLoggingStatus {
  enabled?: false | LoggingEnabled;
}

export type BucketPutBucketLoggingParams = WithBucketParam &
  BucketLoggingStatus;
