import { type StorageClass } from './bucket.js';
import { type WithBucketParam } from './base.js';

export interface AbortIncompleteMultipartUpload {
  /**
   * Specifies the number of days after which Amazon S3 aborts an incomplete multipart upload.
   */
  daysAfterInitiation?: number;
}

export interface LifecycleExpiration {
  /**
   * Indicates at what date the object is to be moved or deleted. Should be in GMT ISO 8601 Format.
   */
  date?: string;
  /**
   * Indicates the lifetime, in days, of the objects that are subject to the rule. The value must be a non-zero
   * positive integer.
   */
  days?: number;
  /**
   * Indicates whether Amazon S3 will remove a delete marker with no noncurrent versions. If set to true, the delete
   * marker will be expired; if set to false the policy takes no action. This cannot be specified with Days or Date
   * in a Lifecycle Expiration Policy.
   */
  expiredObjectDeleteMarker?: boolean;
}

export interface Tag {
  /**
   * Name of the object key.
   */
  key: string;
  /**
   * Value of the tag.
   */
  value: string;
}

export interface LifecycleRuleAndOperator {
  /**
   * Minimum object size to which the rule applies.
   */
  objectSizeGreaterThan?: number;
  /**
   * Maximum object size to which the rule applies.
   */
  objectSizeLessThan?: number;
  /**
   * Prefix identifying one or more objects to which the rule applies.
   */
  prefix?: string;
  /**
   * All of these tags must exist in the object's tag set in order for the rule to apply.
   */
  tags?: Tag[];
}

export interface LifecycleRuleFilter {
  /**
   * This is used in a Lifecycle Rule Filter to apply a logical AND to two or more predicates. The Lifecycle Rule will
   * apply to any object matching all of the predicates configured inside the And operator.
   */
  and?: LifecycleRuleAndOperator;
  /**
   * Minimum object size to which the rule applies.
   */
  objectSizeGreaterThan?: number;
  /**
   * Maximum object size to which the rule applies.
   */
  objectSizeLessThan?: number;
  /**
   * Prefix identifying one or more objects to which the rule applies.
   */
  prefix?: string;
  /**
   * This tag must exist in the object's tag set in order for the rule to apply.
   */
  tag?: Tag;
}

export interface NoncurrentVersionExpiration {
  /**
   * Specifies how many noncurrent versions Amazon S3 will retain. If there are this many more recent noncurrent
   * versions, Amazon S3 will take the associated action.
   */
  newerNoncurrentVersions?: number;
  /**
   * Specifies the number of days an object is noncurrent before Amazon S3 can perform the associated action.
   * The value must be a non-zero positive integer.
   */
  noncurrentDays?: number;
}

export interface NoncurrentVersionTransition {
  /**
   * Specifies how many noncurrent versions Amazon S3 will retain. If there are this many more recent noncurrent
   * versions, Amazon S3 will take the associated action.
   */
  newerNoncurrentVersions?: number;
  /**
   * Specifies the number of days an object is noncurrent before Amazon S3 can perform the associated action.
   */
  noncurrentDays?: number;
  /**
   * The class of storage used to store the object.
   */
  storageClass?: StorageClass;
}

export interface Transition {
  /**
   * Indicates when objects are transitioned to the specified storage class. The date value must be in ISO 8601 format.
   * The time is always midnight UTC.
   */
  date?: string;
  /**
   * Indicates the number of days after creation when objects are transitioned to the specified storage class.
   * The value must be a positive integer.
   */
  days?: number;
  /**
   * The storage class to which you want the object to transition.
   */
  storageClass?: StorageClass;
}

export interface LifecycleRule {
  /**
   * Specifies the days since the initiation of an incomplete multipart upload that Amazon S3 will wait before
   * permanently removing all parts of the upload.
   */
  abortIncompleteMultipartUpload?: AbortIncompleteMultipartUpload;
  /**
   * Specifies the expiration for the lifecycle of the object in the form of date, days and, whether the object has
   * a delete marker.
   */
  expiration?: LifecycleExpiration;
  /**
   * The Filter is used to identify objects that a Lifecycle Rule applies to. A Filter must have exactly one of Prefix,
   * Tag, or And specified. Filter is required if the LifecycleRule does not contain a Prefix element.
   */
  filter?: LifecycleRuleFilter;
  /**
   * Unique identifier for the rule. The value cannot be longer than 255 characters.
   */
  id?: string;
  /**
   * Specifies when noncurrent object versions expire. Upon expiration, Amazon S3 permanently deletes the noncurrent
   * object versions. You set this lifecycle configuration action on a bucket that has versioning enabled (or suspended)
   * to request that Amazon S3 delete noncurrent object versions at a specific period in the object's lifetime.
   */
  noncurrentVersionExpiration?: NoncurrentVersionExpiration;
  /**
   * Specifies the transition rule for the lifecycle rule that describes when noncurrent objects transition to
   * a specific storage class. If your bucket is versioning-enabled (or versioning is suspended), you can set this
   * action to request that Amazon S3 transition noncurrent object versions to a specific storage class at a set period
   * in the object's lifetime.
   */
  noncurrentVersionTransitions?: NoncurrentVersionTransition[];
  /**
   * Prefix identifying one or more objects to which the rule applies. This is no longer used; use Filter instead.
   */
  prefix?: string;
  /**
   * If 'Enabled', the rule is currently being applied. If 'Disabled', the rule is not currently being applied.
   */
  status: 'Enabled' | 'Disabled';
  /**
   * Specifies when an Amazon S3 object transitions to a specified storage class.
   */
  transitions?: Transition[];
}

export interface LifecycleConfiguration {
  /**
   * Container for a lifecycle rule.
   */
  rules: LifecycleRule[];
}

export type LifecyclesGetParams = WithBucketParam;
export type LifecyclesUploadParams = WithBucketParam & LifecycleConfiguration;
export type LifecyclesDeleteParams = WithBucketParam;
