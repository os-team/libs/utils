import { Readable } from 'stream';
import {
  type ApplyServerSideEncryptionByDefault,
  type CommonPrefix,
  type EncodingType,
  type StorageClass,
} from './bucket.js';
import { type ACL } from './acl.js';
import {
  type Owner,
  type WithBucketParam,
  type WithObjectParam,
} from './base.js';

export interface MultipartStartUploadParams extends WithObjectParam {
  /**
   * Data type in a request.
   * See https://en.wikipedia.org/wiki/Media_type
   */
  contentType?: string;
  /**
   * User-defined object metadata.
   */
  meta?: Record<string, string>;
  /**
   * Object storage class.
   */
  storageClass?: StorageClass;
  /**
   * The server-side encryption algorithm used when storing this object.
   */
  serverSideEncryption?: ApplyServerSideEncryptionByDefault;
  /**
   * Access control list.
   */
  acl?: ACL;
}

export interface InitiateMultipartUploadResult {
  /**
   * The name of the bucket to which the multipart upload was initiated.
   */
  bucket: string;
  /**
   * Object key for which the multipart upload was initiated.
   */
  key: string;
  /**
   * ID for the initiated multipart upload.
   */
  uploadId: string;
}

export interface MultipartUploadPartParams extends WithObjectParam {
  /**
   * Part number of part being uploaded. This is a positive integer between 1 and 10,000.
   */
  partNumber: number;
  /**
   * Upload ID identifying the multipart upload whose part is being uploaded.
   */
  uploadId: string;
  /**
   * The content of the part of the file.
   */
  body: Buffer;
  /**
   * The server-side encryption algorithm used when storing this object.
   */
  serverSideEncryption?: ApplyServerSideEncryptionByDefault;
}

export interface MultipartUploadPartResponse {
  eTag: string;
}

export interface MultipartCopyPartParams extends WithObjectParam {
  /**
   * Part number of part being uploaded. This is a positive integer between 1 and 10,000.
   */
  partNumber: number;
  /**
   * Upload ID identifying the multipart upload whose part is being uploaded.
   */
  uploadId: string;
  /**
   * Specifies the source object for the copy operation.
   */
  copySource: string;
  /**
   * The range of bytes to copy from the source object. The range value must use the form bytes=first-last, where
   * the first and last are the zero-based byte offsets to copy. For example, bytes=0-9 indicates that you want
   * to copy the first 10 bytes of the source. You can copy a range only if the source object is greater than 5 MB.
   */
  sourceRange?: string;
  /**
   * Copies the object if its entity tag (ETag) matches the specified tag.
   */
  ifMatch?: string;
  /**
   * Copies the object if it has been modified since the specified time.
   */
  ifModifiedSince?: string;
  /**
   * Copies the object if its entity tag (ETag) is different than the specified ETag.
   */
  ifNoneMatch?: string;
  /**
   * Copies the object if it hasn't been modified since the specified time.
   */
  ifUnmodifiedSince?: string;
  /**
   * The server-side encryption algorithm used when storing this object.
   */
  serverSideEncryption?: ApplyServerSideEncryptionByDefault;
}

export interface MultipartListPartsParams extends WithObjectParam {
  /**
   * Upload ID identifying the multipart upload whose parts are being listed.
   */
  uploadId: string;
  /**
   * Sets the maximum number of parts to return.
   */
  maxParts?: number;
  /**
   * Specifies the part after which listing should begin. Only parts with higher part numbers will be listed.
   */
  partNumberMarker?: string;
}

export interface Initiator {
  /**
   * Name of the Principal.
   */
  displayName?: string;
  /**
   * If the principal is an AWS account, it provides the Canonical User ID. If the principal is an IAM User,
   * it provides a user ARN value.
   */
  id?: string;
}

export interface Part {
  /**
   * Entity tag returned when the part was uploaded.
   */
  eTag?: string;
  /**
   * Date and time at which the part was uploaded.
   */
  lastModified?: string;
  /**
   * Part number identifying the part. This is a positive integer between 1 and 10,000.
   */
  partNumber?: number;
  /**
   * Size in bytes of the uploaded part data.
   */
  size?: number;
}

export interface ListPartsResult {
  /**
   * The name of the bucket to which the multipart upload was initiated. Does not return the access point ARN
   * or access point alias if used.
   */
  bucket: string;
  /**
   * Container element that identifies who initiated the multipart upload. If the initiator is an AWS account, this
   * element provides the same information as the Owner element. If the initiator is an IAM User, this element provides
   * the user ARN and display name.
   */
  initiator: Initiator;
  /**
   * Indicates whether the returned list of parts is truncated. A true value indicates that the list was truncated.
   * A list can be truncated if the number of parts exceeds the limit returned in the MaxParts element.
   */
  isTruncated: boolean;
  /**
   * Object key for which the multipart upload was initiated.
   */
  key: string;
  /**
   * Maximum number of parts that were allowed in the response.
   */
  maxParts: number;
  /**
   * When a list is truncated, this element specifies the last part in the list, as well as the value to use for
   * the part-number-marker request parameter in a subsequent request.
   */
  nextPartNumberMarker?: number;
  /**
   * Container element that identifies the object owner, after the object is created. If multipart upload is initiated
   * by an IAM user, this element provides the parent account ID and display name.
   */
  owner?: Owner;
  /**
   * Container for elements related to a particular part. A response can contain zero or more Part elements.
   */
  parts: Part[];
  /**
   * When a list is truncated, this element specifies the last part in the list, as well as the value to use for
   * the part-number-marker request parameter in a subsequent request.
   */
  partNumberMarker?: number;
  /**
   * Class of storage used to store the uploaded object.
   */
  storageClass: StorageClass;
  /**
   * Upload ID identifying the multipart upload whose parts are being listed.
   */
  uploadId: string;
}

export interface MultipartAbortUploadParams extends WithObjectParam {
  /**
   * Upload ID that identifies the multipart upload.
   */
  uploadId: string;
}

export interface CompletedPart {
  /**
   * Entity tag returned when the part was uploaded.
   */
  eTag?: string;
  /**
   * Part number that identifies the part. This is a positive integer between 1 and 10,000.
   */
  partNumber?: number;
}

export interface CompleteMultipartUpload {
  /**
   * Array of CompletedPart data types.
   * If you do not supply a valid Part with your request, the service sends back an HTTP 400 response.
   */
  parts: CompletedPart[];
}

export interface MultipartCompleteUploadParams
  extends WithObjectParam,
    CompleteMultipartUpload {
  /**
   * ID for the initiated multipart upload.
   */
  uploadId: string;
}

export interface CompleteMultipartUploadResult {
  /**
   * The name of the bucket that contains the newly created object.
   */
  bucket: string;
  /**
   * Entity tag that identifies the newly created object's data. Objects with different object data will have different
   * entity tags. The entity tag is an opaque string. The entity tag may or may not be an MD5 digest of the object data.
   * If the entity tag is not an MD5 digest of the object data, it will contain one or more nonhexadecimal characters
   * and/or will consist of less than 32 or more than 32 hexadecimal digits.
   */
  eTag: string;
  /**
   * The object key of the newly created object.
   */
  key: string;
  /**
   * The URI that identifies the newly created object.
   */
  location: string;
}

export interface MultipartListUploadsParams extends WithBucketParam {
  /**
   * Character you use to group keys.
   * All keys that contain the same string between the prefix, if specified, and the first occurrence of the delimiter
   * after the prefix are grouped under a single result element, CommonPrefixes. If you don't specify the prefix
   * parameter, then the substring starts at the beginning of the key. The keys that are grouped under CommonPrefixes
   * result element are not returned elsewhere in the response.
   */
  delimiter?: string;
  /**
   * Requests Amazon S3 to encode the object keys in the response and specifies the encoding method to use. An object
   * key may contain any Unicode character; however, XML 1.0 parser cannot parse some characters, such as characters
   * with an ASCII value from 0 to 10. For characters that are not supported in XML 1.0, you can add this parameter
   * to request that Amazon S3 encode the keys in the response.
   */
  encodingType?: EncodingType;
  /**
   * Together with upload-id-marker, this parameter specifies the multipart upload after which listing should begin.
   * If upload-id-marker is not specified, only the keys lexicographically greater than the specified key-marker will
   * be included in the list.
   * If upload-id-marker is specified, any multipart uploads for a key equal to the key-marker might also be included,
   * provided those multipart uploads have upload IDs lexicographically greater than the specified upload-id-marker.
   */
  keyMarker?: string;
  /**
   * Sets the maximum number of multipart uploads, from 1 to 1,000, to return in the response body. 1,000 is the maximum number of uploads that can be returned in a response.
   */
  maxUploads?: number;
  /**
   * Lists in-progress uploads only for those keys that begin with the specified prefix. You can use prefixes to
   * separate a bucket into different grouping of keys. (You can think of using prefix to make groups in the same way
   * you'd use a folder in a file system.)
   */
  prefix?: string;
  /**
   * Together with key-marker, specifies the multipart upload after which listing should begin. If key-marker is not
   * specified, the upload-id-marker parameter is ignored. Otherwise, any multipart uploads for a key equal to
   * the key-marker might be included in the list only if they have an upload ID lexicographically greater than
   * the specified upload-id-marker.
   */
  uploadIdMarker?: string;
}

export interface MultipartUpload {
  /**
   * Date and time at which the multipart upload was initiated.
   */
  initiated?: string;
  /**
   * Identifies who initiated the multipart upload.
   */
  initiator?: Initiator;
  /**
   * Key of the object for which the multipart upload was initiated.
   */
  key?: string;
  /**
   * Specifies the owner of the object that is part of the multipart upload.
   */
  owner?: Owner;
  /**
   * The class of storage used to store the object.
   */
  storageClass?: StorageClass;
  /**
   * Upload ID that identifies the multipart upload.
   */
  uploadId?: string;
}

export interface ListMultipartUploadsResult {
  /**
   * The name of the bucket to which the multipart upload was initiated.
   * Does not return the access point ARN or access point alias if used.
   */
  bucket: string;
  /**
   * If you specify a delimiter in the request, then the result returns each distinct key prefix containing
   * the delimiter in a CommonPrefixes element. The distinct key prefixes are returned in the Prefix child element.
   */
  commonPrefixes?: CommonPrefix[];
  /**
   * Contains the delimiter you specified in the request.
   * If you don't specify a delimiter in your request, this element is absent from the response.
   */
  delimiter?: string;
  /**
   * Encoding type used by Amazon S3 to encode object keys in the response.
   * If you specify encoding-type request parameter, Amazon S3 includes this element in the response, and returns
   * encoded key name values in the following response elements: Delimiter, KeyMarker, Prefix, NextKeyMarker, Key.
   */
  encodingType?: EncodingType;
  /**
   * Indicates whether the returned list of multipart uploads is truncated. A value of true indicates that the list
   * was truncated. The list can be truncated if the number of multipart uploads exceeds the limit allowed or specified
   * by max uploads.
   */
  isTruncated: boolean;
  /**
   * The key at or after which the listing began.
   */
  keyMarker: string;
  /**
   * Maximum number of multipart uploads that could have been included in the response.
   */
  maxUploads: number;
  /**
   * When a list is truncated, this element specifies the value that should be used for the key-marker request
   * parameter in a subsequent request.
   */
  nextKeyMarker?: string;
  /**
   * When a list is truncated, this element specifies the value that should be used for the upload-id-marker request
   * parameter in a subsequent request.
   */
  nextUploadIdMarker?: string;
  /**
   * When a prefix is provided in the request, this field contains the specified prefix. The result contains only keys
   * starting with the specified prefix.
   */
  prefix?: string;
  /**
   * Container for elements related to a particular multipart upload. A response can contain zero or more Upload
   * elements.
   */
  uploads: MultipartUpload[];
  /**
   * Upload ID after which listing began.
   */
  uploadIdMarker: string;
}

export interface MultipartUploadParams extends WithObjectParam {
  /**
   * The readable stream
   */
  body: Readable;
  /**
   * Data type in a request.
   * See https://en.wikipedia.org/wiki/Media_type
   */
  contentType?: string;
  /**
   * User-defined object metadata.
   */
  meta?: Record<string, string>;
  /**
   * Object storage class.
   */
  storageClass?: StorageClass;
  /**
   * The server-side encryption algorithm used when storing this object.
   */
  serverSideEncryption?: ApplyServerSideEncryptionByDefault;
  /**
   * Access control list.
   */
  acl?: ACL;
  /**
   * The number of file parts uploaded to a bucket in parallel.
   * The more concurrent parts, the more memory is used (concurrentParts * partSize + bufferSize), but the files
   * upload faster. Set 1 for synchronous uploading of file parts.
   * @default 4
   */
  concurrentParts?: number;
  /**
   * The size of the file parts in bytes.
   * Increase the part size if you want to upload a large file (more than 10GB).
   * Adjust the part size to ensure that the max number of parts must be less than or equal to 10000.
   * The min part size must be greater than or equal to 5MB (5242880 bytes).
   * @default 5242880
   */
  partSize?: number;
  /**
   * The size of the buffer in bytes in which parts of the file is stored, if the file is read faster than it is
   * uploaded to a bucket. The buffer is stored in memory, so the smaller buffer size, the less memory is used, but
   * the file uploading time may be slower. The max number of memory usage is (concurrentParts * partSize + bufferSize).
   * @default 20971520
   */
  bufferSize?: number;
}
