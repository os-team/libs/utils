import { createHash, createHmac, type BinaryLike } from 'crypto';

const AWS_SERVICE = 's3';
const AWS_SIGNING_ALGORITHM = 'AWS4-HMAC-SHA256';

type Region = 'ru-central1-a' | 'ru-central1-b' | 'ru-central1-c';
type Method = 'GET' | 'POST' | 'PUT' | 'HEAD' | 'OPTIONS' | 'DELETE';

export interface SigningManagerOptions {
  accessKeyId: string;
  secretAccessKey: string;
  region: Region;
}

export interface CanonicalRequest {
  method: Method;
  uri: string;
  query?: Record<string, string>;
  headers?: Record<string, string>;
  payload?: BinaryLike;
}

class SigningManager {
  private accessKeyId: string;

  private secretAccessKey: string;

  private region: string;

  public constructor(options: SigningManagerOptions) {
    if (!options.accessKeyId) {
      throw new Error('The access key ID is not specified');
    }
    if (!options.secretAccessKey) {
      throw new Error('The secret access key is not specified');
    }
    this.accessKeyId = options.accessKeyId;
    this.secretAccessKey = options.secretAccessKey;
    this.region = options.region;
  }

  /**
   * Computes a hash by using the SHA256 algorithm.
   */
  public static hash(data: BinaryLike) {
    return createHash('sha256').update(data).digest('hex');
  }

  /**
   * Computes HMAC by using the SHA256 algorithm.
   */
  public static sign(key: BinaryLike, data: BinaryLike) {
    return createHmac('sha256', key).update(data).digest();
  }

  /**
   * Converts the specified date to the string in ISO 8601 format.
   * E.g. 20190801T000000Z.
   */
  public static dateToISO8601(date: Date) {
    return date.toISOString().replace(/[-:]/g, '').replace(/\.\d+/, '');
  }

  /**
   * Converts the specified date to the string in YYYYMMDD format.
   * E.g. 20130524.
   */
  public static dateToString(date: Date) {
    return SigningManager.dateToISO8601(date).slice(0, 8);
  }

  /**
   * Creates the query string.
   */
  public static getQueryString(query: Record<string, string>) {
    return Object.entries(query)
      .map(([key, value]) => [
        encodeURIComponent(key),
        encodeURIComponent(value),
      ])
      .sort(([key1], [key2]) => {
        if (key1 > key2) return 1;
        if (key1 < key2) return -1;
        return 0;
      })
      .map(([key, value]) => `${key}=${value}`)
      .join('&');
  }

  /**
   * Returns the keys of headers sorted alphabetically.
   * Used to compose signed headers.
   */
  private static getHeaderKeys(headers: Record<string, string>) {
    return Object.keys(headers)
      .map((key) => key.toLowerCase())
      .sort()
      .join(';');
  }

  /**
   * Creates a canonical request used to calculate the request signature.
   */
  private static createCanonicalRequest(req: CanonicalRequest) {
    const { method, uri, query = {}, headers = {}, payload = '' } = req;

    const canonicalQueryString = SigningManager.getQueryString(query);
    const canonicalHeaders = Object.entries(headers)
      .map(([key, value]) => [key.toLowerCase(), value])
      .sort(([key1], [key2]) => {
        if (key1 > key2) return 1;
        if (key1 < key2) return -1;
        return 0;
      })
      .map(([key, value]) => `${key}:${value.trim()}\n`)
      .join('');
    const signedHeaders = SigningManager.getHeaderKeys(headers);
    const hashedPayload = SigningManager.hash(payload);

    return [
      method,
      uri,
      canonicalQueryString,
      canonicalHeaders,
      signedHeaders,
      hashedPayload,
    ].join('\n');
  }

  /**
   * Returns the scope that binds the resulting signature to a specific date, a region, and a service.
   */
  private getScope(date: Date) {
    const strDate = SigningManager.dateToString(date);
    return `${strDate}/${this.region}/${AWS_SERVICE}/aws4_request`;
  }

  /**
   * Creates a string used to compute the request signature.
   */
  private createStringToSign(canonicalRequest: string, date: Date) {
    const formattedDate = SigningManager.dateToISO8601(date);
    const scope = this.getScope(date);
    const signedCanonicalRequest = SigningManager.hash(canonicalRequest);

    return [
      AWS_SIGNING_ALGORITHM,
      formattedDate,
      scope,
      signedCanonicalRequest,
    ].join('\n');
  }

  /**
   * Creates a key used to compute the request signature.
   */
  private createSigningKey(date: Date) {
    const strDate = SigningManager.dateToString(date);
    const dateKey = SigningManager.sign(`AWS4${this.secretAccessKey}`, strDate);
    const regionKey = SigningManager.sign(dateKey, this.region);
    const serviceKey = SigningManager.sign(regionKey, 's3');
    return SigningManager.sign(serviceKey, 'aws4_request');
  }

  /**
   * Creates a signature of the request.
   */
  public createSignature(req: CanonicalRequest, date: Date) {
    const signingKey = this.createSigningKey(date);
    const canonicalRequest = SigningManager.createCanonicalRequest(req);
    const stringToSign = this.createStringToSign(canonicalRequest, date);
    return SigningManager.sign(signingKey, stringToSign).toString('hex');
  }

  /**
   * Creates an authorization header used to sign the request.
   */
  public createAuthString(req: CanonicalRequest, date: Date) {
    const scope = this.getScope(date);
    const signedHeaders = SigningManager.getHeaderKeys(req.headers || {});
    const signature = this.createSignature(req, date);

    const authComponents = {
      Credential: `${this.accessKeyId}/${scope}`,
      SignedHeaders: signedHeaders,
      Signature: signature,
    };

    const authToken = Object.entries(authComponents)
      .map(([key, value]) => `${key}=${value}`)
      .join(',');

    return `${AWS_SIGNING_ALGORITHM} ${authToken}`;
  }
}

export default SigningManager;
