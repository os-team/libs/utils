import {
  type CompletedPart,
  type CompleteMultipartUploadResult,
  type MultipartUploadPartResponse,
} from './types/multipart.js';

export type UploadPart = (
  part: Buffer,
  partNumber: number
) => Promise<MultipartUploadPartResponse>;

export type CompleteUpload = (
  parts: CompletedPart[]
) => Promise<CompleteMultipartUploadResult>;

export interface MultipartUploadManagerOptions {
  uploadPart: UploadPart;
  completeUpload: CompleteUpload;
  maxThreads: number;
}

export type Callback = (error?: Error) => void;

class MultipartUploadManager {
  private uploadPart: UploadPart;

  private completeUpload: CompleteUpload;

  private maxThreads: number;

  private busyThreads: number;

  private partNumber: number;

  private next: (() => void) | null;

  private error: Error | null;

  private parts: CompletedPart[];

  public constructor(options: MultipartUploadManagerOptions) {
    this.uploadPart = options.uploadPart;
    this.completeUpload = options.completeUpload;
    this.maxThreads = options.maxThreads;
    this.busyThreads = 0;
    this.partNumber = 0;
    this.next = null;
    this.error = null;
    this.parts = [];
  }

  private getNextPartNumber() {
    this.partNumber += 1;
    return this.partNumber;
  }

  private runNextIfExists() {
    if (!this.next) return;
    const fn = this.next;
    this.next = null;
    fn();
  }

  public pushPart(buffer: Buffer, callback: Callback) {
    // Check if there was an error when uploading the previous parts of a file
    if (this.error) {
      callback(this.error);
      return;
    }

    // If there are no free threads, wait until the upload of one of the file parts is completed
    if (this.busyThreads >= this.maxThreads) {
      this.next = () => this.pushPart(buffer, callback);
      return;
    }

    // Increase the number of busy threads and get the number of the next file part
    this.busyThreads += 1;
    const partNumber = this.getNextPartNumber();

    // Upload the part of the file and save the response
    this.uploadPart(buffer, partNumber)
      .then((res) => {
        this.busyThreads -= 1;
        this.parts[partNumber - 1] = { eTag: res.eTag, partNumber };
        this.runNextIfExists(); // Run the next request if it was called when all threads were busy
        return undefined;
      })
      .catch((error) => {
        this.busyThreads -= 1;
        this.error = error;
      });

    callback();
  }

  public complete(callback: Callback) {
    // Check if there was an error when uploading one of the parts of a file
    if (this.error) {
      callback(this.error);
      return;
    }

    // Wait until all parts of the file are uploaded
    if (this.busyThreads > 0) {
      this.next = () => this.complete(callback);
      return;
    }

    // Complete the file upload
    this.completeUpload(this.parts)
      .then(() => callback())
      .catch((error) => callback(error));
  }
}

export default MultipartUploadManager;
