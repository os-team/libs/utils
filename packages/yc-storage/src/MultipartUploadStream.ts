import { Writable } from 'stream';
import MultipartUploadManager, {
  type Callback,
} from './MultipartUploadManager.js';

interface MultipartUploadStreamOptions {
  uploadManager: MultipartUploadManager;
  partSize: number;
  bufferSize: number;
}

class MultipartUploadStream extends Writable {
  private uploadManager: MultipartUploadManager;

  private partSize: number;

  private partChunks: Buffer[];

  private partLength: number;

  constructor(options: MultipartUploadStreamOptions) {
    super({ highWaterMark: options.bufferSize });
    this.uploadManager = options.uploadManager;
    this.partSize = options.partSize;
    this.partChunks = [];
    this.partLength = 0;
  }

  /**
   * Collects the chunks of a file into a single part and uploads it.
   */
  public _write(chunk, _, callback) {
    this.partChunks.push(chunk);
    this.partLength += chunk.length;

    if (this.partLength >= this.partSize) this.uploadPart(callback);
    else callback();
  }

  /**
   * Upload the last part of a file, if it exists, and complete the multipart upload.
   */
  public _final(callback) {
    const complete: Callback = (error) => {
      if (error) callback(error);
      else this.uploadManager.complete(callback);
    };
    if (this.partLength > 0) this.uploadPart(complete);
    else complete();
  }

  /**
   * Merge all chunks into a file part and upload it to a bucket.
   */
  private uploadPart(callback: Callback) {
    const part = Buffer.concat(this.partChunks, this.partLength);
    this.partChunks = [];
    this.partLength = 0;
    this.uploadManager.pushPart(part, callback);
  }
}

export default MultipartUploadStream;
