interface YCStorageErrorProps {
  code: string;
  message: string;
  resource: string;
  requestId: string;
}

class YCStorageError extends Error {
  public readonly resource: string;

  public readonly requestId: string;

  public constructor(props: YCStorageErrorProps) {
    super(props.message);
    this.name = props.code;
    this.resource = props.resource;
    this.requestId = props.requestId;
  }
}

export default YCStorageError;
