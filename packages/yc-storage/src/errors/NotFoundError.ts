class NotFoundError extends Error {
  public constructor() {
    super('Not Found');
    this.name = 'NotFound';
  }
}

export default NotFoundError;
