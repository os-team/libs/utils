import { test } from 'node:test';
import assert from 'node:assert';
import {
  bytea,
  timestamp,
  timestamptz,
  date,
  time,
  timetz,
  interval,
  point,
  path,
  line,
  lseg,
  box,
  polygon,
  circle,
  cidr,
  inet,
  macaddr,
  macaddr8,
  varbit,
  tsvector,
  tsquery,
  uuid,
  xml,
  json,
  jsonb,
  intRange,
  bigintRange,
  numRange,
  tsRange,
  tstzRange,
  dateRange,
} from './types.js';

test('bytea', () => {
  assert.deepStrictEqual(bytea('\xDEADBEEF'), ['\xDEADBEEF', 'bytea']);
});

test('timestamp', () => {
  assert.deepStrictEqual(timestamp('2023-03-20T10:00:00.123456Z'), [
    '2023-03-20T10:00:00.123456Z',
    'timestamp',
  ]);

  assert.deepStrictEqual(timestamp(1679306400123456), [
    '2023-03-20T10:00:00.123456Z',
    'timestamp',
  ]);

  assert.deepStrictEqual(timestamp(new Date('2023-03-20T10:00:00.123456Z')), [
    '2023-03-20T10:00:00.123Z',
    'timestamp',
  ]);
});

test('timestamptz', () => {
  assert.deepStrictEqual(timestamptz('2023-03-20T10:00:00.123456Z'), [
    '2023-03-20T10:00:00.123456Z',
    'timestamptz',
  ]);

  assert.deepStrictEqual(timestamptz(1679306400123456), [
    '2023-03-20T10:00:00.123456Z',
    'timestamptz',
  ]);

  assert.deepStrictEqual(timestamptz(new Date('2023-03-20T10:00:00.123456Z')), [
    '2023-03-20T10:00:00.123Z',
    'timestamptz',
  ]);
});

test('date', () => {
  assert.deepStrictEqual(date('2023-03-20'), ['2023-03-20', 'date']);

  assert.deepStrictEqual(date(1679306400123456), ['2023-03-20', 'date']);

  assert.deepStrictEqual(date(new Date('2023-03-20T10:00:00.123456Z')), [
    '2023-03-20',
    'date',
  ]);
});

test('time', () => {
  assert.deepStrictEqual(time('10:00:00.123456'), ['10:00:00.123456', 'time']);

  assert.deepStrictEqual(time(1679306400123456), ['10:00:00.123456', 'time']);

  assert.deepStrictEqual(time(new Date('2023-03-20T10:00:00.123456Z')), [
    '10:00:00.123000',
    'time',
  ]);
});

test('timetz', () => {
  assert.deepStrictEqual(timetz('10:00:00.123456'), [
    '10:00:00.123456',
    'timetz',
  ]);

  assert.deepStrictEqual(timetz(1679306400123456), [
    '10:00:00.123456',
    'timetz',
  ]);

  assert.deepStrictEqual(timetz(new Date('2023-03-20T10:00:00.123456Z')), [
    '10:00:00.123000',
    'timetz',
  ]);
});

test('interval', () => {
  assert.deepStrictEqual(interval(2, 'days'), ['2 days', 'interval']);
});

test('point', () => {
  assert.deepStrictEqual(point(1, 2), ['(1,2)', 'point']);
});

test('path', () => {
  assert.deepStrictEqual(
    path([
      [1, 2],
      [3, 4],
      [5, 6],
    ]),
    ['((1,2),(3,4),(5,6))', 'path']
  );
});

test('line', () => {
  assert.deepStrictEqual(line([1, 2, 3]), ['{1,2,3}', 'line']);

  assert.deepStrictEqual(line([1, 2], [3, 4]), ['((1,2),(3,4))', 'line']);
});

test('lseg', () => {
  assert.deepStrictEqual(lseg([1, 2], [3, 4]), ['((1,2),(3,4))', 'lseg']);
});

test('box', () => {
  assert.deepStrictEqual(box([1, 2], [3, 4]), ['((1,2),(3,4))', 'box']);
});

test('polygon', () => {
  assert.deepStrictEqual(
    polygon([
      [1, 2],
      [3, 4],
      [5, 6],
    ]),
    ['((1,2),(3,4),(5,6))', 'polygon']
  );
});

test('circle', () => {
  assert.deepStrictEqual(circle([1, 2], 3), ['<(1,2),3>', 'circle']);
});

test('cidr', () => {
  assert.deepStrictEqual(cidr('192.168.100.128/25'), [
    '192.168.100.128/25',
    'cidr',
  ]);
});

test('inet', () => {
  assert.deepStrictEqual(inet('192.168.0.1/24'), ['192.168.0.1/24', 'inet']);
});

test('macaddr', () => {
  assert.deepStrictEqual(macaddr('08:00:2b:01:02:03'), [
    '08:00:2b:01:02:03',
    'macaddr',
  ]);
});

test('macaddr8', () => {
  assert.deepStrictEqual(macaddr8('08:00:2b:01:02:03:04:05'), [
    '08:00:2b:01:02:03:04:05',
    'macaddr8',
  ]);
});

test('varbit', () => {
  assert.deepStrictEqual(varbit('101'), ['101', 'varbit']);
});

test('tsvector', () => {
  assert.deepStrictEqual(tsvector('The Fat Rats'), [
    'The Fat Rats',
    'tsvector',
  ]);
});

test('tsquery', () => {
  assert.deepStrictEqual(tsquery('fat & rat'), ['fat & rat', 'tsquery']);
});

test('uuid', () => {
  assert.deepStrictEqual(uuid('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11'), [
    'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11',
    'uuid',
  ]);
});

test('xml', () => {
  assert.deepStrictEqual(xml('<foo>bar</foo>'), ['<foo>bar</foo>', 'xml']);
});

test('json', () => {
  assert.deepStrictEqual(json({ foo: 'bar' }), ['{"foo":"bar"}', 'json']);
});

test('jsonb', () => {
  assert.deepStrictEqual(jsonb({ foo: 'bar' }), ['{"foo":"bar"}', 'jsonb']);
});

test('intRange', () => {
  assert.deepStrictEqual(intRange([]), ['(,)', 'int4range']);

  assert.deepStrictEqual(intRange([1]), ['[1,)', 'int4range']);

  // eslint-disable-next-line no-sparse-arrays
  assert.deepStrictEqual(intRange([, 1]), ['(,1]', 'int4range']);

  assert.deepStrictEqual(intRange([1, 2]), ['[1,2]', 'int4range']);

  assert.deepStrictEqual(intRange([1, 2], '()'), ['(1,2)', 'int4range']);

  assert.deepStrictEqual(intRange([1, 2], '(]'), ['(1,2]', 'int4range']);

  assert.deepStrictEqual(intRange([1, 2], '[)'), ['[1,2)', 'int4range']);

  assert.deepStrictEqual(intRange([1, 2], '[]'), ['[1,2]', 'int4range']);
});

test('bigintRange', () => {
  assert.deepStrictEqual(bigintRange([9007199254740990n, 9007199254740991n]), [
    '[9007199254740990,9007199254740991]',
    'int8range',
  ]);
});

test('numRange', () => {
  assert.deepStrictEqual(numRange([1, 1.5]), ['[1,1.5]', 'numrange']);
});

test('tsRange', () => {
  assert.deepStrictEqual(
    tsRange(['2023-03-20T10:00:00.123456Z', '2023-03-21T10:00:00.123456Z']),
    ['[2023-03-20T10:00:00.123456Z,2023-03-21T10:00:00.123456Z]', 'tsrange']
  );
});

test('tstzRange', () => {
  assert.deepStrictEqual(
    tstzRange(['2023-03-20T10:00:00.123456Z', '2023-03-21T10:00:00.123456Z']),
    ['[2023-03-20T10:00:00.123456Z,2023-03-21T10:00:00.123456Z]', 'tstzrange']
  );
});

test('dateRange', () => {
  assert.deepStrictEqual(dateRange(['2023-03-20', '2023-03-21']), [
    '[2023-03-20,2023-03-21]',
    'daterange',
  ]);
});
