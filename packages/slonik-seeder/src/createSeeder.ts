import { type DatabasePool, sql } from 'slonik';

/* eslint-disable @typescript-eslint/no-explicit-any */

export type CustomValue = [string, string]; // [value, type]
export type PrimitiveValue = string | number | bigint | boolean | null;
export type Data = Record<string, PrimitiveValue | CustomValue>;

const isCustomValue = (value: unknown): value is CustomValue =>
  Array.isArray(value) && value.length === 2 && typeof value[1] === 'string';

export type Seeder = <T extends Data | Data[]>(
  table: string,
  data: T
) => Promise<T extends Data ? any : any[]>;

const pgTypeOf = (value: unknown): string => {
  if (isCustomValue(value)) return value[1];
  switch (typeof value) {
    case 'string':
      return 'text';
    case 'number':
      return Number.isInteger(value) ? 'int4' : 'numeric';
    case 'bigint':
      return 'int8';
    case 'boolean':
      return 'bool';
    default:
      throw new Error(`Unable to detect the type of "${value}".`);
  }
};

interface SeederOptions {
  columnTransformer: (column: string) => string;
}

const createSeeder =
  (pool: DatabasePool, options: SeederOptions): Seeder =>
  async (table, data) => {
    const { columnTransformer = (column) => column } = options;

    const rows = Array.isArray(data) ? data : [data];
    if (rows.length === 0) return [];

    const columns: string[] = [];
    const types: string[] = [];
    rows.forEach((row) => {
      Object.entries(row).forEach(([column, value]) => {
        if (value != null && !columns.includes(column)) {
          columns.push(column);
          types.push(pgTypeOf(value));
        }
      });
    });

    const columnsFragment = sql.join(
      columns.map((key) => sql.identifier([columnTransformer(key)])),
      sql.fragment`, `
    );

    const tuples = rows.map((row) =>
      columns.map((column) => {
        const value = row[column];
        if (value === undefined) return null;
        if (typeof value === 'bigint') return Number(value); // Because sql.unnest does not support BigInt
        return isCustomValue(value) ? value[0] : value;
      })
    );

    const query = sql.unsafe`
      INSERT INTO ${sql.identifier(table.split('.'))} (${columnsFragment})
      SELECT *
      FROM ${sql.unnest(tuples, types)}
      RETURNING *
    `;

    const res = await pool.any(query);

    return Array.isArray(data) ? res : res[0];
  };

export default createSeeder;
