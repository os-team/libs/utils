import { type CustomValue } from './createSeeder.js';

/* eslint-disable @typescript-eslint/no-explicit-any */

export type DateValue = string | number | Date;
export type Point = [number, number];
export type ManyPoints = [Point, ...Point[]];
export type Object = Record<string, any>;
export type IntervalUnit =
  | 'years'
  | 'months'
  | 'weeks'
  | 'days'
  | 'hours'
  | 'minutes'
  | 'seconds';

// -----------------------------------------------------------------
// Binary Data Types
// https://www.postgresql.org/docs/current/datatype-binary.html
// -----------------------------------------------------------------

export function bytea(value: string): CustomValue {
  return [value, 'bytea'];
}

// -----------------------------------------------------------------
// Date/Time Types
// https://www.postgresql.org/docs/current/datatype-datetime.html
// -----------------------------------------------------------------

function toTimestamp(value: DateValue): string {
  if (typeof value === 'string') return value;
  if (typeof value === 'number') {
    const microseconds = (value % 1000).toString().padStart(3, '0');
    return new Date(value / 1000)
      .toISOString()
      .replace('Z', `${microseconds}Z`);
  }
  return value.toISOString();
}

function toDate(value: DateValue): string {
  if (typeof value === 'string') return value;
  const d = typeof value === 'number' ? new Date(value / 1000) : value;
  const year = d.getUTCFullYear();
  const month = (d.getUTCMonth() + 1).toString().padStart(2, '0');
  const day = d.getUTCDate().toString().padStart(2, '0');
  return `${year}-${month}-${day}`;
}

function toTime(value: DateValue): string {
  if (typeof value === 'string') return value;
  const d = typeof value === 'number' ? new Date(value / 1000) : value;
  const hour = d.getUTCHours().toString().padStart(2, '0');
  const minute = d.getUTCMinutes().toString().padStart(2, '0');
  const second = d.getUTCSeconds().toString().padStart(2, '0');
  const milliseconds = d.getMilliseconds().toString().padStart(3, '0');
  const microseconds = (typeof value === 'number' ? value % 1000 : 0)
    .toString()
    .padStart(3, '0');
  return `${hour}:${minute}:${second}.${milliseconds}${microseconds}`;
}

export function timestamp(value: DateValue): CustomValue {
  return [toTimestamp(value), 'timestamp'];
}

export function timestamptz(value: DateValue): CustomValue {
  return [toTimestamp(value), 'timestamptz'];
}

export function date(value: DateValue): CustomValue {
  return [toDate(value), 'date'];
}

export function time(value: DateValue): CustomValue {
  return [toTime(value), 'time'];
}

export function timetz(value: DateValue): CustomValue {
  return [toTime(value), 'timetz'];
}

export function interval(n: number, unit: IntervalUnit): CustomValue {
  return [`${n} ${unit}`, 'interval'];
}

// -----------------------------------------------------------------
// Geometric Types
// https://www.postgresql.org/docs/current/datatype-geometric.html
// -----------------------------------------------------------------

export function point(x: number, y: number): CustomValue {
  return [`(${x},${y})`, 'point'];
}

export function path(points: ManyPoints): CustomValue {
  return [`(${points.map((p) => point(...p)[0]).join(',')})`, 'path'];
}

export function line(coefs: [number, number, number]): CustomValue;
export function line(p1: Point, p2: Point): CustomValue;
export function line(...args: any[]): CustomValue {
  return [
    args.length === 1
      ? `{${args[0].join(',')}}`
      : path(args as [Point, Point])[0],
    'line',
  ];
}

export function lseg(p1: Point, p2: Point): CustomValue {
  return [line(p1, p2)[0], 'lseg'];
}

export function box(p1: Point, p2: Point): CustomValue {
  return [line(p1, p2)[0], 'box'];
}

export function polygon(points: ManyPoints): CustomValue {
  return [path(points)[0], 'polygon'];
}

export function circle(p: Point, r: number): CustomValue {
  return [`<${point(...p)[0]},${r}>`, 'circle'];
}

// -----------------------------------------------------------------
// Network Address Types
// https://www.postgresql.org/docs/current/datatype-net-types.html
// -----------------------------------------------------------------

export function cidr(value: string): CustomValue {
  return [value, 'cidr'];
}

export function inet(value: string): CustomValue {
  return [value, 'inet'];
}

export function macaddr(value: string): CustomValue {
  return [value, 'macaddr'];
}

export function macaddr8(value: string): CustomValue {
  return [value, 'macaddr8'];
}

// -----------------------------------------------------------------
// Bit String Types
// https://www.postgresql.org/docs/current/datatype-bit.html
// -----------------------------------------------------------------

export function varbit(value: string): CustomValue {
  return [value, 'varbit'];
}

// -----------------------------------------------------------------
// Text Search Types
// https://www.postgresql.org/docs/current/datatype-textsearch.html
// -----------------------------------------------------------------

export function tsvector(value: string): CustomValue {
  return [value, 'tsvector'];
}

export function tsquery(value: string): CustomValue {
  return [value, 'tsquery'];
}

// -----------------------------------------------------------------
// UUID Type
// https://www.postgresql.org/docs/current/datatype-uuid.html
// -----------------------------------------------------------------

export function uuid(value: string): CustomValue {
  return [value, 'uuid'];
}

// --------
// XML Type
// https://www.postgresql.org/docs/current/datatype-xml.html
// --------

export function xml(value: string): CustomValue {
  return [value, 'xml'];
}

// -----------------------------------------------------------------
// JSON Types
// https://www.postgresql.org/docs/current/datatype-json.html
// -----------------------------------------------------------------

export function json(value: any): CustomValue {
  return [JSON.stringify(value), 'json'];
}

export function jsonb(value: any): CustomValue {
  return [JSON.stringify(value), 'jsonb'];
}

// -----------------------------------------------------------------
// Range Types
// https://www.postgresql.org/docs/current/rangetypes.html
// -----------------------------------------------------------------

export type RangeType = '()' | '[)' | '(]' | '[]';
type Range<T> = [] | [T] | [undefined, T] | [T, T];

const DEFAULT_RANGE_TYPE: RangeType = '[]';

function toRange(range: Range<string>, type: RangeType): string {
  const left = range[0] ? `${type[0]}${range[0]}` : `(`;
  const right = range[1] ? `${range[1]}${type[1]}` : `)`;
  return `${left},${right}`;
}

export function intRange(
  range: Range<number>,
  type: RangeType = DEFAULT_RANGE_TYPE
): CustomValue {
  const strRange = range.map((item) =>
    item !== undefined ? item.toString() : item
  ) as Range<string>;
  return [toRange(strRange, type), 'int4range'];
}

export function bigintRange(
  range: Range<bigint>,
  type: RangeType = DEFAULT_RANGE_TYPE
): CustomValue {
  const strRange = range.map((item) =>
    item !== undefined ? item.toString() : item
  ) as Range<string>;
  return [toRange(strRange, type), 'int8range'];
}

export function numRange(
  range: Range<number>,
  type: RangeType = DEFAULT_RANGE_TYPE
): CustomValue {
  const strRange = range.map((item) =>
    item !== undefined ? item.toString() : item
  ) as Range<string>;
  return [toRange(strRange, type), 'numrange'];
}

export function tsRange(
  range: Range<DateValue>,
  type: RangeType = DEFAULT_RANGE_TYPE
): CustomValue {
  const strRange = range.map((item) =>
    item !== undefined ? toTimestamp(item) : item
  ) as Range<string>;
  return [toRange(strRange, type), 'tsrange'];
}

export function tstzRange(
  range: Range<DateValue>,
  type: RangeType = DEFAULT_RANGE_TYPE
): CustomValue {
  const strRange = range.map((item) =>
    item !== undefined ? toTimestamp(item) : item
  ) as Range<string>;
  return [toRange(strRange, type), 'tstzrange'];
}

export function dateRange(
  range: Range<DateValue>,
  type: RangeType = DEFAULT_RANGE_TYPE
): CustomValue {
  const strRange = range.map((item) =>
    item !== undefined ? toDate(item) : item
  ) as Range<string>;
  return [toRange(strRange, type), 'daterange'];
}
