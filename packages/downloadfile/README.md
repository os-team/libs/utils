# @os-team/downloadfile [![NPM version](https://img.shields.io/npm/v/@os-team/downloadfile)](https://yarnpkg.com/package/@os-team/downloadfile) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/downloadfile)](https://bundlephobia.com/result?p=@os-team/downloadfile)

Downloads a file using JS. Supports: tracking the download progress, aborting the download, changing the name of the downloaded file.

## Usage

Install the package using the following command:

```
yarn add @os-team/downloadfile
```

Download a file:

```ts
import downloadFile from '@os-team/downloadfile';

downloadFile({ url: 'https://domain.com/file.txt' });
```

### Changing the name of the downloaded file

Usually the names of files that are stored in the storage have a unique ID.

Let's assume that we have a file named `BnmiYu-80Ak`. We can download it using `window.open('https://domain.com/BnmiYu-80Ak')`, but in this case, the name of the downloaded file will be `BnmiYu-80Ak`. This is definitely not what we want. Let's use this library to download the file and rename it:

```ts
downloadFile({
  url: 'https://domain.com/BnmiYu-80Ak',
  name: 'file.txt',
});
```

Now the name of the downloaded file will be `file.txt`.

### Specifying the MIME type

If you know the MIME type of the file, specify it as follows:

```ts
downloadFile({
  url: 'https://domain.com/image',
  mime: 'image/jpeg',
});
```

### Tracking the download progress

If the file is large, then it is necessary to display the download progress bar for a user. This can be done as follows:

```ts
downloadFile({
  url: 'https://domain.com/file.txt',
  onProgress: ({ percent }) => console.log(percent),
});
```

The event has 3 numbers: `loaded`, `total`, `percent`.

### Aborting the download

A user may want to cancel the file download. You can do it as follows:

```ts
const abort = downloadFile({ url: 'https://domain.com/file.txt' });

setTimeout(() => {
  // The user decided to cancel the file download
  abort();
}, 5000);
```
