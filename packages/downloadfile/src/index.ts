export interface DownloadFileProgressEvent {
  readonly loaded: number;
  readonly total: number;
  readonly percent: number;
}

export interface DownloadFileOptions {
  /**
   * The URL of the file to download.
   */
  url: string;
  /**
   * The name of the downloaded file.
   * @default undefined
   */
  name?: string;
  /**
   * The mime type of the file.
   */
  mime?: string;
  /**
   * The callback that is called when the download progress was updated.
   * @default undefined
   */
  onProgress?: (e: DownloadFileProgressEvent) => void;
  /**
   * The callback that is called if the file download failed.
   * @default undefined
   */
  onError?: () => void;
  /**
   * The callback that is called if the file download aborted.
   * @default undefined
   */
  onAborted?: () => void;
  /**
   * The callback that is called if the file download completed.
   * @default undefined
   */
  onCompleted?: () => void;
}

const downloadFile = (options: DownloadFileOptions) => {
  const { url, name, mime, onProgress, onError, onAborted, onCompleted } =
    options;
  let completed = false;

  const xhr = new XMLHttpRequest();
  xhr.open('GET', url, true);
  xhr.responseType = 'blob';

  if (onProgress) {
    xhr.addEventListener('progress', (e) => {
      const event: DownloadFileProgressEvent = {
        loaded: e.loaded,
        total: e.total,
        percent: Math.round((e.loaded / e.total) * 100),
      };
      onProgress(event);
    });
  }
  if (onError) {
    xhr.addEventListener('error', () => onError());
  }
  if (onAborted) {
    xhr.addEventListener('abort', () => onAborted());
  }

  xhr.addEventListener('load', () => {
    const file = new Blob([xhr.response], { type: mime });

    const link = document.createElement('a');
    link.href = window.URL.createObjectURL(file);
    if (name && typeof link.download !== 'undefined') {
      link.download = name;
    }
    link.click();

    completed = true;
    if (onCompleted) onCompleted();
  });

  xhr.send();

  return () => {
    if (!completed) xhr.abort();
  };
};

export default downloadFile;
