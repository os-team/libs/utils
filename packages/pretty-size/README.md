# @os-team/pretty-size [![NPM version](https://img.shields.io/npm/v/@os-team/pretty-size)](https://yarnpkg.com/package/@os-team/pretty-size) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/pretty-size)](https://bundlephobia.com/result?p=@os-team/pretty-size)

A tiny converter of number of bytes into a human-readable string. Support for all measures up to petabyte.

## Usage

Install the package using the following command:

```
yarn add @os-team/pretty-size
```

Convert the file size as follows:

```ts
import prettySize from '@os-team/pretty-size';

const size = prettySize(1.123 * 10 ** 6); // 1.1 MB
```

By defualt, the precision is `1`. You can set your own value as follows:

```ts
const size = prettySize(1.123 * 10 ** 3, {
  precision: 2,
}); // 1.12 KB
```

By default, the base of the number is `10`. You can set your own value as follows:

```ts
const size = prettySize(1.123 * 2 ** 10, {
  base: 2,
}); // 1.1 KB
```
