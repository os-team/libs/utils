const measurements = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];

interface Options {
  /**
   * The number of digits to the right of the decimal point.
   * @default 1
   */
  precision?: number;
  /**
   * The base of the number.
   * @default 10
   */
  base?: 2 | 10;
}

const toString = (size: number, options: Options, index = 0) => {
  const { precision = 1, base = 10 } = options;
  const b = base === 10 ? 1000 : 1024;

  if (size < b || index === measurements.length - 1) {
    const n = 10 ** precision;
    const roundedSize = Math.round(size * n) / n;
    return `${roundedSize} ${measurements[index]}`;
  }

  return toString(size / b, options, index + 1);
};

const prettySize = (bytes: number, options: Options = {}) =>
  toString(bytes, options);

export default prettySize;
