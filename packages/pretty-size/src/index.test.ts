import { it } from 'node:test';
import assert from 'node:assert';
import prettySize from './index.js';

it('Should return the size in bytes', () => {
  const size = prettySize(100);
  assert.equal(size, '100 B');
});

it('Should return the size in kilobytes', () => {
  const size = prettySize(1.123 * 10 ** 3);
  assert.equal(size, '1.1 KB');
});

it('Should return the size in megabytes', () => {
  const size = prettySize(1.123 * 10 ** 6);
  assert.equal(size, '1.1 MB');
});

it('Should return the size in gigabytes', () => {
  const size = prettySize(1.123 * 10 ** 9);
  assert.equal(size, '1.1 GB');
});

it('Should return the size in terabytes', () => {
  const size = prettySize(1.123 * 10 ** 12);
  assert.equal(size, '1.1 TB');
});

it('Should return the size in petabytes', () => {
  const size = prettySize(1.123 * 10 ** 15);
  assert.equal(size, '1.1 PB');
});

it('Should round the size to a precision of 2', () => {
  const size = prettySize(1.123 * 10 ** 3, {
    precision: 2,
  });
  assert.equal(size, '1.12 KB');
});

it('Should use the base of the number 2', () => {
  const size = prettySize(1.1234 * 2 ** 10, {
    base: 2,
  });
  assert.equal(size, '1.1 KB');
});
