# @os-team/stream-length [![NPM version](https://img.shields.io/npm/v/@os-team/stream-length)](https://yarnpkg.com/package/@os-team/stream-length) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/stream-length)](https://bundlephobia.com/result?p=@os-team/stream-length)

Gets and validates the length of a stream.

## Usage

Install the package using the following command:

```
yarn add @os-team/stream-length
```

Get the length of a stream as follows:

```ts
import streamLength from '@os-team/stream-length';

(async () => {
  const stream = streamLength(createReadableStream());
  await consumeReadableStream(stream);
  const fileSize = stream.length; // It is important to get the length after the stream has beed consumed
})();
```

If you want to validate the length of a stream, specify the `maxLength` option:

```ts
const stream = streamLength(createReadableStream(), {
  maxLength: 100,
});
```

To change the error message, specify the `errorMessage` option:

```ts
const stream = streamLength(createReadableStream(), {
  maxLength: 100,
  errorMessage: 'The file must be no larger than 100 bytes',
});
```
