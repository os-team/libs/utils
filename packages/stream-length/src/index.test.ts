import { it } from 'node:test';
import assert from 'node:assert';
import { Readable } from 'stream';
import streamLength from './index.js';

const length = 100;

const createReadableStream = () => {
  const buffer = Buffer.from('a'.repeat(length), 'utf8');

  const readable = new Readable();
  readable._read = () => {};
  readable.push(buffer);
  readable.push(null);

  return readable;
};

const consumeReadableStream = (readable: Readable) =>
  new Promise((resolve, reject) => {
    readable.on('data', () => {});
    readable.on('error', reject);
    readable.on('end', resolve);
  });

it('Should return the length of a stream', async () => {
  const stream = streamLength(createReadableStream());
  await consumeReadableStream(stream);
  assert.equal(stream.length, length);
});

process.on('unhandledRejection', (err) => {
  console.error(err);
  process.exit(1);
});

it('Should throw the error because the length of a stream is too large', async () => {
  const error = new Error('The file must be no larger than 100 bytes');
  await assert.rejects(async () => {
    const stream = streamLength(createReadableStream(), {
      maxLength: length - 1,
      error,
    });
    return await consumeReadableStream(stream);
  }, error);
});
