import { PassThrough, Readable, type TransformCallback } from 'stream';

interface Options {
  /**
   * The maximum length of a stream.
   * @default undefined
   */
  maxLength?: number;
  /**
   * The error.
   * @default undefined
   */
  error?: Error;
}

/* eslint-disable @typescript-eslint/no-explicit-any */

class Length extends PassThrough {
  public length: number;

  private readonly maxLength: number;

  private readonly error: Error;

  public constructor(options: Options) {
    super();

    this.length = 0;
    this.maxLength = options.maxLength || 0;
    this.error =
      options.error ||
      new Error(
        `The stream length must be no more than ${this.maxLength} bytes`
      );
  }

  public _transform(
    chunk: any,
    _: BufferEncoding,
    callback: TransformCallback
  ): void {
    this.length += chunk.length;

    if (this.maxLength && this.length > this.maxLength) {
      callback(this.error);
    } else {
      callback(null, chunk);
    }
  }
}

const streamLength = (readable: Readable, options: Options = {}) => {
  const length = new Length(options);
  readable.on('error', (error) => length.destroy(error));
  return readable.pipe(length);
};

export default streamLength;
