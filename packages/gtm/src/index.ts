/* eslint-disable @typescript-eslint/no-explicit-any */

export interface GTMEnv {
  /**
   * The authorization code.
   * Passed by the `gtm_auth` parameter.
   */
  auth: string;
  /**
   * The environment code. E.g. env-1.
   * Passed by the `gtm_preview` parameter.
   */
  preview: string;
}

const getUrlParamsByEnv = (env: GTMEnv) =>
  `&gtm_auth=${env.auth}&gtm_preview=${env.preview}&gtm_cookies_win=x`;

const getContainerSnippet = (
  id: string,
  dataLayerName: string,
  env?: GTMEnv
) => `
  (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl${
    env ? `+'${getUrlParamsByEnv(env)}'` : ''
  };f.parentNode.insertBefore(j,f);
  })(window,document,'script','${dataLayerName}','${id}');
`;

const getIFrameSnippet = (id: string, env?: GTMEnv) => `
  <iframe src='https://www.googletagmanager.com/ns.html?id=${id}${
    env ? getUrlParamsByEnv(env) : ''
  }'
  height='0' width='0' style='display:none;visibility:hidden'></iframe>
`;

const getDataLayerSnippet = (
  dataLayer: Record<string, any>,
  dataLayerName: string
) => `
  window.${dataLayerName} = window.${dataLayerName} || [];
  window.${dataLayerName}.push(${JSON.stringify(dataLayer)}});
`;

export interface GTMInitOptions {
  /**
   * An object that contains all of the information that you want to pass to GTM.
   * @default undefined
   */
  dataLayer?: Record<string, any>;
  /**
   * The custom data layer name.
   * @default dataLayer
   */
  dataLayerName?: string;
  /**
   * Whether the GTM should be initialized in non-production mode.
   * @default false
   */
  debug?: boolean;
  /**
   * The custom environment.
   * @default undefined
   */
  env?: GTMEnv;
}

const DEFAULT_DATA_LAYER_NAME = 'dataLayer';

const init = (id: string, options: GTMInitOptions = {}): void => {
  const {
    dataLayer,
    dataLayerName = DEFAULT_DATA_LAYER_NAME,
    debug = false,
    env,
  } = options;

  // Add GTM scripts in either production environment or debug mode
  if (process.env.NODE_ENV !== 'production' || debug) return;

  // Add the data layer snippet after the opening <head> tag
  let dataLayerScript;
  if (dataLayer) {
    dataLayerScript = document.createElement('script');
    dataLayerScript.innerHTML = getDataLayerSnippet(dataLayer, dataLayerName);
    document.head.insertBefore(dataLayerScript, document.head.firstChild);
  }

  // Add the container snippet after the data layer, if it exists,
  // otherwise add the container after the opening <head> tag.
  const script = document.createElement('script');
  script.innerHTML = getContainerSnippet(id, dataLayerName, env);
  const refChild = dataLayerScript
    ? dataLayerScript.nextSibling
    : document.head.firstChild;
  document.head.insertBefore(script, refChild);

  // Add the iFrame snippet after the opening <body> tag
  const noScript = document.createElement('noscript');
  noScript.innerHTML = getIFrameSnippet(id, env);
  document.body.insertBefore(noScript, document.body.firstChild);
};

const push = (
  dataLayer: Record<string, any>,
  dataLayerName = DEFAULT_DATA_LAYER_NAME
): void => {
  if (!window[dataLayerName]) window[dataLayerName] = [];
  return window[dataLayerName].push(dataLayer);
};

const GoogleTagManager = { init, push };

export default GoogleTagManager;
