# @os-team/tinkoff-oplata [![NPM version](https://img.shields.io/npm/v/@os-team/tinkoff-oplata)](https://yarnpkg.com/package/@os-team/tinkoff-oplata) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/tinkoff-oplata)](https://bundlephobia.com/result?p=@os-team/tinkoff-oplata)

Tinkoff Oplata API. All methods described in the [official documentation](https://oplata.tinkoff.ru/develop/api/) are implemented.

## Example

```ts
const client = new TinkoffOplataClient({
  terminalKey: 'terminalKey',
  password: 'password',
});

const res = await client.init({
  Amount: 100,
  OrderId: '1',
  Description: 'Тестовый платеж',
  DATA: {
    Phone: '+71234567890',
    Email: 'name@domain.com',
  },
  Receipt: {
    Email: 'name@domain.com',
    Phone: '+71234567890',
    EmailCompany: 'name@company.com',
    Taxation: 'osn',
    Items: [
      {
        Name: 'Наименование товара',
        Price: 30000,
        Quantity: 3.0,
        Amount: 90000,
        Tax: 'vat10',
      },
    ],
  },
});
```
