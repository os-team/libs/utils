import crypto from 'crypto';
import fetch from 'node-fetch';
import * as types from './types/index.js';

/* eslint-disable @typescript-eslint/no-explicit-any */

interface Props {
  terminalKey: string;
  password: string;
}

const TINKOFF_URL = 'https://securepay.tinkoff.ru';

class TinkoffOplataClient {
  public terminalKey: string;

  public password: string;

  public constructor(props: Props) {
    this.terminalKey = props.terminalKey;
    this.password = props.password;
  }

  /**
   * Создает платеж.
   */
  public init(data: types.InitRequest): Promise<types.InitResponse> {
    return this.makeRequest('/v2/Init', data);
  }

  /**
   * Подтверждает платеж передачей реквизитов и списанием/блокировкой средств.
   */
  public finishAuthorize(
    data: types.FinishAuthorizeRequest
  ): Promise<types.FinishAuthorizeResponse> {
    return this.makeRequest('/v2/FinishAuthorize', data);
  }

  /**
   * Выполняет запрос на ACS.
   */
  public static async acs(
    url: string,
    data: types.ACSRequest
  ): Promise<types.ACSResponse> {
    const params = TinkoffOplataClient.getUrlEncodedData(data);
    const res = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      },
      body: params,
    });
    const response = await res.json();
    return response as types.ACSResponse;
  }

  /**
   * Подтверждает платеж и списывает ранее заблокированные средства.
   */
  public confirm(data: types.ConfirmRequest): Promise<types.ConfirmResponse> {
    return this.makeRequest('/v2/Confirm', data);
  }

  /**
   * Отменяет платеж.
   */
  public cancel(data: types.CancelRequest): Promise<types.CancelResponse> {
    return this.makeRequest('/v2/Cancel', data);
  }

  /**
   * Возвращает текущий статус платежа.
   */
  public getState(
    data: types.GetStateRequest
  ): Promise<types.GetStateResponse> {
    return this.makeRequest('/v2/GetState', data);
  }

  /**
   * Отправляет все неотправленные нотификации.
   */
  public resend(): Promise<types.ResendResponse> {
    return this.makeRequest('/v2/Resend');
  }

  /**
   * Осуществляет проверку результатов прохождения 3-D Secure.
   */
  public submit3DSAuthorization(
    data: types.Submit3DSAuthorizationRequest
  ): Promise<types.Submit3DSAuthorizationResponse> {
    return this.makeUrlencodedRequest('/Submit3DSAuthorization', data);
  }

  /**
   * Выполняет автоплатеж.
   */
  public charge(data: types.ChargeRequest): Promise<types.ChargeResponse> {
    return this.makeRequest('/v2/Charge', data);
  }

  /**
   * Регистрирует покупателя и его данные в системе продавца.
   */
  public addCustomer(
    data: types.AddCustomerRequest
  ): Promise<types.AddCustomerResponse> {
    return this.makeRequest('/v2/AddCustomer', data);
  }

  /**
   * Возвращает данные покупателя.
   */
  public getCustomer(
    data: types.GetCustomerRequest
  ): Promise<types.GetCustomerResponse> {
    return this.makeRequest('/v2/GetCustomer', data);
  }

  /**
   * Удаляет данные зарегистрированного покупателя.
   */
  public removeCustomer(
    data: types.RemoveCustomerRequest
  ): Promise<types.RemoveCustomerResponse> {
    return this.makeRequest('/v2/RemoveCustomer', data);
  }

  /**
   * Возвращает список сохраненных карт зарегистрированного покупателя.
   */
  public getCardList(
    data: types.GetCardListRequest
  ): Promise<types.GetCardListResponse> {
    return this.makeRequest('/v2/GetCardList', data);
  }

  /**
   * Удаляет привязанную карту покупателя.
   */
  public removeCard(
    data: types.RemoveCardRequest
  ): Promise<types.RemoveCardResponse> {
    return this.makeRequest('/v2/RemoveCard', data);
  }

  /**
   * Проверяет токен уведомления.
   */
  public checkToken(data: types.Notification): boolean {
    return data.Token === this.generateToken(data);
  }

  /**
   * Генерирует токен для подписания запроса.
   */
  protected generateToken(data: Record<string, any>): string {
    // Exclude Receipt and DATA keys
    const { Receipt, DATA, Token, ...dataToSign } = data;

    // Add password
    dataToSign.Password = this.password;

    const value = Object.keys(dataToSign)
      .sort() // Sort keys in ascending order
      .map((key) => dataToSign[key]) // Replace keys to values
      .join(''); // Concatenate all values

    // Return the SHA-256 hash
    return crypto.createHash('sha256').update(value).digest('hex');
  }

  protected async makeRequest(
    method: string,
    data: Record<string, any> = {}
  ): Promise<any> {
    const signedData = this.signData(data);
    const res = await fetch(`${TINKOFF_URL}${method}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(signedData),
    });
    return res.json();
  }

  protected async makeUrlencodedRequest(
    method: string,
    data: Record<string, any> = {}
  ): Promise<any> {
    const signedData = this.signData(data);
    const params = TinkoffOplataClient.getUrlEncodedData(signedData);

    const res = await fetch(`${TINKOFF_URL}${method}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
      },
      body: params,
    });

    return res.json();
  }

  public static getUrlEncodedData(data: Record<string, any>): string {
    return Object.entries(data)
      .map(
        ([key, value]) =>
          `${encodeURIComponent(key)}=${encodeURIComponent(value)}`
      )
      .join('&');
  }

  protected signData(data: Record<string, any>): Record<string, any> {
    const clonedData = JSON.parse(JSON.stringify(data));
    clonedData.TerminalKey = this.terminalKey;
    clonedData.Token = this.generateToken(clonedData);
    return clonedData;
  }
}

export * from './types/index.js';
export default TinkoffOplataClient;
