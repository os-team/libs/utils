import type { ErrorResponse, Status } from './general.js';

export interface Submit3DSAuthorizationRequest {
  /**
   * Уникальный идентификатор транзакции в системе Банка.
   */
  MD: string;
  /**
   * Шифрованная строка, содержащая результаты 3-D Secure аутентификации.
   */
  PaRes: string;
  /**
   * Уникальный идентификатор транзакции в системе банка.
   */
  PaymentId: number;
}

export interface Submit3DSAuthorizationResponse extends ErrorResponse {
  /**
   * Идентификатор терминала.
   */
  TerminalKey: string;
  /**
   * Идентификатор заказа в системе продавца.
   */
  OrderId: string;
  /**
   * Успешность операции.
   */
  Success: boolean;
  /**
   * Статус платежа.
   */
  Status: Status;
  /**
   * Сумма в копейках.
   */
  Amount: number;
  /**
   * Уникальный идентификатор транзакции в системе банка.
   */
  PaymentId: number;
  /**
   * Идентификатор рекуррентного платежа.
   */
  RebillId?: string;
  /**
   * Идентификатор карты в системе банка.
   */
  CardId?: string;
}
