import type { Data, ErrorResponse, Receipt, Status } from './general.js';

export interface InitRequest {
  /**
   * Сумма в копейках.
   */
  Amount?: number;
  /**
   * Идентификатор заказа в системе продавца.
   */
  OrderId: string;
  /**
   * IP-адрес покупателя.
   */
  IP?: string;
  /**
   * Описание заказа.
   */
  Description?: string;
  /**
   * Язык платежной формы.
   */
  Language?: 'ru' | 'en';
  /**
   * Идентификатор родительского платежа.
   */
  Recurrent?: 'Y';
  /**
   * Идентификатор покупателя в системе продавца.
   */
  CustomerKey?: string;
  /**
   * Cрок жизни ссылки.
   */
  RedirectDueDate?: string;
  /**
   * Адрес для получения http нотификаций.
   */
  NotificationURL?: string;
  /**
   * Страница успеха.
   */
  SuccessURL?: string;
  /**
   * Страница ошибки.
   */
  FailURL?: string;
  /**
   * Тип оплаты.
   */
  PayType?:
    | 'O' // одностадийная
    | 'T'; // двухстадийная
  /**
   * Массив данных чека.
   */
  Receipt?: Receipt;
  /**
   * Дополнительные параметры платежа.
   */
  DATA?: Data;
}

export interface InitResponse extends ErrorResponse {
  /**
   * Идентификатор терминала.
   */
  TerminalKey: string;
  /**
   * Сумма в копейках.
   */
  Amount: number;
  /**
   * Идентификатор заказа в системе продавца.
   */
  OrderId: string;
  /**
   * Успешность операции.
   */
  Success: boolean;
  /**
   * Статус платежа.
   */
  Status?: Status;
  /**
   * Идентификатор платежа в системе банка.
   */
  PaymentId: number;
  /**
   * Ссылка на платежную форму.
   */
  PaymentURL?: string;
}
