import { type ErrorResponse, type Status } from './general.js';

export interface GetStateRequest {
  /**
   * Идентификатор платежа в системе банка.
   */
  PaymentId: number;
  /**
   * IP-адрес покупателя.
   */
  IP?: string;
}

export interface GetStateResponse extends ErrorResponse {
  /**
   * Идентификатор терминала.
   */
  TerminalKey: string;
  /**
   * Идентификатор заказа в системе продавца.
   */
  OrderId: string;
  /**
   * Успешность операции.
   */
  Success: boolean;
  /**
   * Статус платежа.
   */
  Status: Status;
  /**
   * Уникальный идентификатор транзакции в системе банка.
   */
  PaymentId: number;
}
