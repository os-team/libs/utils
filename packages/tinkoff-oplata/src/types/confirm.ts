import type { ErrorResponse, Receipt, Status } from './general.js';

export interface ConfirmRequest {
  /**
   * Идентификатор платежа в системе банка.
   */
  PaymentId: number;
  /**
   * Сумма в копейках.
   */
  Amount?: number;
  /**
   * IP-адрес покупателя.
   */
  IP?: string;
  /**
   * Массив данных чека.
   */
  Receipt?: Receipt;
}

export interface ConfirmResponse extends ErrorResponse {
  /**
   * Идентификатор терминала.
   */
  TerminalKey: string;
  /**
   * Идентификатор заказа в системе продавца.
   */
  OrderId: string;
  /**
   * Успешность операции.
   */
  Success: boolean;
  /**
   * Статус платежа.
   */
  Status: Status;
  /**
   * Уникальный идентификатор транзакции в системе банка.
   */
  PaymentId: number;
}
