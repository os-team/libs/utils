import { type ErrorResponse } from './general.js';

export interface AddCustomerRequest {
  /**
   * Идентификатор покупателя в системе продавца.
   */
  CustomerKey: string;
  /**
   * Электронная почта покупателя.
   */
  Email?: string;
  /**
   * Телефон покупателя в формате +71234567890.
   */
  Phone?: string;
  /**
   * IP-адрес покупателя.
   */
  IP?: string;
}

export interface AddCustomerResponse extends ErrorResponse {
  /**
   * Идентификатор терминала.
   */
  TerminalKey: string;
  /**
   * Идентификатор покупателя в системе продавца.
   */
  CustomerKey: string;
  /**
   * Успешность операции.
   */
  Success: boolean;
}
