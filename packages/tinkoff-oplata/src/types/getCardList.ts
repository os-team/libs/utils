import { type CardStatus, type CardType } from './general.js';

export interface GetCardListRequest {
  /**
   * Идентификатор покупателя в системе продавца.
   */
  CustomerKey: string;
  /**
   * IP-адрес покупателя.
   */
  IP?: string;
}

export interface GetCardListResponse {
  /**
   * Идентификатор сохраненной карты в системе банка.
   */
  CardId: string;
  /**
   * Замаскированный номер карты.
   */
  Pan: string;
  /**
   * Срок действия карты.
   */
  ExpDate: string;
  /**
   * Тип карты.
   */
  CardType: CardType;
  /**
   * Статус карты.
   */
  Status: CardStatus;
  /**
   * Идентификатор автоплатежа.
   */
  RebillId?: string;
}
