import { type ErrorResponse } from './general.js';

export interface RemoveCustomerRequest {
  /**
   * Идентификатор покупателя в системе продавца.
   */
  CustomerKey: string;
  /**
   * IP-адрес покупателя.
   */
  IP?: string;
}

export interface RemoveCustomerResponse extends ErrorResponse {
  /**
   * Идентификатор терминала.
   */
  TerminalKey: string;
  /**
   * Идентификатор покупателя в системе продавца.
   */
  CustomerKey: string;
  /**
   * Успешность операции.
   */
  Success: boolean;
}
