import { type ErrorResponse } from './general.js';

export interface GetCustomerRequest {
  /**
   * Идентификатор покупателя в системе продавца.
   */
  CustomerKey: string;
  /**
   * IP-адрес покупателя.
   */
  IP?: string;
}

export interface GetCustomerResponse extends ErrorResponse {
  /**
   * Идентификатор терминала.
   */
  TerminalKey: string;
  /**
   * Идентификатор покупателя в системе продавца.
   */
  CustomerKey: string;
  /**
   * Электронная почта покупателя.
   */
  Email?: string;
  /**
   * Телефон покупателя в формате +71234567890.
   */
  Phone?: string;
  /**
   * Успешность операции.
   */
  Success: boolean;
}
