import type { ErrorResponse, Status } from './general.js';

export interface ChargeRequest {
  /**
   * Уникальный идентификатор транзакции в системе банка.
   */
  PaymentId: number;
  /**
   * Идентификатор автоплатежа.
   */
  RebillId: string;
  /**
   * Получение покупателем уведомлений на электронную почту
   */
  SendEmail?: boolean;
  /**
   * Электронная почта покупателя.
   */
  InfoEmail?: string;
  /**
   * IP-адрес покупателя.
   */
  IP?: string;
}

export interface ChargeResponse extends ErrorResponse {
  /**
   * Идентификатор терминала.
   */
  TerminalKey: string;
  /**
   * Сумма в копейках.
   */
  Amount: number;
  /**
   * Идентификатор заказа в системе продавца.
   */
  OrderId: string;
  /**
   * Успешность операции.
   */
  Success: boolean;
  /**
   * Статус платежа.
   */
  Status: Status;
  /**
   * Уникальный идентификатор транзакции в системе банка.
   */
  PaymentId: number;
}
