import type { Data, ErrorResponse, Status } from './general.js';

export interface Notification extends Pick<ErrorResponse, 'ErrorCode'> {
  /**
   * Идентификатор терминала.
   */
  TerminalKey: string;
  /**
   * Идентификатор заказа в системе продавца.
   */
  OrderId: string;
  /**
   * Успешность операции.
   */
  Success: boolean;
  /**
   * Статус платежа.
   */
  Status: Status;
  /**
   * Уникальный идентификатор транзакции в системе банка.
   */
  PaymentId: number;
  /**
   * Сумма в копейках.
   */
  Amount: number;
  /**
   * Идентификатор рекуррентного платежа.
   */
  RebillId: string;
  /**
   * Идентификатор карты в системе банка.
   */
  CardId: string;
  /**
   * Замаскированный номер карты.
   */
  Pan: string;
  /**
   * Срок действия карты.
   */
  ExpDate: string;
  /**
   * Дополнительные параметры платежа.
   */
  DATA: Data;
  /**
   * Подпись запроса.
   */
  Token: string;
}
