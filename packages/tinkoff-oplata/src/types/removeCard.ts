import type { CardStatus, CardType, ErrorResponse } from './general.js';

export interface RemoveCardRequest {
  /**
   * Идентификатор покупателя в системе продавца.
   */
  CustomerKey: string;
  /**
   * Идентификатор сохраненной карты в системе банка.
   */
  CardId: string;
  /**
   * IP-адрес покупателя.
   */
  IP?: string;
}

export interface RemoveCardResponse extends ErrorResponse {
  /**
   * Идентификатор терминала.
   */
  TerminalKey: string;
  /**
   * Идентификатор покупателя в системе продавца.
   */
  CustomerKey: string;
  /**
   * Идентификатор сохраненной карты в системе банка.
   */
  CardId: string;
  /**
   * Статус карты.
   */
  Status: CardStatus;
  /**
   * Успешность операции.
   */
  Success: boolean;
  /**
   * Тип карты.
   */
  CardType: CardType;
}
