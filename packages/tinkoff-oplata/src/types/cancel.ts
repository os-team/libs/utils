import type { ErrorResponse, Receipt, Status } from './general.js';

export interface CancelRequest {
  /**
   * Идентификатор платежа в системе банка.
   */
  PaymentId: number;
  /**
   * Сумма в копейках.
   */
  Amount?: number;
  /**
   * IP-адрес покупателя.
   */
  IP?: string;
  /**
   * Массив данных чека.
   */
  Receipt?: Receipt;
}

export interface CancelResponse extends ErrorResponse {
  /**
   * Идентификатор терминала.
   */
  TerminalKey: string;
  /**
   * Идентификатор заказа в системе продавца.
   */
  OrderId: string;
  /**
   * Успешность операции.
   */
  Success: boolean;
  /**
   * Статус платежа.
   */
  Status: Status;
  /**
   * Уникальный идентификатор транзакции в системе банка.
   */
  PaymentId: number;
  /**
   * Сумма до возврата в копейках.
   */
  OriginalAmount: number;
  /**
   * Сумма после возврата в копейках.
   */
  NewAmount: number;
}
