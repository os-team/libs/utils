import type { Data, ErrorResponse, Status } from './general.js';

export interface FinishAuthorizeRequest {
  /**
   * Зашифрованные данные карты.
   */
  CardData: string;
  /**
   * Данные карт.
   */
  EncryptedPaymentData: string;
  /**
   * Сумма в копейках.
   */
  Amount?: number;
  /**
   * Дополнительные параметры платежа.
   */
  DATA?: Data;
  /**
   * Email для отправки информации об оплате.
   */
  InfoEmail?: string;
  /**
   * IP-адрес клиента.
   */
  IP?: string;
  /**
   * Уникальный идентификатор транзакции в системе банка.
   */
  PaymentId: number;
  /**
   * Телефон покупателя в формате +71234567890.
   */
  Phone?: string;
  /**
   * Отправлять ли клиенту информацию на почту об оплате.
   */
  SendEmail?: boolean;
  /**
   * Способ платежа.
   */
  Route?: 'ACQ';
  /**
   * Источник платежа.
   */
  Source?: 'Cards' | 'ApplePay' | 'GooglePay';
}

export interface FinishAuthorizeResponse extends ErrorResponse {
  /**
   * Идентификатор терминала.
   */
  TerminalKey: string;
  /**
   * Идентификатор заказа в системе продавца.
   */
  OrderId: string;
  /**
   * Успешность операции.
   */
  Success: boolean;
  /**
   * Статус транзакции.
   */
  Status: Status;
  /**
   * Сумма в копейках.
   */
  Amount: number;
  /**
   * Уникальный идентификатор транзакции в системе банка.
   */
  PaymentId: number;
  /**
   * Идентификатор карты в системе банка.
   */
  CardId?: string;
  /**
   * URL ACS банка.
   */
  ACSUrl?: string;
  /**
   * Уникальный идентификатор транзакции в системе банка.
   */
  MD?: string;
  /**
   * Результат аутентификации 3-D Secure.
   */
  PaReq?: string;
}
