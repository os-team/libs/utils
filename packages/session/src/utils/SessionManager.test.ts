import { describe, it, beforeEach, after } from 'node:test';
import assert from 'node:assert';
import { Redis } from 'ioredis';
import SessionManager, {
  getSessionKey,
  getSessionListKey,
} from './SessionManager.js';

const redis = new Redis({
  port: 6379,
  host: 'localhost',
  db: 15,
});

interface InitDataItem {
  userId: number;
  sessionIds: string[];
}

const initData: InitDataItem[] = [
  {
    userId: 1,
    sessionIds: ['id11', 'id12'],
  },
  {
    userId: 2,
    sessionIds: ['id21', 'id22'],
  },
];

const getMockedSession = (userId: number) => ({
  userId,
  createdAt: 0,
  regeneratedAt: 0,
  lastSeenAt: 0,
});

// Seed sessions before each test
beforeEach(async () => {
  // Clear the database
  await redis.flushdb();

  // Seed sessions
  await redis.mset(
    ...initData.reduce<string[]>(
      (acc, { userId, sessionIds }) => [
        ...acc,
        ...sessionIds.reduce<string[]>(
          (sessAcc, sessionId) => [
            ...sessAcc,
            getSessionKey(sessionId),
            JSON.stringify(getMockedSession(userId)),
          ],
          []
        ),
      ],
      []
    )
  );

  // Seed session lists
  for (const { userId, sessionIds } of initData) {
    await redis.rpush(getSessionListKey(userId), ...sessionIds);
  }
});

// Disconnect from Redis
after(async () => {
  await redis.flushdb();
  redis.disconnect();
});

/**
 * Checks whether the data in Redis has been changed.
 */
const initDataNotAffected = async (index: number) => {
  const { userId, sessionIds } = initData[index];

  // Sessions data is not affected
  const redisSessionsData = await redis.mget(
    ...sessionIds.map((id) => getSessionKey(id))
  );
  assert.notEqual(redisSessionsData[0], null);
  assert.deepStrictEqual(
    JSON.parse(redisSessionsData[0] as string),
    getMockedSession(userId)
  );
  assert.notEqual(redisSessionsData[1], null);
  assert.deepStrictEqual(
    JSON.parse(redisSessionsData[1] as string),
    getMockedSession(userId)
  );

  // Session list is not affected
  const redisSessionList = await redis.lrange(getSessionListKey(userId), 0, -1);
  assert.deepStrictEqual(redisSessionList, sessionIds);
};

describe('create', () => {
  it('Should create a new session', async () => {
    const ttl = 1000;
    const sessionManager = new SessionManager(redis, {
      ttl,
      maxSessionCountPerUser: initData[0].sessionIds.length + 1,
    });

    const sessionId = 'sessionId';
    const data = getMockedSession(initData[0].userId);
    await sessionManager.create(sessionId, data);

    const redisSessionData = await redis.get(getSessionKey(sessionId));
    assert.notEqual(redisSessionData, null);
    assert.deepStrictEqual(JSON.parse(redisSessionData as string), data);

    const redisSessionTtl = await redis.ttl(getSessionKey(sessionId));
    assert.equal(redisSessionTtl, ttl);

    const redisSessionList = await redis.lrange(
      getSessionListKey(initData[0].userId),
      0,
      -1
    );
    assert.deepStrictEqual(redisSessionList, [
      sessionId,
      ...initData[0].sessionIds,
    ]);

    await initDataNotAffected(1);
  });

  it('Should delete the exceeded sessions', async () => {
    const sessionManager = new SessionManager(redis, {
      maxSessionCountPerUser: initData[0].sessionIds.length,
    });

    const sessionId = 'sessionId';
    const data = getMockedSession(initData[0].userId);
    await sessionManager.create(sessionId, data);

    const redisDeletedSessionData = await redis.get(
      getSessionKey(initData[0].sessionIds[initData[0].sessionIds.length - 1])
    );
    assert.equal(redisDeletedSessionData, null);

    const redisSessionList = await redis.lrange(
      getSessionListKey(initData[0].userId),
      0,
      -1
    );
    assert.deepStrictEqual(redisSessionList, [
      sessionId,
      ...initData[0].sessionIds.slice(0, initData[0].sessionIds.length - 1),
    ]);

    await initDataNotAffected(1);
  });

  it('Should create a new session with the specified TTL', async () => {
    const sessionManager = new SessionManager(redis, {
      ttl: 1000,
      maxSessionCountPerUser: initData[0].sessionIds.length + 1,
    });

    const sessionId = 'sessionId';
    const data = getMockedSession(initData[0].userId);
    const ttl = 100;
    await sessionManager.create(sessionId, data, ttl);

    const redisSessionTtl = await redis.ttl(getSessionKey(sessionId));
    assert.equal(redisSessionTtl, ttl);

    await initDataNotAffected(1);
  });

  it('Should create a new session without TTL', async () => {
    const sessionManager = new SessionManager(redis, {
      ttl: 0,
      maxSessionCountPerUser: initData[0].sessionIds.length + 1,
    });

    const sessionId = 'sessionId';
    const data = getMockedSession(initData[0].userId);
    await sessionManager.create(sessionId, data);

    const redisSessionTtl = await redis.ttl(getSessionKey(sessionId));
    assert.equal(redisSessionTtl, -1);

    await initDataNotAffected(1);
  });
});

describe('update', () => {
  it('Should update the existing session', async () => {
    const sessionManager = new SessionManager(redis, {
      ttl: 1000,
    });

    const sessionId = initData[0].sessionIds[0];
    const data = {
      ...getMockedSession(initData[0].userId),
      lastSeenAt: 10,
    };

    const ttl = 100;
    await redis.expire(getSessionKey(sessionId), ttl);

    await sessionManager.update(sessionId, data);

    const redisSessionData = await redis.get(getSessionKey(sessionId));
    assert.notEqual(redisSessionData, null);
    assert.deepStrictEqual(JSON.parse(redisSessionData as string), data);

    const redisSessionTtl = await redis.ttl(getSessionKey(sessionId));
    assert.equal(redisSessionTtl, ttl);

    const redisSessionList = await redis.lrange(
      getSessionListKey(initData[0].userId),
      0,
      -1
    );
    assert.deepStrictEqual(redisSessionList, initData[0].sessionIds);

    await initDataNotAffected(1);
  });
});

describe('delete', () => {
  it('Should not delete the session because it is not associated with the specified user', async () => {
    const sessionManager = new SessionManager(redis);

    const sessionId = initData[1].sessionIds[0];
    await sessionManager.delete(sessionId, initData[0].userId);

    const redisSessionData = await redis.get(getSessionKey(sessionId));
    assert.notEqual(redisSessionData, null);

    await initDataNotAffected(0);
    await initDataNotAffected(1);
  });

  it('Should delete the existing session', async () => {
    const sessionManager = new SessionManager(redis);

    const sessionId = initData[0].sessionIds[0];
    await sessionManager.delete(sessionId, initData[0].userId);

    const redisSessionData = await redis.get(getSessionKey(sessionId));
    assert.equal(redisSessionData, null);

    const redisSessionList = await redis.lrange(
      getSessionListKey(initData[0].userId),
      0,
      -1
    );
    assert.deepStrictEqual(redisSessionList, [
      ...initData[0].sessionIds.filter((i) => i !== sessionId),
    ]);

    await initDataNotAffected(1);
  });

  it('Should delete the existing session after delay', async () => {
    const ttlOnDeletion = 10;
    const sessionManager = new SessionManager(redis, {
      ttlOnDeletion,
    });

    const sessionId = initData[0].sessionIds[0];
    await sessionManager.delete(sessionId, initData[0].userId, true);

    const redisSessionTtl = await redis.ttl(getSessionKey(sessionId));
    assert.equal(redisSessionTtl, ttlOnDeletion);

    const redisSessionList = await redis.lrange(
      getSessionListKey(initData[0].userId),
      0,
      -1
    );
    assert.deepStrictEqual(redisSessionList, [
      ...initData[0].sessionIds.filter((i) => i !== sessionId),
    ]);

    await initDataNotAffected(1);
  });
});

describe('deleteAll', () => {
  it('Should delete all sessions related to the user', async () => {
    const sessionManager = new SessionManager(redis);
    await sessionManager.deleteAll(initData[0].userId);

    const redisSessionsData = await redis.mget(
      initData[0].sessionIds.map((id) => getSessionKey(id))
    );
    assert.deepStrictEqual(redisSessionsData, [null, null]);

    const redisSessionList = await redis.lrange(
      getSessionListKey(initData[0].userId),
      0,
      -1
    );
    assert.deepStrictEqual(redisSessionList, []);

    await initDataNotAffected(1);
  });

  it('Should delete all sessions except the specified one', async () => {
    const sessionManager = new SessionManager(redis);
    const exceptSessionId = initData[0].sessionIds[0];
    await sessionManager.deleteAll(initData[0].userId, exceptSessionId);

    const redisSessionsData = await redis.mget(
      initData[0].sessionIds.map((id) => getSessionKey(id))
    );
    assert.notEqual(redisSessionsData[0], null);
    assert.equal(redisSessionsData[1], null);

    const redisSessionList = await redis.lrange(
      getSessionListKey(initData[0].userId),
      0,
      -1
    );
    assert.deepStrictEqual(redisSessionList, [exceptSessionId]);

    await initDataNotAffected(1);
  });

  it('Should delete all sessions except the specified one (no sessions to delete)', async () => {
    const sessionManager = new SessionManager(redis);
    const exceptSessionId = initData[0].sessionIds[0];
    await sessionManager.delete(initData[0].sessionIds[1], initData[0].userId);
    await sessionManager.deleteAll(initData[0].userId, exceptSessionId);

    const redisSessionsData = await redis.get(getSessionKey(exceptSessionId));
    assert.notEqual(redisSessionsData, null);

    const redisSessionList = await redis.lrange(
      getSessionListKey(initData[0].userId),
      0,
      -1
    );
    assert.deepStrictEqual(redisSessionList, [exceptSessionId]);

    await initDataNotAffected(1);
  });

  it('Should delete all sessions (exceptSessionId is incorrect)', async () => {
    const sessionManager = new SessionManager(redis);
    const exceptSessionId = 'incorrect';
    await sessionManager.deleteAll(initData[0].userId, exceptSessionId);

    const redisSessionsData = await redis.mget(
      initData[0].sessionIds.map((id) => getSessionKey(id))
    );
    assert.deepStrictEqual(redisSessionsData, [null, null]);

    const redisSessionList = await redis.lrange(
      getSessionListKey(initData[0].userId),
      0,
      -1
    );
    assert.deepStrictEqual(redisSessionList, []);

    await initDataNotAffected(1);
  });
});

describe('get', () => {
  it('Should return the existing session', async () => {
    const sessionManager = new SessionManager(redis);
    const sessionId = initData[0].sessionIds[0];

    const sessionData = await sessionManager.get(sessionId);
    assert.deepStrictEqual(sessionData, getMockedSession(initData[0].userId));

    await initDataNotAffected(0);
    await initDataNotAffected(1);
  });

  it('Should return null because the session does not exist', async () => {
    const sessionManager = new SessionManager(redis);
    const sessionId = 'incorrect';

    const sessionData = await sessionManager.get(sessionId);
    assert.equal(sessionData, null);

    await initDataNotAffected(0);
    await initDataNotAffected(1);
  });
});

describe('getTtl', () => {
  it('Should return the TTL of the existing session', async () => {
    const sessionManager = new SessionManager(redis);
    const sessionId = initData[0].sessionIds[0];

    const ttl = 100;
    await redis.expire(getSessionKey(sessionId), ttl);

    const sessionTtl = await sessionManager.getTtl(sessionId);
    assert.equal(sessionTtl, ttl);

    await initDataNotAffected(0);
    await initDataNotAffected(1);
  });

  it('Should return -2 because the session does not exist', async () => {
    const sessionManager = new SessionManager(redis);
    const sessionId = 'incorrect';

    const sessionTtl = await sessionManager.getTtl(sessionId);
    assert.equal(sessionTtl, -2);

    await initDataNotAffected(0);
    await initDataNotAffected(1);
  });
});

describe('list', () => {
  it('Should return the session list of the existing user', async () => {
    await redis.lpush(
      getSessionListKey(initData[0].userId),
      'non-existent-session'
    );

    const sessionManager = new SessionManager(redis);
    const sessionId = initData[0].sessionIds[0];
    const { userId } = initData[0];

    const list = await sessionManager.list(userId, sessionId);
    assert.equal(list.length, initData[0].sessionIds.length);

    assert.equal(list[0].userId, userId);
    assert.equal(list[0].createdAt, 0);
    assert.equal(list[0].regeneratedAt, 0);
    assert.equal(list[0].lastSeenAt, 0);
    assert.equal(typeof list[0].id, 'string');
    assert.equal(list[0].current, true);

    assert.equal(list[1].userId, userId);
    assert.equal(list[1].createdAt, 0);
    assert.equal(list[1].regeneratedAt, 0);
    assert.equal(list[1].lastSeenAt, 0);
    assert.equal(typeof list[1].id, 'string');
    assert.equal(list[1].current, false);

    await initDataNotAffected(1);
  });

  it('Should return the empty session list because the user does not exist', async () => {
    const sessionManager = new SessionManager(redis);
    const sessionId = initData[0].sessionIds[0];
    const userId = 0;

    const list = await sessionManager.list(userId, sessionId);
    assert.equal(list.length, 0);

    await initDataNotAffected(0);
    await initDataNotAffected(1);
  });
});
