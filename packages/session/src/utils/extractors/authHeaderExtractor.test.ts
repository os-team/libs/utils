import { it } from 'node:test';
import assert from 'node:assert';
import type { Request } from 'express';
import authHeaderExtractor from './authHeaderExtractor.js';

const sessionId = 'V1StGXR8_Z5jdHi6B-myT';
const maxLength = sessionId.length;

it('Should return null because there is no authorization header', () => {
  const req = {
    headers: {},
  } as Request;

  const extractedSessionId = authHeaderExtractor(req, maxLength);
  assert.equal(extractedSessionId, null);
});

it('Should return null because the type of the authorization header is incorrect', () => {
  const req = {
    headers: {
      authorization: `Basic ${sessionId}`,
    },
  } as Request;

  const extractedSessionId = authHeaderExtractor(req, maxLength);
  assert.equal(extractedSessionId, null);
});

it('Should return null because the session ID contains a colon (do not use in the Redis key)', () => {
  const req = {
    headers: {
      authorization: `Bearer ${sessionId.replace('_', ':')}`,
    },
  } as Request;

  const extractedSessionId = authHeaderExtractor(req, maxLength);
  assert.equal(extractedSessionId, null);
});

it('Should return null because the session ID is too long', () => {
  const req = {
    headers: {
      authorization: `Bearer ${sessionId}`,
    },
  } as Request;

  const extractedSessionId = authHeaderExtractor(req, maxLength - 1);
  assert.equal(extractedSessionId, null);
});

it('Should return the session ID', () => {
  const req = {
    headers: {
      authorization: `Bearer ${sessionId}`,
    },
  } as Request;

  const extractedSessionId = authHeaderExtractor(req, maxLength);
  assert.equal(extractedSessionId, sessionId);
});
