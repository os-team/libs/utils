import type { Request } from 'express';

const cookieExtractor = (
  req: Request,
  cookieName: string,
  maxLength: number
): string | null => {
  const re = new RegExp(
    `(?:(?<=^${cookieName}=)|(?<=[\\s;]${cookieName}=))[A-Za-z0-9_-]{1,${maxLength}}(?=;|$)`
  );
  const groups = (req.headers.cookie || '').match(re);
  return groups && groups[0] ? groups[0] : null;
};

export default cookieExtractor;
