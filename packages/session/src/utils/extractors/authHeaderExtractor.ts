import type { Request } from 'express';

const authHeaderExtractor = (
  req: Request,
  maxLength: number
): string | null => {
  const re = new RegExp(`(?<=^Bearer\\s)[A-Za-z0-9_-]{1,${maxLength}}$`);
  const groups = (req.headers.authorization || '').match(re);
  return groups && groups[0] ? groups[0] : null;
};

export default authHeaderExtractor;
