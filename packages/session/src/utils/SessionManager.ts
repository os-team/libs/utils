import type { Redis } from 'ioredis';
import AES from 'crypto-js/aes.js';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface SessionData extends Record<string, any> {
  readonly userId: number;
  readonly createdAt: number;
  readonly regeneratedAt: number;
  readonly lastSeenAt: number;
}

export interface ListItem extends SessionData {
  id: string;
  current: boolean;
}

export const getSessionKey = (sessionId: string): string =>
  `session:${sessionId}`;
export const getSessionListKey = (userId: number): string =>
  `user:${userId}:sessions`;

interface SessionManagerOptions {
  ttl?: number;
  ttlOnDeletion?: number;
  maxSessionCountPerUser?: number;
  secret?: string;
  prefix?: string;
}

class SessionManager {
  protected redis: Redis;

  protected ttl: number;

  protected ttlOnDeletion: number;

  protected maxSessionCountPerUser: number;

  protected secret: string;

  protected prefix: string;

  public constructor(redis: Redis, options: SessionManagerOptions = {}) {
    const {
      ttl = 31540000, // 1 year
      ttlOnDeletion = 60, // 1 minute
      maxSessionCountPerUser = 100,
      secret = 'secret',
      prefix = '',
    } = options;

    this.redis = redis;
    this.ttl = ttl;
    this.ttlOnDeletion = ttlOnDeletion;
    this.maxSessionCountPerUser = maxSessionCountPerUser;
    this.secret = secret;
    this.prefix = prefix && !prefix.endsWith(':') ? `${prefix}:` : prefix;
  }

  /**
   * Creates a new session.
   */
  public async create(
    id: string,
    data: SessionData,
    ttl?: number
  ): Promise<void> {
    const sessionKey = `${this.prefix}${getSessionKey(id)}`;
    const seconds = ttl || this.ttl;

    // Create a new session
    if (seconds > 0) {
      await this.redis.setex(sessionKey, seconds, JSON.stringify(data));
    } else {
      await this.redis.set(sessionKey, JSON.stringify(data));
    }

    // Push a new session to the session list
    const sessionListKey = `${this.prefix}${getSessionListKey(data.userId)}`;
    await this.redis.lpush(sessionListKey, id);

    // Get the number of sessions in list
    const count = await this.redis.llen(sessionListKey);

    // Remove the last session if the number of sessions is exceeded
    const exceededBy = count - this.maxSessionCountPerUser;
    if (exceededBy > 0) {
      const sessionIdsToDelete =
        (await this.redis.rpop(sessionListKey, exceededBy)) || [];
      await this.redis.del(
        ...sessionIdsToDelete.map(
          (item) => `${this.prefix}${getSessionKey(item)}`
        )
      );
    }
  }

  /**
   * Updates the existing session.
   */
  public async update(id: string, data: SessionData): Promise<void> {
    await this.redis.set(
      `${this.prefix}${getSessionKey(id)}`,
      JSON.stringify(data),
      'KEEPTTL'
    );
  }

  /**
   * Deletes the existing session.
   */
  public async delete(
    id: string,
    userId: number,
    afterDelay = false
  ): Promise<void> {
    const sessionListKey = `${this.prefix}${getSessionListKey(userId)}`;
    const sessionKey = `${this.prefix}${getSessionKey(id)}`;

    // Check if the session ID is associated with the specified user
    const pos = await this.redis.lpos(sessionListKey, id);
    if (pos === null) return;

    if (afterDelay && this.ttlOnDeletion > 0) {
      // Delete the existing session after ttlOnDeletion seconds
      await this.redis.expire(sessionKey, this.ttlOnDeletion);
    } else {
      // Delete the existing session immediately
      await this.redis.del(sessionKey);
    }

    // Delete the existing session from the session list
    await this.redis.lrem(sessionListKey, 0, id);
  }

  /**
   * Deletes all user's sessions.
   */
  public async deleteAll(
    userId: number,
    exceptSessionId?: string
  ): Promise<void> {
    const sessionListKey = `${this.prefix}${getSessionListKey(userId)}`;

    // Get the session ids
    const sessionIds = await this.redis.lrange(sessionListKey, 0, -1);

    if (!exceptSessionId) {
      // Delete all sessions
      await this.redis.del(
        ...sessionIds.map((id) => `${this.prefix}${getSessionKey(id)}`),
        sessionListKey
      );
      return;
    }

    const sessionIdsToDelete = sessionIds.filter(
      (id) => id !== exceptSessionId
    );

    if (sessionIdsToDelete.length > 0) {
      // Delete all sessions except the specified one
      await this.redis.del(
        ...sessionIdsToDelete.map((id) => `${this.prefix}${getSessionKey(id)}`),
        sessionListKey
      );

      // Recreate the session list with the specified one
      if (sessionIds.includes(exceptSessionId)) {
        await this.redis.lpush(sessionListKey, exceptSessionId);
      }
    }
  }

  /**
   * Gets the existing session.
   */
  public async get(id: string): Promise<SessionData | null> {
    const sessionKey = `${this.prefix}${getSessionKey(id)}`;
    const data = await this.redis.get(sessionKey);
    return data ? JSON.parse(data) : null;
  }

  /**
   * Gets the TTL of the session.
   */
  public async getTtl(id: string): Promise<number> {
    const sessionKey = `${this.prefix}${getSessionKey(id)}`;
    return this.redis.ttl(sessionKey);
  }

  /**
   * Gets the list of all user's sessions.
   */
  public async list(userId: number, id: string): Promise<ListItem[]> {
    const sessionListKey = `${this.prefix}${getSessionListKey(userId)}`;

    // Get the session ids
    const sessionIds = await this.redis.lrange(sessionListKey, 0, -1);

    if (sessionIds.length === 0) return [];

    // Get the index of the current session
    const currentSessionIndex = sessionIds.indexOf(id);

    // Get sessions data by ids
    const sessions = await this.redis.mget(
      ...sessionIds.map(
        (sessionId) => `${this.prefix}${getSessionKey(sessionId)}`
      )
    );

    return sessions
      .map((session, index) => {
        if (session === null) return null;
        return {
          ...JSON.parse(session),
          id: AES.encrypt(sessionIds[index], this.secret).toString(),
          current: index === currentSessionIndex,
        };
      })
      .filter((i) => i !== null);
  }
}

export default SessionManager;
