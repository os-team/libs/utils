import {
  describe,
  beforeEach,
  afterEach,
  before,
  after,
  it,
  mock,
} from 'node:test';
import assert from 'node:assert';
import { Server } from 'http';
import superagent from 'superagent';
import express, { type Request, type Response } from 'express';
import { Redis } from 'ioredis';
import AES from 'crypto-js/aes.js';
import session, { type SessionOptions } from './index.js';
import SessionManager, {
  getSessionKey,
  getSessionListKey,
  type SessionData,
} from './utils/SessionManager.js';

const PORT = 3001;
const URL = `http://localhost:${PORT}`;

const redis = new Redis({
  port: 6379,
  host: 'localhost',
  db: 15,
});

const sessionTtl = 100;
const sessionManager = new SessionManager(redis, {
  ttl: sessionTtl,
});

let server: Server;

beforeEach(async () => {
  await redis.flushdb();
});

afterEach(async () => {
  if (!server) return;
  await new Promise<void>((resolve, reject) => {
    server.close((err) => {
      if (err) reject(err);
      resolve();
    });
  });
});

before(() => {
  mock.timers.enable({
    apis: ['Date'],
    now: new Date(1000000000000),
  });
});

after(async () => {
  await redis.flushdb();
  redis.disconnect();
});

/**
 * Starts the server with the session middleware.
 * Each test creates a server with its own custom session middleware options.
 */
const startServer = async (
  sessionOptions: Omit<SessionOptions, 'redis'>,
  middleware: (req: Request, res: Response) => void | Promise<void>
) => {
  const app = express();
  app.use(session({ redis, ...sessionOptions }));
  app.all('*', async (req, res) => {
    await middleware(req, res);
    res.send();
  });
  await new Promise<void>((resolve) => {
    server = app.listen(PORT, resolve);
  });
};

/* eslint-disable @typescript-eslint/no-explicit-any */

describe('Basic features', () => {
  it('Should populate the session management methods', async () => {
    // Test the middleware
    await startServer({}, (req: any) => {
      assert.notEqual(req.session, undefined);
      assert.equal(typeof req.session.create, 'function');
      assert.equal(typeof req.session.update, 'function');
      assert.equal(typeof req.session.regenerateId, 'function');
      assert.equal(typeof req.session.destroy, 'function');
      assert.equal(typeof req.session.destroyAll, 'function');
      assert.equal(typeof req.session.list, 'function');
    });

    // Make the request
    await superagent.get(URL);
  });

  it('Should not find the session because the session ID was not passed', async () => {
    // Test the middleware
    await startServer({}, (req: any) => {
      assert.equal(req.session.id, undefined);
      assert.equal(req.session.data, undefined);
      assert.equal(req.session.expiresIn, undefined);
    });

    // Make the request
    await superagent.get(URL);
  });

  it('Should not find the session because an incorrect session ID was passed', async () => {
    // Seed the session to Redis
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create('sessionId', sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
    };

    // Test the middleware
    await startServer(sessionOptions, (req: any) => {
      assert.equal(req.session.id, undefined);
      assert.equal(req.session.data, undefined);
      assert.equal(req.session.expiresIn, undefined);
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=incorrect`);
  });

  it('Should find the existing session using a cookie', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    await startServer(sessionOptions, (req: any) => {
      assert.equal(req.session.id, sessionId);
      assert.deepStrictEqual(req.session.data, {
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      assert.equal(req.session.expiresIn, sessionTtl);
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });

  it('Should find the existing session using the authorization header', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    await startServer(sessionOptions, (req: any) => {
      assert.equal(req.session.id, sessionId);
      assert.deepStrictEqual(req.session.data, {
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      assert.equal(req.session.expiresIn, sessionTtl);
    });

    // Make the request
    await superagent.get(URL).set('Authorization', `Bearer ${sessionId}`);
  });
});

describe('Idle timeout', () => {
  it('Should not delete the session because the idle timeout is 0 (disabled)', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any) => {
      // Check if the session exists
      assert.equal(req.session.id, sessionId);
      assert.deepStrictEqual(req.session.data, {
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      assert.equal(req.session.expiresIn, sessionTtl);

      // Check if the session was not deleted from Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      assert.notEqual(redisSessionData, null);
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });

  it('Should not delete the session because the idle timeout has not expired yet', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: Date.now(),
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 1000,
      renewalTimeout: 0,
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any) => {
      // Check if the session exists
      assert.equal(req.session.id, sessionId);
      assert.deepStrictEqual(req.session.data, {
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      assert.equal(req.session.expiresIn, sessionTtl);

      // Check if the session was not deleted from Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      assert.notEqual(redisSessionData, null);
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });

  it('Should delete the session because the idle timeout has expired', async () => {
    const idleTimeout = 1000;

    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: Date.now() - idleTimeout * 1000,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout,
      renewalTimeout: 0,
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any, res: Response) => {
      // Check if the session does not exist
      assert.equal(req.session.id, undefined);
      assert.equal(req.session.data, undefined);
      assert.equal(req.session.expiresIn, undefined);

      // Check if the session was deleted from Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      assert.equal(redisSessionData, null);
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      assert.equal(redisSessionList.length, 0);

      // Check if the Set-Cookie header was passed in the response
      const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
      assert.equal(
        setCookieHeader.startsWith(`${sessionOptions.cookieName}=;`),
        true
      );
      assert.equal(
        setCookieHeader.includes('Expires=Thu, 01 Jan 1970 00:00:00 GMT'),
        true
      );

      // Check if the Cache-Control header was passed in the response
      assert.equal(res.getHeader('Cache-Control'), 'no-store');
      assert.equal(res.getHeader('Pragma'), 'no-cache');
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });
});

describe('Renewal timeout', () => {
  it('Should not regenerate the session ID because the renewal timeout is 0 (disabled)', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any) => {
      const updatedSessionData = {
        ...sessionData,
        lastSeenAt: Date.now(),
      };

      // Check if the session ID was not updated
      assert.equal(req.session.id, sessionId);
      assert.deepStrictEqual(req.session.data, updatedSessionData);
      assert.equal(req.session.expiresIn, sessionTtl);

      // Check if the session ID was not updated in Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      assert.notEqual(redisSessionData, null);
      assert.deepStrictEqual(
        JSON.parse(redisSessionData as string),
        updatedSessionData
      );
      const redisSessionTtl = await redis.ttl(getSessionKey(sessionId));
      assert.equal(redisSessionTtl, sessionTtl);
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      assert.deepStrictEqual(redisSessionList, [sessionId]);
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });

  it('Should not regenerate the session ID because the renewal timeout has not expired yet', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: Date.now(),
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 1000,
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any) => {
      const updatedSessionData = {
        ...sessionData,
        lastSeenAt: Date.now(),
      };

      // Check if the session ID was not updated
      assert.equal(req.session.id, sessionId);
      assert.deepStrictEqual(req.session.data, updatedSessionData);
      assert.equal(req.session.expiresIn, sessionTtl);

      // Check if the session ID was not updated in Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      assert.notEqual(redisSessionData, null);
      assert.deepStrictEqual(
        JSON.parse(redisSessionData as string),
        updatedSessionData
      );
      const redisSessionTtl = await redis.ttl(getSessionKey(sessionId));
      assert.equal(redisSessionTtl, sessionTtl);
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      assert.deepStrictEqual(redisSessionList, [sessionId]);
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });

  it('Should regenerate the session ID because the renewal timeout has expired', async () => {
    const renewalTimeout = 1000;
    const deletionTimeout = 5;

    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: Date.now() - renewalTimeout * 1000,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      tokenHeaderName: 'tokenHeaderName',
      tokenExpirationHeaderName: 'tokenExpirationHeaderName',
      length: sessionId.length - 1,
      maxLengthExistingIds: sessionId.length,
      absoluteTimeout: sessionTtl * 10, // To check req.session.expiresIn
      idleTimeout: 0,
      renewalTimeout,
      deletionTimeout,
    };

    // Test the middleware
    const updatedSessionData = {
      ...sessionData,
      regeneratedAt: Date.now(),
      lastSeenAt: Date.now(),
    };
    let isFirstRequest = true;
    let newSessionId = '';
    await startServer(sessionOptions, async (req: any, res: Response) => {
      if (isFirstRequest) {
        // Check if the session ID was updated
        assert.equal(req.session.id.length, sessionOptions.length);
        assert.deepStrictEqual(req.session.data, updatedSessionData);
        assert.equal(req.session.expiresIn, sessionTtl);

        // Check if the old session exists and marked as not allowed for regeneration ID
        const redisSessionData = await redis.get(getSessionKey(sessionId));
        assert.notEqual(redisSessionData, null);
        assert.deepStrictEqual(JSON.parse(redisSessionData as string), {
          ...sessionData,
          lastSeenAt: Date.now(),
          renewalNotAllowed: true,
        });

        // Check if the TTL of the old session was updated
        const redisSessionTtl = await redis.ttl(getSessionKey(sessionId));
        assert.equal(redisSessionTtl, deletionTimeout);

        // Check if a new session exists
        const redisNewSessionData = await redis.get(
          getSessionKey(req.session.id)
        );
        assert.notEqual(redisNewSessionData, null);
        assert.deepStrictEqual(
          JSON.parse(redisNewSessionData as string),
          updatedSessionData
        );

        // Check if the TTL of the new session is correct
        const redisNewSessionTtl = await redis.ttl(
          getSessionKey(req.session.id)
        );
        assert.equal(redisNewSessionTtl, sessionTtl);

        // Check the session list
        const redisSessionList = await redis.lrange(
          getSessionListKey(sessionData.userId),
          0,
          -1
        );
        assert.deepStrictEqual(redisSessionList, [req.session.id]);

        // Check if the Set-Cookie header was passed in the response
        const expiresAt = new Date(Date.now() + sessionTtl * 1000);
        const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
        assert.equal(
          setCookieHeader.startsWith(
            `${sessionOptions.cookieName}=${req.session.id};`
          ),
          true
        );
        assert.equal(
          setCookieHeader.includes(`Expires=${expiresAt.toUTCString()}`),
          true
        );

        // Check if the token header was passed in the response
        assert.equal(
          res.getHeader(sessionOptions.tokenHeaderName),
          req.session.id
        );
        assert.equal(
          res.getHeader(sessionOptions.tokenExpirationHeaderName),
          expiresAt.toUTCString()
        );

        // Check if the Cache-Control header was passed in the response
        assert.equal(res.getHeader('Cache-Control'), 'no-store');
        assert.equal(res.getHeader('Pragma'), 'no-cache');

        isFirstRequest = false;
        newSessionId = req.session.id;
      } else {
        // Check if the session ID was not updated
        assert.equal(req.session.id, sessionId);
        assert.deepStrictEqual(req.session.data, {
          ...sessionData,
          lastSeenAt: Date.now(),
          renewalNotAllowed: true,
        });
        assert.equal(req.session.expiresIn, deletionTimeout);

        // Check the session list
        const redisSessionList = await redis.lrange(
          getSessionListKey(sessionData.userId),
          0,
          -1
        );
        assert.deepStrictEqual(redisSessionList, [newSessionId]);
      }
    });

    // Make the request for the first time (should update the session ID)
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);

    // Make the request for the second time (should NOT update the session ID anymore)
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });
});

describe('create', () => {
  it('Should create a new session', async () => {
    const absoluteTimeout = 1000;

    const sessionOptions = {
      cookieName: 'cookieName',
      tokenHeaderName: 'tokenHeaderName',
      tokenExpirationHeaderName: 'tokenExpirationHeaderName',
      length: 5,
      absoluteTimeout,
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any, res: Response) => {
      // Check if the session does not exist
      assert.equal(req.session.id, undefined);
      assert.equal(req.session.data, undefined);
      assert.equal(req.session.expiresIn, undefined);

      // Create a new session
      const newSessionDataInput = {
        userId: 1,
        ip: req.ip,
      };
      await req.session.create(newSessionDataInput);

      // Check if the new session was created
      const newSessionData = {
        ...newSessionDataInput,
        createdAt: Date.now(),
        regeneratedAt: Date.now(),
        lastSeenAt: Date.now(),
      };
      assert.equal(req.session.id.length, sessionOptions.length);
      assert.deepStrictEqual(req.session.data, newSessionData);
      assert.equal(req.session.expiresIn, absoluteTimeout);

      // Check if the new session was created in Redis
      const redisNewSessionData = await redis.get(
        getSessionKey(req.session.id)
      );
      assert.notEqual(redisNewSessionData, null);
      assert.deepStrictEqual(
        JSON.parse(redisNewSessionData as string),
        newSessionData
      );

      // Check if the TTL of the new session is correct
      const redisNewSessionTtl = await redis.ttl(getSessionKey(req.session.id));
      assert.equal(redisNewSessionTtl, absoluteTimeout);

      // Check the session list in Redis
      const redisSessionList = await redis.lrange(
        getSessionListKey(newSessionData.userId),
        0,
        -1
      );
      assert.deepStrictEqual(redisSessionList, [req.session.id]);

      // Check if the Set-Cookie header was passed in the response
      const expiresAt = new Date(Date.now() + absoluteTimeout * 1000);
      const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
      assert.equal(
        setCookieHeader.startsWith(
          `${sessionOptions.cookieName}=${req.session.id};`
        ),
        true
      );
      assert.equal(
        setCookieHeader.includes(`Expires=${expiresAt.toUTCString()}`),
        true
      );

      // Check if the token header was passed in the response
      assert.equal(
        res.getHeader(sessionOptions.tokenHeaderName),
        req.session.id
      );
      assert.equal(
        res.getHeader(sessionOptions.tokenExpirationHeaderName),
        expiresAt.toUTCString()
      );

      // Check if the Cache-Control header was passed in the response
      assert.equal(res.getHeader('Cache-Control'), 'no-store');
      assert.equal(res.getHeader('Pragma'), 'no-cache');
    });

    // Make the request
    await superagent.get(URL);
  });

  it('Should create a new session (simulate the session fixation attack)', async () => {
    const absoluteTimeout = 1000;

    // Seed the session for an attacker to Redis
    const attackerSessionId = 'attackerSessionId';
    const attackerSessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(attackerSessionId, attackerSessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      length: attackerSessionId.length,
      absoluteTimeout,
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any, res: Response) => {
      const updatedAttackerSessionData = {
        ...attackerSessionData,
        lastSeenAt: Date.now(),
      };

      // Check if the session exists
      assert.equal(req.session.id, attackerSessionId);
      assert.deepStrictEqual(req.session.data, updatedAttackerSessionData);
      assert.equal(req.session.expiresIn, sessionTtl);

      // Create a new session for the victim user
      const newSessionDataInput = {
        userId: 2,
        privateData: 'privateData',
      };
      await req.session.create(newSessionDataInput);

      // Check if the new session was created
      const newSessionData = {
        ...newSessionDataInput,
        createdAt: Date.now(),
        regeneratedAt: Date.now(),
        lastSeenAt: Date.now(),
      };
      assert.equal(req.session.id.length, sessionOptions.length);
      assert.deepStrictEqual(req.session.data, newSessionData);
      assert.equal(req.session.expiresIn, absoluteTimeout);

      // Check if the attacker session exists in Redis
      const redisAttackerSessionData = await redis.get(
        getSessionKey(attackerSessionId)
      );
      assert.notEqual(redisAttackerSessionData, null);
      assert.deepStrictEqual(
        JSON.parse(redisAttackerSessionData as string),
        updatedAttackerSessionData
      );

      // Check if the new session was created in Redis
      const redisNewSessionData = await redis.get(
        getSessionKey(req.session.id)
      );
      assert.notEqual(redisNewSessionData, null);
      assert.deepStrictEqual(
        JSON.parse(redisNewSessionData as string),
        newSessionData
      );

      // Check the session list of the attacker in Redis
      const redisAttackerSessionList = await redis.lrange(
        getSessionListKey(attackerSessionData.userId),
        0,
        -1
      );
      assert.deepStrictEqual(redisAttackerSessionList, [attackerSessionId]);

      // Check the session list of the victim user in Redis
      const redisNewSessionList = await redis.lrange(
        getSessionListKey(newSessionData.userId),
        0,
        -1
      );
      assert.deepStrictEqual(redisNewSessionList, [req.session.id]);

      // Check if the Set-Cookie header was passed in the response
      const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
      assert.equal(
        setCookieHeader.startsWith(
          `${sessionOptions.cookieName}=${req.session.id};`
        ),
        true
      );
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${attackerSessionId}`);
  });

  it('Should send a cookie with the specified options', async () => {
    const sessionOptions = {
      cookieOptions: {
        domain: 'domain.com',
        path: '/path',
        secure: true,
        httpOnly: true,
        sameSite: 'lax',
      },
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    await startServer(
      sessionOptions as SessionOptions,
      async (req: any, res: Response) => {
        // Create a new session
        await req.session.create({
          userId: 1,
          ip: req.ip,
        });

        // Check if the specified cookie options is used in the Set-Cookie header
        const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
        assert.equal(
          setCookieHeader.includes(
            `Domain=${sessionOptions.cookieOptions.domain}`
          ),
          true
        );
        assert.equal(
          setCookieHeader.includes(`Path=${sessionOptions.cookieOptions.path}`),
          true
        );
        assert.equal(setCookieHeader.includes('Secure'), true);
        assert.equal(setCookieHeader.includes('HttpOnly'), true);
        assert.equal(setCookieHeader.includes('SameSite=Lax'), true);
      }
    );

    // Make the request
    await superagent.get(URL);
  });

  it('Should remove the last session if the number of sessions is exceeded', async () => {
    // Seed sessions to Redis
    const sessionId1 = 'sessionId1';
    const sessionId2 = 'sessionId2';
    const sessionId3 = 'sessionId3';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId1, sessionData);
    await sessionManager.create(sessionId2, sessionData);
    await sessionManager.create(sessionId3, sessionData);

    const sessionOptions = {
      maxSessionCountPerUser: 2,
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any) => {
      // Create a new session
      await req.session.create({
        userId: sessionData.userId,
      });

      // Check if the 2 most recent sessions exists in Redis
      const redisNewSessionData = await redis.get(
        getSessionKey(req.session.id)
      );
      assert.notEqual(redisNewSessionData, null);
      const redisSessionData3 = await redis.get(getSessionKey(sessionId3));
      assert.notEqual(redisSessionData3, null);

      // Check if the 2 oldest sessions were deleted from Redis
      const redisSessionData2 = await redis.get(getSessionKey(sessionId2));
      assert.equal(redisSessionData2, null);
      const redisSessionData1 = await redis.get(getSessionKey(sessionId1));
      assert.equal(redisSessionData1, null);

      // Check the session list in Redis
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      assert.deepStrictEqual(redisSessionList, [req.session.id, sessionId3]);
    });

    // Make the request
    await superagent.get(URL);
  });
});

describe('update', () => {
  it('Should not update anything because the session does not exist', async () => {
    // Test the middleware
    await startServer({}, async (req: any) => {
      // Check if the session does not exist
      assert.equal(req.session.id, undefined);
      assert.equal(req.session.data, undefined);
      assert.equal(req.session.expiresIn, undefined);

      // Try to update the session
      await req.session.update({
        ip: req.ip,
      });

      // Check if nothing happened in Redis
      const keys = await redis.keys('*');
      assert.equal(keys.length, 0);
    });

    // Make the request
    await superagent.get(URL);
  });

  it('Should update the existing session', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
      custom: 'custom',
      unnecessary: 'unnecessary',
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      absoluteTimeout: 1000,
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any) => {
      const updatedSessionData = {
        ...sessionData,
        lastSeenAt: Date.now(),
      };

      // Check the session data
      assert.deepStrictEqual(req.session.data, updatedSessionData);

      // Update the existing session
      await req.session.update({
        userId: 2, // Should be ignored
        createdAt: 10, // Should be ignored
        regeneratedAt: 10, // Should be ignored
        lastSeenAt: 10, // Should be ignored
        custom: 'new-custom', // Should be updated
        unnecessary: undefined, // Should be deleted
        ip: req.ip, // Should be added
      });

      // Check if the session data was updated
      const newSessionData = {
        userId: 1,
        createdAt: 0,
        regeneratedAt: 0,
        lastSeenAt: Date.now(),
        custom: 'new-custom', // Should be updated
        ip: req.ip, // Should be added
      };
      assert.deepStrictEqual(req.session.data, newSessionData);

      // Check if the session data was updated in Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      assert.notEqual(redisSessionData, null);
      assert.deepStrictEqual(
        JSON.parse(redisSessionData as string),
        newSessionData
      );

      // Check if the TTL of the session is correct
      const redisSessionTtl = await redis.ttl(getSessionKey(sessionId));
      assert.equal(redisSessionTtl, sessionTtl);
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });
});

describe('regenerateId', () => {
  it('Should not regenerate ID because the session does not exist', async () => {
    // Test the middleware
    await startServer({}, async (req: any) => {
      // Check if the session does not exist
      assert.equal(req.session.id, undefined);
      assert.equal(req.session.data, undefined);
      assert.equal(req.session.expiresIn, undefined);

      // Try to regenerate the session ID
      await req.session.regenerateId();

      // Check if nothing happened in Redis
      const keys = await redis.keys('*');
      assert.equal(keys.length, 0);
    });

    // Make the request
    await superagent.get(URL);
  });

  it('Should regenerate the session ID and delete the old session immediately', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      tokenHeaderName: 'tokenHeaderName',
      tokenExpirationHeaderName: 'tokenExpirationHeaderName',
      length: sessionId.length - 1,
      maxLengthExistingIds: sessionId.length,
      absoluteTimeout: sessionTtl * 10, // To check req.session.expiresIn
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any, res: Response) => {
      const updatedSessionData = {
        ...sessionData,
        lastSeenAt: Date.now(),
      };

      // Check if the session exists
      assert.equal(req.session.id, sessionId);
      assert.deepStrictEqual(req.session.data, updatedSessionData);
      assert.equal(req.session.expiresIn, sessionTtl);

      // Regenerate the session ID
      await req.session.regenerateId();

      const regeneratedSessionData = {
        ...updatedSessionData,
        regeneratedAt: Date.now(),
      };

      // Check if the session ID was updated
      assert.equal(req.session.id.length, sessionOptions.length);
      assert.deepStrictEqual(req.session.data, regeneratedSessionData);
      assert.equal(req.session.expiresIn, sessionTtl);

      // Check if the old session was deleted from Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      assert.equal(redisSessionData, null);

      // Check if a new session exists
      const redisNewSessionData = await redis.get(
        getSessionKey(req.session.id)
      );
      assert.notEqual(redisNewSessionData, null);
      assert.deepStrictEqual(
        JSON.parse(redisNewSessionData as string),
        regeneratedSessionData
      );

      // Check if the TTL of the new session is correct
      const redisNewSessionTtl = await redis.ttl(getSessionKey(req.session.id));
      assert.equal(redisNewSessionTtl, sessionTtl);

      // Check the session list
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      assert.deepStrictEqual(redisSessionList, [req.session.id]);

      // Check if the Set-Cookie header was passed in the response
      const expiresAt = new Date(Date.now() + sessionTtl * 1000);
      const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
      assert.equal(
        setCookieHeader.startsWith(
          `${sessionOptions.cookieName}=${req.session.id};`
        ),
        true
      );
      assert.equal(
        setCookieHeader.includes(`Expires=${expiresAt.toUTCString()}`),
        true
      );

      // Check if the token header was passed in the response
      assert.equal(
        res.getHeader(sessionOptions.tokenHeaderName),
        req.session.id
      );
      assert.equal(
        res.getHeader(sessionOptions.tokenExpirationHeaderName),
        expiresAt.toUTCString()
      );

      // Check if the Cache-Control header was passed in the response
      assert.equal(res.getHeader('Cache-Control'), 'no-store');
      assert.equal(res.getHeader('Pragma'), 'no-cache');
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });

  it('Should regenerate the session ID and delete the old session after delay', async () => {
    const deletionTimeout = 5;

    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      tokenHeaderName: 'tokenHeaderName',
      tokenExpirationHeaderName: 'tokenExpirationHeaderName',
      length: sessionId.length - 1,
      maxLengthExistingIds: sessionId.length,
      absoluteTimeout: sessionTtl * 10, // To check req.session.expiresIn
      idleTimeout: 0,
      renewalTimeout: 0,
      deletionTimeout,
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any, res: Response) => {
      const updatedSessionData = {
        ...sessionData,
        lastSeenAt: Date.now(),
      };

      // Check if the session exists
      assert.equal(req.session.id, sessionId);
      assert.deepStrictEqual(req.session.data, updatedSessionData);
      assert.equal(req.session.expiresIn, sessionTtl);

      // Regenerate the session ID
      await req.session.regenerateId(true);

      const regeneratedSessionData = {
        ...updatedSessionData,
        regeneratedAt: Date.now(),
      };

      // Check if the session ID was updated
      assert.equal(req.session.id.length, sessionOptions.length);
      assert.deepStrictEqual(req.session.data, regeneratedSessionData);
      assert.equal(req.session.expiresIn, sessionTtl);

      // Check if the old session exists and marked as not allowed for regeneration ID
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      assert.notEqual(redisSessionData, null);
      assert.deepStrictEqual(JSON.parse(redisSessionData as string), {
        ...updatedSessionData,
        renewalNotAllowed: true,
      });

      // Check if the TTL of the old session was updated
      const redisSessionTtl = await redis.ttl(getSessionKey(sessionId));
      assert.equal(redisSessionTtl, deletionTimeout);

      // Check if a new session exists
      const redisNewSessionData = await redis.get(
        getSessionKey(req.session.id)
      );
      assert.notEqual(redisNewSessionData, null);
      assert.deepStrictEqual(
        JSON.parse(redisNewSessionData as string),
        regeneratedSessionData
      );

      // Check if the TTL of the new session is correct
      const redisNewSessionTtl = await redis.ttl(getSessionKey(req.session.id));
      assert.equal(redisNewSessionTtl, sessionTtl);

      // Check the session list
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      assert.deepStrictEqual(redisSessionList, [req.session.id]);

      // Check if the Set-Cookie header was passed in the response
      const expiresAt = new Date(Date.now() + sessionTtl * 1000);
      const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
      assert.equal(
        setCookieHeader.startsWith(
          `${sessionOptions.cookieName}=${req.session.id};`
        ),
        true
      );
      assert.equal(
        setCookieHeader.includes(`Expires=${expiresAt.toUTCString()}`),
        true
      );

      // Check if the token header was passed in the response
      assert.equal(
        res.getHeader(sessionOptions.tokenHeaderName),
        req.session.id
      );
      assert.equal(
        res.getHeader(sessionOptions.tokenExpirationHeaderName),
        expiresAt.toUTCString()
      );

      // Check if the Cache-Control header was passed in the response
      assert.equal(res.getHeader('Cache-Control'), 'no-store');
      assert.equal(res.getHeader('Pragma'), 'no-cache');
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });
});

describe('destroy', () => {
  it('Should not delete anything because the session does not exist', async () => {
    // Test the middleware
    await startServer({}, async (req: any) => {
      // Check if the session does not exist
      assert.equal(req.session.id, undefined);
      assert.equal(req.session.data, undefined);
      assert.equal(req.session.expiresIn, undefined);

      // Try to delete the session
      await req.session.destroy();

      // Check if nothing happened in Redis
      const keys = await redis.keys('*');
      assert.equal(keys.length, 0);
    });

    // Make the request
    await superagent.get(URL);
  });

  it('Should delete the current session', async () => {
    // Seed sessions to Redis
    const sessionId1 = 'sessionId1';
    const sessionId2 = 'sessionId2';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId1, sessionData);
    await sessionManager.create(sessionId2, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any, res: Response) => {
      // Check if the current session exists
      assert.equal(req.session.id, sessionId2);
      assert.deepStrictEqual(req.session.data, {
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      assert.equal(req.session.expiresIn, sessionTtl);

      // Delete the existing session
      await req.session.destroy();

      // Check if the current session was deleted
      assert.equal(req.session.id, undefined);
      assert.equal(req.session.data, undefined);
      assert.equal(req.session.expiresIn, undefined);

      // Check if the current session was deleted from Redis
      const redisSessionData2 = await redis.get(getSessionKey(sessionId2));
      assert.equal(redisSessionData2, null);

      // Check if the other session exists in Redis
      const redisSessionData1 = await redis.get(getSessionKey(sessionId1));
      assert.notEqual(redisSessionData1, null);

      // Check the session list in Redis
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      assert.deepStrictEqual(redisSessionList, [sessionId1]);

      // Check if the Set-Cookie header was passed in the response
      const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
      assert.equal(
        setCookieHeader.startsWith(`${sessionOptions.cookieName}=;`),
        true
      );
      assert.equal(
        setCookieHeader.includes('Expires=Thu, 01 Jan 1970 00:00:00 GMT'),
        true
      );

      // Check if the Cache-Control header was passed in the response
      assert.equal(res.getHeader('Cache-Control'), 'no-store');
      assert.equal(res.getHeader('Pragma'), 'no-cache');
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId2}`);
  });

  it('Should delete another session of the current user', async () => {
    // Seed sessions to Redis
    const sessionId1 = 'sessionId1';
    const sessionId2 = 'sessionId2';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId1, sessionData);
    await sessionManager.create(sessionId2, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
      secret: 'secret',
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any) => {
      // Check if the current session exists
      assert.equal(req.session.id, sessionId2);
      assert.deepStrictEqual(req.session.data, {
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      assert.equal(req.session.expiresIn, sessionTtl);

      // Get the ID
      const id = AES.encrypt(sessionId1, sessionOptions.secret).toString();

      // Delete the existing session
      await req.session.destroy(id);

      // Check if the current session exists
      assert.equal(req.session.id, sessionId2);
      assert.deepStrictEqual(req.session.data, {
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      assert.equal(req.session.expiresIn, sessionTtl);

      // Check if the other session was deleted from Redis
      const redisSessionData1 = await redis.get(getSessionKey(sessionId1));
      assert.equal(redisSessionData1, null);

      // Check if the current session exists in Redis
      const redisSessionData2 = await redis.get(getSessionKey(sessionId2));
      assert.notEqual(redisSessionData2, null);
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId2}`);
  });

  it('Should not delete another session of the another user', async () => {
    // Seed sessions to Redis
    const sessionId = 'sessionId';
    const sessionIdAnotherUser = 'sessionIdAnotherUser';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    const sessionDataAnotherUser: SessionData = {
      userId: 2,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);
    await sessionManager.create(sessionIdAnotherUser, sessionDataAnotherUser);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
      secret: 'secret',
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any) => {
      // Check if the current session exists
      assert.equal(req.session.id, sessionId);
      assert.deepStrictEqual(req.session.data, {
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      assert.equal(req.session.expiresIn, sessionTtl);

      // Get the ID
      const id = AES.encrypt(
        sessionIdAnotherUser,
        sessionOptions.secret
      ).toString();

      // Delete the existing session
      await req.session.destroy(id);

      // Check if the current session exists
      assert.equal(req.session.id, sessionId);
      assert.deepStrictEqual(req.session.data, {
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      assert.equal(req.session.expiresIn, sessionTtl);

      // Check if sessions exists in Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      assert.notEqual(redisSessionData, null);
      const redisSessionDataAnotherUser = await redis.get(
        getSessionKey(sessionIdAnotherUser)
      );
      assert.notEqual(redisSessionDataAnotherUser, null);
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });

  it('Should do nothing because the ID is incorrect', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
      secret: 'secret',
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any) => {
      // Check if the current session exists
      assert.equal(req.session.id, sessionId);
      assert.deepStrictEqual(req.session.data, {
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      assert.equal(req.session.expiresIn, sessionTtl);

      // Delete the existing session
      await req.session.destroy('incorrect');

      // Check if the current session exists
      assert.equal(req.session.id, sessionId);
      assert.deepStrictEqual(req.session.data, {
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      assert.equal(req.session.expiresIn, sessionTtl);

      // Check if the session exists in Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      assert.notEqual(redisSessionData, null);
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });
});

describe('destroyAll', () => {
  it('Should not delete anything because the session does not exist', async () => {
    // Test the middleware
    await startServer({}, async (req: any) => {
      // Check if the session does not exist
      assert.equal(req.session.id, undefined);
      assert.equal(req.session.data, undefined);
      assert.equal(req.session.expiresIn, undefined);

      // Try to delete all sessions
      await req.session.destroyAll();

      // Check if nothing happened in Redis
      const keys = await redis.keys('*');
      assert.equal(keys.length, 0);
    });

    // Make the request
    await superagent.get(URL);
  });

  it("Should delete all user's sessions", async () => {
    // Seed sessions to Redis
    const sessionId1 = 'sessionId1';
    const sessionId2 = 'sessionId2';
    const sessionIdAnotherUser = 'sessionIdAnotherUser';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    const sessionDataAnotherUser: SessionData = {
      userId: 2,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId1, sessionData);
    await sessionManager.create(sessionId2, sessionData);
    await sessionManager.create(sessionIdAnotherUser, sessionDataAnotherUser);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any, res: Response) => {
      // Check if the current session exists
      assert.equal(req.session.id, sessionId2);
      assert.deepStrictEqual(req.session.data, {
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      assert.equal(req.session.expiresIn, sessionTtl);

      // Delete all user's sessions
      await req.session.destroyAll();

      // Check if the current session was deleted
      assert.equal(req.session.id, undefined);
      assert.equal(req.session.data, undefined);
      assert.equal(req.session.expiresIn, undefined);

      // Check if all user's sessions were deleted from Redis
      const redisSessionData1 = await redis.get(getSessionKey(sessionId1));
      assert.equal(redisSessionData1, null);
      const redisSessionData2 = await redis.get(getSessionKey(sessionId2));
      assert.equal(redisSessionData2, null);

      // Check the session list in Redis
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      assert.deepStrictEqual(redisSessionList, []);

      // Check if the session associated with another user exists in Redis
      const redisSessionDataAnotherUser = await redis.get(
        getSessionKey(sessionIdAnotherUser)
      );
      assert.notEqual(redisSessionDataAnotherUser, null);

      // Check the session list associated with another user in Redis
      const redisSessionListAnotherUser = await redis.lrange(
        getSessionListKey(sessionDataAnotherUser.userId),
        0,
        -1
      );
      assert.deepStrictEqual(redisSessionListAnotherUser, [
        sessionIdAnotherUser,
      ]);

      // Check if the Set-Cookie header was passed in the response
      const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
      assert.equal(
        setCookieHeader.startsWith(`${sessionOptions.cookieName}=;`),
        true
      );
      assert.equal(
        setCookieHeader.includes('Expires=Thu, 01 Jan 1970 00:00:00 GMT'),
        true
      );

      // Check if the Cache-Control header was passed in the response
      assert.equal(res.getHeader('Cache-Control'), 'no-store');
      assert.equal(res.getHeader('Pragma'), 'no-cache');
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId2}`);
  });

  it("Should delete all user's sessions except the current one", async () => {
    // Seed sessions to Redis
    const sessionId1 = 'sessionId1';
    const sessionId2 = 'sessionId2';
    const sessionIdAnotherUser = 'sessionIdAnotherUser';
    const sessionData: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    const sessionDataAnotherUser: SessionData = {
      userId: 2,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId1, sessionData);
    await sessionManager.create(sessionId2, sessionData);
    await sessionManager.create(sessionIdAnotherUser, sessionDataAnotherUser);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any) => {
      // Check if the current session exists
      assert.equal(req.session.id, sessionId2);
      assert.deepStrictEqual(req.session.data, {
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      assert.equal(req.session.expiresIn, sessionTtl);

      // Delete all user's sessions except the current one
      await req.session.destroyAll(true);

      // Check if the current session exists
      assert.equal(req.session.id, sessionId2);
      assert.deepStrictEqual(req.session.data, {
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      assert.equal(req.session.expiresIn, sessionTtl);

      // Check if all user's sessions were deleted from Redis except the current one
      const redisSessionData1 = await redis.get(getSessionKey(sessionId1));
      assert.equal(redisSessionData1, null);
      const redisSessionData2 = await redis.get(getSessionKey(sessionId2));
      assert.notEqual(redisSessionData2, null);

      // Check the session list in Redis
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      assert.deepStrictEqual(redisSessionList, [sessionId2]);

      // Check if the session associated with another user exists in Redis
      const redisSessionDataAnotherUser = await redis.get(
        getSessionKey(sessionIdAnotherUser)
      );
      assert.notEqual(redisSessionDataAnotherUser, null);

      // Check the session list associated with another user in Redis
      const redisSessionListAnotherUser = await redis.lrange(
        getSessionListKey(sessionDataAnotherUser.userId),
        0,
        -1
      );
      assert.deepStrictEqual(redisSessionListAnotherUser, [
        sessionIdAnotherUser,
      ]);
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId2}`);
  });
});

describe('list', () => {
  it('Should return the empty array because the session does not exist', async () => {
    // Test the middleware
    await startServer({}, async (req: any) => {
      // Check if the session does not exist
      assert.equal(req.session.id, undefined);
      assert.equal(req.session.data, undefined);
      assert.equal(req.session.expiresIn, undefined);

      // Try to get a list of all user's sessions
      const list = await req.session.list();

      // Check if the list is empty
      assert.equal(list.length, 0);
    });

    // Make the request
    await superagent.get(URL);
  });

  it("Should return all user's sessions", async () => {
    // Seed sessions to Redis
    const sessionId1 = 'sessionId1';
    const sessionId2 = 'sessionId2';
    const sessionIdAnotherUser = 'sessionIdAnotherUser';
    const sessionData1: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
      os: 'macos',
    };
    const sessionData2: SessionData = {
      userId: 1,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
      os: 'ios',
    };
    const sessionDataAnotherUser: SessionData = {
      userId: 2,
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId1, sessionData1);
    await sessionManager.create(sessionId2, sessionData2);
    await sessionManager.create(sessionIdAnotherUser, sessionDataAnotherUser);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    await startServer(sessionOptions, async (req: any) => {
      // Get a list of all user's sessions
      const list = await req.session.list();

      // Check the session list
      assert.equal(list.length, 2);

      assert.equal(list[0].userId, sessionData2.userId);
      assert.equal(list[0].createdAt, sessionData2.createdAt);
      assert.equal(list[0].regeneratedAt, sessionData2.regeneratedAt);
      assert.equal(list[0].lastSeenAt, Date.now());
      assert.equal(list[0].os, sessionData2.os);
      assert.equal(typeof list[0].id, 'string');
      assert.equal(list[0].current, true);

      assert.equal(list[1].userId, sessionData1.userId);
      assert.equal(list[1].createdAt, sessionData1.createdAt);
      assert.equal(list[1].regeneratedAt, sessionData1.regeneratedAt);
      assert.equal(list[1].lastSeenAt, sessionData1.lastSeenAt);
      assert.equal(list[1].os, sessionData1.os);
      assert.equal(typeof list[1].id, 'string');
      assert.equal(list[1].current, false);
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId2}`);
  });
});
