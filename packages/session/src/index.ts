import { Redis } from 'ioredis';
import type { RequestHandler } from 'express';
import { nanoid } from 'nanoid';
import AES from 'crypto-js/aes.js';
import encUtf8 from 'crypto-js/enc-utf8.js';
import SessionManager, {
  type ListItem,
  type SessionData,
} from './utils/SessionManager.js';
import cookieExtractor from './utils/extractors/cookieExtractor.js';
import authHeaderExtractor from './utils/extractors/authHeaderExtractor.js';

export interface CookieOptions {
  domain?: string;
  path?: string;
  secure?: boolean;
  httpOnly?: boolean;
  sameSite?: boolean | 'lax' | 'strict' | 'none';
}

export interface SessionOptions {
  /**
   * The redis client.
   */
  redis: Redis;
  /**
   * The name of the cookie.
   * See https://cheatsheetseries.owasp.org/cheatsheets/Session_Management_Cheat_Sheet.html#session-id-name-fingerprinting
   * @default sid
   */
  cookieName?: string;
  /**
   * The options of the cookie.
   * @default undefined
   */
  cookieOptions?: CookieOptions;
  /**
   * The name of the header in which the new session ID will be passed.
   * If not specified, the token will only be sent using the set-cookie header.
   * It can be used to send a new token to the mobile app.
   * @default undefined
   */
  tokenHeaderName?: string;
  /**
   * The name of the header in which the expiration date of the new session
   * will be passed.
   * @default X-Token-Expires-At
   */
  tokenExpirationHeaderName?: string;
  /**
   * The length of the session ID.
   * 1 character is 6 bits (it uses A-Za-z0-9_-).
   * See https://cheatsheetseries.owasp.org/cheatsheets/Session_Management_Cheat_Sheet.html#session-id-length
   * @default 50
   */
  length?: number;
  /**
   * The maximum length of session IDs already stored in Redis.
   * Used to validate the length of the session ID.
   * By default, it is equal to length.
   * @default undefined
   */
  maxLengthExistingIds?: number;
  /**
   * The maximum number of seconds a session can be active since the given
   * session was initially created. Must be greater than 0.
   * By default, 1 year.
   * @default 31540000
   */
  absoluteTimeout?: number;
  /**
   * The number of seconds a session will remain active in case there is
   * no activity in the session. To disable set 0.
   * By default, 30 days.
   * @default 2592000
   */
  idleTimeout?: number;
  /**
   * The number of seconds after which the session is automatically renewed
   * in the middle of the user session. To disable set 0.
   * By default, 30 minutes.
   * @default 1800
   */
  renewalTimeout?: number;
  /**
   * The number of seconds after which the old session will be completely
   * deleted after the session ID is regenerated. During this time, the client
   * should start using the new session ID.
   * By default, 1 minute.
   * @default 60
   */
  deletionTimeout?: number;
  /**
   * The maximum number of sessions per user.
   * If the number of active sessions exceeds the specified number, the oldest
   * ones are deleted. This number should not be too large if you send a list
   * of all user sessions to the client side.
   * @default 100
   */
  maxSessionCountPerUser?: number;
  /**
   * The secret passphrase used to encrypt session IDs in the list method.
   * @default secret
   */
  secret?: string;
  /**
   * The prefix for all redis keys.
   * @default undefined
   */
  prefix?: string;
}

/* eslint-disable @typescript-eslint/no-explicit-any */

export interface Session {
  // Session management methods
  create: (
    data: Omit<SessionData, 'createdAt' | 'regeneratedAt' | 'lastSeenAt'>
  ) => Promise<void>;
  update: (data: Record<string, any>) => Promise<void>;
  regenerateId: (deleteAfterDelay?: boolean) => Promise<void>;
  destroy: (id?: string) => Promise<void>;
  destroyAll: (exceptCurrent?: boolean) => Promise<void>;
  list: () => Promise<ListItem[]>;

  // Session info
  id?: string;
  data?: SessionData;
  expiresIn?: number;
}

const session =
  (options: SessionOptions): RequestHandler =>
  async (req: any, res, next) => {
    const {
      redis,
      cookieName = 'sid',
      tokenHeaderName,
      tokenExpirationHeaderName = 'X-Token-Expires-At',
      length = 50,
      idleTimeout = 2592000,
      renewalTimeout = 1800,
      deletionTimeout = 60,
      maxSessionCountPerUser = 100,
      secret = 'secret',
      prefix,
    } = options;

    const absoluteTimeout =
      options.absoluteTimeout === undefined || options.absoluteTimeout <= 0
        ? 31540000
        : options.absoluteTimeout;

    const cookieOptions: CookieOptions = {
      secure: process.env.NODE_ENV === 'production',
      httpOnly: true,
      sameSite: 'lax',
      ...options.cookieOptions,
    };

    const maxLengthExistingIds = options.maxLengthExistingIds || length;

    const sessionManager = new SessionManager(redis, {
      ttl: absoluteTimeout,
      ttlOnDeletion: deletionTimeout,
      maxSessionCountPerUser,
      secret,
      prefix,
    });

    const genSessionId = () => nanoid(length);

    const noCache = () => {
      // See https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control#preventing_caching
      res.setHeader('Cache-Control', 'no-store');
      res.setHeader('Pragma', 'no-cache');
    };

    const sendSessionId = (sessionId: string, ttl: number) => {
      const expiresAt = new Date(Date.now() + ttl * 1000);

      // Send the session ID using a cookie
      res.cookie(cookieName, sessionId, {
        ...cookieOptions,
        expires: expiresAt,
      });

      // Send the session ID using a header
      if (tokenHeaderName) {
        res.setHeader(tokenHeaderName, sessionId);
        res.setHeader(tokenExpirationHeaderName, expiresAt.toUTCString());
      }

      noCache();
    };

    const sendEmptySessionId = () => {
      res.clearCookie(cookieName, cookieOptions);
      noCache();
    };

    const create: Session['create'] = async (data) => {
      const now = Date.now();
      const sessionId = genSessionId();
      const sessionData = {
        ...data,
        createdAt: now,
        regeneratedAt: now,
        lastSeenAt: now,
      } as SessionData;

      // Create a new session
      await sessionManager.create(sessionId, sessionData);

      // Set the session to the request
      req.session.id = sessionId;
      req.session.data = sessionData;
      req.session.expiresIn = absoluteTimeout;

      // Send the session ID to the client
      sendSessionId(sessionId, absoluteTimeout);
    };

    const update: Session['update'] = async (data) => {
      if (!req.session.id || !req.session.data) return;

      // Update the session data
      const nextData = {
        ...req.session.data,
        ...data,
        userId: req.session.data.userId,
        createdAt: req.session.data.createdAt,
        regeneratedAt: req.session.data.regeneratedAt,
        lastSeenAt: req.session.data.lastSeenAt,
      };
      await sessionManager.update(req.session.id, nextData);

      // Update the session data in the request
      req.session.data = JSON.parse(JSON.stringify(nextData));
    };

    const regenerateId: Session['regenerateId'] = async (
      deleteAfterDelay = false
    ) => {
      if (!req.session.id || !req.session.data) return;

      // Get the TTL
      const ttl = await sessionManager.getTtl(req.session.id);

      const newSessionId = genSessionId();
      const newSessionData = {
        ...req.session.data,
        regeneratedAt: Date.now(),
      };

      // Mark the old session as not allowed for regeneration ID
      if (deleteAfterDelay) {
        await sessionManager.update(req.session.id, {
          ...req.session.data,
          renewalNotAllowed: true,
        });
      }

      // Recreate the session
      await sessionManager.delete(
        req.session.id,
        req.session.data.userId,
        deleteAfterDelay
      );
      await sessionManager.create(newSessionId, newSessionData, ttl);

      // Update the session in the request
      req.session.id = newSessionId;
      req.session.data = newSessionData;
      req.session.expiresIn = ttl;

      // Send the session ID to the client
      sendSessionId(newSessionId, ttl);
    };

    const destroy: Session['destroy'] = async (id?: string) => {
      if (!req.session.id || !req.session.data) return;

      try {
        const sessionId = id
          ? AES.decrypt(id, secret).toString(encUtf8)
          : req.session.id;

        if (!sessionId) return; // If an invalid id is passed

        // Delete the session
        await sessionManager.delete(sessionId, req.session.data.userId);

        if (sessionId === req.session.id) {
          // Set the empty session ID to the request
          req.session.id = undefined;
          req.session.data = undefined;
          req.session.expiresIn = undefined;

          // Send the empty session ID to the client
          sendEmptySessionId();
        }
      } catch {} // eslint-disable-line no-empty
    };

    const destroyAll: Session['destroyAll'] = async (exceptCurrent = false) => {
      if (!req.session.id || !req.session.data) return;

      // Delete all sessions associated with the user
      await sessionManager.deleteAll(
        req.session.data.userId,
        exceptCurrent ? req.session.id : undefined
      );

      if (!exceptCurrent) {
        // Set the empty session ID to the request
        req.session.id = undefined;
        req.session.data = undefined;
        req.session.expiresIn = undefined;

        // Send the empty session ID to the client
        sendEmptySessionId();
      }
    };

    const list: Session['list'] = () => {
      if (!req.session.id || !req.session.data) return Promise.resolve([]);
      return sessionManager.list(req.session.data.userId, req.session.id);
    };

    // Set the session management methods to the request
    req.session = {
      create,
      update,
      regenerateId,
      destroy,
      destroyAll,
      list,
    };

    // Get the session ID
    const sessionId =
      cookieExtractor(req, cookieName, maxLengthExistingIds) ||
      authHeaderExtractor(req, maxLengthExistingIds);

    // Check if the session ID has been detected
    if (!sessionId) {
      next();
      return;
    }

    // Load the session data from Redis
    const data = await sessionManager.get(sessionId);

    // Check if the session data exists
    if (!data) {
      // Send the empty session ID to the client
      sendEmptySessionId();

      next();
      return;
    }

    const now = Date.now();

    // Delete the session if the idle timeout has expired
    if (idleTimeout > 0 && now - data.lastSeenAt >= idleTimeout * 1000) {
      // Delete the session
      await sessionManager.delete(sessionId, data.userId);

      // Send the empty session ID to the client
      sendEmptySessionId();

      next();
      return;
    }

    // Update lastSeenAt to the current timestamp in ms
    const updatedData = { ...data, lastSeenAt: now };
    await sessionManager.update(sessionId, updatedData);

    // Set the session to the request
    req.session.id = sessionId;
    req.session.data = updatedData;
    req.session.expiresIn = await sessionManager.getTtl(sessionId);

    // Regenerate the session ID if the renewal timeout has expired
    if (
      renewalTimeout > 0 &&
      !data.renewalNotAllowed &&
      now - data.regeneratedAt >= renewalTimeout * 1000
    ) {
      await regenerateId(true);
    }

    next();
  };

export type { SessionData, ListItem } from './utils/SessionManager.js';

export default session;
