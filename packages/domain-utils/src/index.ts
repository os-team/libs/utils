export { default as getRootDomain } from './utils/getRootDomain.js';
export { default as getSecondLevelDomain } from './utils/getSecondLevelDomain.js';
export { default as getTopLevelDomain } from './utils/getTopLevelDomain.js';
