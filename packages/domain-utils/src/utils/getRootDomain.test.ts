import { test } from 'node:test';
import assert from 'node:assert';
import getRootDomain from './getRootDomain.js';

test('domain.com', () => {
  const root = getRootDomain('domain.com');
  assert.equal(root, 'domain.com');
});

test('sub.domain.com', () => {
  const root = getRootDomain('sub.domain.com');
  assert.equal(root, 'domain.com');
});
