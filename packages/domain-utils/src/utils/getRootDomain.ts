import getTopLevelDomain from './getTopLevelDomain.js';
import getSecondLevelDomain from './getSecondLevelDomain.js';

const getRootDomain = (host: string): string => {
  const tld = getTopLevelDomain(host);
  const sld = getSecondLevelDomain(host);
  return `${sld}.${tld}`;
};

export default getRootDomain;
