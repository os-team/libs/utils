import { test } from 'node:test';
import assert from 'node:assert';
import getTopLevelDomain from './getTopLevelDomain.js';

test('domain.com', () => {
  const tld = getTopLevelDomain('domain.com');
  assert.equal(tld, 'com');
});

test('domain.ru', () => {
  const tld = getTopLevelDomain('domain.ru');
  assert.equal(tld, 'ru');
});

test('sub.domain.com', () => {
  const tld = getTopLevelDomain('sub.domain.com');
  assert.equal(tld, 'com');
});
