import { test } from 'node:test';
import assert from 'node:assert';
import getSecondLevelDomain from './getSecondLevelDomain.js';

test('domain.com', () => {
  const sld = getSecondLevelDomain('domain.com');
  assert.equal(sld, 'domain');
});

test('sub.domain.com', () => {
  const sld = getSecondLevelDomain('sub.domain.com');
  assert.equal(sld, 'domain');
});

test('sub.sub.domain.com', () => {
  const sld = getSecondLevelDomain('sub.sub.domain.com');
  assert.equal(sld, 'domain');
});
