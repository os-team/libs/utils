const getTopLevelDomain = (host: string): string =>
  host.slice(host.lastIndexOf('.') + 1);

export default getTopLevelDomain;
