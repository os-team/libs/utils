# @os-team/aws-ses [![NPM version](https://img.shields.io/npm/v/@os-team/aws-ses)](https://yarnpkg.com/package/@os-team/aws-ses) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/aws-ses)](https://bundlephobia.com/result?p=@os-team/aws-ses)

The wrapper of the @aws-sdk/client-ses which allows to set the shared mail data.

## Usage

### Step 1. Install the package

Install the package using the following command:

```
yarn add @os-team/aws-ses
```

### Step 2. Set AWS environment variables

To make requests to AWS you need to create the access key ID and secret access key in the AWS Management Console. Follow [these instructions](https://docs.aws.amazon.com/sdk-for-javascript/v3/developer-guide/getting-your-credentials.html).

The AWS SDK automatically detects credentials from `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` environment variables. So you can set it. See more [here](https://docs.aws.amazon.com/sdk-for-javascript/v3/developer-guide/loading-node-credentials-environment.html).

### Step 3. Create an instance of AwsSes

The shared mail data will be used when sending each email.

```ts
const sharedMailData = {
  Source: 'Company <name@m.domain.com>',
  ReplyToAddresses: ['support@domain.com'],
};

const mailer = new AwsSes({ region: 'us-east-1' }, sharedMailData);
```

### Step 4. Send an email

```ts
const sent = await mailer.send({
  Destination: {
    ToAddresses: ['user@domain.com'],
  },
  Message: {
    Subject: {
      Charset: 'UTF-8',
      Data: 'Subject',
    },
    Body: {
      Text: {
        Charset: 'UTF-8',
        Data: 'Message',
      },
    },
  },
});
```

See about sending emails in the [Amazon SES documentation](https://docs.aws.amazon.com/ses/latest/dg/index.html).
