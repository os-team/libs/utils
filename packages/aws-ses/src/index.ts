import {
  SESClient,
  SendEmailCommand,
  type SESClientConfig,
  type SendEmailCommandInput,
} from '@aws-sdk/client-ses';

class AwsSes {
  protected readonly sesClient: SESClient;

  protected readonly sharedMailData: Partial<SendEmailCommandInput>;

  public constructor(
    config: SESClientConfig,
    sharedMailData: Partial<SendEmailCommandInput> = {}
  ) {
    this.sesClient = new SESClient(config);
    this.sharedMailData = sharedMailData;
  }

  public async send(data: Partial<SendEmailCommandInput>): Promise<boolean> {
    const res = await this.sesClient.send(
      new SendEmailCommand({
        Source: undefined,
        Destination: undefined,
        Message: undefined,
        ...this.sharedMailData,
        ...data,
      })
    );
    return res.$metadata.httpStatusCode === 200;
  }
}

export default AwsSes;
