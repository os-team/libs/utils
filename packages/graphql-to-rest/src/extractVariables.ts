import { type TypeNode } from 'graphql/language/ast.js';
import {
  GraphQLBoolean,
  GraphQLSchema,
  isEqualType,
  isScalarType,
  Kind,
  type VariableDefinitionNode,
} from 'graphql';
import type { Request } from 'express';

/* eslint-disable @typescript-eslint/no-explicit-any */

const convertVarValue = (
  value: any,
  type: TypeNode,
  schema: GraphQLSchema
): any => {
  if (type.kind === Kind.NAMED_TYPE) {
    const namedType = schema.getType(type.name.value);

    if (isScalarType(namedType)) {
      if (isEqualType(GraphQLBoolean, namedType)) {
        return value === 'true';
      }

      try {
        return namedType.serialize(value);
      } catch {
        return value;
      }
    }

    return value;
  }
  if (type.kind === Kind.LIST_TYPE && Array.isArray(value)) {
    return value.map((item) => convertVarValue(item, type.type, schema));
  }
  if (type.kind === Kind.NON_NULL_TYPE) {
    return convertVarValue(value, type.type, schema);
  }
  return value;
};

const extractVariables = (
  req: Request,
  variableDefinitions: ReadonlyArray<VariableDefinitionNode>,
  schema: GraphQLSchema
): Record<string, any> => {
  const params = {
    ...(req.body || {}),
    ...(req.query || {}),
  };
  return Object.values(variableDefinitions).reduce((acc, varDef) => {
    const varName = varDef.variable.name.value;
    const varValue = params[varName];
    if (varValue === undefined || varValue === null) return acc;
    return {
      ...acc,
      [varName]: convertVarValue(varValue, varDef.type, schema),
    };
  }, {});
};

export default extractVariables;
