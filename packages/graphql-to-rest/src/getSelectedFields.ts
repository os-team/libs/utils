import type { Request } from 'express';
import { type SelectedFields } from '@graphql-tools/utils';

const filterSelectedFields = (fields: string[]) =>
  fields.map((item) => item.trim()).filter((item) => !!item);

const convertSelectedFields = (fields: string[]): SelectedFields =>
  fields.reduce((acc, field) => ({ ...acc, [field]: true }), {});

const getSelectedFields = (
  req: Request,
  fieldsKey: string
): SelectedFields | undefined => {
  const queryFields = (req.query || {})[fieldsKey];
  if (typeof queryFields === 'string') {
    const fields = filterSelectedFields(queryFields.split(','));
    return convertSelectedFields(fields);
  }

  const bodyFields = (req.body || {})[fieldsKey];
  if (
    Array.isArray(bodyFields) &&
    bodyFields.every((item) => typeof item === 'string')
  ) {
    const fields = filterSelectedFields(bodyFields);
    return convertSelectedFields(fields);
  }

  return undefined;
};

export default getSelectedFields;
