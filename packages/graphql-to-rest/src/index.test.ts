import { describe, it, test, before, after } from 'node:test';
import assert from 'node:assert';
import 'setimmediate';
import http from 'http';
import express from 'express';
import superagent from 'superagent';
import { buildSchema } from 'graphql';
import graphqlToRest from './index.js';

const schema = buildSchema(`
  type Car {
    id: ID!
    model: String!
  }
  type User {
    id: ID!
    name: String!
    car: Car!
  }
  type Task {
    id: ID!
    name: String!
    description: String!
    user: User
    subtask: Task
  }
  input CreateTaskInput {
    name: String!
    description: String!
  }
  type Query {
    intArg(arg: Int!): Int!
    floatArg(arg: Float!): Float!
    stringArg(arg: String!): String!
    booleanArg(arg: Boolean!): Boolean!
    idArg(arg: ID!): ID!
    listArg(arg: [Int!]!): [Int!]!
    task: Task!
    taskFieldsArg(fields: String): Task!
    taskWithUser: Task!
    taskWithSubtask: Task!
  }
  type Mutation {
    createTask(input: CreateTaskInput!): Task!
    deleteTask: Boolean!
  }
`);

const BASE_TASK = {
  id: 'task1',
  name: 'name',
  description: 'description',
};

const root = {
  intArg: ({ arg }) => arg,
  floatArg: ({ arg }) => arg,
  stringArg: ({ arg }) => arg,
  booleanArg: ({ arg }) => arg,
  idArg: ({ arg }) => arg,
  listArg: ({ arg }) => arg,
  task: () => BASE_TASK,
  taskFieldsArg: () => BASE_TASK,
  taskWithUser: () => ({
    ...BASE_TASK,
    user: {
      id: 'user1',
      name: 'name',
      car: {
        id: 'car1',
        model: 'model',
      },
    },
  }),
  taskWithSubtask: () => ({
    ...BASE_TASK,
    subtask: {
      ...BASE_TASK,
      id: 'task2',
      subtask: {
        ...BASE_TASK,
        id: 'task3',
      },
    },
  }),
  createTask: ({ input }) => ({
    ...BASE_TASK,
    name: input.name,
    description: input.description,
  }),
  deleteTask: () => true,
};

const PORT = 4001;
let server: http.Server;

before(() => {
  // Start the server
  const app = express();
  app.use('/rest', graphqlToRest(schema, { rootValue: root }));
  app.use(
    '/depthLimit2',
    graphqlToRest(schema, { rootValue: root, depthLimit: 2 })
  );
  app.use(
    '/circularReferenceDepth2',
    graphqlToRest(schema, {
      rootValue: root,
      circularReferenceDepth: 2,
    })
  );
  app.use(
    '/include',
    graphqlToRest(schema, {
      rootValue: root,
      include: ['task'],
    })
  );
  app.use(
    '/exclude',
    graphqlToRest(schema, {
      rootValue: root,
      exclude: ['task'],
    })
  );
  app.use(
    '/methodDelete',
    graphqlToRest(schema, {
      rootValue: root,
      method: (field) => (field.startsWith('delete') ? 'DELETE' : null),
    })
  );
  app.use(
    '/fieldsKey',
    graphqlToRest(schema, {
      rootValue: root,
      fieldsKey: '_fields',
    })
  );
  app.use(
    '/errorHandler',
    graphqlToRest(schema, {
      rootValue: root,
      errorHandler: (errors) => ({
        body: { error: errors[0] },
        status: 200,
      }),
    })
  );
  server = app.listen(PORT);
});

after(() => {
  server.close();
});

describe('Should convert query argument values depending on their types', () => {
  test('Int', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/int-arg`)
      .query({ arg: 141 });

    assert.equal(res.body, 141);
  });

  test('Float', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/float-arg`)
      .query({ arg: 141.59 });

    assert.equal(res.body, 141.59);
  });

  test('String', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/string-arg`)
      .query({ arg: 'hello' });

    assert.equal(res.body, 'hello');
  });

  test('Boolean', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/boolean-arg`)
      .query({ arg: true });

    assert.equal(res.body, true);
  });

  test('ID', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/id-arg`)
      .query({ arg: 'abcd' });

    assert.equal(res.body, 'abcd');
  });

  test('List of Int', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/list-arg`)
      .query({ arg: [141, 59, 5] });

    assert.deepStrictEqual(res.body, [141, 59, 5]);
  });

  it('Should return a GraphQL error if the argument type is specified incorrectly', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/int-arg`)
      .query({ arg: 'incorrect' })
      .ok(() => true);

    assert.notEqual(res.body.errors, undefined);
  });
});

it('Should limit the depth', async () => {
  // depthLimit: 4 (default)
  const res1 = await superagent.get(
    `http://localhost:${PORT}/rest/task-with-user`
  );
  const task1 = res1.body;
  assert.notEqual(task1.user, undefined);
  assert.notEqual(task1.user.car, undefined);

  // depthLimit: 2 (specified)
  const res2 = await superagent.get(
    `http://localhost:${PORT}/depthLimit2/task-with-user`
  );
  const task2 = res2.body;
  assert.notEqual(task2.user, undefined);
  assert.equal(task2.user.car, undefined);
});

it('Should limit the circular reference depth', async () => {
  // circularReferenceDepth: 1 (default)
  const res1 = await superagent.get(
    `http://localhost:${PORT}/rest/task-with-subtask`
  );
  const task1 = res1.body;
  assert.notEqual(task1.subtask, undefined);
  assert.equal(task1.subtask.subtask, undefined);

  // circularReferenceDepth: 2 (specified)
  const res2 = await superagent.get(
    `http://localhost:${PORT}/circularReferenceDepth2/task-with-subtask`
  );
  const task2 = res2.body;
  assert.notEqual(task2.subtask, undefined);
  assert.notEqual(task2.subtask.subtask, undefined);
});

it('Should include only the task query', async () => {
  const res1 = await superagent
    .get(`http://localhost:${PORT}/include/task`)
    .ok((r) => r.status < 500);
  assert.equal(res1.statusCode, 200);

  const res2 = await superagent
    .get(`http://localhost:${PORT}/include/task-with-user`)
    .ok((r) => r.status < 500);
  assert.equal(res2.statusCode, 404);
});

it('Should exclude the task query', async () => {
  const res1 = await superagent
    .get(`http://localhost:${PORT}/exclude/task`)
    .ok((r) => r.status < 500);
  assert.equal(res1.statusCode, 404);

  const res2 = await superagent
    .get(`http://localhost:${PORT}/exclude/task-with-user`)
    .ok((r) => r.status < 500);
  assert.equal(res2.statusCode, 200);
});

it('Should redefine the method type', async () => {
  // All mutations have the POST method (default)
  const res1 = await superagent
    .delete(`http://localhost:${PORT}/rest/delete-task`)
    .ok((r) => r.status < 500);
  assert.equal(res1.statusCode, 404);

  // All methods starting with "delete" have the DELETE method (specified)
  const res2 = await superagent.delete(
    `http://localhost:${PORT}/methodDelete/delete-task`
  );
  const status = res2.body;
  assert.equal(status, true);
});

describe('Should return only selected fields', () => {
  test('Query', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/task`)
      .query({ fields: 'id, name' });

    const task = res.body;
    assert.notEqual(task.id, undefined);
    assert.notEqual(task.name, undefined);
    assert.equal(task.description, undefined);
  });

  test('Mutation', async () => {
    const res = await superagent
      .post(`http://localhost:${PORT}/rest/create-task`)
      .send({
        input: {
          name: 'My task',
          description: 'My description',
        },
        fields: ['id', 'name'],
      });

    const task = res.body;
    assert.notEqual(task.id, undefined);
    assert.notEqual(task.name, undefined);
    assert.equal(task.description, undefined);
  });

  test('Custom fieldsKey', async () => {
    const res1 = await superagent
      .get(`http://localhost:${PORT}/fieldsKey/task`)
      .query({ fields: 'id, name' });
    const task1 = res1.body;
    assert.notEqual(task1.id, undefined);
    assert.notEqual(task1.name, undefined);
    assert.notEqual(task1.description, undefined);

    const res2 = await superagent
      .get(`http://localhost:${PORT}/fieldsKey/task`)
      .query({ _fields: 'id, name' });
    const task2 = res2.body;
    assert.notEqual(task2.id, undefined);
    assert.notEqual(task2.name, undefined);
    assert.equal(task2.description, undefined);
  });

  it('Should ignore selected fields because the query has the same argument', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/task-fields-arg`)
      .query({ fields: 'id, name' });

    const task = res.body;
    assert.notEqual(task.id, undefined);
    assert.notEqual(task.name, undefined);
    assert.notEqual(task.description, undefined);
  });
});

it('Should change the error format and status code', async () => {
  const res = await superagent
    .get(`http://localhost:${PORT}/errorHandler/int-arg`)
    .query({ arg: 'incorrect' });

  assert.equal(res.status, 200);
  assert.notEqual(res.body.error, undefined);
  assert.equal(res.body.errors, undefined);
});
