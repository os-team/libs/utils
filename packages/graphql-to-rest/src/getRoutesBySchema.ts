import { GraphQLSchema, OperationTypeNode } from 'graphql';
import graphqlToRestName from './graphqlToRestName.js';

export type Method =
  | 'GET'
  | 'POST'
  | 'PUT'
  | 'DELETE'
  | 'PATCH'
  | 'OPTIONS'
  | 'HEAD'
  | 'ALL';

interface Route {
  method: Method;
  path: string;
  kind: OperationTypeNode;
  field: string;
  args: string[];
}

const getRoutesBySchema = (schema: GraphQLSchema): Route[] => {
  const queries = schema.getQueryType()?.getFields() || {};
  const mutations = schema.getMutationType()?.getFields() || {};

  return [
    ...Object.values(queries).map<Route>((item) => ({
      method: 'GET',
      path: `/${graphqlToRestName(item.name)}`,
      kind: OperationTypeNode.QUERY,
      field: item.name,
      args: item.args.map((arg) => arg.name),
    })),
    ...Object.values(mutations).map<Route>((item) => ({
      method: 'POST',
      path: `/${graphqlToRestName(item.name)}`,
      kind: OperationTypeNode.MUTATION,
      field: item.name,
      args: item.args.map((arg) => arg.name),
    })),
  ];
};

export default getRoutesBySchema;
