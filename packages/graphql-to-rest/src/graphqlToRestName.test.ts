import { test } from 'node:test';
import assert from 'node:assert';
import graphqlToRestName from './graphqlToRestName.js';

test('camelCaseCamelCase', () => {
  const restName = graphqlToRestName('camelCaseCamelCase');
  assert.equal(restName, 'camel-case-camel-case');
});

test('is1Number', () => {
  const restName = graphqlToRestName('is1Number');
  assert.equal(restName, 'is1-number');
});

test('is123Number', () => {
  const restName = graphqlToRestName('is123Number');
  assert.equal(restName, 'is123-number');
});

test('manyCCapitalLLLetters', () => {
  const restName = graphqlToRestName('manyCCapitalLLLetters');
  assert.equal(restName, 'many-ccapital-llletters');
});
