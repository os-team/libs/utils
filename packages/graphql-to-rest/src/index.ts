import express, { type Request, type Response, Router } from 'express';
import {
  type DocumentNode,
  execute,
  getOperationAST,
  GraphQLSchema,
  Kind,
  type ExecutionArgs,
  GraphQLError,
} from 'graphql';
import { buildOperationNodeForField } from '@graphql-tools/utils';
import extractVariables from './extractVariables.js';
import getSelectedFields from './getSelectedFields.js';
import getRoutesBySchema, { type Method } from './getRoutesBySchema.js';

/* eslint-disable @typescript-eslint/no-explicit-any */

export interface ExpressContext {
  req: Request;
  res: Response;
}

export type ValueOrPromise<T> = T | Promise<T>;

export type Context<TContext> =
  | TContext
  | ((value: ExpressContext) => ValueOrPromise<TContext>);

export type CustomMethod =
  | Method
  | Record<string, Method>
  | ((field: string) => Method | null);

export interface ErrorResponse {
  body: any;
  status: number;
}

export type ErrorHandler = (
  errors: ReadonlyArray<GraphQLError>
) => ErrorResponse;

export interface Options<TContext>
  extends Pick<ExecutionArgs, 'rootValue' | 'fieldResolver' | 'typeResolver'> {
  /**
   * The GraphQL context.
   * @default undefined
   */
  context?: Context<TContext>;
  /**
   * The maximum depth of fields in GraphQL.
   * @default 2
   */
  depthLimit?: number;
  /**
   * The maximum circular reference depth of fields in GraphQL.
   * @default 1
   */
  circularReferenceDepth?: number;
  /**
   * Queries/mutations that only must be included.
   * @default undefined
   */
  include?: ReadonlyArray<string>;
  /**
   * Queries/mutations that must be excluded.
   * @default undefined
   */
  exclude?: ReadonlyArray<string>;
  /**
   * Allows to specify a method for any query and mutation.
   * @default GET for queries, POST for mutations.
   */
  method?: CustomMethod;
  /**
   * The argument name to specify the selected fields.
   * @default 'fields'
   */
  fieldsKey?: string;
  /**
   * Allows to change the error format and status code.
   * @default { errors: [], status: 500 }
   */
  errorHandler?: ErrorHandler;
}

const createDocumentNode = (
  options: Parameters<typeof buildOperationNodeForField>[0]
): DocumentNode => ({
  kind: Kind.DOCUMENT,
  definitions: [buildOperationNodeForField(options)],
});

const parseCustomMethod = (method: CustomMethod, field: string) => {
  if (typeof method === 'string') return method;
  if (typeof method === 'object') return method[field];
  return method(field);
};

const graphqlToRest = <TContext extends Record<string, any>>(
  schema: GraphQLSchema,
  options: Options<TContext> = {}
): Router => {
  const {
    context,
    fieldsKey = 'fields',
    depthLimit = 4,
    circularReferenceDepth = 1,
    method: customMethod = {},
    include,
    exclude,
    errorHandler = (errors) => ({ body: { errors }, status: 500 }),
    ...executionArgs
  } = options;

  const routes = getRoutesBySchema(schema);
  const router = Router();
  router.use(express.json());

  routes.forEach(({ method, path, kind, field, args }) => {
    if (
      (include && !include.includes(field)) ||
      (exclude && exclude.includes(field))
    ) {
      return;
    }

    const curMethod = parseCustomMethod(customMethod, field) || method;

    router[curMethod.toLowerCase()](path, async (req, res) => {
      const contextValue: TContext | undefined =
        typeof context === 'function'
          ? await (context as any)({ req, res })
          : context;

      const document = createDocumentNode({
        schema,
        kind,
        field,
        selectedFields: !args.includes(fieldsKey)
          ? getSelectedFields(req, fieldsKey)
          : undefined,
        depthLimit,
        circularReferenceDepth,
      });

      const operationAST = getOperationAST(document);
      const operationName = operationAST?.name?.value;
      const variableDefinitions = operationAST?.variableDefinitions || [];
      const variableValues = extractVariables(req, variableDefinitions, schema);

      const result = await execute({
        schema,
        document,
        contextValue,
        variableValues,
        operationName,
        ...executionArgs,
      });

      if (result.errors) {
        const errorResponse = errorHandler(result.errors);
        res.status(errorResponse.status).json(errorResponse.body);
        return;
      }

      res.json(result.data && result.data[field]);
    });
  });

  return router;
};

export default graphqlToRest;
