export const tldLocaleMap = {
  com: 'en',
  ru: 'ru',
};

export const detectLanguageByHost = (host: string): string => {
  const tld = host.slice(host.lastIndexOf('.') + 1);
  return tldLocaleMap[tld];
};

export const browserTldLanguageDetector = {
  name: 'tld',
  lookup: (): string => detectLanguageByHost(window.location.hostname),
};

export const httpTldLanguageDetector = {
  name: 'tld',
  lookup: (req, _, options): string => {
    const headers = options.getHeaders(req);
    return detectLanguageByHost(headers.host);
  },
};
