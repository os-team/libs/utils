import {
  createMethodMiddlewareDecorator,
  type ResolverData,
} from 'type-graphql';
import TypeGraphQLValidationError, {
  type Constraint,
} from './TypeGraphQLValidationError.js';

/* eslint-disable @typescript-eslint/no-explicit-any */

type ValidateRes = boolean | [false, object]; // [false, tKeys]
type MayBePromise<T> = T | Promise<T>;

export interface Validator {
  name: string;
  validate: (
    value: unknown,
    data: ResolverData<any>
  ) => MayBePromise<ValidateRes>;
  isWall?: boolean;
  tKeys?: Record<string, any>;
}

export interface ValidateArgsOptions {
  arg?: string;
  ns?: string;
  tKey?: string;
}

const toSnakeCase = (value: string) =>
  value.replace(/[A-Z]/g, (c) => `_${c.toLowerCase()}`);

const ValidateArgs = (
  validatorsMap: Record<string, Validator[]>,
  options: ValidateArgsOptions = {}
): MethodDecorator => {
  const { arg, ns = 'validation', tKey } = options;
  const tKeyPrefix = tKey ? `${tKey}.` : '';

  return createMethodMiddlewareDecorator(async (action, next) => {
    const { args, context } = action;
    const input = arg ? args[arg] : args;

    // Check if the input is an object
    if (typeof input !== 'object') {
      throw new Error(
        `The ${arg ? `${arg} argument` : 'args'} must be an object`
      );
    }

    if (
      !('req' in context) ||
      typeof context.req !== 'object' ||
      Array.isArray(context.req) ||
      context.req === null ||
      !('t' in context.req) ||
      typeof context.req.t !== 'function'
    ) {
      throw new Error('The ctx.req.t function does not exist');
    }

    const { t } = context.req;
    const data: Record<string, Constraint> = {};

    // Validate the argument and save the error messages
    for (const key in validatorsMap) {
      const validators = validatorsMap[key];
      for (const validator of validators) {
        // Run the current validator
        const validateRes = await validator.validate(input[key], action);
        const [isValid, tKeys] = Array.isArray(validateRes)
          ? validateRes
          : [validateRes, {}];

        // Skip the next validators if the current validator is wall and is valid
        if (validator.isWall) {
          if (isValid) break;
          else continue;
        }

        // Add a new constraint if the value is invalid
        if (!isValid) {
          const validatorTKey = `${tKeyPrefix}${key}.${validator.name}`;
          data[key] = {
            code: toSnakeCase(validator.name),
            message: t(`${ns}:${validatorTKey}`, {
              ...tKeys,
              ...validator.tKeys,
            }),
          };
          break;
        }
      }
    }

    // Throw out the error if there are any constraints
    if (Object.keys(data).length > 0) {
      throw new TypeGraphQLValidationError(data);
    }

    return next();
  });
};

export default ValidateArgs;
