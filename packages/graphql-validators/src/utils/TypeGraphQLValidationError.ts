import { GraphQLError } from 'graphql';

export interface Constraint {
  code: string;
  message: string;
}

class TypeGraphQLValidationError extends GraphQLError {
  public readonly data: Record<string, Constraint>;

  public constructor(data: Record<string, Constraint>) {
    super('The data has been entered incorrectly. Fix the mistakes.', {
      extensions: {
        code: 'invalid_data',
        data,
      },
    });
    this.data = data;
  }
}

export default TypeGraphQLValidationError;
