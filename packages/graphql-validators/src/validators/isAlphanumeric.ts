import { type Validator } from '../utils/ValidateArgs.js';

/**
 * Checks if the string alphanumerical.
 */
const isAlphanumeric: Validator = {
  name: 'isNotAlphanumeric',
  validate: (value) =>
    typeof value === 'string' && /^[0-9A-Za-z]+$/.test(value),
};

export default isAlphanumeric;
