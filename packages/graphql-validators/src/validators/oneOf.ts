import { type Validator } from '../utils/ValidateArgs.js';

const isArrayOfStrings = (value: unknown): value is string[] =>
  Array.isArray(value) && value.every((item) => typeof item === 'string');
const isArrayOfNumbers = (value: unknown): value is number[] =>
  Array.isArray(value) && value.every((item) => typeof item === 'number');

/**
 * Checks if the string is equal to one of the given values.
 */
const oneOf = (
  values: string[] | ReadonlyArray<string> | number[] | ReadonlyArray<number>
): Validator => ({
  name: 'isNotOneOf',
  validate: (value) =>
    (isArrayOfStrings(values) &&
      typeof value === 'string' &&
      values.includes(value)) ||
    (isArrayOfNumbers(values) &&
      typeof value === 'number' &&
      values.includes(value)),
  tKeys: { values: values.join(', ') },
});

export default oneOf;
