import { type Validator } from '../utils/ValidateArgs.js';

/**
 * Checks if the string numerical.
 */
const isNumeric: Validator = {
  name: 'isNotNumeric',
  validate: (value) => typeof value === 'string' && /^[0-9]+$/.test(value),
};

export default isNumeric;
