import { type Validator } from '../utils/ValidateArgs.js';

// Supports unicode
const EMAIL_RE =
  /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;

/**
 * Checks if the string is an email.
 */
const isEmail: Validator = {
  name: 'isNotEmail',
  validate: (value) => typeof value === 'string' && EMAIL_RE.test(value),
};

export default isEmail;
