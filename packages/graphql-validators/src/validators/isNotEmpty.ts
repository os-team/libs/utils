import { type Validator } from '../utils/ValidateArgs.js';

/**
 * Checks if the trimmed string is not empty.
 */
const isNotEmpty: Validator = {
  name: 'isEmpty',
  validate: (value) => typeof value === 'string' && value.trim().length > 0,
};

export default isNotEmpty;
