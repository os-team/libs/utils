import { type Validator } from '../utils/ValidateArgs.js';

/**
 * Checks if the string length is not more than given number.
 */
const maxLength = (n: number): Validator => ({
  name: 'maxLength',
  validate: (value) => typeof value === 'string' && value.length <= n,
  tKeys: { max: n },
});

export default maxLength;
