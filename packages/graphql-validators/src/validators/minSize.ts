import { type Validator } from '../utils/ValidateArgs.js';

/**
 * Checks if the array size is not less than given number.
 */
const minSize = (n: number): Validator => ({
  name: 'minSize',
  validate: (value) => Array.isArray(value) && value.length >= n,
  tKeys: { min: n },
});

export default minSize;
