import { type Validator } from '../utils/ValidateArgs.js';

/**
 * Checks if the number is not less than given number.
 */
const min = (n: number): Validator => ({
  name: 'min',
  validate: (value) => typeof value === 'number' && value >= n,
  tKeys: { min: n },
});

export default min;
