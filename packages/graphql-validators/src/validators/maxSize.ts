import { type Validator } from '../utils/ValidateArgs.js';

/**
 * Checks if the array size is not more than given number.
 */
const maxSize = (n: number): Validator => ({
  name: 'maxSize',
  validate: (value) => Array.isArray(value) && value.length <= n,
  tKeys: { max: n },
});

export default maxSize;
